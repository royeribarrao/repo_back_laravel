<?php

use App\Http\Controllers\Web\GestionRepoRetailController;

Route::prefix('repo-retail/')->group(function () {
    Route::post('crear-gestion/{tienda_id}', [GestionRepoRetailController::class, 'crearGestionRepoRetail']);
    Route::post('asignar-informacion-producto/{gestion_id}', [GestionRepoRetailController::class, 'asignarInformacionProductoRepoRetail']);
    Route::post('asignar-datos-delivery/{gestion_id}', [GestionRepoRetailController::class, 'asignarDatosDeliveryRepoRetail']);
    Route::get('confirmacion-solicitud/{gestion_id}', [GestionRepoRetailController::class, 'confirmarGestionSinPago']);
    Route::get('gestion/codigo-repo/{codigo_repo}', [GestionRepoRetailController::class, 'getGestionById']);
    Route::get('gestion/{gestion_id}', [GestionRepoRetailController::class, 'show']);
    Route::post('agregar-nuevos-productos/{gestion_id}', [GestionRepoRetailController::class, 'agregarNuevosProductos']);
});