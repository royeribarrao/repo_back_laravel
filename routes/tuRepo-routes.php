<?php

use App\Http\Controllers\TuRepo\TuRepoWebController;
use App\Http\Controllers\TuRepo\TuRepoCuponDescuentoController;
use App\Http\Controllers\Web\PayUController;

Route::get('tu-repo/tiendas', [TuRepoWebController::class, 'allTiendasWeb']);
Route::get('tu-repo/tienda/{id}', [TuRepoWebController::class, 'showInfoTienda']);
Route::post('tu-repo/crear-servicio/{tienda_id}', [TuRepoWebController::class, 'createService']);
Route::post('tu-repo/acerca-producto/{servicio_id}', [TuRepoWebController::class, 'asignAboutProduct']);
Route::get('tu-repo/informacion-servicio/{servicio_id}', [TuRepoWebController::class, 'getInfoService']);
Route::post('tu-repo/datos-delivery/{servicio_id}', [TuRepoWebController::class, 'asignDatosDeliveryService']);
Route::post('tu-repo/formulario-pago', [PayUController::class, 'cobroUnPaso']);
Route::get('tu-repo/servicio/{servicio_id}', [TuRepoWebController::class, 'getServicioById']);
Route::get('tu-repo/servicio/{id}/{codigo}', [TuRepoWebController::class, 'getServicioByCodigoAndId']);
Route::get('tu-repo/servicio-enviar-email/{servicio_id}/{email}', [TuRepoWebController::class, 'enviarMailConfirmacion']);
Route::get('tu-repo/estado-servicio', [TuRepoWebController::class, 'getCantidadServiciosActual']);
Route::get('tu-repo/inventario-web/{tienda_id}', [TuRepoWebController::class, 'obtenerInventario']);

Route::get('tu-repo/cupon/validar-cupon/{codigo_cupon}', [TuRepoCuponDescuentoController::class, 'validarCupon']);