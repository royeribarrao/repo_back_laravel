<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TiendaController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OperadorLogisticoController;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\MaintenanceSedeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\DevolucionController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\CambioController;
use App\Http\Controllers\CambioDeluxeController;
use App\Http\Controllers\DistritoController;
use App\Http\Controllers\Repo\EstadisticaController;
use App\Http\Controllers\Repo\RepoProcesosFinalizadosController;
use App\Http\Controllers\Repo\RepoDevolucionController;
use App\Http\Controllers\Repo\RepoCambioController;
use App\Http\Controllers\Repo\RepoCambioExpressController;
use App\Http\Controllers\Repo\RepoFacturacionController;
use App\Http\Controllers\Web\CambioDevolucionController;
use App\Http\Controllers\Web\GestionController;
use App\Http\Controllers\Tienda\ConfiguracionController;
use App\Http\Controllers\Tienda\TiendaProcesosFinalizadosController;
use App\Http\Controllers\Tienda\TiendaProductoController;
use App\Http\Controllers\Tienda\TiendaPerfilController;
use App\Http\Controllers\Tienda\TiendaVentaController;
use App\Http\Controllers\Tienda\TiendaEstadisticaController;
use App\Http\Controllers\Tienda\TiendaCambioController;
use App\Http\Controllers\Tienda\TiendaCambioDeluxeController;
use App\Http\Controllers\Tienda\TiendaDevolucionController;
use App\Http\Controllers\TuRepo\TuRepoWebController;
use App\Http\Controllers\TuRepo\TuRepoCuponDescuentoController;

use App\Http\Controllers\Wiqli\WiqliPedidoController;
use App\Http\Controllers\Wiqli\WiqliProductoController;
use App\Http\Controllers\Wiqli\WiqliCategoriaController;
use App\Http\Controllers\Wiqli\WiqliUnidadController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Rutas para repo
Route::group(['middleware' => 'auth:api'], function () use ($router) {

    Route::get('users/sedes', [UserController::class, 'getSedes']);
    Route::get('users/update-state/{id}', [UserController::class, 'cambiarEstado']);
    Route::get('users/profiles', [UserController::class, 'getProfiles']);
    Route::get('users/logout', [UserController::class, 'logout']);
    Route::get('users', [UserController::class, 'get']);
    Route::get('users/{id}', [UserController::class, 'show']);
    Route::post('users', [UserController::class , 'store']);
    Route::post('users/{id}', [UserController::class, 'update']);
    //Diagnostics
    Route::get('maintenances/sedes', [MaintenanceSedeController::class, 'get']);
    Route::get('maintenances/sedes/simple', [MaintenanceSedeController::class, 'getAll']);
    Route::get('maintenances/sedes/{id}', [MaintenanceSedeController::class, 'show']);
    Route::post('maintenances/sedes', [MaintenanceSedeController::class, 'store']);
    Route::post('maintenances/sedes/state', [MaintenanceSedeController::class, 'state']);
    Route::post('maintenances/sedes/{id}', [MaintenanceSedeController::class, 'update']);

    Route::get('perfil', [TiendaPerfilController::class, 'get']);
    Route::post('perfil/{id}', [TiendaPerfilController::class , 'update']);

    Route::get('stores', [TiendaController::class, 'get']);
    Route::get('stores/simple', [TiendaController::class, 'getAll']);
    Route::get('stores/{id}', [TiendaController::class, 'show']);
    Route::post('stores', [TiendaController::class, 'store']);
    Route::post('stores/state', [TiendaController::class, 'state']);
    Route::post('stores/{id}', [TiendaController::class, 'update']);

    

    Route::get('repo/devoluciones', [RepoDevolucionController::class, 'allDevoluciones']);
    Route::get('repo/devoluciones/{id}', [RepoDevolucionController::class, 'show']);
    Route::post('repo/devoluciones/create-waybill/{gestion_id}', [RepoDevolucionController::class, 'createWayBill']);
    Route::post('repo/devoluciones/generate-carrier-waybill/{gestion_id}/{carrier_codigo}', [RepoDevolucionController::class, 'generateCarrierWayBill']);
    
    Route::get('repo/cambios-estandar', [RepoCambioController::class, 'allCambios']);
    Route::post('repo/cambios-estandar/create-waybill/{gestionId}/{carrierId}', [RepoCambioController::class, 'createWayBill']);
    Route::post('repo/cambios-estandar/generate-carrier-waybill/{gestion_id}/{carrier_codigo}', [RepoCambioController::class, 'generateCarrierWayBill']);
    //Route::get('cambios/{id}', [CambioController::class, 'show']);

    Route::get('repo/cambios-expres', [RepoCambioExpressController::class, 'allCambios']);
    Route::post('repo/cambios-expres/recojo/create-waybill/{gestion_id}', [RepoCambioExpressController::class, 'createWayBillRecojo']);
    Route::post('repo/cambios-expres/entrega/create-waybill/{gestion_id}', [RepoCambioExpressController::class, 'createWayBillEntrega']);

    Route::post('repo/cambios-expres/recojo/olva/crear-servicio-logistico/{gestion_id}/{carrierId}', [RepoCambioExpressController::class, 'createServicioLogisticoRecojo']);
    Route::post('repo/cambios-expres/entrega/olva/crear-servicio-logistico/{gestion_id}/{carrierId}', [RepoCambioExpressController::class, 'createServicioLogisticoEntrega']);
    
    Route::get('cambios-deluxe/error', [CambioDeluxeController::class, 'cambiosTodosErrorPorTienda']);
    Route::get('cambio-deluxe/{id}', [CambioDeluxeController::class, 'getById']);

    Route::get('repo/procesos-finalizados', [RepoProcesosFinalizadosController::class, 'getAll']);
    Route::get('procesos-finalizados', [TiendaProcesosFinalizadosController::class, 'getAll']);
    
    Route::post('gestion/cambios/actualizar/{id}', [CambioController::class, 'updateState']);
    Route::get('gestion/{id}', [CambioController::class, 'obtenerGestionByID']);

    Route::get('configuracion', [ConfiguracionController::class, 'getConfig']);
    Route::get('configuracion/tienda/{id}', [ConfiguracionController::class, 'getConfiguracionByTienda']);
    Route::post('configuracion/tienda/{id}', [ConfiguracionController::class, 'update']);
    Route::post('configuracion/tienda', [ConfiguracionController::class, 'store']);

    Route::get('repo/facturacion', [RepoFacturacionController::class, 'getData']);
    Route::post('repo/facturacion/eliminar/{gestion_id}', [RepoFacturacionController::class, 'eliminarDeFacturacion']);
    Route::post('repo/facturacion/agregar/{gestion_id}', [RepoFacturacionController::class, 'agregarAFacturacion']);
    Route::get('repo/facturacion/tienda/{tienda_id}/{fecha_inicial}/{fecha_final}', [RepoFacturacionController::class, 'getFacturacionByTienda']);

    Route::get('repo/operadores-logisticos/{gestion_id}', [OperadorLogisticoController::class, 'show']);
    // Route::get('gestion/cambios', 'Web\GestionController@allCambios');
    // Route::get('gestion/devoluciones', 'Web\GestionController@allDevoluciones');
    Route::get('authinfo', [UserController::class, 'getAuthenticated']);
});

//Rutas para tiendas
Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::get('tienda/cambios-estandar', [TiendaCambioController::class, 'allCambios']);
    Route::get('tienda/cambios/{id}', [TiendaCambioController::class, 'show']);
    Route::get('tienda/cambios-estandar/aceptar-cambio/{gestion_id}/{tracker_id}', [TiendaCambioController::class, 'aceptarCambio']);
    Route::get('tienda/cambios-estandar/error/todos', [TiendaCambioController::class, 'cambiosTodosErrorPorTienda']);
    Route::get('tienda/cambios/error/aceptar/{id}', [TiendaCambioController::class, 'aceptarCambioError']);
    Route::get('tienda/cambios/error/denegar/{id}', [TiendaCambioController::class, 'denegarCambioError']);
    Route::post('tienda/cambios-estandar/denegar-cambio/{id}', [TiendaDevolucionController::class, 'rechazarDevolucion']);

    Route::get('tienda/cambios-expres', [TiendaCambioDeluxeController::class, 'allCambios']);
    Route::get('tienda/cambios-expres/pendientes/todos', [TiendaCambioDeluxeController::class, 'allCambiosPendientes']);
    Route::get('tienda/cambios-expres/{id}', [TiendaCambioDeluxeController::class, 'show']);
    Route::post('tienda/cambios-expres/denegar-cambio/{id}', [TiendaDevolucionController::class, 'rechazarDevolucion']);
    Route::get('configuracion/general', [ConfiguracionController::class, 'allMotivos']);

    Route::get('ventas', [TiendaVentaController::class, 'get']);
    Route::get('ventas/{id}', [TiendaVentaController::class, 'show']);
    Route::post('ventas/state', [TiendaVentaController::class, 'state']);
    Route::post('ventas/upload-csv', [TiendaVentaController::class, 'uploadCsv']);

    Route::get('estadistica/desglose-tipo-proceso', [TiendaEstadisticaController::class, 'porcentajeByTipoProcesos']);
    Route::get('estadistica/ordenes-totales', [TiendaEstadisticaController::class, 'totalGestiones']);
    Route::get('estadistica/efectividad-ordenes', [TiendaEstadisticaController::class, 'porcentajeAceptacion']);
    Route::get('estadistica/estado-procesos', [TiendaEstadisticaController::class, 'porcentajeByEstadoProcesos']);
    Route::get('estadistica/motivo-procesos', [TiendaEstadisticaController::class, 'gestionesByMotivo']);
    Route::get('estadistica/monto-total-tipo-proceso', [TiendaEstadisticaController::class, 'montoTotalByTipoProceso']);
    Route::post('estadistica/fechas', [TiendaEstadisticaController::class, 'fechas']);

    Route::get('tienda/devoluciones/{id}', [TiendaDevolucionController::class, 'show']);
    Route::get('tienda/devoluciones', [TiendaDevolucionController::class, 'allDevoluciones']);
    Route::get('tienda/devoluciones/por-confirmar/todos', [TiendaDevolucionController::class, 'allDevolucionesPorConfirmar']);
    Route::get('tienda/devoluciones/producto-devuelto/{gestion_id}/{tracker_id}', [TiendaDevolucionController::class, 'productoDevuelto']);
    Route::get('tienda/devoluciones/producto-no-devuelto/{gestion_id}/{tracker_id}', [TiendaDevolucionController::class, 'productoNoDevuelto']);
    Route::post('tienda/devoluciones-error-tienda/aceptar/{id}', [TiendaDevolucionController::class, 'aceptarDevolucionError']);
    Route::post('tienda/devoluciones-error-tienda/rechazar/{id}', [TiendaDevolucionController::class, 'denegarDevolucionError']);
    Route::get('tienda/devoluciones/aceptar-devolucion/{gestion_id}/{tracker_id}', [TiendaDevolucionController::class, 'aceptarDevolucion']);
    Route::post('tienda/devoluciones/denegar-devolucion/{id}', [TiendaDevolucionController::class, 'rechazarDevolucion']);
    Route::post('tienda/devoluciones/actualizar-estado/{id}', [TiendaDevolucionController::class, 'updateState']);
    Route::post('tienda/devoluciones/devolucion-dinero/{gestionId}/{trackerId}', [TiendaDevolucionController::class, 'devolverDinero']);

    

    Route::get('products', [TiendaProductoController::class, 'get']);
    Route::get('products/{id}', [TiendaProductoController::class, 'show']);
    Route::post('products', [TiendaProductoController::class, 'store']);
    Route::post('products/state', [TiendaProductoController::class, 'state']);
    Route::post('products/upload-csv', [TiendaProductoController::class, 'uploadCsv']);
    Route::post('products/agregar-inventario/{id}', [TiendaProductoController::class, 'agregarInventario']);
    Route::post('products/disminuir-inventario/{id}', [TiendaProductoController::class, 'disminuirInventario']);
    Route::post('products/{id}', [TiendaProductoController::class, 'update']);
    Route::post('products/delete/{id}', [TiendaProductoController::class, 'delete']);

    Route::get('distritos', [DistritoController::class, 'todosPaginate']);
    Route::post('distritos/state', [DistritoController::class, 'cambiarEstado']);
});

//Rutas para tu-Repo
Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::get('tu-repo/servicios', [TuRepoWebController::class, 'getServices']);
    Route::post('tu-repo/servicios/state', [TuRepoWebController::class, 'updateStatus']);
    Route::get('tu-repo/cupones-descuento/todos', [TuRepoCuponDescuentoController::class, 'todos']);
    Route::post('tu-repo/cupon/crear', [TuRepoCuponDescuentoController::class, 'crearCupon']);
});

//Rutas para wiqli
Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::get('wiqli/pedidos', [WiqliPedidoController::class, 'all']);
    Route::get('wiqli/productos', [WiqliProductoController::class, 'allAdmin']);
    Route::get('wiqli/producto/{productoId}', [WiqliProductoController::class, 'show']);

    Route::get('wiqli/unidades/todos', [WiqliUnidadController::class, 'all']);
    Route::get('wiqli/categorias/todos', [WiqliCategoriaController::class, 'all']);
});