<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TiendaController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Web\CambioDevolucionController;
use App\Http\Controllers\Web\ContactoController;
use App\Http\Controllers\Web\TiendaAliadaController;
use App\Http\Controllers\Web\OperadorAliadoController;
use App\Http\Controllers\Tienda\TiendaCambioController;
use App\Http\Controllers\Tienda\ConfiguracionController;
use App\Http\Controllers\DistritoController;
use App\Http\Controllers\CambioController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\ReactController;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\Repo\RepoCambioController;
use App\Http\Controllers\Repo\RepoFacturacionController;

Route::post('oauth/token', [ AuthController::class, 'login'])->name('api/login');
Route::post('oauth-tienda/token', [ AuthController::class, 'loginTienda'])->name('api/login-tienda');
Route::post('oauth-cliente/token', [ AuthController::class, 'loginCliente'])->name('api/login-cliente');


Route::post('registro-usuario', [RegisterController::class, 'register']);
Route::post('login-cliente', [UserController::class, 'loginCliente']);
Route::post('contacto/email', [ContactoController::class, 'enviarEmail']);

Route::get('distritos', [DistritoController::class, 'todos']);
Route::get('tienda/obtener-x-nombre', [TiendaController::class, 'obtenerTiendaByNombre']);
Route::get('tiendas-aliadas', [TiendaController::class, 'allWeb']);
Route::post('tienda-aliada/registro', [TiendaAliadaController::class, 'register']);
Route::post('operador-aliado/registro', [OperadorAliadoController::class, 'register']);


Route::get('tienda/informacion/{tienda_id}', [TiendaController::class, 'getInformacionTienda']);
Route::get('cambios-devoluciones', [CambioDevolucionController::class, 'obtenerCompra']);
Route::get('productos-tienda/{tienda_id}', [CambioDevolucionController::class, 'getAllProductsByStore']);
/////////////////////pdf-publico-facturacion
Route::get('ver-pdf/{tienda_id}/{fecha_inicial}/{fecha_final}', [RepoFacturacionController::class, 'createPdf']);
///////////////////////// falta confirmar
Route::post('cambios-second/{id}', [CambioController::class, 'createWayBill2']);
Route::post('cambios-tienda/{id}', [TiendaCambioController::class, 'updateState']);
Route::get('procesos/usuario/{id}', [UserController::class, 'getProcesoById']);
Route::get('configuracion/tienda/{id}', [ConfiguracionController::class, 'getConfiguracionByTienda']);
Route::get('configuracion/tienda/codigo-repo/{codigo}', [ConfiguracionController::class, 'getConfiguracionByCodigoRepo']);
////////////////////
Route::get('crear-olva', [ExampleController::class, 'createOrden']);
Route::get('get-olva', [ExampleController::class, 'getOrden']);
Route::get('get-batch', [ExampleController::class, 'getBatch']);
Route::get('create-olva/{id}', [RepoCambioController::class, 'createWayBill']);
Route::get('estado-olva', [ExampleController::class, 'verTrackerOlva']);
Route::get('gestionesOlva', [ExampleController::class, 'verGestionesOlva']);


require( app_path().'/../routes/repo-routes.php');
require( app_path().'/../routes/repoRetail-routes.php');
require( app_path().'/../routes/tuRepo-routes.php');
require( app_path().'/../routes/wiqli.php');
Route::get('/{any}', [ReactController::class, 'index'])->where('any', '^(?!api).*$');