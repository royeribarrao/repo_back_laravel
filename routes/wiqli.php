<?php

use App\Http\Controllers\Wiqli\WiqliProductoController;
use App\Http\Controllers\Wiqli\WiqliPedidoController;

Route::get('wiqli/productos/todos', [WiqliProductoController::class, 'all']);
Route::post('wiqli/crear-pedido', [WiqliPedidoController::class, 'crearPedido']);
Route::get('wiqli/exportar-pedido', [WiqliPedidoController::class, 'exportExcel']);

Route::get('wiqli/ver-data', [WiqliPedidoController::class, 'all']);
Route::get('wiqli/ver-pdf/{pedidoId}', [WiqliPedidoController::class, 'verPdf']);
Route::get('prueba', [WiqliPedidoController::class, 'prueba']);

Route::get('prueba2', function () {
    return view('wiqli.avisoWiqli');
});