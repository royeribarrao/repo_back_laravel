<?php

use App\Http\Controllers\Web\GestionRepoController;
use App\Http\Controllers\Web\RepoGestionController;
use App\Http\Controllers\Web\PayUController;
use App\Http\Controllers\Web\GestionController;

Route::prefix('repo/')->group(function () {
    Route::get('gestion/{gestion_id}', [GestionController::class, 'show']);
    Route::post('crear-gestion/{codigo_compra}/{tienda_id}', [RepoGestionController::class, 'crearProceso']);
    Route::post('crear-gestion/devolucion/{codigo_compra}/{tienda_id}', [RepoGestionController::class, 'crearProcesoDevolucion']);
    Route::post('agregar-nuevos-productos/{tienda_id}/{gestion_id}', [RepoGestionController::class, 'agregarNuevosProductos']);
    Route::post('asignar-datos-delivery/{gestion_id}/{codigo_repo}', [RepoGestionController::class, 'guardarDatosDelivery']);
    Route::post('pagar-gestion', [PayUController::class, 'cobroServicioRepo']);
    Route::post('confirmar-gestion/sin-pago/{gestion_id}', [GestionRepoController::class, 'confirmarGestionSinPago']);
    Route::get('formulario-pago/{gestion_id}', [GestionRepoController::class, 'show']);
});