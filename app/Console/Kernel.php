<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\ActualizarTrackerDevolucion;
use App\Console\Commands\ActualizarTrackerCambioDeluxe;
use App\Console\Commands\ActualizarTrackerCambioEstandar;
use App\Console\Commands\VerificarEstadoPayU;
use App\Console\Commands\ActualizarInventarioWooCommerce;
use App\Console\Commands\EstablecerFalseInventarioWoocommerce;
use App\Console\Commands\ActualizarVentasWoocommerce;
use App\Console\Commands\AgregarNuevosProductosWoocommerce;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        ActualizarTrackerDevolucion::class,
        ActualizarTrackerCambioDeluxe::class,
        ActualizarTrackerCambioEstandar::class,
        VerificarEstadoPayU::class,
        ActualizarInventarioWooCommerce::class,
        EstablecerFalseInventarioWoocommerce::class,
        ActualizarVentasWoocommerce::class,
        AgregarNuevosProductosWoocommerce::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('actualizar_tracker_devolucion')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('actualizar_tracker_cambio_deluxe')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('actualizar_tracker_cambio_estandar')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('verificar_estado_payu')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('command:establecer_false_inventario_woocommerce')
                  ->timezone('America/Lima')
                  ->thursdays()
                  ->at('22:00');
        $schedule->command('command:actualizar_inventario_woocommerce')
                  ->timezone('America/Lima')
                  ->fridays();
        $schedule->command('command:actualizar_ventas_woocommerce')
                  ->timezone('America/Lima')
                  ->daily();
        $schedule->command('command:agregar_nuevos_productos_woocommerce')
                  ->timezone('America/Lima')
                  ->everyMinute();
    }
    
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
