<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrackerCambioEstandar;
use App\Models\Gestion;
use App\Models\ServicioLogistico;
use App\Models\Tienda;
use App\Mail\OperadorLogisticoEnCaminoRecojo;
use App\Mail\ProductoEntregadoTienda;
use App\Mail\ProductoNuevoEnCamino;
use App\Mail\ProductoNuevoEntregado;
use App\Mail\ServicioTecnico\LogisticoEnCaminoTecnico;
use App\Mail\ServicioTecnico\ProductoEnCamino;
use Mail;

class OlvaActualizarTrackerEstandar extends Command
{
    protected $signature = 'command:olvaUpdateTrackerEstandar';

    protected $description = 'Actualizar todos los trackers estandars pertenecientes a Olva.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $client   = new \GuzzleHttp\Client();
        $gestiones = Gestion::with([
                                'servicioLogistico' => function ($q){
                                    $q->where('carrier_id', 3);
                                }])
                            ->where('en_proceso', true)
                            ->whereIn('tipo_servicio', [1,4])
                            ->where('confirmacion_web', true)
                            ->where('confirmacion_tienda', true)
                            ->get();
        foreach($gestiones as $key => $gestion){
            $tracker = TrackerCambioEstandar::where('gestion_id', $gestion->id)->first();
            
            $gestion_actual = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
            $servicio_logistico_recojo = ServicioLogistico::where('gestion_id', $gestion->id)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->where('tipo', 1)
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $servicio_logistico_entrega = ServicioLogistico::where('gestion_id', $gestion->id)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->where('tipo', 2)
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $tienda = Tienda::find($gestion_actual->tienda_id);
            if($gestion_actual->estado > 1 && $gestion_actual->estado < 7){
                $request = $client->request('GET', $servicio_logistico_recojo->tracking_link);
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                $estados = $array_response['events'];
                foreach($estados as $key => $item){
                    if(isset($item['label'])){
                        // if($item['label'] == "En camino a recojo" && $tracker->estado == 2){
                        //     $tracker->update([
                        //         'en_camino' => true,
                        //         'nombre_estado' => 'En camino',
                        //         'estado' => 3
                        //     ]);
                        //     if($gestion_actual->tipo_servicio == 4){
                        //         Mail::to($gestion_actual->delivery->correo)->send(new LogisticoEnCaminoTecnico($gestion_actual, $gestion_actual->delivery, $tienda, 'OLVA'));
                        //     }else{
                        //         Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                        //     }
                        //     $gestion_actual->update([
                        //         'estado' => 3
                        //     ]);
                        // }
                        if($item['label'] == "Recogido"){
                            $tracker->update([
                                'en_camino' => true,
                                'producto_recogido' => true,
                                'nombre_estado' => 'Producto recogido',
                                'estado' => 4
                            ]);
                            $gestion_actual->update([
                                'estado' => 4
                            ]);
                        }
                        if($item['label'] == "Entregado"){
                            $tracker->update([
                                'en_camino' => true,
                                'producto_recogido' => true,
                                'producto_devuelto' => true,
                                'nombre_estado' => 'Producto devuelto a tienda',
                                'estado' => 5
                            ]);
                            Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                            $gestion_actual->update([
                                'estado' => 5
                            ]);
                        }
                    }
                }
            }elseif($gestion_actual->estado > 6){
                $request = $client->request('GET', $servicio_logistico_entrega->tracking_link);
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                if(isset($array_response['events'])){
                    $estados = $array_response['events'];
                    foreach($estados as $key => $item){
                        if(isset($item['label'])){
                            if($item['label'] == "En camino a entrega"){
                                $tracker->update([
                                    'producto_nuevo_en_camino' => true,
                                    'nombre_estado' => 'Producto nuevo en camino.',
                                    'estado' => 8
                                ]);
                                if($gestion_actual->tipo_servicio == 4){
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoEnCamino($gestion_actual, $gestion_actual->delivery, $tienda));
                                }else{
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                }
                                $gestion_actual->update([
                                    'estado' => 8
                                ]);
                            }
                            if($item['label'] == "Entregado"){
                                $tracker->update([
                                    'producto_nuevo_en_camino' => true,
                                    'producto_nuevo_entregado' => true,
                                    'nombre_estado' => 'Producto nuevo entregado',
                                    'estado' => 9
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                $gestion->update([
                                    'estado' => 9,
                                    'en_proceso' => false,
                                    'finalizado' => true
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
}
