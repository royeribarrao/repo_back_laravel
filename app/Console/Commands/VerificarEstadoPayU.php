<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SdkPayU\lib\PayU\util\PayUParameters;
use App\SdkPayU\lib\PayU\PayUReports;
use App\Models\PayUTransaccion;

class VerificarEstadoPayU extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verificar_estado_payu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para verificar el estado de los procesos pendientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $procesos = PayUTransaccion::where('responseCode', 'PENDING_TRANSACTION_CONFIRMATION')
                                ->orWhere('responseCode', 'PENDING_TRANSACTION_TRANSMISSION')
                                ->orWhere('state', 'PENDING')
                                ->get();
        foreach($procesos as $key => $item)
        {
            $transaccion = PayUTransaccion::find($item->id);
            if($item->orderId)
            {
                $parameters = array(PayUParameters::ORDER_ID => "$item->orderId");

                $order = PayUReports::getOrderDetail($parameters);
                
                if($order->status == "APPROVED")
                {
                    $transaccion->update([
                        'state' => "APPROVED"
                    ]);
                    $gestion = Gestion::find($transaccion->servicio_id);
                    $gestion->update([
                        'confirmacion_web' => true
                    ]);
                    $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();

                    $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.sku_producto', 'products.sku_producto')
                            ->get()
                            ->toArray();
                    Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, $new_productos));
                    Mail::to('contacto@repo.com.pe')->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo"));
                }
                if($order->status == "PENDING")
                {
                    $transaccion->update([
                        'state' => "PENDING"
                    ]);
                }
            }
            if($item->orderId && $item->state == "PENDING")
            {
                $parameters = array(PayUParameters::ORDER_ID => "$item->orderId");

                $order = PayUReports::getOrderDetail($parameters);
                if($order->status == "APPROVED")
                {
                    $transaccion->update([
                        'state' => "APPROVED"
                    ]);
                }
            }
        }
    }
}
