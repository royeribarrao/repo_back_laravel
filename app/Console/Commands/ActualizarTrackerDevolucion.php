<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrackerDevolucion;
use App\Models\Gestion;
use App\Models\ServicioLogistico;
use App\Mail\AvisoTrackerDevolucion;
use App\Mail\OperadorLogisticoEnCaminoRecojo;
use App\Mail\ProductoEntregadoTienda;
use Mail;

class ActualizarTrackerDevolucion extends Command
{

    protected $signature = 'actualizar_tracker_devolucion';

    protected $description = 'Actualiza todas las devoluciones, consumiendo el estatus de logix.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=';
        $gestiones = Gestion::where('en_proceso', true)
                                ->where('tipo_servicio', 3)
                                ->where('confirmacion_web', true)
                                ->where('confirmacion_tienda', true)
                                ->get();
        
        foreach($gestiones as $key => $gestion){
            
            $tracker = TrackerDevolucion::where('gestion_id', $gestion->id)->first();
            $gestion_actual = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
            $servicio_logistico = ServicioLogistico::select(
                                        'waybillNumber',
                                        'carriers.codigo',
                                        'carriers.nombre'
                                    )
                                    ->where('gestion_id', $gestion->id)
                                    ->whereNotNull('carrier_id')
                                    ->whereNotNull('carrierWaybill')
                                    ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                    ->first();
            $request = $client->request('GET', $url.$servicio_logistico->waybillNumber);
            $response = $request->getBody()->getContents();
            $str=str_replace("\r\n","",$response);
            $array_response = json_decode($str, true);
            $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];
            
            if($servicio_logistico->codigo == 'NIREX'){
                $this->revisarTrackerNirex($estados, $tracker, $gestion_actual, $servicio_logistico);
                // foreach($estados as $key => $item){
                //     if(isset($item['remarks'])){
                //         if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 2){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'nombre_estado' => 'En camino',
                //                 'estado' => 3
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                //             $gestion_actual->update([
                //                 'estado' => 3
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been picked-up & on its way to drop-off" && $tracker->estado == 3){
                //             $tracker->update([
                //                 'producto_recogido' => true,
                //                 'nombre_estado' => 'Producto recogido',
                //                 'estado' => 4
                //             ]);
                //             $gestion_actual->update([
                //                 'estado' => 4
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been dropped off." && $tracker->estado != 5){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'producto_recogido' => true,
                //                 'producto_devuelto' => true,
                //                 'nombre_estado' => 'Producto devuelto',
                //                 'estado' => 5
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                //             $gestion_actual->update([
                //                 'estado' => 5
                //             ]);
                //         }
                //     }
                // }
            }elseif($servicio_logistico->codigo == 'CARGUI'){
                $this->revisarTrackerCargui($estados, $tracker, $gestion_actual, $servicio_logistico);
                // foreach($estados as $key => $item){
                //     if(isset($item['remarks'])){
                //         if($item['remarks'] == "The driver will start the order." && $tracker->estado == 2){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'nombre_estado' => 'En camino',
                //                 'estado' => 3
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                //             $gestion_actual->update([
                //                 'estado' => 3
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been picked-up & on its way to drop-off." && $tracker->estado == 3){
                //             $tracker->update([
                //                 'producto_recogido' => true,
                //                 'nombre_estado' => 'Producto recogido',
                //                 'estado' => 4
                //             ]);
                //             $gestion_actual->update([
                //                 'estado' => 4
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been dropped off." && $tracker->estado != 5){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'producto_recogido' => true,
                //                 'producto_devuelto' => true,
                //                 'nombre_estado' => 'Producto devuelto',
                //                 'estado' => 5
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                //             $gestion_actual->update([
                //                 'estado' => 5
                //             ]);
                //         }
                //     }
                // }
            }elseif($servicio_logistico->codigo == 'OLVA'){
                $this->revisarTrackerOlva($estados, $tracker, $gestion_actual, $servicio_logistico);
                // foreach($estados as $key => $item){
                //     if(isset($item['remarks'])){
                //         if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'nombre_estado' => 'En camino',
                //                 'estado' => 3
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                //             $gestion_actual->update([
                //                 'estado' => 3
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3){
                //             $tracker->update([
                //                 'producto_recogido' => true,
                //                 'nombre_estado' => 'Producto recogido',
                //                 'estado' => 4
                //             ]);
                //             $gestion_actual->update([
                //                 'estado' => 4
                //             ]);
                //         }
                //         if($item['remarks'] == "Product has been dropped off" && $tracker->estado != 5){
                //             $tracker->update([
                //                 'en_camino' => true,
                //                 'producto_recogido' => true,
                //                 'producto_devuelto' => true,
                //                 'nombre_estado' => 'Producto devuelto',
                //                 'estado' => 5
                //             ]);
                //             Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                //             $gestion_actual->update([
                //                 'estado' => 5
                //             ]);
                //         }
                //     }
                // }
            }
        }
    }

    public function revisarTrackerNirex($estados, $tracker, $gestion_actual, $servicio_logistico)
    {
        foreach($estados as $key => $item){
            if(isset($item['remarks'])){
                if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 2){
                    $tracker->update([
                        'en_camino' => true,
                        'nombre_estado' => 'En camino',
                        'estado' => 3
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                    $gestion_actual->update([
                        'estado' => 3
                    ]);
                }
                if($item['remarks'] == "Product has been picked-up & on its way to drop-off" && $tracker->estado == 3){
                    $tracker->update([
                        'producto_recogido' => true,
                        'nombre_estado' => 'Producto recogido',
                        'estado' => 4
                    ]);
                    $gestion_actual->update([
                        'estado' => 4
                    ]);
                }
                if($item['remarks'] == "Product has been dropped off." && $tracker->estado != 5){
                    $tracker->update([
                        'en_camino' => true,
                        'producto_recogido' => true,
                        'producto_devuelto' => true,
                        'nombre_estado' => 'Producto devuelto',
                        'estado' => 5
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                    $gestion_actual->update([
                        'estado' => 5
                    ]);
                }
            }
        }
    }

    public function revisarTrackerCargui($estados, $tracker, $gestion_actual, $servicio_logistico)
    {
        foreach($estados as $key => $item){
            if(isset($item['remarks'])){
                if($item['remarks'] == "The driver will start the order." && $tracker->estado == 2){
                    $tracker->update([
                        'en_camino' => true,
                        'nombre_estado' => 'En camino',
                        'estado' => 3
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                    $gestion_actual->update([
                        'estado' => 3
                    ]);
                }
                if($item['remarks'] == "Product has been picked-up & on its way to drop-off." && $tracker->estado == 3){
                    $tracker->update([
                        'producto_recogido' => true,
                        'nombre_estado' => 'Producto recogido',
                        'estado' => 4
                    ]);
                    $gestion_actual->update([
                        'estado' => 4
                    ]);
                }
                if($item['remarks'] == "Product has been dropped off." && $tracker->estado != 5){
                    $tracker->update([
                        'en_camino' => true,
                        'producto_recogido' => true,
                        'producto_devuelto' => true,
                        'nombre_estado' => 'Producto devuelto',
                        'estado' => 5
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                    $gestion_actual->update([
                        'estado' => 5
                    ]);
                }
            }
        }
    }

    public function revisarTrackerOlva($estados, $tracker, $gestion_actual, $servicio_logistico)
    {
        foreach($estados as $key => $item){
            if(isset($item['remarks'])){
                if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2){
                    $tracker->update([
                        'en_camino' => true,
                        'nombre_estado' => 'En camino',
                        'estado' => 3
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico));
                    $gestion_actual->update([
                        'estado' => 3
                    ]);
                }
                if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3){
                    $tracker->update([
                        'producto_recogido' => true,
                        'nombre_estado' => 'Producto recogido',
                        'estado' => 4
                    ]);
                    $gestion_actual->update([
                        'estado' => 4
                    ]);
                }
                if($item['remarks'] == "Product has been dropped off" && $tracker->estado != 5){
                    $tracker->update([
                        'en_camino' => true,
                        'producto_recogido' => true,
                        'producto_devuelto' => true,
                        'nombre_estado' => 'Producto devuelto',
                        'estado' => 5
                    ]);
                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                    $gestion_actual->update([
                        'estado' => 5
                    ]);
                }
            }
        }
    }
}