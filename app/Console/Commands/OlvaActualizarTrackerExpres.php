<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrackerCambioDeluxe;
use App\Models\Gestion;
use App\Models\ServicioLogistico;
use App\Mail\OperadorLogisticoEnCaminoRecojo;
use App\Mail\ProductoEntregadoTienda;
use App\Mail\ProductoNuevoEnCamino;
use App\Mail\ProductoNuevoEntregado;
use Mail;

class OlvaActualizarTrackerExpres extends Command
{
    protected $signature = 'command:olvaUpdateTrackerExpres';

    protected $description = 'Actualizar todos los trackers expres pertenecientes a Olva.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $client   = new \GuzzleHttp\Client();
        $gestiones = Gestion::with([
                            'servicioLogistico' => function ($q){
                                $q->where('carrier_id', 3);
                            }])
                        ->where('en_proceso', true)
                        ->where('tipo_servicio', 2)
                        ->where('confirmacion_web', true)
                        ->where('confirmacion_tienda', true)
                        ->get();
        foreach($gestiones as $key => $gestion){
            
            $tracker = TrackerCambioDeluxe::where('gestion_id', $gestion->id)->first();
            $gestion_actual = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
            $servicio_logistico_recojo = ServicioLogistico::where('gestion_id', $gestion->id)
                                                ->where('tipo', 1)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $servicio_logistico_entrega = ServicioLogistico::where('gestion_id', $gestion->id)
                                                    ->where('tipo', 2)
                                                    ->whereNotNull('carrier_id')
                                                    ->whereNotNull('carrierWaybill')
                                                    ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                    ->first();
            if($gestion_actual->estado < 5)
            {
                if($servicio_logistico_recojo)
                {
                    $request = $client->request('GET', $servicio_logistico_recojo->tracking_link);
                    $response = $request->getBody()->getContents();
                    $str=str_replace("\r\n","",$response);
                    $array_response = json_decode($str, true);
                    $estados = $array_response['events'];
                    foreach($estados as $key => $item)
                    {
                        if(isset($item['label']))
                        {
                            if($item['label'] == "Recogido")
                            {
                                $tracker->update([
                                    'en_camino' => true,
                                    'producto_recogido' => true,
                                    'nombre_estado' => 'Producto recogido',
                                    'estado' => 4
                                ]);
                                $gestion_actual->update([
                                    'estado' => 4
                                ]);
                            }
                            if($item['label'] == "Entregado")
                            {
                                $tracker->update([
                                    'en_camino' => true,
                                    'producto_recogido' => true,
                                    'producto_devuelto' => true,
                                    'nombre_estado' => 'Producto devuelto',
                                    'estado' => 5
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                $gestion_actual->update([
                                    'estado' => 5
                                ]);
                            }
                        }
                    }
                }  
            }elseif($gestion_actual->estado > 4){
                if($servicio_logistico_entrega)
                {
                    $request = $client->request('GET', $servicio_logistico_entrega->tracking_link);
                    $response = $request->getBody()->getContents();
                    $str=str_replace("\r\n","",$response);
                    $array_response = json_decode($str, true);
                    $estados = $array_response['events'];
                    foreach($estados as $key => $item){
                        if(isset($item['label'])){
                            if($item['label'] == "En camino a entrega"){
                                $tracker->update([
                                    'producto_nuevo_en_camino' => true,
                                    'nombre_estado' => 'En camino',
                                    'estado' => 6
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                $gestion_actual->update([
                                    'estado' => 6
                                ]);
                            }
                            if($item['label'] == "Entregado"){
                                $tracker->update([
                                    'producto_nuevo_en_camino' => true,
                                    'producto_nuevo_entregado' => true,
                                    'nombre_estado' => 'Producto devuelto',
                                    'estado' => 7
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                $gestion->update([
                                    'estado' => 7,
                                    'en_proceso' => false,
                                    'finalizado' => true
                                ]);
                            }
                        }
                    }
                }
            }                                       
        }
    }
}
