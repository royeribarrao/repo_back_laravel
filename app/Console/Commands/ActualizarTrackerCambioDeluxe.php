<?php

namespace App\Console\Commands;
use App\Models\TrackerCambioDeluxe;
use App\Models\Gestion;
use App\Models\ServicioLogistico;
use Illuminate\Console\Command;
use Mail;
use App\Mail\OperadorLogisticoEnCaminoRecojo;
use App\Mail\ProductoEntregadoTienda;
use App\Mail\ProductoNuevoEnCamino;
use App\Mail\ProductoNuevoEntregado;

class ActualizarTrackerCambioDeluxe extends Command
{

    protected $signature = 'actualizar_tracker_cambio_deluxe';

    protected $description = 'Actualiza todos los cambios deluxe, consumiendo el estatus de logix.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=';
        $gestiones = Gestion::where('en_proceso', true)
                                ->where('tipo_servicio', 2)
                                ->where('confirmacion_web', true)
                                ->where('confirmacion_tienda', true)
                                ->get();
        
        foreach($gestiones as $key => $gestion){
            
            $tracker = TrackerCambioDeluxe::where('gestion_id', $gestion->id)->first();
            $gestion_actual = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
            $servicio_logistico_recojo = ServicioLogistico::select(
                                                    'waybillNumber',
                                                    'carriers.codigo',
                                                    'carriers.nombre'
                                                )
                                                ->where('gestion_id', $gestion->id)
                                                ->where('tipo', 1)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $servicio_logistico_entrega = ServicioLogistico::select(
                                                        'waybillNumber',
                                                        'carriers.codigo',
                                                        'carriers.nombre'
                                                    )
                                                    ->where('gestion_id', $gestion->id)
                                                    ->where('tipo', 2)
                                                    ->whereNotNull('carrier_id')
                                                    ->whereNotNull('carrierWaybill')
                                                    ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                    ->first();
            if($gestion_actual->estado < 5)
            {
                if($servicio_logistico_recojo)
                {
                    $request = $client->request('GET', $url.$servicio_logistico_recojo->waybillNumber);
                    $response = $request->getBody()->getContents();
                    $str=str_replace("\r\n","",$response);
                    $array_response = json_decode($str, true);
                    $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];
                    if($servicio_logistico_recojo->codigo == 'NIREX')
                    {
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if(isset($item['remarks'])){
                                    if(isset($item['remarks'])){
                                        if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 2){
                                            $tracker->update([
                                                'en_camino' => true,
                                                'nombre_estado' => 'En camino',
                                                'estado' => 3
                                            ]);
                                            Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                            $gestion_actual->update([
                                                'estado' => 3
                                            ]);
                                        }
                                        if($item['remarks'] == "Product has been picked-up & on its way to drop-off" && $tracker->estado == 3){
                                            $tracker->update([
                                                'producto_recogido' => true,
                                                'nombre_estado' => 'Producto recogido',
                                                'estado' => 4
                                            ]);
                                            $gestion_actual->update([
                                                'estado' => 4
                                            ]);
                                        }
                                        if($item['remarks'] == "Product has been dropped off." && $tracker->estado != 5){
                                            $tracker->update([
                                                'producto_devuelto' => true,
                                                'nombre_estado' => 'Producto devuelto',
                                                'estado' => 5
                                            ]);
                                            Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                            $gestion_actual->update([
                                                'estado' => 5
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    elseif($servicio_logistico_recojo->codigo == 'CARGUI')
                    {
                        foreach($estados as $key => $item)
                        {
                            if(isset($item['remarks']))
                            {
                                if(isset($item['remarks']))
                                {
                                    if(isset($item['remarks']))
                                    {
                                        if($item['remarks'] == "The driver will start the order." && $tracker->estado == 2)
                                        {
                                            $tracker->update([
                                                'en_camino' => true,
                                                'nombre_estado' => 'En camino',
                                                'estado' => 3
                                            ]);
                                            Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                            $gestion_actual->update([
                                                'estado' => 3
                                            ]);
                                        }
                                        if($item['remarks'] == "Product has been picked-up & on its way to drop-off." && $tracker->estado == 3)
                                        {
                                            $tracker->update([
                                                'producto_recogido' => true,
                                                'nombre_estado' => 'Producto recogido',
                                                'estado' => 4
                                            ]);
                                            $gestion_actual->update([
                                                'estado' => 4
                                            ]);
                                        }
                                        if($item['remarks'] == "Product has been dropped off.")
                                        {
                                            $tracker->update([
                                                'producto_devuelto' => true,
                                                'nombre_estado' => 'Producto devuelto',
                                                'estado' => 5
                                            ]);
                                            Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                            $gestion_actual->update([
                                                'estado' => 5
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach($estados as $key => $item)
                        {
                            if(isset($item['remarks']))
                            {
                                if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2)
                                {
                                    $tracker->update([
                                        'en_camino' => true,
                                        'nombre_estado' => 'En camino',
                                        'estado' => 3
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                    $gestion_actual->update([
                                        'estado' => 3
                                    ]);
                                }
                                if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3)
                                {
                                    $tracker->update([
                                        'producto_recogido' => true,
                                        'nombre_estado' => 'Producto recogido',
                                        'estado' => 4
                                    ]);
                                    $gestion_actual->update([
                                        'estado' => 4
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off")
                                {
                                    $tracker->update([
                                        'producto_devuelto' => true,
                                        'nombre_estado' => 'Producto devuelto',
                                        'estado' => 5
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                    $gestion_actual->update([
                                        'estado' => 5
                                    ]);
                                }
                            }
                        }
                    }
                }  
            }elseif($gestion_actual->estado > 4){
                if($servicio_logistico_entrega)
                {
                    $request = $client->request('GET', $url.$servicio_logistico_entrega->waybillNumber);
                    $response = $request->getBody()->getContents();
                    $str=str_replace("\r\n","",$response);
                    $array_response = json_decode($str, true);
                    $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];
                    if($servicio_logistico_entrega->codigo == 'NIREX'){
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 5){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'En camino',
                                        'estado' => 6
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    $gestion_actual->update([
                                        'estado' => 6
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off." && $tracker->estado == 6){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto devuelto',
                                        'estado' => 7
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 7,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }elseif($servicio_logistico_entrega->codigo == 'CARGUI'){
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "The driver will start the order." && $tracker->estado == 5){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'En camino',
                                        'estado' => 6
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    $gestion_actual->update([
                                        'estado' => 6
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off." && $tracker->estado == 6){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto devuelto',
                                        'estado' => 7
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 7,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }else{
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "In transit/on its way to drop-off"){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'En camino',
                                        'estado' => 6
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    $gestion_actual->update([
                                        'estado' => 6
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 6){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto devuelto',
                                        'estado' => 7
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 7,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}