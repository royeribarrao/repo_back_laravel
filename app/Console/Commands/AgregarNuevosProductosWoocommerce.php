<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Automattic\WooCommerce\Client;
use App\Models\Producto;

class AgregarNuevosProductosWoocommerce extends Command
{
    protected $signature = 'command:agregar_nuevos_productos_woocommerce';

    protected $description = 'Servirá para agregar nuevos productos de las tiendas que tengan integración con Woocommerce.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $wc = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        $page = 1;
        $products = [];
        $all_products = [];
        do{
            try {
                $products = $wc->get('products',['per_page' => 50, 'page' => $page]);
            }catch(HttpClientException $e){
                die("Can't get products: $e");
            }
            $all_products = array_merge($all_products,$products);
            $page++;
        } while (count($products) > 0);

        foreach($all_products as $key => $item){
            if ($item->type == "variable") {
                $variation = $wc->get('products/'.$item->id);
                foreach ($variation->variations as $var) {
                    $prod = $wc->get('products/'.$var);
                    $nuevo_producto = Producto::where('producto_id', $var)->first();
                    if (!$nuevo_producto) {
                        Producto::create([
                            'tienda_id' => 1,
                            'integracion_id' => 2,
                            'producto_id' => $prod->id
                        ]);
                    }elseif($nuevo_producto){
                        
                    }
                }
            }
        }
    }
}