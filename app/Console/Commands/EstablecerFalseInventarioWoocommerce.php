<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Woocommerce\WoocommerceProducto;

class EstablecerFalseInventarioWoocommerce extends Command
{
    protected $signature = 'command:establecer_false_inventario_woocommerce';

    protected $description = 'Actualiza el inventario estableciendo la columna actualizado a false.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        WoocommerceProducto::update(['actualizado' => false]);
    }
}
