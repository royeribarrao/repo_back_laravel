<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Sale;
use Automattic\WooCommerce\Client;

class ActualizarVentasWoocommerce extends Command
{
    protected $signature = 'command:actualizar_ventas_woocommerce';

    protected $description = 'Actualizará las ventas de las tiendas que tengan integración woocommerce.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        for ($page=1; $page < 5; $page++) { 
            $query = [
                'page' => $page,
                'per_page' => 15
            ];

            $ventas = $woocommerce->get('orders', $query);
        
            foreach($ventas as $key => $venta)
            {
                $venta_repo = Sale::where('codigo_compra', $venta->id)->where('integracion_id', 2)->first();
                if($venta_repo){
                    return "Tarea Finalizada";
                }else{
                    foreach($venta->line_items as $key => $line)
                    {
                        Sale::create([
                            'tienda_id'=> 1,
                            'codigo_compra'=> $venta->id,
                            'fecha_compra'=> $venta->date_created,
                            'sku'=> $line->sku,
                            'nombre_producto'=> $line->name,
                            'cantidad'=> $line->quantity,
                            'precio_pagado'=> $line->total,
                            'numero_documento'=> $venta->billing->company,
                            'nombre_cliente'=> $venta->billing->first_name,
                            'apellido_cliente'=> $venta->billing->last_name,
                            'telefono'=>  $venta->billing->phone,
                            'email'=>  $venta->billing->email,
                            'direccion'=> $venta->billing->address_1,
                            'distrito'=> $venta->billing->city
                        ]);
                    }
                }
            }
        }
    }
}
