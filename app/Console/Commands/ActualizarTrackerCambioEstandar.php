<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrackerCambioEstandar;
use App\Models\Gestion;
use App\Models\ServicioLogistico;
use App\Models\Tienda;
use App\Mail\OperadorLogisticoEnCaminoRecojo;
use App\Mail\ProductoEntregadoTienda;
use App\Mail\ProductoNuevoEnCamino;
use App\Mail\ProductoNuevoEntregado;
use App\Mail\ServicioTecnico\LogisticoEnCaminoTecnico;
use App\Mail\ServicioTecnico\ProductoEnCamino;
use Mail;

class ActualizarTrackerCambioEstandar extends Command
{

    protected $signature = 'actualizar_tracker_cambio_estandar';

    protected $description = 'Actualiza todos los cambios estandar, consumiendo el estatus de logix.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // $royer = Gestion::find(5);
        // $royer->update(['imagen_evidencia' => 'royer']);
        
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=';
        $gestiones = Gestion::where('en_proceso', true)
                                ->whereIn('tipo_servicio', [1,4])
                                ->where('confirmacion_web', true)
                                ->where('confirmacion_tienda', true)
                                ->get();
        
        foreach($gestiones as $key => $gestion){
            
            $tracker = TrackerCambioEstandar::where('gestion_id', $gestion->id)->first();
            
            $gestion_actual = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
            $servicio_logistico_recojo = ServicioLogistico::select(
                                                    'waybillNumber',
                                                    'carriers.codigo',
                                                    'carriers.nombre'
                                                )
                                                ->where('gestion_id', $gestion->id)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->where('tipo', 1)
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $servicio_logistico_entrega = ServicioLogistico::select(
                                                    'waybillNumber',
                                                    'carriers.codigo',
                                                    'carriers.nombre'
                                                )
                                                ->where('gestion_id', $gestion->id)
                                                ->whereNotNull('carrier_id')
                                                ->whereNotNull('carrierWaybill')
                                                ->where('tipo', 2)
                                                ->join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
                                                ->first();
            $tienda = Tienda::find($gestion_actual->tienda_id);
            if($gestion_actual->estado > 1 && $gestion_actual->estado < 7){
                $request = $client->request('GET', $url.$servicio_logistico_recojo->waybillNumber);
                
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];
                if($servicio_logistico_recojo->codigo == 'NIREX'){
                    
                    foreach($estados as $key => $item){
                        if(isset($item['remarks'])){
                            if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 2){
                                $tracker->update([
                                    'en_camino' => true,
                                    'nombre_estado' => 'En camino',
                                    'estado' => 3
                                ]);
                                if($gestion_actual->tipo_servicio == 4){
                                    Mail::to($gestion_actual->delivery->correo)->send(new LogisticoEnCaminoTecnico($gestion_actual, $gestion_actual->delivery, $tienda, 'NIREX'));
                                }else{
                                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                }
                                
                                $gestion_actual->update([
                                    'estado' => 3
                                ]);
                            }
                            if($item['remarks'] == "Product has been picked-up & on its way to drop-off" && $tracker->estado == 3){
                                $tracker->update([
                                    'producto_recogido' => true,
                                    'nombre_estado' => 'Producto recogido',
                                    'estado' => 4
                                ]);
                                $gestion_actual->update([
                                    'estado' => 4
                                ]);
                            }
                            if($item['remarks'] == "Product has been dropped off."){
                                $tracker->update([
                                    'producto_devuelto' => true,
                                    'nombre_estado' => 'Producto devuelto a tienda',
                                    'estado' => 5
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                $gestion_actual->update([
                                    'estado' => 5
                                ]);
                            }
                        }
                    }
                }elseif($servicio_logistico_recojo->codigo == 'CARGUI'){
                    
                    foreach($estados as $key => $item){
                        if(isset($item['remarks'])){
                            if($item['remarks'] == "The driver will start the order." && $tracker->estado == 2){
                                $tracker->update([
                                    'en_camino' => true,
                                    'nombre_estado' => 'En camino',
                                    'estado' => 3
                                ]);
                                if($gestion_actual->tipo_servicio == 4){
                                    Mail::to($gestion_actual->delivery->correo)->send(new LogisticoEnCaminoTecnico($gestion_actual, $gestion_actual->delivery, $tienda, 'CARGUI'));
                                }else{
                                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                }
                                $gestion_actual->update([
                                    'estado' => 3
                                ]);
                            }
                            if($item['remarks'] == "Product has been picked-up & on its way to drop-off." && $tracker->estado == 3){
                                $tracker->update([
                                    'producto_recogido' => true,
                                    'nombre_estado' => 'Producto recogido',
                                    'estado' => 4
                                ]);
                                $gestion_actual->update([
                                    'estado' => 4
                                ]);
                            }
                            if($item['remarks'] == "Product has been dropped off." && $tracker->estado == 4){
                                $tracker->update([
                                    'producto_devuelto' => true,
                                    'nombre_estado' => 'Producto devuelto a tienda',
                                    'estado' => 5
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                $gestion_actual->update([
                                    'estado' => 5
                                ]);
                            }
                        }
                    }
                }else{
                    foreach($estados as $key => $item){
                        if(isset($item['remarks'])){
                            if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2){
                                $tracker->update([
                                    'en_camino' => true,
                                    'nombre_estado' => 'En camino',
                                    'estado' => 3
                                ]);
                                if($gestion_actual->tipo_servicio == 4){
                                    Mail::to($gestion_actual->delivery->correo)->send(new LogisticoEnCaminoTecnico($gestion_actual, $gestion_actual->delivery, $tienda, 'OLVA'));
                                }else{
                                    Mail::to($gestion_actual->delivery->correo)->send(new OperadorLogisticoEnCaminoRecojo($gestion_actual, $servicio_logistico_recojo));
                                }
                                $gestion_actual->update([
                                    'estado' => 3
                                ]);
                            }
                            if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3){
                                $tracker->update([
                                    'producto_recogido' => true,
                                    'nombre_estado' => 'Producto recogido',
                                    'estado' => 4
                                ]);
                                $gestion_actual->update([
                                    'estado' => 4
                                ]);
                            }
                            if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 4){
                                $tracker->update([
                                    'producto_devuelto' => true,
                                    'nombre_estado' => 'Producto devuelto a tienda',
                                    'estado' => 5
                                ]);
                                Mail::to($gestion_actual->delivery->correo)->send(new ProductoEntregadoTienda($gestion_actual));
                                $gestion_actual->update([
                                    'estado' => 5
                                ]);
                            }
                        }
                    }
                }
            }elseif($gestion_actual->estado > 6){
                $request = $client->request('GET', $url.$servicio_logistico_entrega->waybillNumber);
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                if(isset($array_response['waybillTrackDetailList'])){
                    $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];
                    if($servicio_logistico_entrega->codigo == 'NIREX'){
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "Driver has accepted the order and now is in transit/on its way to pick-up." && $tracker->estado == 7){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'Producto nuevo en camino.',
                                        'estado' => 8
                                    ]);
                                    if($gestion_actual->tipo_servicio == 4){
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoEnCamino($gestion_actual, $gestion_actual->delivery, $tienda));
                                    }else{
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    }
                                    
                                    $gestion_actual->update([
                                        'estado' => 8
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off."){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto nuevo entregado',
                                        'estado' => 9
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 9,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }elseif($servicio_logistico_entrega->codigo == 'CARGUI'){
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "The driver will start the order." && $tracker->estado == 7){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'Producto nuevo en camino.',
                                        'estado' => 8
                                    ]);
                                    if($gestion_actual->tipo_servicio == 4){
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoEnCamino($gestion_actual, $gestion_actual->delivery, $tienda));
                                    }else{
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    }
                                    $gestion_actual->update([
                                        'estado' => 8
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off." && $tracker->estado == 8){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto nuevo entregado',
                                        'estado' => 9
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 9,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }else{
                        foreach($estados as $key => $item){
                            if(isset($item['remarks'])){
                                if($item['remarks'] == "In transit/on its way to drop-off" && $tracker->estado == 7){
                                    $tracker->update([
                                        'producto_nuevo_en_camino' => true,
                                        'nombre_estado' => 'Producto nuevo en camino.',
                                        'estado' => 8
                                    ]);
                                    if($gestion_actual->tipo_servicio == 4){
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoEnCamino($gestion_actual, $gestion_actual->delivery, $tienda));
                                    }else{
                                        Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEnCamino($gestion_actual, $servicio_logistico_entrega));
                                    }
                                    $gestion_actual->update([
                                        'estado' => 8
                                    ]);
                                }
                                if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 8){
                                    $tracker->update([
                                        'producto_nuevo_entregado' => true,
                                        'nombre_estado' => 'Producto nuevo entregado',
                                        'estado' => 9
                                    ]);
                                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoNuevoEntregado($gestion_actual, $servicio_logistico_entrega));
                                    $gestion->update([
                                        'estado' => 9,
                                        'en_proceso' => false,
                                        'finalizado' => true
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}