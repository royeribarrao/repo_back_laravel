<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Woocommerce\WoocommerceProducto;
use Automattic\WooCommerce\Client;

class ActualizarInventarioWooCommerce extends Command
{
    protected $signature = 'command:actualizar_inventario_woocommerce';

    protected $description = 'Actualiza el inventario de todas las tiendas que tengan integración a woocommerce';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // $productos = WoocommerceProducto::where('actualizado', false)->get();
        // $woocommerce = new Client(
        //     'https://inmaculada.shop/', 
        //     'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
        //     'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
        //     [
        //         'version' => 'wc/v3'
        //     ]
        // );
        // foreach($productos as $key => $producto)
        // {
        //     $producto_repo = WoocommerceProducto::find($producto->id);
        //     $producto_woo = $woocommerce->get('products/'.$producto->producto_id);
        //     $producto_repo->update([
        //         'color' => isset($producto_woo->attributes[0]) ? $producto_woo->attributes[0]->option : '',
        //         'description' => $producto_woo->description,
        //         'image' => isset($producto_woo->images[0]) ? $producto_woo->images[0]->src : '',
        //         'name' => $producto_woo->name,
        //         'parent_id' => $producto_woo->parent_id,
        //         'permalink' => $producto_woo->permalink,
        //         'price' => $producto_woo->price ? $producto_woo->price : 0,
        //         'purchasable' => $producto_woo->purchasable,
        //         'regular_price' => $producto_woo->regular_price ? $producto_woo->regular_price : 0,
        //         'sale_price' => $producto_woo->sale_price ? $producto_woo->sale_price : 0,
        //         'sku' => $producto_woo->sku,
        //         'status' => $producto_woo->status,
        //         'stock_quantity' => $producto_woo->stock_quantity ? $producto_woo->stock_quantity : 0,
        //         'talla' => isset($producto_woo->attributes[1]) ? $producto_woo->attributes[1]->option : '',
        //         'type' => $producto_woo->type,
        //         'actualizado' => true
        //     ]);
        // }
        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        $productos = Producto::where('actualizado', false)->where('integracion_id', 2)->get();
        foreach($productos as $key => $producto)
        {
            $producto_repo = Producto::where('producto_id', $producto->id)->first();
            $producto_woo = $woocommerce->get('products/'.$producto->producto_id);
            if($producto_repo){
                $producto_repo->update([
                    'color' => isset($producto_woo->attributes[0]) ? $producto_woo->attributes[0]->option : '',
                    'description' => $producto_woo->description,
                    'image' => isset($producto_woo->images[0]) ? $producto_woo->images[0]->src : '',
                    'name' => $producto_woo->name,
                    'parent_id' => $producto_woo->parent_id,
                    'permalink' => $producto_woo->permalink,
                    'price' => $producto_woo->price ? $producto_woo->price : 0,
                    'purchasable' => $producto_woo->purchasable,
                    'regular_price' => $producto_woo->regular_price ? $producto_woo->regular_price : 0,
                    'sale_price' => $producto_woo->sale_price ? $producto_woo->sale_price : 0,
                    'sku' => $producto_woo->sku,
                    'status' => $producto_woo->status,
                    'stock_quantity' => $producto_woo->stock_quantity ? $producto_woo->stock_quantity : 0,
                    'talla' => isset($producto_woo->attributes[1]) ? $producto_woo->attributes[1]->option : '',
                    'type' => $producto_woo->type,
                    'actualizado' => true
                ]);
            }elseif (!$producto_repo) {
                $producto_repo->create([
                    'tienda_id' => 1,
                    'integracion_id' => 2,
                    'color' => isset($producto_woo->attributes[0]) ? $producto_woo->attributes[0]->option : '',
                    'description' => $producto_woo->description,
                    'image' => isset($producto_woo->images[0]) ? $producto_woo->images[0]->src : '',
                    'name' => $producto_woo->name,
                    'parent_id' => $producto_woo->parent_id,
                    'permalink' => $producto_woo->permalink,
                    'price' => $producto_woo->price ? $producto_woo->price : 0,
                    'purchasable' => $producto_woo->purchasable,
                    'regular_price' => $producto_woo->regular_price ? $producto_woo->regular_price : 0,
                    'sale_price' => $producto_woo->sale_price ? $producto_woo->sale_price : 0,
                    'sku' => $producto_woo->sku,
                    'status' => $producto_woo->status,
                    'stock_quantity' => $producto_woo->stock_quantity ? $producto_woo->stock_quantity : 0,
                    'talla' => isset($producto_woo->attributes[1]) ? $producto_woo->attributes[1]->option : '',
                    'type' => $producto_woo->type
                ]);
            }
        }
    }
}