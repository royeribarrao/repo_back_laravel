<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;
use App\Models\TrackerCambioEstandar;
use App\Models\TrackerCambioDeluxe;
use App\Models\ServicioLogistico;
use App\Models\Carrier;
use App\Mail\ConfirmacionOperadorLogisticoRecojo;
use App\Mail\ConfirmacionOperadorLogisticoEntrega;
use Carbon\Carbon;
use Mail;

class CambioController extends Controller
{
    public $secure_key = '425B4F4FF84046AE8BEF2F3F11EEB559';
    public $host_desarrollo = 'http://34.235.9.12:6969/Rachna';
    public $host_produccion = 'https://api.logixplatform.com';
    public $createWaybill = '/webservice/v2/CreateWaybill?secureKey=9CDFBA6281DB43F4B8EB8532AEAB4395';
    public $generateCarrierWaybill = '/webservice/v2/GenerateCarrierWaybill?secureKey=9CDFBA6281DB43F4B8EB8532AEAB4395';

    public function obtenerGestionByID(Request $request, $id){
        $gestion = Gestion::find($id);
        return $gestion;
    }

    public function cambiosTodosErrorPorTienda(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto', 
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->whereIn('tipo_servicio', [1,2])
                            ->where('imagen_evidencia', '!=', '')
                            ->where('estado', 1)
                            ->where('confirmacion_web', true)
                            ->where('en_proceso', false)
                            ->paginate(15);
        return $gestiones_error;
    }

    public function aceptarCambioError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'en_proceso' => 1
        ]);
        return $gestion;
    }

    public function denegarCambioError(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto', 
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->whereIn('tipo_servicio', [1,2])
                            ->where('imagen_evidencia', '!=', '')
                            ->paginate(15);
        return $gestiones_error;
    }

    public function allCambios(Request $request){
        $user = \Auth::user();
        $gestiones = Gestion::with([
                            'productoGestion.producto', 
                            'nuevosProductos.producto', 
                            'cliente', 
                            'tienda', 
                            'trackerCambioEstandar',
                            'servicioLogistico',
                            'delivery' => function ($q) use ($request){
                                if (isset($request->cliente) && $request->cliente !== '') {
                                    $q->where('nombres','like', '%'.$request->cliente.'%');
                                }
                            }
                        ])
                        ->where(function ($q) use ($request){
                            if (isset($request->codigo) && $request->codigo !== '') {
                                $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                            }
                        })
                        ->where(function ($q) use ($request){
                            if (isset($request->tienda_id) && $request->tienda_id != '') {
                                $q->where('tienda_id','=', $request->tienda_id);
                            }
                        })
                        ->where('tienda_id', '=', $user->tienda_id)
                        ->where('tipo_servicio', 1)
                        ->where('en_proceso', true)
                        ->where('confirmacion_web', true)
                        ->paginate(15);
        return $gestiones;
    }

    public function updateState($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $message)
    {
        $gestion = Gestion::with(
            'servicioLogistico.carrier', 
            'tienda',
            'delivery'
        )
        ->find($id);
        $tracker = TrackerCambioEstandar::where('gestion_id',$gestion->id)->first();
        $carrier = Carrier::where('codigo', $carrier_codigo)->first();
        $estado_gestion = $gestion->estado;

        
        
        if($estado_gestion == 1){
            $gestion->update([
                'estado' => 2
            ]);
            $servicio_logistico = ServicioLogistico::create([
                'gestion_id' => $id,
                'carrier_id' => $carrier->id,
                'waybillNumber' => $waybillNumber,
                'carrierWayBill' => $carrierWayBill,
                'tipo' => 1
            ]);
            $tracker->update([
                'operador_logistico_confirmado' => true,
                'nombre_estado' => 'Operador Logístico Confirmado',
                'estado' => 2
            ]);
            Mail::to($gestion->delivery->correo)->send(new ConfirmacionOperadorLogisticoRecojo($gestion, $carrier));
            return response()->json([
                'state'=> true,
                'message' => $message
            ]);
        }else if($estado_gestion == 6){
            $date_original = Carbon::now()->format('Y-m-d H:i:s');
            $date = substr($date_original, 11, 2);
            $date = (int)($date);
            if($date < 11){
                $nueva_fecha = Carbon::now()->addDay()->toDateString();
            }elseif($date >= 11){
                $nueva_fecha = Carbon::now()->addDays(2)->toDateString();
            }
            $gestion->update([
                'estado' => 7
            ]);
            $servicio_logistico = ServicioLogistico::create([
                'gestion_id' => $id,
                'carrier_id' => $carrier->id,
                'waybillNumber' => $waybillNumber,
                'carrierWayBill' => $carrierWayBill,
                'tipo' => 2
            ]);
            $tracker->update([
                'operador_logistico_confirmado_vuelta' => true,
                'nombre_estado' => 'Operador Logístico Confirmado',
                'estado' => 7,
            ]);
            Mail::to($gestion->delivery->correo)->send(new ConfirmacionOperadorLogisticoEntrega($gestion, $carrier, $nueva_fecha));
            return response()->json([
                'state'=> true,
                'message' => $message
            ]);
        }else if($estado_gestion != 1){
            return response()->json([
                'state'=> false,
                'message' => 'El operador logístico ya fue seleccionado.'
            ]);
        }
    }

    public function createWayBill(Request $request, $id)
    {
        $carrier = $request->all();
        $gestion = Gestion::find($id);
        $datos_delivery = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);
        $client   = new \GuzzleHttp\Client();
        $url = $this->host_produccion.$this->createWaybill;
        if($gestion->estado == 1){
            $body = $this->setJson(1, $gestion, $datos_delivery, $tienda, $carrier['codigo'], $carrier['fecha']);
        }elseif($gestion->estado == 6){
            $body = $this->setJson(6, $gestion, $tienda, $datos_delivery, $carrier['codigo'], $carrier['fecha']);
        }else{
            return response()->json([
                'state'=> false,
                'message' => 'Espere el estado correcto para elegir el operador logistico.'
            ]);
        }

        $create_waybill = $client->request('POST', $url, [
            'headers' => ['AccessKey' => '27E98AF8A43748A18D5668ADAB71ED40', 'Content-Type' => 'application/json'],
            'json' => $body
        ]);
        $response = $create_waybill->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);


        if(isset($array_response['waybillNumber'])){
            $waybillNumber = $array_response['waybillNumber'];
            $servicio_logistico = ServicioLogistico::create([
                'gestion_id' => $id,
                'waybillNumber' => $waybillNumber
            ]);

            return $create_waybill;
        }
        
        return response()->json([
            'state'=> false,
            'message' => $array_response['message']
        ]);
    }

    public function generateCarrierWayBill(Request $request, $id, $carrier_codigo){
        $client = new \GuzzleHttp\Client();
        $gestion = Gestion::find($id);
        $carrierProduct = $this->determinarCarrierProduct($carrier_codigo, $gestion);
        $url = $this->host_produccion.$this->generateCarrierWaybill;
        $generate_carrier = $client->request('POST', $url, 
        [
            'multipart' => [
                [
                    'name' => 'waybillNumber',
                    'contents' => $request->data['waybillNumber']
                ],
                [
                    'name' => 'carrierCode',
                    'contents' => $carrier_codigo
                ],
                [
                    'name' => 'carrierProduct',
                    'contents' => $carrierProduct
                ],
            ],
        ]);
        $response = $generate_carrier->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        $carrierWayBill = '';
        if(isset($array_response['carrierWaybill'])){
            $codigo1 = 'hola'.$array_response['carrierWaybill'];
            $carrierWayBill = substr($codigo1, 4);
        }

        if($carrierWayBill == ''){
            return response()->json([
                'state'=> false,
                'message' => $array_response['message']
            ]);
        }
        if($carrierWayBill){
            return $this->updateState($id, $carrier_codigo, $request->data['waybillNumber'], $carrierWayBill, $array_response['message']);
        }
    }

    public function setJson($tipo, $gestion, $cliente, $tienda, $carrier_codigo, $fecha)
    {
        if($carrier_codigo == 'OLVA'){
            $reference = $gestion->codigo_repo.'-ida';
        }else{
            $reference = $gestion->codigo_repo;
        }

        if($tipo == 1){
            $CustomerPhone = $cliente->celular;
            $CustomerAddress = $cliente->direccion;
            $CustomerName = $cliente->nombres.' '.$cliente->apellidos;
            $consignorGeoLocation = "$cliente->latitud,$cliente->longitud";

            $ConsigneeAddress = $tienda->address;
            $ConsigneeEmail = $tienda->email;
            $consigneeGeoLocation = "$tienda->latitud,$tienda->longitud";
            $ConsigneeName = $tienda->business_name;
            $ConsigneePhone = $tienda->phone;
        }elseif($tipo == 6){
            $CustomerPhone = $cliente->phone;
            $CustomerAddress = $cliente->address;
            $CustomerName = $cliente->business_name;
            $consignorGeoLocation = "$cliente->latitud,$cliente->longitud";

            $ConsigneeAddress = $tienda->direccion;
            $ConsigneeEmail = $tienda->correo;
            $consigneeGeoLocation = "$tienda->latitud,$tienda->longitud";
            $ConsigneeName = $tienda->nombres.' '.$tienda->apellidos;
            $ConsigneePhone = $tienda->celular;
        }
        
        $body = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => $CustomerPhone,
                "CustomerAddress" => utf8_encode($CustomerAddress),
                "CustomerName" => $CustomerName,
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => $consignorGeoLocation,
                "DeliveryDate" => "$fecha",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeAddress" => utf8_encode($ConsigneeAddress),
                "ConsigneeEmail" => $ConsigneeEmail,
                "consigneeGeoLocation" =>$consigneeGeoLocation,
                "ConsigneeName" => $ConsigneeName,
                "ConsigneePhone" => $ConsigneePhone,
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$reference",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                        "barCode" => "",
                        "packageCount" => 1,
                        "length" => 1.0,
                        "width" => 1.0,
                        "height" => 1.0,
                        "weight" => 1.0,
                        "itemCount" => 1,
                        "chargedWeight" => 1.0,
                        "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        return $body;
    }

    public function determinarCarrierProduct($carrier_codigo, $gestion)
    {
        $carrierProduct = "BOX";
        if($carrier_codigo == 'CARGUI'){
            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 3){
                $carrierProduct = 3;
            }else if($gestion->tipo_servicio == 2){
                $carrierProduct = 2;
            }
        }
        return $carrierProduct;
    }
}