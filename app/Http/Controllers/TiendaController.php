<?php

namespace App\Http\Controllers;

use App\Services\Contracts\StoreInterface as StoreService;
use Illuminate\Http\Request;
use App\Models\Tienda;
use App\Models\ContactoTienda;
use App\Models\TiendaDatosFee;

class TiendaController extends Controller
{
    protected $storeService;

    public function __construct(
        StoreService $store
    )
    {
        $this->storeService = $store;
    }

    public function get(Request $request) {
        // $req = $request->all();
        // $stores = $this->storeService->paginate(15, $req);
        $stores = Tienda::with('contacto')
                    ->where(function ($q) use ($request){
                        if (isset($request->name) && $request->name !== '') {
                            $q->where('business_name','like', '%'.$request->name.'%');
                        }
                    })
                    ->where(function ($q) use ($request){
                        if (isset($request->ruc) && $request->ruc !== '') {
                            $q->where('ruc','like', '%'.$request->ruc.'%');
                        }
                    })
                    ->paginate(15);
        return response()->json($stores);
    }

    public function getAll(Request $request) {
        $stores = $this->storeService->all();    
        return response()->json($stores);
    }

    public function allWeb() {
        $stores = Tienda::with('configuracion')
                    ->orderBy('orden', 'asc')
                    ->orderBy('aliada', 'desc')
                    ->orderBy('business_name', 'asc')
                    ->get();
        return response()->json($stores);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request)
    {
        $nuevaTienda = $this->storeService->create($request->all());
        $contactoTienda = ContactoTienda::create([
            'tienda_id' => $nuevaTienda->id,
            'nombre_contacto' => $request['nombre_contacto'],
            'apellido_contacto' => $request['apellido_contacto'],
            'telefono_contacto' => $request['telefono_contacto']
        ]);      
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda creada correctamente.'
        ]);
    }
    
    public function show($id) {
        $store = Tienda::with('tiendaFee')->find($id);
        return $store;
    }

    public function getInformacionTienda($id)
    {
        $tienda = Tienda::with(['opciones.opcion', 'configuracion'])->find($id);
        return $tienda;
    }

    public function update($id, Request $request) 
    {
        $info = $request->datosFee;
        

        $this->storeService->update($request->all(), $id);
        $contactoTienda = ContactoTienda::where('tienda_id', $id)->first();
        $datos_fee = TiendaDatosFee::where('tienda_id', $id)->get();
        
        if($datos_fee){
            foreach($request->datosFee as $key => $item){
                $valores = json_decode($item, true);
                
                $tienda_fee = TiendaDatosFee::where('tienda_id', $id)->where('codigo', $valores['codigo'])->first();
                $tienda_fee->update([
                    'tipo' => $valores['tipo'],
                    'monto' => $valores['monto']
                ]);
            }
        }else{
            foreach($request->datosFee as $key => $item){
                $valores = json_decode($item, true);
                TiendaDatosFee::create([
                    'tienda_id' => $id,
                    'codigo' => $valores['codigo'],
                    'nombre' => $valores['nombre'],
                    'tipo' => $valores['tipo'],
                    'monto' => $valores['monto']
                ]);
            }
        }
        if($contactoTienda){
            $contactoTienda->update([
                'nombre_contacto' => $request['nombre_contacto'],
                'apellido_contacto' => $request['apellido_contacto'],
                'telefono_contacto' => $request['telefono_contacto']
            ]);
        }else{
            ContactoTienda::create([
                'nombre_contacto' => $request['nombre_contacto'],
                'apellido_contacto' => $request['apellido_contacto'],
                'telefono_contacto' => $request['telefono_contacto']
            ]);
        }
        
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda actualizada correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->storeService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda actualizado correctamente.'
        ]);
    }

    public function obtenerTiendaByNombre(Request $request)
    {
        $tiendas = Tienda::where('business_name','like', '%'.$request->nombre.'%')->orderBy('business_name', 'asc')->get();
        if(!count($tiendas)){
            $tienda = Tienda::where('business_name', 'Otra tienda')->orderBy('business_name', 'asc')->get();
            return $tienda;
        }
        return $tiendas;
    }
}