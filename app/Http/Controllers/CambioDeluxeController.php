<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;
use App\Models\TrackerCambioDeluxe;
use App\Models\ServicioLogistico;
use App\Models\Carrier;
use App\Mail\ConfirmacionOperadorLogisticoRecojo;
use Mail;

class CambioDeluxeController extends Controller
{
    public function obtenerGestionByID(Request $request, $id){
        $gestion = Gestion::find($id);
        return $gestion;
    }

    public function cambiosTodosErrorPorTienda(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto',
                                'productoGestion.motivoDevolucion',
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->where('tipo_servicio', 2)
                            ->where('estado', 1)
                            ->where('confirmacion_tienda', false)
                            ->where('confirmacion_web', true)
                            ->paginate(15);
        return $gestiones_error;
    }

    public function aceptarCambioError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'en_proceso' => 1
        ]);
        return $gestion;
    }

    public function denegarCambioError(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto', 
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->where('tipo_servicio', 2)
                            ->where('imagen_evidencia', '!=', '')
                            ->paginate(15);
        return $gestiones_error;
    }

    public function allCambios(Request $request){
        $user = \Auth::user();
        $gestiones = Gestion::with([
                            'productoGestion.producto', 
                            'nuevosProductos.producto', 
                            'cliente', 
                            'tienda' => function ($q) use ($request){
                                if (isset($request->cliente) && $request->cliente !== '') {
                                    $q->where('nombres','like', '%'.$request->cliente.'%');
                                }
                            }, 
                            'trackerCambioDeluxe',
                            'servicioLogistico',
                            'delivery'
                        ])
                        ->where(function ($q) use ($request){
                            if (isset($request->codigo) && $request->codigo !== '') {
                                $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                            }
                        })
                        ->where(function ($q) use ($request){
                            if (isset($request->tienda_id) && $request->tienda_id != '') {
                                $q->where('tienda_id','=', $request->tienda_id);
                            }
                        })
                        ->where('tipo_servicio', 2)
                        ->where('en_proceso', true)
                        ->where('confirmacion_web', true)
                        ->paginate(15);
        return $gestiones;
    }

    public function updateState($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $message){
        $gestion = Gestion::with(
                        'servicioLogistico.carrier', 
                        'tienda',
                        'delivery'
                    )
                    ->find($id);
        $tracker = TrackerCambioDeluxe::where('gestion_id',$gestion->id)->first();
        $carrier = Carrier::where('codigo', $carrier_codigo)->first();
        $estado_gestion = $gestion->estado;
        $servicio_logistico = ServicioLogistico::create([
            'gestion_id' => $id,
            'carrier_id' => $carrier->id,
            'waybillNumber' => $waybillNumber,
            'carrierWayBill' => $carrierWayBill,
            'tipo' => 1
        ]);
        $tracker->update([
            'operador_logistico_confirmado' => true,
            'nombre_estado' => 'Operador Logístico Confirmado',
            'estado' => 2
        ]);
        
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionOperadorLogisticoRecojo($gestion, $carrier));
        return response()->json([
            'state'=> true,
            'message' => $message,
            'waybillNumber' => $waybillNumber
        ]);
    }

    public function updateStateEntrega($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $message){
        $gestion = Gestion::with(
            'servicioLogistico.carrier', 
            'tienda',
            'delivery'
        )
        ->find($id);
        $tracker = TrackerCambioDeluxe::where('gestion_id',$gestion->id)->first();
        $carrier = Carrier::where('codigo', $carrier_codigo)->first();
        $estado_gestion = $gestion->estado;
        $gestion->update([
            'estado' => 2
        ]);
        $servicio_logistico = ServicioLogistico::create([
            'gestion_id' => $id,
            'carrier_id' => $carrier->id,
            'waybillNumber' => $waybillNumber,
            'carrierWayBill' => $carrierWayBill,
            'tipo' => 2
        ]);
        return response()->json([
            'state'=> true,
            'message' => $message,
            'waybillNumber' => $waybillNumber
        ]);
    }

    public function createWayBill(Request $request, $id){
        $carrier = $request->all();
        $gestion = Gestion::find($id);
        $datos_delivery = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);
        $reference = $gestion->codigo_repo.'-idayvuelta_origin';
        $nombre_completo = $datos_delivery->nombres.' '.$datos_delivery->apellidos;
        $body = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => "$datos_delivery->celular",
                "CustomerAddress" => utf8_encode($datos_delivery->direccion),
                "CustomerName" => "$nombre_completo",
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => "$datos_delivery->latitud,$datos_delivery->longitud",
                "DeliveryDate" => "$gestion->fecha_recojo",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeAddress" => utf8_encode($tienda->address),
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeEmail" => "$tienda->email",
                "consigneeGeoLocation" =>"$tienda->latitud,$tienda->longitud",
                "ConsigneeName" => "$tienda->business_name",
                "ConsigneePhone" => "$tienda->phone",
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$reference",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                            "barCode" => "",
                            "packageCount" => 1,
                            "length" => 1.0,
                            "width" => 1.0,
                            "height" => 1.0,
                            "weight" => 1.0,
                            "itemCount" => 1,
                            "chargedWeight" => 1.0,
                            "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        $client   = new \GuzzleHttp\Client();
        //$url = 'http://34.235.9.12:6969/Rachna/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $url = 'https://api.logixplatform.com/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        
        $headers = array( 
            'AccessKey' => 'logixerp',
            'Content-Type' => 'application/json'
            );
        
        if($gestion->estado == 1){
            $create_waybill = $client->request('POST', $url, [
                'headers' => ['AccessKey' => 'logixerp', 'Content-Type' => 'application/json'],
                'json' => $body
            ]);
        }else{
            return response()->json([
                'state'=> false,
                'message' => 'Espere el estado correcto para elegir el operador logistico.'
            ]);
        }
        $response = $create_waybill->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        
        if(isset($array_response['waybillNumber'])){
            $waybillNumber = $array_response['waybillNumber'];
            return $this->generateCarrierWayBill($id, $carrier['codigo'], $waybillNumber);
        }
        
        return response()->json([
            'state'=> false,
            'message' => $array_response['message']
        ]);
    }

    public function createWayBill2(Request $request, $id){
        $carrier = $request->all();
        $gestion = Gestion::find($id);
        $datos_delivery = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);
        $reference = $gestion->codigo_repo.'-idayvuelta_destination';
        $nombre_completo = $datos_delivery->nombres.' '.$datos_delivery->apellidos;
        //4 pegues para
        $body2 = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => "$tienda->phone",
                "CustomerAddress" => utf8_encode($tienda->address),
                "CustomerName" => "$tienda->business_name",
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => "$tienda->latitud,$tienda->longitud",
                "DeliveryDate" => "$gestion->fecha_solicitud",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeAddress" => utf8_encode($datos_delivery->direccion),
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeEmail" => "$datos_delivery->correo",
                "consigneeGeoLocation" =>"$datos_delivery->latitud,$datos_delivery->longitud",
                "ConsigneeName" => "$nombre_completo",
                "ConsigneePhone" => "$datos_delivery->celular",
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$reference",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                            "barCode" => "",
                            "packageCount" => 1,
                            "length" => 1.0,
                            "width" => 1.0,
                            "height" => 1.0,
                            "weight" => 1.0,
                            "itemCount" => 1,
                            "chargedWeight" => 1.0,
                            "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        $client   = new \GuzzleHttp\Client();
        //$url = 'http://34.235.9.12:6969/Rachna/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $url = 'https://api.logixplatform.com/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $headers = array( 
            'AccessKey' => 'logixerp',
            'Content-Type' => 'application/json'
            );
        $create_waybill = $client->request('POST', $url, [
            'headers' => ['AccessKey' => 'logixerp', 'Content-Type' => 'application/json'],
            'json' => $body2
        ]);
        $response = $create_waybill->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);

        if(isset($array_response['waybillNumber'])){
            $waybillNumber = $array_response['waybillNumber'];
            return $this->generateCarrierWayBill2($id, $carrier['codigo'], $waybillNumber);
        }
        
        return response()->json([
            'state'=> false,
            'message' => $array_response['message']
        ]);
    }

    //public function generateCarrierWayBill(Request $request, $id, $carrier_codigo){
    public function generateCarrierWayBill($id, $carrier_codigo, $waybillNumber){
        
        $client = new \GuzzleHttp\Client();
        $gestion = Gestion::find($id);
        $carrierProduct = "BOX";
        if($carrier_codigo == 'CARGUI'){
            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 3){
                $carrierProduct = 3;
            }else if($gestion->tipo_servicio == 2){
                $carrierProduct = 2;
            }
        }
        //$url = 'http://34.235.9.12:6969/Rachna/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $url = 'https://api.logixplatform.com/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $generate_carrier = $client->request('POST', $url, 
        [
            'multipart' => [
                [
                    'name' => 'waybillNumber',
                    'contents' => $waybillNumber
                ],
                [
                    'name' => 'carrierCode',
                    'contents' => $carrier_codigo
                ],
                [
                    'name' => 'carrierProduct',
                    'contents' => $carrierProduct
                ],
            ],
        ]);

        $response = $generate_carrier->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        
        $carrierWayBill = '';
        $valini = 'hola';
        if(isset($array_response['carrierWaybill'])){
            $codigo1 = $valini.$array_response['carrierWaybill'];
            $carrierWayBill = substr($codigo1, 4);
        }
        
        if($carrierWayBill == ''){
            return response()->json([
                'state'=> false,
                'message' => $array_response['message']
            ]);
        }
        
        if($carrierWayBill){
            return $this->updateState($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $array_response['message']);
        }
    }

    public function generateCarrierWayBill2($id, $carrier_codigo, $waybillNumber){
        $client = new \GuzzleHttp\Client();
        $gestion = Gestion::find($id);
        $carrierProduct = "BOX";
        if($carrier_codigo == 'CARGUI'){
            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 3){
                $carrierProduct = 3;
            }else if($gestion->tipo_servicio == 2){
                $carrierProduct = 2;
            }
        }
        //$url = 'http://34.235.9.12:6969/Rachna/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $url = 'https://api.logixplatform.com/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $generate_carrier = $client->request('POST', $url, 
        [
            'multipart' => [
                [
                    'name' => 'waybillNumber',
                    'contents' => $waybillNumber
                ],
                [
                    'name' => 'carrierCode',
                    'contents' => $carrier_codigo
                ],
                [
                    'name' => 'carrierProduct',
                    'contents' => $carrierProduct
                ],
            ],
        ]);
        $response = $generate_carrier->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        
        $carrierWayBill = '';
        $valini = 'hola';
        if(isset($array_response['carrierWaybill'])){
            $codigo1 = $valini.$array_response['carrierWaybill'];
            $carrierWayBill = substr($codigo1, 4);
        }
        
        if($carrierWayBill == ''){
            return response()->json([
                'state'=> false,
                'message' => $array_response['message']
            ]);
        }

        if($carrierWayBill){
            return $this->updateStateEntrega($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $array_response['message']);
        }
    }

    public function getById($id){
        $gestion = Gestion::find($id);
        return $gestion;
    }
}