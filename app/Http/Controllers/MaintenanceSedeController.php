<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Contracts\MaintenanceSedeInterface as MaintenanceSedeService;

class MaintenanceSedeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $maintenanceSedeService;

    public function __construct(
        MaintenanceSedeService $maintenenanceSedeService
    )
    {
        $this->maintenanceSedeService = $maintenenanceSedeService;
    }

    public function get(Request $request) {
        $sedes = $this->maintenanceSedeService->paginate();    
        return response()->json($sedes);
    }

    public function getAll(Request $request) {
        $sedes = $this->maintenanceSedeService->all();    
        return response()->json($sedes);
    }

    public function store(Request $request) {

        $this->maintenanceSedeService->create($request->all());          
        return response()->json([
            'state'=> 1,
            'message' => 'Sede creada correctamente.'
        ]);
    }

    public function show($id) {
        $user = $this->maintenanceSedeService->find($id);
        return response()->json($user);
    }

    public function update($id, Request $request) {
        $this->maintenanceSedeService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Sede actualizada correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->maintenanceSedeService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Servicio actualizado correctamente.'
        ]);
    }

}
