<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Distrito;

class DistritoController extends Controller
{
    public function show($id)
    {
        $distrito = Distrito::find($id);
        return $distrito;
    }

    public function todos()
    {
        $distritos = Distrito::where('activo', true)->orderBy('nombre')->get();
        return $distritos;
    }

    public function todosPaginate()
    {
        $distritos = Distrito::paginate(15);
        return $distritos;
    }

    public function cambiarEstado(Request $request)
    {
        $distrito = Distrito::find($request->id);
        $distrito->update([
            'activo' => !$distrito->activo
        ]);
        return response()->json([
            'state'=> true,
            'message' => "Distrito actualizado"
        ]);
    }
}