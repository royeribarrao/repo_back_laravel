<?php

namespace App\Http\Controllers\Repo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;
use App\Models\TrackerDevolucion;
use App\Models\ServicioLogistico;
use App\Models\Carrier;
use App\Models\TiendaDatosFee;
use App\Mail\ConfirmacionOperadorLogisticoRecojo;
use Mail;
use Codedge\Fpdf\Fpdf\Fpdf;

class RepoFacturacionController extends Controller
{
    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function getData(Request $request){
        $parametros = $request->all();
        $data = Gestion::with(['productoGestion', 'tienda', 'servicio', 'nuevosProductos'])
                ->where(function ($q) use ($parametros){
                    if (isset($parametros['tienda']) && $parametros['tienda'] !== '') {
                    $q->where('tienda_id', $parametros['tienda']);
                    }
                })
                ->where(function ($q) use ($parametros){
                    if (isset($parametros['fecha']) && $parametros['fecha'] !== '') {
                        if(isset($parametros['fecha'][0]) && isset($parametros['fecha'][1])){
                            $q->whereBetween('created_at', [$parametros['fecha'][0], $parametros['fecha'][1]]);
                        }
                    }
                })
                ->where('confirmacion_web', true)
                ->where('confirmacion_tienda', true)
                ->where('finalizado', true)
                ->paginate(15);
        return $data;
    }

    public function getFacturacionByTienda(Request $request, $tienda_id, $fecha_inicial, $fecha_final)
    {
        $tienda = Tienda::find($tienda_id);
        $tienda_fee_devolucion = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 1)->first();
        $tienda_fee_cambio = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 2)->first();
        $tienda_fee_upselling = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 3)->first();
        $gestiones = Gestion::with(['productoGestion', 'tienda', 'servicio', 'nuevosProductos'])
            ->whereBetween('created_at', [$fecha_inicial, $fecha_final])
            ->where('confirmacion_web', true)
            ->where('confirmacion_tienda', true)
            ->where('es_facturado', true)
            ->where('finalizado', true)
            ->where('tienda_id', $tienda_id)
            ->get();
        $total_fee_logistico = 0;
        $total_upselling = 0;
        $total_fee_repo = 0;
        $total_cobro = 0;
        foreach($gestiones as $key => $gestion){
            $monto_base = 0;
            foreach($gestion->productoGestion as $key => $item){
                $monto_base += $item->precio;
            }
            $monto_nuevos_productos = 0;
            foreach($gestion->nuevosProductos as $item){
                $monto_nuevos_productos += $item->precio;
            }

            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2 || $gestion->tipo_servicio == 4){
                if($tienda_fee_cambio->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_cambio->monto/100;
                }elseif($tienda_fee_cambio->tipo == 2){
                    $fee_repo = $tienda_fee_cambio->monto;
                }
            }elseif($gestion->tipo_servicio == 3){
                if($tienda_fee_devolucion->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_devolucion->monto/100;
                }elseif($tienda_fee_devolucion->tipo == 2){
                    $fee_repo = $tienda_fee_devolucion->monto;
                }
            }

            $fee_logistico = 0;
            $upselling = $monto_nuevos_productos-$monto_base;
            if(!$gestion->cliente_paga_logistico){
                if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 4){
                    $fee_logistico = 15;
                }
                if($gestion->tipo_servicio == 2){
                    $fee_logistico = 20;
                }
                if($gestion->tipo_servicio == 3){
                    $fee_logistico = 10;
                }
            }

            if($tienda_fee_upselling->tipo == 1){
                $fee_upselling = $monto_base*$tienda_fee_upselling->monto/100;
            }elseif($tienda_fee_upselling->tipo == 2){
                $fee_upselling = $tienda_fee_upselling->monto;
            }

            if($gestion->tipo_servicio == 3 || $gestion->tipo_servicio == 4){
                $fee_upselling = 0;
            }
            
            $total_fee_logistico += $fee_logistico;
            $total_upselling += $fee_upselling;
            $total_fee_repo += $fee_repo;
        }

        return response()->json([
            'fee_logistico'=> $total_fee_logistico+($total_fee_logistico*18/100),
            'fee_upselling' => $total_upselling,
            'fee_repo' => $total_fee_repo,
            'total_cobro' => $total_cobro,
            'tienda_fee_devolucion' => $tienda_fee_devolucion,
            'tienda_fee_cambio' => $tienda_fee_cambio,
            'tienda_fee_upselling' => $tienda_fee_upselling
        ]);
    }

    public function eliminarDeFacturacion($id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'es_facturado' => false
        ]);

        return response()->json([
            'state'=> true,
            'message' => 'Proceso retirado de facturación exitosamente.'
        ]);
    }

    public function agregarAFacturacion($id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'es_facturado' => true
        ]);

        return response()->json([
            'state'=> true,
            'message' => 'Proceso agregar a facturación exitosamente.'
        ]);
    }

    public function createPdf(Request $request, $tienda_id, $fecha_inicial, $fecha_final)
    {
        $tienda = Tienda::find($tienda_id);
        $tienda_fee_devolucion = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 1)->first();
        $text_fee_devolucion = $tienda_fee_devolucion->tipo == 1 ? $tienda_fee_devolucion->monto.'%' : $tienda_fee_devolucion->monto;
        
        $tienda_fee_cambio = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 2)->first();
        $text_fee_cambio = $tienda_fee_cambio->tipo == 1 ? $tienda_fee_cambio->monto.'%' : $tienda_fee_cambio->monto;

        $tienda_fee_upselling = TiendaDatosFee::where('tienda_id', $tienda_id)->where('codigo', 3)->first();
        $text_fee_upselling = $tienda_fee_upselling->tipo == 1 ? $tienda_fee_upselling->monto.'%' : $tienda_fee_upselling->monto;

        $gestiones = Gestion::with(['productoGestion', 'tienda', 'servicio', 'nuevosProductos'])
            ->whereBetween('created_at', [$fecha_inicial, $fecha_final])
            ->where('confirmacion_web', true)
            ->where('confirmacion_tienda', true)
            ->where('es_facturado', true)
            ->where('finalizado', true)
            ->where('tienda_id', $tienda_id)
            ->get();
        //return $gestiones[0]->productoGestion;   
        $pdf = new FPDF('P','mm', 'A4');
        $pdf->SetMargins(10, 10, 10);
        $pdf->AddPage('L');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(150,10, utf8_decode('Facturación: ').$tienda->business_name, 1, 0, 'L', 0);
        $pdf->Cell(0,10, utf8_decode('Periodo: ').$fecha_inicial.' al '.$fecha_final, 1, 1, 'R', 0);
        $pdf->SetFont('Arial','B',12);
        $pdf->Ln(3);
        $total_fee_logistico = 0;
        $total_upselling = 0;
        $total_fee_repo = 0;
        $total_cobro = 0;
        foreach($gestiones as $key => $gestion){
            $monto_base = 0;
            foreach($gestion->productoGestion as $key => $item){
                $monto_base += $item->precio;
            }

            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2 || $gestion->tipo_servicio == 4){
                if($tienda_fee_cambio->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_cambio->monto/100;
                }elseif($tienda_fee_cambio->tipo == 2){
                    $fee_repo = $tienda_fee_cambio->monto;
                }
            }elseif($gestion->tipo_servicio == 3){
                if($tienda_fee_devolucion->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_devolucion->monto/100;
                }elseif($tienda_fee_devolucion->tipo == 2){
                    $fee_repo = $tienda_fee_devolucion->monto;
                }
            }

            $monto_nuevos_productos = 0;
            foreach($gestion->nuevosProductos as $item){
                $monto_nuevos_productos += $item->precio;
            }

            $fee_logistico = 0;
            
            if(!$gestion->cliente_paga_logistico){
                if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 4){
                    $fee_logistico = 15;
                }
                if($gestion->tipo_servicio == 2){
                    $fee_logistico = 20;
                }
                if($gestion->tipo_servicio == 3){
                    $fee_logistico = 10;
                }
            }

            $fee_upselling = $monto_nuevos_productos-$monto_base;
            if($tienda_fee_upselling->tipo == 1){
                $fee_upselling = $monto_base*$tienda_fee_upselling->monto/100;
            }elseif($tienda_fee_upselling->tipo == 2){
                $fee_upselling = $tienda_fee_upselling->monto;
            }

            if($gestion->tipo_servicio == 3 || $gestion->tipo_servicio == 4){
                $fee_upselling = 0;
            }
            
            $total_fee_logistico += $fee_logistico+$fee_logistico*18/100;
            $total_upselling += $fee_upselling;
            $total_fee_repo += $fee_repo;
        }
        $pdf->Cell(187,8, utf8_decode('Total fee logístico: '), 1, 0, 'L', 0);
        $pdf->Cell(90,8, utf8_decode('S/ '.$total_fee_logistico), 1, 1, 'R', 0);
        $pdf->Ln(3);
        $pdf->Cell(187,8, utf8_decode('Total upselling: '), 1, 0, 'L', 0);
        $pdf->Cell(90,8, utf8_decode('S/ '.$total_upselling), 1, 1, 'R', 0);
        $pdf->Ln(3);
        $pdf->Cell(187,8, utf8_decode('Total fee Repo('.$text_fee_devolucion.' D/ '.$text_fee_cambio.' C/ '.$text_fee_upselling.' U): '), 1, 0, 'L', 0);
        $pdf->Cell(90,8, utf8_decode('S/ '.$total_fee_repo), 1, 1, 'R', 0);
        $pdf->Ln(3);
        $pdf->Cell(187,8, utf8_decode('Total de cobro: '), 1, 0, 'L', 0);
        $pdf->Cell(90,8, utf8_decode('S/ '.($total_fee_logistico+$total_upselling+$total_fee_repo)), 1, 1, 'R', 0);
        $pdf->Ln(5);
        $pdf->Cell(20,10, utf8_decode('Código'), 1, 0, 'C', 0);
        $pdf->Cell(97,10, utf8_decode('Producto a devolver'), 1, 0, 'C', 0);
        $pdf->Cell(30,10, utf8_decode('Monto base'), 1, 0, 'C', 0);
        $pdf->Cell(20,10, utf8_decode('Proceso'), 1, 0, 'C', 0);
        $pdf->Cell(25,10, utf8_decode('Fee Repo'), 1, 0, 'C', 0);
        $pdf->Cell(30,10, utf8_decode('Fee logístico'), 1, 0, 'C', 0);
        $pdf->Cell(25,10, utf8_decode('Upselling'), 1, 0, 'C', 0);
        $pdf->Cell(30,10, utf8_decode('Fee Upselling'), 1, 1, 'C', 0);
        $pdf->SetFont('Arial','B',10);
        foreach($gestiones as $key => $gestion){
            $cantidad_elementos = count($gestion->productoGestion);
            $altura = $cantidad_elementos*$cantidad_elementos;
            $nombre_productos = null;
            $monto_base = 0;
            foreach($gestion->productoGestion as $key => $item){
                $monto_base += $item->precio;
                //$pdf->Cell(97,10, utf8_decode('Polo Pique Polo Ralph Laurenssssss'), 1, 0, 'L', 0);
            }
            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2 || $gestion->tipo_servicio == 4){
                if($tienda_fee_cambio->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_cambio->monto/100;
                }elseif($tienda_fee_cambio->tipo == 2){
                    $fee_repo = $tienda_fee_cambio->monto;
                }
            }elseif($gestion->tipo_servicio == 3){
                if($tienda_fee_devolucion->tipo == 1){
                    $fee_repo = $monto_base*$tienda_fee_devolucion->monto/100;
                }elseif($tienda_fee_devolucion->tipo == 2){
                    $fee_repo = $tienda_fee_devolucion->monto;
                }
            }

            $monto_nuevos_productos = 0;
            foreach($gestion->nuevosProductos as $item){
                $monto_nuevos_productos += $item->precio;
            }
            $fee_logistico = 0;
            if(!$gestion->cliente_paga_logistico){
                if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 4){
                    $fee_logistico = 15 + 15*18/100;
                }
                if($gestion->tipo_servicio == 2){
                    $fee_logistico = 20 + 20*18/100;
                }
                if($gestion->tipo_servicio == 3){
                    $fee_logistico = 10 + 10*18/100;
                }
            }
            
            if($tienda_fee_upselling->tipo == 1){
                $upselling = "S/ ".($monto_nuevos_productos-$monto_base);
                $fee_upselling = "S/ ".$monto_base*$tienda_fee_upselling->monto/100;
            }elseif($tienda_fee_upselling->tipo == 2){
                $upselling = "S/ ".($monto_nuevos_productos-$monto_base);
                $fee_upselling = $tienda_fee_upselling->monto;
            }

            if($gestion->tipo_servicio == 3 || $gestion->tipo_servicio == 4){
                $upselling = "N.A";
                $fee_upselling = "N.A";
            }

            // $upselling = "S/ ".($monto_nuevos_productos-$monto_base);
            // $fee_upselling = "S/ ".(($monto_nuevos_productos-$monto_base)*5/100);
            // if($gestion->tipo_servicio == 3){
            //     $upselling = "N.A";
            //     $fee_upselling = "N.A";
            // }

            $total_fee_logistico += $fee_logistico;
            $total_upselling += $monto_nuevos_productos-$monto_base;
            $total_fee_repo += $fee_repo;

            $pdf->Cell(20,10, utf8_decode($gestion->codigo_repo), 1, 0, 'L', 0);
            $pdf->Cell(97,10, utf8_decode($gestion->productoGestion[0]->nombre_producto), 1, 0, 'L', 0);
            $pdf->Cell(30,10, utf8_decode('S/ '.$monto_base), 1, 0, 'C', 0);
            $pdf->Cell(20,10, utf8_decode($gestion->servicio->nombre), 1, 0, 'C', 0);
            $pdf->Cell(25,10, utf8_decode('S/ '.$fee_repo), 1, 0, 'C', 0);
            $pdf->Cell(30,10, utf8_decode('S/ '.$fee_logistico), 1, 0, 'C', 0);
            $pdf->Cell(25,10, utf8_decode($upselling), 1, 0, 'C', 0);
            $pdf->Cell(30,10, utf8_decode($fee_upselling), 1, 1, 'C', 0);
        }
        $pdf->Output();
        exit;
    }
}