<?php

namespace App\Http\Controllers\Repo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\ProductoGestion;
use App\Models\ServicioLogistico;

class EstadisticaController extends Controller
{
    public function getAuthenticated() 
    {
        $usuario = \Auth::user();
        $usuario->tienda = \Auth::user()->tienda;
        return $usuario;
    }

    public function porcentajeByTipoProcesos()
    {
        $user = \Auth::user();
        $p_estandares = Gestion::where('tipo_servicio', 1)
                    ->where('tienda_id', $user->tienda_id)
                    ->count();
        $p_deluxes = Gestion::where('tipo_servicio', 2)
                    ->where('tienda_id', $user->tienda_id)
                    ->count();
        $p_devoluciones = Gestion::where('tipo_servicio', 3)
                    ->where('tienda_id', $user->tienda_id)
                    ->count();
        $total_gestiones = Gestion::all()->count();
        $porcentaje_estandar = $p_estandares/$total_gestiones;
        $porcentaje_deluxe = $p_deluxes/$total_gestiones;
        $porcentaje_devolucion = $p_devoluciones/$total_gestiones;

        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["Estándar ($p_estandares)", $p_estandares],
                ["Express ($p_deluxes)", $p_deluxes],
                ["Devolución ($p_devoluciones)", $p_devoluciones]
            ]
        ]);
    }

    public function totalGestiones(){
        $user = \Auth::user();
        $total_gestiones = Gestion::where('tienda_id', $user->tienda_id)->get()->count();
        $total_cambios = Gestion::where('tienda_id', $user->tienda_id)->whereIn('tipo_servicio', [1, 2])->get()->count();
        $total_devoluciones = Gestion::where('tienda_id', $user->tienda_id)->where('tipo_servicio', 3)->get()->count();
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'gestiones' => $total_gestiones,
            'cambios' => $total_cambios,
            'devoluciones' => $total_devoluciones
        ]);
    }

    public function porcentajeByEstadoProcesos(){
        $user = \Auth::user();
        $gestiones_proceso = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('en_proceso', 1)
                            ->where('confirmacion_web', 1)
                            ->get()
                            ->count();
        $gestiones_aceptado = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->get()
                            ->count();
        $gestiones_rechazado = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('estado', 20)
                            ->get()
                            ->count();
        $total_gestiones = Gestion::where('tienda_id', $user->tienda_id)->get()->count();
        $porcentaje_gestiones_proceso = $gestiones_proceso/$total_gestiones;
        $porcentaje_gestiones_finalizado = $gestiones_aceptado/$total_gestiones;
        $porcentaje_gestiones_cancelado = $gestiones_rechazado/$total_gestiones;
        
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["Proceso ($gestiones_proceso)", $gestiones_proceso],
                ["Aceptado ($gestiones_aceptado)", $gestiones_aceptado],
                ["Rechazado ($gestiones_rechazado)", $gestiones_rechazado]
            ]
        ]);
    }

    public function porcentajeAceptacion(){
        $user = \Auth::user();
        $total_cambios = Gestion::where('tienda_id', $user->tienda_id)
                            ->whereIn('tipo_servicio', [1, 2])
                            ->get()
                            ->count();
        $total_cambios_aceptados = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->whereIn('tipo_servicio', [1, 2])
                            ->get()
                            ->count();
        $total_devoluciones = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('tipo_servicio', 3)
                            ->get()
                            ->count();
        $total_devoluciones_aceptadas = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->where('tipo_servicio', 3)
                            ->get()
                            ->count();
        $porcentaje_devoluciones_aceptadas = $total_devoluciones_aceptadas/$total_devoluciones;
        $porcentaje_cambios_aceptados = $total_cambios_aceptados/$total_cambios;

        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'cambios_aceptados' => $porcentaje_cambios_aceptados,
            'devoluciones_aceptadas' => $porcentaje_devoluciones_aceptadas
        ]);
    }

    public function gestionesByMotivo(){
        $user = \Auth::user();
        // $motivo_1 = Gestion::with(['productoGestion' => function ($q) {
        //     $q->where('motivo', 1);
        // }])
        //     ->where('tienda_id', $user->tienda_id)
        //     ->get()
        //     ->count();
        $motivo_1 = ProductoGestion::where('motivo', 1)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->get()
                ->count();
        $motivo_2 = ProductoGestion::where('motivo', 2)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->get()
                ->count();
        $motivo_3 = ProductoGestion::where('motivo', 3)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->get()
                ->count();
        $motivo_4 = ProductoGestion::where('motivo', 4)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->get()
                ->count();
        $motivo_5 = ProductoGestion::where('motivo', 5)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->get()
                ->count();
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["No me quedó bien ($motivo_1)", $motivo_1],
                ["No era lo que esperaba ($motivo_2)", $motivo_2],
                ["Pedí otro producto por error ($motivo_3)", $motivo_3],
                ["Me entregaron otro producto ($motivo_4)", $motivo_4],
                ["Producto dañado ($motivo_5)", $motivo_5]
            ]
        ]);
    }

    public function montoTotalByTipoProceso(){
        $user = \Auth::user();
        $monto_total_cambios = ProductoGestion::where('gestiones.tienda_id', $user->tienda_id)
            ->whereIn('gestiones.tipo_servicio', [1, 2])
            ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
            ->sum('productos_gestion.precio');
        // $monto_total_devoluciones = Gestion::join('productos_gestion', 'gestion.id', 'productos_gestion.gestion_id')
        //     ->where('tienda_id', $user->tienda_id)
        //     ->where('tipo_servicio', 3)
        //     ->sum('precio');
        $monto_total_devoluciones = ProductoGestion::where('gestiones.tienda_id', $user->tienda_id)
            ->where('tipo_servicio', 3)
            ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
            ->sum('productos_gestion.precio');
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'total_cambios' => $monto_total_cambios,
            'total_devoluciones' => $monto_total_devoluciones
        ]);
    }

    public function totalUsuariosRegistrados(){
        $user = \Auth::user();
        $usuarios = User::where('rol_id', 2)->get()->count();
        
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'usuarios' => $usuarios
        ]);
    }

    public function splitProveedoresLogisticos(){
        $operadores = ServicioLogistico::join('carriers', 'servicio_logistico.carrier_id', 'carriers.id')
            ->groupBy('carrier_id')
            ->get();
        
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'operadores' => $operadores
        ]);
    }
}