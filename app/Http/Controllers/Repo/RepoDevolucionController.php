<?php

namespace App\Http\Controllers\Repo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;
use App\Models\TrackerDevolucion;
use App\Models\ServicioLogistico;
use App\Models\Carrier;
use App\Mail\ConfirmacionOperadorLogisticoRecojo;
use Mail;

class RepoDevolucionController extends Controller
{
    public $secure_key = '425B4F4FF84046AE8BEF2F3F11EEB559';
    public $host_desarrollo = 'http://34.235.9.12:6969/Rachna';
    public $host_produccion = 'https://api.logixplatform.com';
    public $createWaybill = '/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
    public $generateCarrierWaybill = '/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
    
    public function allDevoluciones(Request $request){
        $gestiones = Gestion::with([
                                'productoGestion.producto',
                                'productoGestion.motivoDevolucion', 
                                'cliente', 
                                'tienda',
                                'trackerDevolucion',
                                'servicioLogistico',
                                'delivery'
                            ])
                            ->where(function ($q) use ($request){
                                if (isset($request->codigo) && $request->codigo != '') {
                                    $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                                }
                            })
                            ->where(function ($q) use ($request){
                                if (isset($request->tienda_id) && $request->tienda_id != '') {
                                    $q->where('tienda_id','=', $request->tienda_id);
                                }
                            })
                            ->where('tipo_servicio', 3)
                            ->where('en_proceso', true)
                            ->where('confirmacion_web', true)
                            ->where('confirmacion_tienda', true)
                            ->orderBy('created_at', 'desc')
                            ->paginate(10);
        return $gestiones;
    }

    public function updateState($id, $carrier_codigo, $waybillNumber, $carrierWayBill, $message)
    {
        $gestion = Gestion::with(
                        'servicioLogistico.carrier', 
                        'tienda',
                        'delivery'
                    )
                    ->find($id);
        $tracker = TrackerDevolucion::where('gestion_id',$gestion->id)->first();
        $carrier = Carrier::where('codigo', $carrier_codigo)->first();
        $estado_gestion = $gestion->estado;
    
        $servicioActual = ServicioLogistico::where('waybillNumber',$waybillNumber)->first();

        if($estado_gestion == 1){
            $gestion->update([
                'estado' => 2
            ]);
            $servicio_logistico = ServicioLogistico::create([
                'gestion_id' => $id,
                'carrier_id' => $carrier->id,
                'waybillNumber' => $waybillNumber,
                'carrierWayBill' => $carrierWayBill
            ]);
            $tracker->update([
                'operador_logistico_confirmado' => true,
                'nombre_estado' => 'Operador Logístico Confirmado',
                'estado' => 2
            ]);
            Mail::to($gestion->delivery->correo)->send(new ConfirmacionOperadorLogisticoRecojo($gestion, $carrier));
            return response()->json([
                'state'=> true,
                'message' => $message
            ]);
        }else if($estado_gestion != 1){
            return response()->json([
                'state'=> false,
                'message' => 'El operador logístico ya fue seleccionado.'
            ]);
        }
    }

    public function createWayBill(Request $request, $id)
    {
        $client   = new \GuzzleHttp\Client();
        $gestion = Gestion::find($id);
        $datos_delivery = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);
        $body = $this->setJson($gestion, $datos_delivery, $tienda);
        $url = $this->host_produccion.$this->createWaybill;
        $create_waybill = $client->request('POST', $url, [
            'headers' => ['AccessKey' => 'logixerp', 'Content-Type' => 'application/json'],
            'json' => $body
        ]);

        $response = $create_waybill->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);

        if(isset($array_response['waybillNumber'])){
            return $create_waybill;
        }
        
        return response()->json([
            'state'=> false,
            'message' => $array_response['message']
        ]);
    }

    public function generateCarrierWayBill(Request $request, $id, $carrier_codigo){
        $client = new \GuzzleHttp\Client();
        $carrier_cod = $carrier_codigo;
        $carrierProduct = $this->determinarCarrierProduct($carrier_codigo);
        $url = $this->host_produccion.$this->generateCarrierWaybill;
        $generate_carrier = $client->request('POST', $url, 
        [
            'multipart' => [
                [
                    'name' => 'waybillNumber',
                    'contents' => $request->data['waybillNumber']
                ],
                [
                    'name' => 'carrierCode',
                    'contents' => $carrier_cod
                ],
                [
                    'name' => 'carrierProduct',
                    'contents' => $carrierProduct
                ],
            ],
        ]);

        $response = $generate_carrier->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        
        $carrierWayBill = '';
        if(isset($array_response['carrierWaybill'])){
            $codigo1 = 'hola'.$array_response['carrierWaybill'];
            $carrierWayBill = substr($codigo1, 4);
        }
        
        if($carrierWayBill == ''){
            return response()->json([
                'state'=> false,
                'message' => $array_response['message']
            ]);
        }

        return $this->updateState($id, $carrier_codigo, $request->data['waybillNumber'], $carrierWayBill, $array_response['message']);
    }

    public function setJson($gestion, $cliente, $tienda)
    {
        $nombre_completo = $cliente->nombres.' '.$cliente->apellidos;
        $direccion_completa = $cliente->direccion.' '.$cliente->referencia;
        $body = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => "$cliente->celular",
                "CustomerAddress" => "$direccion_completa",
                "CustomerName" => "$nombre_completo",
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => "$cliente->latitud,$cliente->longitud",
                "DeliveryDate" => "$gestion->fecha_recojo",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeAddress" => "$tienda->address",
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeEmail" => "$tienda->email",
                "consigneeGeoLocation" =>"$tienda->latitud,$tienda->longitud",
                "ConsigneeName" => "$tienda->business_name",
                "ConsigneePhone" => "$tienda->phone",
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$gestion->codigo_repo",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                        "barCode" => "",
                        "packageCount" => 1,
                        "length" => 1.0,
                        "width" => 1.0,
                        "height" => 1.0,
                        "weight" => 1.0,
                        "itemCount" => 1,
                        "chargedWeight" => 1.0,
                        "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        return $body;
    }

    public function determinarCarrierProduct($carrier_codigo)
    {
        $carrierProduct = "BOX";
        if($carrier_codigo == 'CARGUI'){
            $carrierProduct = 3;
        }
        return $carrierProduct;
    }
}