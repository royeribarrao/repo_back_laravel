<?php

namespace App\Http\Controllers\Repo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gestion;

class RepoProcesosFinalizadosController extends Controller
{
    public function getAll(Request $request){
        $gestiones = Gestion::with([
                        'productoGestion.producto',
                        'nuevosProductos.producto', 
                        'cliente', 
                        'tienda',
                        'delivery'
                    ])
                    ->where(function ($q) use ($request){
                        if (isset($request->codigo) && $request->codigo !== '') {
                            $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                        }
                    })
                    ->where(function ($q) use ($request){
                        if (isset($request->tienda_id) && $request->tienda_id != '') {
                            $q->where('tienda_id','=', $request->tienda_id);
                        }
                    })
                    ->where('finalizado', true)
                    ->paginate(15);
        return $gestiones;
    }
}