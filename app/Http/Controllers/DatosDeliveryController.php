<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\ProductoGestion;
use App\Models\NuevoProductoServicioCambio;

class DatosDeliveryController extends Controller
{
    public function guardarDatosDelivery(Request $request, $codigo_repo)
    {
        $gestion = Gestion::where('codigo_repo' , $codigo_repo)->first();
        $data = $request->all();
        $datos_delivery = DatosDelivery::create([
            'nombres' => $data['nombre_cliente'],
            'apellidos' => $data['apellido_cliente'],
            'departamento' => $data['departamento'],
            'provincia' => $data['provincia'],
            'distrito' => $data['distrito'],
            'direccion' => $data['direccion'],
            'referencia' => $data['referencia'],
            'celular' => $data['telefono'],
            'correo' => $data['email'],
            'fecha_recojo' => $data['fecha_recojo'],
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud']
        ]);
        $gestion->update([
            'datos_delivery_id' => $datos_delivery->id,
            'fecha_recojo' => $data['fecha_recojo']
        ]);

        // $producto_gestion = ProductoGestion::where('gestion_id', $gestion->id)->sum('precio');
        // $producto_gestion1 = ProductoGestion::where('gestion_id', $gestion->id)->whereIn('motivo', [4,5])->get()->count();
        // if($producto_gestion1 > 0){
        //     $cliente_paga_logistico = false;
        // }elseif($producto_gestion1 == 0){
        //     $cliente_paga_logistico = true;
        // }
        $ir_payu = false;
        // if($gestion->tipo_servicio == 3 && $cliente_paga_logistico == true){
        //     $ir_payu = true;
        // }
        // if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2){
        //     $nuevos_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
        //         ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
        //         ->sum('products.precio_final');
        //         if($producto_gestion == $nuevos_productos){
        //             $recargo_nuevos_productos = false;
        //         }
        //         if($nuevos_productos > $producto_gestion){
        //             $recargo_nuevos_productos = true;
        //         }

        //     if($cliente_paga_logistico && $recargo_nuevos_productos){
        //         $ir_payu = true;
        //     }elseif($cliente_paga_logistico && !$recargo_nuevos_productos){
        //         $ir_payu = true;
        //     }elseif(!$cliente_paga_logistico && $recargo_nuevos_productos){
        //         $ir_payu = true;
        //     }elseif(!$cliente_paga_logistico && !$recargo_nuevos_productos){
        //         $ir_payu = false;
        //     }
        // }

        if($gestion->cliente_paga_logistico || $gestion->cobro_neto > 0){
            $ir_payu = true;
        }
        
        return response()->json([
            'state' => true,
            'gestion' => $gestion,
            // 'producto_gestion' => isset($producto_gestion) ? $producto_gestion : '',
            // 'producto_gestion1' => $producto_gestion1,
            //'nuevos_productos' => isset($nuevos_productos) ? $nuevos_productos : '',
            //'pago_logistico'=> $cliente_paga_logistico,
            'payU' => $ir_payu,
            'message' => 'Datos delivery creado correctamente.'
        ]);
    }
}