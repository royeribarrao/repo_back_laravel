<?php

namespace App\Http\Controllers;

use App\Services\Contracts\ProductInterface as ProductService;
use Illuminate\Http\Request;
use App\Models\Producto;
use DB;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(
        ProductService $product
    )
    {
        $this->productService = $product;
    }

    public function get(Request $request) {
        $req = $request->all();
        $products = $this->productService->paginate(15, $req);    
        return response()->json($products);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {
        $data = json_decode($request->values, true);
        $user = \Auth::user();
        Producto::create([
            'tienda_id' => $user->tienda_id,
            'marca' => $data['marca'],
            'precio_final' => $data['precio_final'],
            'sku_description' => $data['sku_description'],
            'sku_producto' => $data['sku_producto'],
            'stock_quantity' => $data['stock'],
            'talla' => $data['talla'],
            'imagen' => $data['imagen']
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto creado correctamente.'
        ]);
    }
    
    public function show($id) {
        $product = $this->productService->find($id);
        return response()->json($product);
    }

    public function update($id, Request $request) {
        $data = json_decode($request->values);
        // $image_deco = $request->image;
            
        //     $file = $request->file('image');
        //     $nombre =  $file->getClientOriginalName();
        //     $extension = $file->getClientOriginalExtension();
        //     $unique_name = md5($nombre. time());
        //     $name_image = $unique_name . "." . $extension;
        //     \Storage::put('public/productos', file_get_contents($file));
        $producto = Producto::find($id);
        $producto->marca = $data->marca;
        $producto->precio_final = $data->precio_final;
        $producto->sku_description = $data->sku_description;
        $producto->sku_producto = $data->sku_producto;
        $producto->stock_quantity = $data->stock;
        $producto->talla = $data->talla;
        $producto->imagen = $data->imagen;
        $producto->save();
        //$this->productService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto actualizado correctamente.'
        ]);
    }

    public function delete($id) {
        $this->productService->delete($id);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto eliminado correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->productService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto actualizado correctamente.'
        ]);
    }

    public function uploadCsv(Request $request)
    {
        $files = $request->file('file_csv');
        return array_map( 
            function($file) { 
                if ($file) {
                    
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $tempPath = $file->getRealPath();
                    $fileSize = $file->getSize();
                    $this->checkUploadedFileProperties($extension, $fileSize);
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location . "/" . $filename);
                    $file = fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    $j = 0;
                    $user = \Auth::user();
                    foreach ($importData_arr as $importData) {
                        
                        try {
                            DB::beginTransaction();
                            if($importData !== ""){
                                $j++;
                                $producto = Producto::where('tienda_id', $user->tienda_id)->where('sku_producto', $importData[0])->first();
                                if ($producto){
                                    $producto->update([
                                        'stock_quantity' => $importData[4],
                                        'imagen' => $importData[6],
                                        'precio_final' => $importData[5]
                                    ]);
                                }
                                if(!$producto) {
                                    Producto::create([
                                        'tienda_id' => $user->tienda_id,
                                        'sku_producto' => $importData[0],
                                        'sku_description' => $importData[1],
                                        'marca' => $importData[2],
                                        'talla' => $importData[3],
                                        'imagen' => $importData[6],
                                        'stock_quantity' => $importData[4],
                                        'sku_precio' => 0,
                                        'descuento' => 0,
                                        'precio_final' => $importData[5],
                                        'dimensiones' => '',
                                        'categoria' => ''
                                    ]);
                                }
                            }
                            
                            
                            // if (Producto::where('tienda_id', $user->tienda_id)
                            //     ->where('sku_producto', $importData[0])
                            //     ->doesntExist())
                            // {
                            //     Producto::create([
                            //         'tienda_id' => $user->tienda_id,
                            //         'sku_producto' => $importData[0],
                            //         'sku_description' => $importData[1],
                            //         'marca' => $importData[2],
                            //         'talla' => $importData[3],
                            //         'imagen' => $importData[6],
                            //         'stock' => $importData[4],
                            //         'sku_precio' => 0,
                            //         'descuento' => 0,
                            //         'precio_final' => $importData[5],
                            //         'dimensiones' => '',
                            //         'categoria' => ''
                            //     ]);
                            // }
                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                        }
                    }
                    return response()->json([
                        'message' => "$j productos subidos satisfactoriamente"
                    ]);
                } else {
                    throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
                }
            },
            $files
        );
        // $file = $request->file('file_csv');
        // if ($file) {
        //     $filename = $file->getClientOriginalName();
        //     $extension = $file->getClientOriginalExtension();
        //     $tempPath = $file->getRealPath();
        //     $fileSize = $file->getSize();
        //     $this->checkUploadedFileProperties($extension, $fileSize);
        //     $location = 'uploads';
        //     $file->move($location, $filename);
        //     $filepath = public_path($location . "/" . $filename);
        //     $file = fopen($filepath, "r");
        //     $importData_arr = array();
        //     $i = 0;
        //     while (($filedata = fgetcsv($file, 1000, ";")) !== FALSE) {
        //         $num = count($filedata);
        //         if ($i == 0) {
        //             $i++;
        //             continue;
        //         }
        //         for ($c = 0; $c < $num; $c++) {
        //             $importData_arr[$i][] = $filedata[$c];
        //         }
        //         $i++;
        //     }
        //     $j = 0;
            
        //     foreach ($importData_arr as $importData) {
        //         $j++;
        //         try {
        //             DB::beginTransaction();
        //             Product::create([
        //                 'sku_producto' => $importData[0],
        // 'sku_description' => $importData[1],
        // 'marca' => $importData[9],
        // 'talla' => $importData[4],
        // 'imagen' => $importData[8],
        // 'stock' => $importData[2],
        // 'sku_precio' => $importData[6],
        // 'descuento' => $importData[6]-$importData[5],
        // 'precio_final' => $importData[6]-($importData[6]-$importData[5]),
        // 'dimensiones' => $importData[3],
        // 'categoria' => $importData[7]
        //             ]);
        //             DB::commit();
        //         } catch (\Exception $e) {
        //             DB::rollBack();
        //         }
        //     }
        //     return response()->json([
        //         'message' => "$j productos subidos satisfactoriamente"
        //     ]);
        // } else {
        //     throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
        // }
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('El archivo no fue cargado', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Extensión de archivo invalido', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }

    public function sendEmail($email, $name)
    {
        $data = array(
        'email' => $email,
        'name' => $name,
        'subject' => 'Welcome Message',
        );
        Mail::send('welcomeEmail', $data, function ($message) use ($data) {
        $message->from('welcome@myapp.com');
        $message->to($data['email']);
        $message->subject($data['subject']);
        });
    }

    public function agregarInventario($id)
    {
        $producto = Producto::find($id);
        $producto->update([
            'stock_quantity' => $producto->stock_quantity + 1
        ]);
    }

    public function disminuirInventario($id)
    {
        $producto = Producto::find($id);
        $producto->update([
            'stock_quantity' => $producto->stock_quantity - 1
        ]);
    }
}