<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\Wiqli\WiqliProducto;
use App\Http\Controllers\Controller;

class WiqliProductoController extends Controller
{
    public function all(Request $request)
    {
        $productos = WiqliProducto::with(['unidad', 'categoria'])
                            ->where('activo', 1)
                            ->get();
        return $productos;
    }

    public function allAdmin(Request $request)
    {
        
        $productos = WiqliProducto::with(['unidad', 'categoria'])
                ->where(function ($q) use ($request){
                    if (isset($request->nombre) && $request->nombre !== '') {
                        $q->where('wiqli_productos.nombre','like', '%'.$request->nombre.'%');
                    }
                })
                ->where(function ($q) use ($request){
                    if (isset($request->categoria_id) && $request->categoria_id !== '') {
                        $q->where('wiqli_categorias_producto.id', $request->categoria_id);
                    }
                })
                ->where('activo', 1)
                ->join('wiqli_categorias_producto', 'wiqli_productos.categoria_id', 'wiqli_categorias_producto.id')
                ->paginate(10);
        return $productos;
    }

    public function show($id)
    {
        $producto = WiqliProducto::find($id);
        return $producto;
    }
}
