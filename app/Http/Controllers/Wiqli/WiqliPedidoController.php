<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;

class WiqliPedidoController extends Controller
{
    protected $correo_dev;
    protected $correo_prod_renzo;
    protected $correo_prod_ricardo;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_royer');
        $this->correo_prod_renzo = config('constants.correo_renzo');
        $this->correo_prod_ricardo = config('constants.correo_ricardo');
    }

    public function all(Request $request)
    {
        $fechas = [];
        if(isset($request->fecha)){
            $fechas[0] = substr($request->fecha[0], 1, 10);
            $fechas[1] = substr($request->fecha[1], 1, 10);
        }
        $pedidos = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])
                ->where(function ($q) use ($fechas){
                    if (isset($fechas[0]) && isset($fechas[1])) {
                        $q->whereBetween('created_at', [$fechas[0], $fechas[1]]);
                    }
                })
                ->paginate(10);
        return $pedidos;
    }

    public function crearPedido(Request $request)
    {
        $productos = $request->productos;
        $otrosFrutas = json_decode($request->otrosFrutas, true);
        $otrosVerduras = json_decode($request->otrosVerduras, true);
        $otrosCarnes = json_decode($request->otrosCarnes, true);
        $otrosMenestras = json_decode($request->otrosMenestras, true);
        $cliente = $request->cliente;
        $new_cliente = $this->guardarCliente($cliente);
        
        $new_pedido = WiqliPedido::create([
            'cliente_id' => $new_cliente->id,
            'costo_delivery' => 10.00,
            'fecha_entrega' => $cliente['fecha_recojo'],
            'observacion' => isset($cliente['observacion']) ? $cliente['observacion'] : ''
        ]);

        $total_productos = $this->guardarDetallePedido($productos, $new_pedido->id);
        $this->guardarDetallePedidoOtrosFrutas($otrosFrutas, $new_pedido->id);
        $this->guardarDetallePedidoOtrosVerduras($otrosVerduras, $new_pedido->id);
        $this->guardarDetallePedidoOtrosCarnes($otrosCarnes, $new_pedido->id);
        $this->guardarDetallePedidoOtrosMenestras($otrosMenestras, $new_pedido->id);


        $new_pedido->update([
            'total' => $total_productos
        ]);

        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $new_pedido->id)
                    ->get();
        $productosAdicionales = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '=', 999)
                    ->where('pedido_id', $new_pedido->id)
                    ->get();

        Mail::to('royer@repo.com.pe')->send(new AvisoDashboard($new_pedido, $new_cliente, $productosPedido, $productosAdicionales));
        Mail::to($new_cliente->correo)->send(new AvisoUsuario($new_pedido, $new_cliente, $productosPedido, $productosAdicionales));
        Mail::to($this->correo_prod_renzo)->send(new AvisoDashboard($new_pedido, $new_cliente, $productosPedido, $productosAdicionales));
        Mail::to($this->correo_prod_ricardo)->send(new AvisoDashboard($new_pedido, $new_cliente, $productosPedido, $productosAdicionales));

        return response()->json([
            'state'=> 1,
            'message' => 'Pedido creado correctamente.'
        ]);

    }

    public function guardarDetallePedido($productos = [], $pedido_id)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedido_id,
                'producto_id' => $producto['id'],
                'cantidad' => $producto['cantidad'],
                'precio_unitario' => $producto['precio_unitario'],
                'total'  => $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $total += $producto['cantidad'] * $producto['precio_unitario'];
        }

        return $total;
    }

    public function guardarDetallePedidoOtrosFrutas($productos = [], $pedido_id)
    {
        if(isset($productos['otrasFrutas'])){
            foreach ($productos['otrasFrutas'] as $key => $producto) {
                WiqliDetallePedido::create([
                    'pedido_id' => $pedido_id,
                    'producto_id' => 999,
                    'nombre_desc' => $producto['productoAdicionalNombre'],
                    'cantidad_desc' => $producto['productoAdicionalCantidad']
                ]);
            }
        }
    }

    public function guardarDetallePedidoOtrosVerduras($productos = [], $pedido_id)
    {
        if(isset($productos['otrasVerduras'])){
            foreach ($productos['otrasVerduras'] as $key => $producto) {
                WiqliDetallePedido::create([
                    'pedido_id' => $pedido_id,
                    'producto_id' => 999,
                    'nombre_desc' => $producto['productoAdicionalNombre'],
                    'cantidad_desc' => $producto['productoAdicionalCantidad']
                ]);
            }
        }
    }

    public function guardarDetallePedidoOtrosCarnes($productos = [], $pedido_id)
    {
        if(isset($productos['otrasCarnes'])){
            foreach ($productos['otrasCarnes'] as $key => $producto) {
                WiqliDetallePedido::create([
                    'pedido_id' => $pedido_id,
                    'producto_id' => 999,
                    'nombre_desc' => $producto['productoAdicionalNombre'],
                    'cantidad_desc' => $producto['productoAdicionalCantidad']
                ]);
            }
        }
    }

    public function guardarDetallePedidoOtrosMenestras($productos = [], $pedido_id)
    {
        if(isset($productos['otrasMenestras'])){
            foreach ($productos['otrasMenestras'] as $key => $producto) {
                WiqliDetallePedido::create([
                    'pedido_id' => $pedido_id,
                    'producto_id' => 999,
                    'nombre_desc' => $producto['productoAdicionalNombre'],
                    'cantidad_desc' => $producto['productoAdicionalCantidad']
                ]);
            }
        }
    }

    public function guardarCliente($cliente)
    {
        $new_cliente = WiqliCliente::create([
            'nombres' => $cliente['nombres'],
            'apellidos' => $cliente['apellidos'],
            'direccion' => $cliente['direccion'],
            'referencia' => $cliente['referencia'],
            'provincia' => 'Lima',
            'departamento' => 'Lima',
            'telefono' => $cliente['telefono'],
            'correo' => $cliente['correo']
        ]);
        return $new_cliente;
    }

    public function exportExcel(Request $request)
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function verPdf($id)
    {
        //return Excel::download(new PedidoExport, 'reporte.xlsx');
        $pedido = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])->where('id', 6)->get();
        $data = [
            'productos' => $pedido[0]['detalle'],
            'pedido' => $pedido[0]
        ];
        
        $pdf = Pdf::loadView('exports.pedido', $data);
        return $pdf->download('invoice.pdf');
        return (new PedidoExport)->download('reporte.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    public function prueba()
    {

        $pedidos = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('pedido_id');


        $totalesCantidad = [];
        $prods = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('producto_id');
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totalesCantidad, $subtotal);
        }



        $productos = WiqliProducto::with('unidad')->get()->toArray();
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')
            ->where('producto_id', '!=', 999)
            ->orderBy('producto_id')
            ->get()
            ->toArray();
        $detalle = [];
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }

        foreach ($new_prods as $key => $value) {
            $new_prods[$key]['cantidad'] = $totalesCantidad[$key];
            $new_prods[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }

        $idsProductos = [];
        foreach ($new_prods as $key => $value) {
            array_push($idsProductos, $value['id']);
        }
        
        $data1 = [];
        $cantidad = 0;
        $precio = 0;
        $total = 0;
        $producto_id = 0;
        $pedido_id = 0;
        $llaveProducto = 0;
        foreach($idsProductos as $key => $idProducto) {
            $llavePedido = 0;
            foreach ($pedidos as $key1 => $pedido) {
                foreach ($pedido as $key2 => $producto) {
                    if($producto['producto_id'] == $idProducto){
                        $data1[$key]['personas'][$key1]['cantidad'] = $producto['cantidad'];
                        $data1[$key]['personas'][$key1]['precio'] = $producto['precio_unitario'];
                        $data1[$key]['personas'][$key1]['total'] = $producto['total'];
                        $data1[$key]['personas'][$key1]['producto_id'] = $producto['producto_id'];
                        $data1[$key]['personas'][$key1]['pedido_id'] = $producto['pedido_id'];
                    }
                    else{
                        $data1[$key]['personas'][$key1]['cantidad'] = 0;
                        $data1[$key]['personas'][$key1]['precio'] = 0;
                        $data1[$key]['personas'][$key1]['total'] = 0;
                        $data1[$key]['personas'][$key1]['producto_id'] = 0;
                        $data1[$key]['personas'][$key1]['pedido_id'] = $producto['pedido_id'];
                    }
                        
                }
                $llavePedido = $llavePedido + 1;
            }
            $llaveProducto = $llaveProducto + 1;
        }
        return $data1;




        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;

        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        return $productosAptos;




        $productos = WiqliProducto::with('unidad')->get()->toArray();
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')->get()->toArray();
        $detalle = [];
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        $clientes = WiqliPedido::with(['cliente', 'detalle'])->get();

        $totales_cantidad = [];
        $totales_dinero = [];
        $prods = WiqliDetallePedido::all()->groupBy('producto_id');
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totales_cantidad, $subtotal);
        }

        foreach ($productosAptos as $key => $value) {
            $productosAptos[$key]['cantidad'] = $totales_cantidad[$key];
            $productosAptos[$key]['total'] = $value['precio_unitario'] * $totales_cantidad[$key];
        }

        foreach ($productosAptos as $key => $producto) {
            $id_productoApto= $key;
            foreach ($clientes as $key => $cliente) {
                $id_pedido = $cliente['id'];
                foreach ($cliente['detalle'] as $key => $detalle) {
                    if($detalle['id'] == $producto['id'])
                    {
                        $productosAptos[$id_productoApto]['personas'][$key]['cantidad'] = $detalle['cantidad'];
                        $productosAptos[$id_productoApto]['personas'][$key]['precio'] = $detalle['precio_unitario'];
                    }
                }
            }
        }
        return $productosAptos;


        $clientes = WiqliPedido::with(['cliente', 'detalle'])
                        ->get();          
        $clientes = WiqliDetallePedido::join('wiqli_pedidos', 'wiqli_detalles_pedido.pedido_id', 'wiqli_pedidos.id')
                    ->join('wiqli_clientes', 'wiqli_pedidos.cliente_id', 'wiqli_clientes.id')
                    ->join('wiqli_productos', 'wiqli_detalles_pedido.producto_id', 'wiqli_productos.id')
                    ->get();
                    
        $productos = WiqliProducto::get()->toArray();
        $pedidos = WiqliPedido::with(['cliente', 'detalle'])->get();
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')->get()->toArray();
        $detalle = [];
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        $data = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        return $data;
    }
}
