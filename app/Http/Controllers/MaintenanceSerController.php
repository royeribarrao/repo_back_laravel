<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Contracts\MaintenanceSerInterface as MaintenanceSerService;

class MaintenanceSerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $maintenanceSerService;

    public function __construct(
        MaintenanceSerService $maintenanceSerService
    )
    {
        $this->maintenanceSerService = $maintenanceSerService;
    }

    public function get(Request $request) {
        $services = $this->maintenanceSerService->paginate();    
        return response()->json($services);
    }

    public function getAll(Request $request) {
        $services = $this->maintenanceSerService->all(['name', 'id', 'phone']);    
        return response()->json($services);
    }

    public function store(Request $request) {

        $this->maintenanceSerService->create($request->all());          
        return response()->json([
            'state'=> 1,
            'message' => 'Servicio creado correctamente.'
        ]);
    }

    public function show($id) {
        $service = $this->maintenanceSerService->find($id);
        return response()->json($service);
    }

    public function update($id, Request $request) {
        $this->maintenanceSerService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Servicio actualizado correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->maintenanceSerService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Servicio actualizado correctamente.'
        ]);
    }

}
