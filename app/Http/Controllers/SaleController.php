<?php

namespace App\Http\Controllers;

use App\Services\Contracts\SaleInterface as SaleService;
use Illuminate\Http\Request;
use App\Models\Sale;
use DB;
use Illuminate\Http\Response;

class SaleController extends Controller
{
    protected $saleService;

    public function __construct(
        SaleService $sale
    )
    {
        $this->saleService = $sale;
    }

    public function get(Request $request) {
        $req = $request->all();
        $sales = $this->saleService->paginate(15, $req);    
        return response()->json($sales);
    }

    public function getAll(Request $request) {
        $sales = $this->saleService->all();    
        return response()->json($sales);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {

        $this->saleService->create($request->all());           
        return response()->json([
            'state'=> 1,
            'message' => 'Venta creada correctamente.'
        ]);
    }
    
    public function show($id) {
        $sale = $this->saleService->find($id);
        return response()->json($sale);
    }

    public function update($id, Request $request) {
        $this->saleService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Venta actualizada correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->saleService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Venta actualizado correctamente.'
        ]);
    }

    public function uploadCsv(Request $request)
    {
        $files = $request->file('file_csv');
        return array_map( 
            function($file) {
                if ($file) {
                    $user = \Auth::user();
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $tempPath = $file->getRealPath();
                    $fileSize = $file->getSize();
                    $this->checkUploadedFileProperties($extension, $fileSize);
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location . "/" . $filename);
                    $file = fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    $j = 0;
                    
                    foreach ($importData_arr as $importData) {
                        $j++;
                        try {
                            DB::beginTransaction();
                            if($importData !== ""){
                                Sale::create([
                                    'tienda_id' => $user->tienda_id,
                                    'codigo_compra' => $importData[0],
                                    'fecha_compra' => $importData[1],
                                    'sku' => $importData[2],
                                    'nombre_producto' => $importData[3],
                                    'marca' => '',
                                    'cantidad' => $importData[4],
                                    'precio_pagado' => $importData[5],
                                    'numero_documento' => $importData[13],
                                    'nombre_cliente' => $importData[6],
                                    'apellido_cliente' => $importData[7],
                                    'telefono' => $importData[8],
                                    'email' => $importData[9],
                                    'direccion' => $importData[10],
                                    'distrito' => $importData[11],
                                    'provincia' => '',
                                    'departamento' => ''
                                ]);
                            }
                            
                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                        }
                    }
                    return response()->json([
                        'message' => "$j ventas subidos satisfactoriamente"
                    ]);
                } else {
                    throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
                }
            },
            $files
        );
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('El archivo no fue cargado', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Extensión de archivo invalido', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }
}