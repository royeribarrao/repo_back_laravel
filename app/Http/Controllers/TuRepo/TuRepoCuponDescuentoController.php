<?php

namespace App\Http\Controllers\TuRepo;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\TuRepo\TuRepoCuponDescuento;
use Carbon\Carbon;

class TuRepoCuponDescuentoController extends Controller
{
    public function todos()
    {
        $cupones = TuRepoCuponDescuento::paginate(15);
        return $cupones;
    } 

    public function crearCupon(Request $request)
    {
        $data = $request->all();
        TuRepoCuponDescuento::create([
            'codigo' => $data['codigo'],
            'descuento' => $data['descuento'],
            'cantidad_total' => $data['cantidad_total'],
            'cantidad_restante' => $data['cantidad_total'],
            'fecha_inicio' => substr($data['fecha_inicio'], 0, 10),
            'fecha_expiracion' => substr($data['fecha_expiracion'], 0, 10)
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Cupon creado correctamente.'
        ]);
    }

    public function validarCupon($codigo)
    {
        $cupon = TuRepoCuponDescuento::where('codigo', $codigo)->first();
        if($cupon){
            $newformat_fecha_exp = Carbon::createFromFormat('Y-m-d', $cupon->fecha_expiracion)->format('Y-m-d');
            $newformat_fecha_ini = Carbon::createFromFormat('Y-m-d', $cupon->fecha_inicio)->format('Y-m-d');
            if($cupon->cantidad_restante > 0){
                if($newformat_fecha_exp >= now()->toDateString() && $newformat_fecha_ini <= now()->toDateString()){
                    return response()->json([
                        'state'=> true,
                        'cupon' => $cupon,
                        'message' => 'El cupón es válido.'
                    ]);
                }
                if($newformat_fecha_exp <= now()->toDateString()){
                    return response()->json([
                        'state'=> false,
                        'message' => 'Cupón no válido.'
                    ]);
                }
                if($newformat_fecha_ini >= now()->toDateString()){
                    return response()->json([
                        'state'=> false,
                        'message' => 'Cupón no válido.'
                    ]);
                }
            }
            
        }
        return response()->json([
            'state'=> false,
            'message' => 'El código ingresado no existe.'
        ]);
    } 
}