<?php

namespace App\Http\Controllers\TuRepo;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\TuRepo\RepoServicio;
use App\Models\TuRepo\RepoServicioOpcion;
use App\Models\TuRepo\RepoTiendas;
use App\Models\TuRepo\RepoServicioNuevoProducto;
use App\Models\Tienda;
use App\Models\Producto;
use App\Models\ConfiguracionTienda;
use App\Models\TuRepo\RepoDatosDelivery;
use App\Mail\TuRepo\ConfirmacionProceso;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Models\Woocommerce\WoocommerceProducto;
use Mail;

class TuRepoWebController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function allTiendasWeb(Request $request) {
        $stores = RepoTiendas::with('opciones')->get();    
        return response()->json($stores);
    }

    public function showInfoTienda($id) {
        if($id == "ninguna"){

        }
        $tienda = Tienda::with(['opciones.opcion', 'configuracion'])->find($id);
        
        return response()->json($tienda);
    }

    public function createService(Request $request, $id)
    {
        $data = $request->all();
        $servicio = RepoServicio::create([
            'tienda_id' => $id,
            'nombre_tienda' => isset($data['nombre_tienda']) ? $data['nombre_tienda'] : '',
            'referencia_tienda' => isset($data['referencia_tienda']) ? $data['referencia_tienda'] : '',
            'codigo' => isset($data['estado']) ? 'NW'.random_int(99999, 1000000) : random_int(99999, 1000000),
            'distrito' => $data['distrito'],
            'nueva_talla' => isset($data['nueva_talla']) ? $data['nueva_talla'] : ''
        ]);
        if(isset($data['peticion'])){
            if(is_array($data['peticion'])){
                foreach($data['peticion'] as $key => $item){
                    RepoServicioOpcion::create([
                        'servicio_id' => $servicio->id,
                        'opcion_id' => $item,
                        'prioridad' => $key
                    ]);
                }
            }else{
                RepoServicioOpcion::create([
                    'servicio_id' => $servicio->id,
                    'opcion_id' => $data['peticion'],
                    'prioridad' => 2
                ]);
            }
            
        }
        
        return response()->json([
            'state'=> 1,
            'message' => 'servicio creado correctamente',
            'id' => $servicio->id
        ]);
    }

    public function asignAboutProduct(Request $request, $id)
    {
        $data = $request->all();
        $imagesUploaded = $request->file('filesBoleta');
        
        $image = array_map( 
            function($file) {
                $fileDirectory = 'archivos/';

                $originalName = $file->getClientOriginalName();
                $filename = pathinfo($originalName, PATHINFO_FILENAME);
                $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                $newFileName = $filename . '_' . uniqid() . '.'. $extension;
    
                $nFile = [
                    'path' => $newFileName,
                    'full_path' => $fileDirectory . $newFileName
                ];
                Storage::disk('tu_repo')->put($nFile['full_path'],  file_get_contents($file));
    
                return $nFile;
            },
            $imagesUploaded
        );

        if(isset($data['filesProducto'])){
            $imagesProducto = $request->file('filesProducto');
            $imageProducto = array_map( 
                function($file) {
                    $fileDirectory = 'archivos/';
    
                    $originalName = $file->getClientOriginalName();
                    $filename = pathinfo($originalName, PATHINFO_FILENAME);
                    $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                    $newFileName = $filename . '_' . uniqid() . '.'. $extension;
        
                    $nFile = [
                        'path' => $newFileName,
                        'full_path' => $fileDirectory . $newFileName
                    ];
                    Storage::disk('tu_repo')->put($nFile['full_path'],  file_get_contents($file));
        
                    return $nFile;
                },
                $imagesProducto
            );
        }
        

        $servicio = RepoServicio::find($id);
        $servicio->update([
            'link_producto' => $data['link_producto'],
            'motivo' => $data['motivo'],
            'precio' => $data['precio'],
            'producto' => $data['producto'],
            'imagen_boleta' => $image[0]['full_path'],
            'imagen_producto' => isset($imageProducto) ? $imageProducto[0]['full_path'] : '',
            'comentario' => isset($data['comentario']) ? $data['comentario'] : ''
        ]);

        return response()->json([
            'state'=> 1,
            'message' => 'Datos acerca del producto actualizado correctamente',
            'servicio' => $servicio
        ]);
    }

    public function getInfoService($id){
        $servicio = RepoServicio::find($id);
        $opciones = RepoServicioOpcion::where('servicio_id', $id)
                        ->where('opcion_id', 1)
                        ->get()
                        ->toArray();
        $existe_yape = false;
        if(count($opciones) > 0){
            $existe_yape = true;
        }
        $api_key = 'pO9Iev4lV6w91e4WVUlIu6DDI0';
        $merchant_id = 953133;
        $referencia = $servicio->id;
        $promocion = RepoServicio::count();
        if($promocion < 92){
            $es_promocion = 10.00;
        }elseif($promocion > 91){
            $es_promocion = 20.00;
        }
        $md5 = md5($api_key.'~'.$merchant_id.'~'.$referencia.'~'.$es_promocion.'~'."PEN", false);
        return response()->json([
            'state' => true,
            'referenceCode'=> $servicio->codigo,
            'total_pago' => $es_promocion,
            'signature' => $md5,
            'transferencia' => $existe_yape,
            'id' => $servicio->id
        ]);
    }

    public function asignDatosDeliveryService(Request $request, $id){
        $data = $request->all();
        $servicio = RepoServicio::find($id);
        $datos_delivery = RepoDatosDelivery::create([
            'apellidos' => $data['apellidos'],
            'celular' => $data['celular'],
            'direccion_recojo' => $data['direccion'],
            'distrito' => $data['distrito'],
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud'],
            'dni' => $data['dni'],
            'email' => $data['email'],
            'fecha_recojo' => $data['fecha_recojo'],
            'nombres' => $data['nombres'],
            'servicio_id' => $servicio->id,
            'metodo_pago' => isset($data['metodo']) ? $data['metodo'] : ''
        ]);

        return response()->json([
            'state'=> true,
            'message' => 'Datos delivery creado correctamente',
            'codigo' => $servicio->codigo,
            'id' => $servicio->id
        ]);
    }

    public function getServices()
    {
        $servicios = RepoServicio::with(['opciones.opcion', 'delivery', 'tienda'])
            ->where('confirmacion_web', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(15);
        return $servicios;
    }

    public function updateStatus(Request $request)
    {
        $servicio = RepoServicio::find($request->id);
        if($request->state == 1){
            $state = 2;
        }else if($request->state == 2){
            $state = 3;
        }
        $servicio->update(['estado' => $state]);

        return response()->json([
            'state'=> true,
            'message' => 'Estado actualizado correctamente'
        ]);
    }

    public function getServicioById($id)
    {
        $servicio = RepoServicio::with(['opciones.opcion', 'delivery', 'tienda'])->where('id', $id)->first();
        return $servicio;
    }

    public function getServicioByCodigoAndId($id, $codigo)
    {
        $servicio = RepoServicio::with(['opciones.opcion', 'delivery', 'tienda'])
                    ->where('id', $id)
                    ->where('codigo', $codigo)
                    ->first();
        return $servicio;
    }

    public function getCantidadServiciosActual()
    {
        $servicio = RepoServicio::count();
        if($servicio < 92){
            $promocion = true;
        }elseif($servicio > 91){
            $promocion = false;
        }
        return response()->json([
            'state'=> true,
            'promocion' => $promocion
        ]);
    }

    public function enviarMailConfirmacion($id, $email)
    {
        $servicio = RepoServicio::with(['opciones.opcion', 'delivery', 'tienda'])->where('id', $id)->first();
        $servicio->update(['confirmacion_web' => 1]);
        $fecha = substr($servicio->delivery->fecha_recojo,0, 10);
        Mail::to($email)->send(new ConfirmacionProceso($servicio, $servicio->codigo, $fecha));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($servicio->codigo, "TuRepo", $servicio->tienda->business_name));
        return response()->json([
            'state'=> true,
            'message' => 'Correo enviado correctamente'
        ]);
    }

    public function obtenerInventario($id)
    {
        $configuracion = ConfiguracionTienda::where('tienda_id', $id)->first();
        if($configuracion->integracion_id == 1){
            $products = Producto::where('stock_quantity', '>', 0)
            ->where('tienda_id', $id)
            ->orderBy('name', 'asc')
            ->paginate(12);
            
            if(isset($data['descripcion']) &&  $data['descripcion'] != ''){
                $products = Producto::where('stock_quantity', '>', 0)
                        ->where('tienda_id', $id)
                        ->where(function ($query) use ($data){
                            $query->where('marca', 'like', '%'.$data['descripcion'].'%')
                            ->orWhere('name', 'like', '%'.$data['descripcion'].'%');
                        })
                        ->orderBy('name', 'asc')
                        ->paginate(12);
            }
        }elseif($configuracion->integracion_id == 2){
            $products = WoocommerceProducto::where('stock_quantity', '>', 0)
            ->where('tienda_id', $id)
            ->orderBy('name', 'asc')
            ->paginate(12);
            
            if(isset($data['descripcion']) &&  $data['descripcion'] != ''){
                $products = WoocommerceProducto::where('stock_quantity', '>', 0)
                        ->where('tienda_id', $id)
                        ->where(function ($query) use ($data){
                            $query->where('name', 'like', '%'.$data['descripcion'].'%');
                        })
                        ->orderBy('name', 'asc')
                        ->paginate(12);
            }
        }
        
        return $products;
    }

    public function agregarNuevosProductos(Request $request, $servicio_id)
    {
        $data = $request->all();
        foreach($data as $key => $item){
            RepoServicioNuevoProducto::create([
                'producto_id' => $item['id'],
                'servicio_id' => $servicio_id,
                'precio' => $item['precio_final']
            ]);
        }
        
        return response()->json([
            'state'=> true,
            'message' => 'Productos añadidos correctamente'
        ]);
    }
}