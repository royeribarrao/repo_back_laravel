<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Models\Gestion;
use App\Models\Sale;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\DatosDelivery;
use App\Models\Producto;
use App\Models\ConfiguracionTienda;
use App\Models\TrackerCambioEstandar;
use App\Models\TrackerCambioDeluxe;
use App\Models\TrackerDevolucion;
use App\Models\TiendaMotivoDevolucion;
use App\Models\Carrier;
use App\Models\Woocommerce\WoocommerceProducto;
use App\Mail\ConfirmacionPedido;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\Tienda\NuevoProceso;
use Mail;

class RepoGestionController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function crearProceso(Request $request, $codigo_compra, $tienda_id)
    {
        $data = json_decode($request->all()['gestion'], true);
        $tarea = $request->all()['tarea'];
        $total_monto_productos_gestion = 0;
        $cliente_paga_logistico = true;

        foreach($data as $key => $item){
            $total_monto_productos_gestion += $item['producto']['precio_pagado'];
            if($item['pago_logistico'] == 2)
            {
                $cliente_paga_logistico = false;
            }
        }
        
        $new_gestion = Gestion::create([
            'entorno_id' => 1,
            'codigo_compra' => $codigo_compra,
            'tienda_id' => $tienda_id,
            'total_productos_gestion' => $total_monto_productos_gestion,
            'confirmacion_tienda' => $cliente_paga_logistico,
            'cliente_paga_logistico' => $cliente_paga_logistico
        ]);

        if($tarea == 'cambio'){
            $new_gestion->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($tarea == 'devolucion'){
            $new_gestion->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        
        foreach($data as $key => $item){
            $imageName = null;
            $comentario = null;
            if(isset($item['imagen']) && $item['imagen'] != null && $item['imagen'] != ""){
                
                $comentario = $item['imagen']['comentario'];
                if(isset($item['imagen']['imagen']['thumbUrl'])){
                    $image = $item['imagen']['imagen']['thumbUrl'];
                    $ext = explode(';base64',$image);
                    $ext = explode('/',$ext[0]);			
                    $ext = $ext[1];
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = uniqid().'.'.$ext;
        
                    Storage::disk('my_files')->put("productos-gestiones/".$imageName,  base64_decode($image));
                }
            }
            
            ProductoGestion::create([
                'producto_id' => $item['producto']['id'],
                'sku_producto' => $item['producto']['sku'],
                'gestion_id' => $new_gestion->id,
                'motivo' => $item['motivo'],
                'imagen_producto' => $imageName,
                'comentario_evidencia' => $comentario,
                'nombre_producto' => $item['producto']['nombre_producto'],
                'precio' => $item['producto']['precio_pagado']
            ]);
        }

        return response()->json([
            'state'=> true,
            'gestion' => $new_gestion,
            'message' => 'Proceso creado correctamente.'
        ]);
    }

    public function crearProcesoDevolucion(Request $request, $codigo_compra, $tienda_id)
    {
        $data = json_decode($request->all()['gestion'], true);
        $total_monto_productos_gestion = 0;
        $cliente_paga_logistico = true;

        foreach($data['gestion'] as $key => $item){
            $total_monto_productos_gestion += $item['producto']['precio_pagado'];
            if($item['pago_logistico'] == 2)
            {
                $cliente_paga_logistico = false;
            }
        }
        
        $new_gestion = Gestion::create([
            'entorno_id' => 1,
            'codigo_compra' => $codigo_compra,
            'tienda_id' => $tienda_id,
            'tipo_servicio' => $data['servicio'],
            'total_productos_gestion' => $total_monto_productos_gestion,
            'total_devolucion' => $total_monto_productos_gestion,
            'confirmacion_tienda' => $cliente_paga_logistico,
            'cliente_paga_logistico' => $cliente_paga_logistico,
            'cobro_neto' => 0,
            'total_pago' => $cliente_paga_logistico ? 10 : 0
        ]);

        $new_gestion->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        
        foreach($data['gestion'] as $key => $item){
            $imageName = null;
            $comentario = null;
            if(isset($item['imagen']) && $item['imagen'] != null && $item['imagen'] != ""){
                
                $comentario = $item['imagen']['comentario'];
                if(isset($item['imagen']['imagen']['thumbUrl'])){
                    $image = $item['imagen']['imagen']['thumbUrl'];
                    $ext = explode(';base64',$image);
                    $ext = explode('/',$ext[0]);			
                    $ext = $ext[1];
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = uniqid().'.'.$ext;
        
                    Storage::disk('my_files')->put("productos-gestiones/".$imageName,  base64_decode($image));
                }
            }
            
            ProductoGestion::create([
                'producto_id' => $item['producto']['id'],
                'sku_producto' => $item['producto']['sku'],
                'gestion_id' => $new_gestion->id,
                'motivo' => $item['motivo'],
                'imagen_producto' => $imageName,
                'comentario_evidencia' => $comentario,
                'nombre_producto' => $item['producto']['nombre_producto'],
                'precio' => $item['producto']['precio_pagado']
            ]);
        }

        return response()->json([
            'state'=> true,
            'gestion' => $new_gestion,
            'message' => 'Proceso creado correctamente.'
        ]);
    }

    public function agregarNuevosProductos(Request $request, $tienda_id, $gestion_id)
    {
        $data = $request->all();

        $total_nuevos_productos = 0;
        foreach($data['nuevos_productos'] as $key => $item){
            $total_nuevos_productos += $item['price'];
        }

        $gestion = Gestion::find($gestion_id);

        if($data['servicio'] == 1){
            $costo_logistico = 15;
        }elseif($data['servicio'] == 2){
            $costo_logistico = 20;
        }elseif($data['servicio'] == 3){
            $costo_logistico = 10;
        }

        $gestion->update([
            'tipo_servicio' => $data['servicio'],
            'total_nuevos_productos' => $total_nuevos_productos,
            'costo_logistico' => $costo_logistico,
            'cobro_neto' => $total_nuevos_productos - $gestion->total_productos_gestion,
            'total_pago' => $gestion->cliente_paga_logistico ? $data['total_pago'] : $total_nuevos_productos - $gestion->total_productos_gestion,
            'total_devolucion' => $data['total_devolucion']
        ]);

        foreach($data['nuevos_productos'] as $key => $item){
            NuevoProductoServicioCambio::create([
                'producto_id' => $item['id'],
                'price' => $item['price'],
                'sku' => $item['sku'],
                'name' => $item['name'],
                'gestion_id' => $gestion_id,
                'cantidad' => $item['cantidad']
            ]);
        }
        return response()->json([
            'state'=> true,
            'message' => 'Productos agregados correctamente.'
        ]);
    }

    public function guardarDatosDelivery(Request $request, $gestion_id, $codigo_repo)
    {
        $gestion = Gestion::find($gestion_id);
        $data = $request->all();
        $datos_delivery = DatosDelivery::create([
            'gestion_id' => $gestion_id,
            'apellidos' => $data['apellidos'],
            'departamento' => $data['departamento'],
            'provincia' => $data['provincia'],
            'distrito' => $data['distrito'],
            'direccion' => $data['direccion'],
            'referencia' => $data['referencia'],
            'celular' => $data['celular'],
            'correo' => $data['email'],
            'fecha_recojo' => $data['fecha_recojo'],
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud']
        ]);
        $gestion->update([
            'datos_delivery_id' => $datos_delivery->id,
            'fecha_recojo' => $data['fecha_recojo']
        ]);
        $ir_payu = false;
        if($gestion->cliente_paga_logistico || $gestion->cobro_neto > 0){
            $ir_payu = true;
        }
        
        return response()->json([
            'state' => true,
            'gestion' => $gestion,
            'payU' => $ir_payu,
            'message' => 'Datos delivery creado correctamente.'
        ]);
    }
}