<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Web\OperadorAliado;
use App\Models\Web\OperadorAliado as OperadorModel;
use Mail;

class OperadorAliadoController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        OperadorModel::create($data);
        Mail::to($this->correo_prod)->send(new OperadorAliado($data));
        return response()->json([
            'state'=> true,
            'message' => 'Información enviada correctamente.'
        ]);
    }
    
    public function hola()
    {
        return "hola";
    }
}