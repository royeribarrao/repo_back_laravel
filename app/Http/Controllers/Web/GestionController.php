<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Gestion;
use App\Models\User;
use App\Models\Sale;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\DatosDelivery;
use App\Models\Producto;
use App\Models\TrackerCambioEstandar;
use App\Models\TrackerCambioDeluxe;
use App\Models\TrackerDevolucion;
use App\Models\TiendaMotivoDevolucion;
use App\Models\Carrier;
use App\Mail\ConfirmacionPedido;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\Tienda\NuevoProceso;
use Mail;

class GestionController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function allCambios(Request $request){
        $gestiones = Gestion::where('tipo_servicio', '=', 2)->get();
        return $gestiones;
    }

    public function allDevoluciones(Request $request){
        $gestiones = Gestion::where('tipo_servicio', '=', 1)->get();
        return $gestiones;
    }

    public function storeInvitado(Request $request, $codigo_compra, $codigo_tienda)
    {
        $info = json_decode($request->all()['info'], true);
        $productos_gestion = $info['productos_gestion'];
        $servicio = $info['servicio'];
        $nuevos_productos = $info['nuevos_productos'];
        $total_devolucion = $info['total_devolucion'];
        $total_pago = $info['total_pago'];
        
        $total_monto_productos_gestion = 0;
        foreach($productos_gestion as $key => $item){
            $total_monto_productos_gestion += $item['producto']['precio_pagado'];
        }

        $cliente_pago_logistico = true;
        foreach($productos_gestion as $key => $item){
            if($item['motivo'] == '5' || $item['motivo'] == '4'){
                $cliente_pago_logistico = false;
            }else{
                $cliente_pago_logistico = true;
            }
        }
        
        $new_gestion = Gestion::create([
            'codigo_repo' => '',
            'fecha_recojo' => '',
            'tipo_servicio' => $servicio['codigo'],
            'codigo_compra' => $codigo_compra,
            'cliente_id' => 3,
            'tienda_id' => $codigo_tienda,
            'imagen_evidencia' => '',
            'total_productos_gestion' => $total_monto_productos_gestion,
            'costo_logistico' => $servicio['costo_tamano'] + $servicio['precio_base'],
            'cobro_neto' => $total_pago - ($servicio['costo_tamano'] + $servicio['precio_base']),
            'total_pago' => $total_pago,
            'total_devolucion' => $total_devolucion,
            'estado' => true,
            'en_proceso' => $cliente_pago_logistico
        ]);

        if($servicio['codigo'] == 1){
            TrackerCambioEstandar::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($servicio['codigo'] == 2){
            TrackerCambioDeluxe::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($servicio['codigo'] == 3){
            TrackerDevolucion::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }
        
        if($servicio['codigo'] == 1 || $servicio['codigo'] === 2){
            $new_gestion->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($servicio['codigo'] == 3){
            $new_gestion->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        
        foreach($productos_gestion as $key => $item){
            $imageName = null;
            $comentario = null;
            if(isset($item['imagen']) && $item['imagen'] != null && $item['imagen'] != ""){
                
                $comentario = $item['imagen']['comentario'];
                if(isset($item['imagen']['imagen']['thumbUrl'])){
                    $image = $item['imagen']['imagen']['thumbUrl'];  // your base64 encoded
                    $ext = explode(';base64',$image);
                    $ext = explode('/',$ext[0]);			
                    $ext = $ext[1];
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = uniqid().'.'.$ext;
        
                    Storage::disk('my_files')->put("productos-gestiones/".$imageName,  base64_decode($image));
                    //Storage::put("productos-gestiones/".$imageName,  base64_decode($image));
                    //\File::put(storage_path(). '/' . $imageName, base64_decode($image));
                    // return "guardado";
                    
                    // $newFileName = uniqid().'.'.$item['imagen']['imagen']->extension();
                    // return $newFileName;
                    // Storage::put("gestiones/logos".$newFileName,  \File::get($file));
                }
            }
            
            ProductoGestion::create([
                'producto_id' => $item['producto']['id'],
                'sku_producto' => $item['producto']['sku'],
                'gestion_id' => $new_gestion->id,
                'motivo' => $item['motivo'],
                'evidencia' => $imageName,
                'comentario_evidencia' => $comentario,
                'nombre_producto' => $item['producto']['nombre_producto'],
                'precio' => $item['producto']['precio_pagado']
            ]);
        }
        
        if(count($nuevos_productos) > 0 && $servicio['codigo'] !== 3){
            foreach($nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'gestion_id' => $new_gestion->id,
                    'producto_id' => $item['id'],
                    'precio' => $item['precio_final'],
                    'sku_producto' => $item['sku_producto'],
                    'cantidad' => $item['cantidad']
                ]);
            }
        }
        return response()->json([
            'state'=> true,
            'codigo' => $new_gestion->codigo_repo,
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function storeInvitadoNuevaForma(Request $request, $codigo_compra, $codigo_tienda)
    {
        $info = json_decode($request->all()['info'], true);
        $productos_gestion = $info['productos_gestion'];
        $servicio = $info['servicio'];
        $nuevos_productos = $info['nuevos_productos'];
        $total_devolucion = $info['total_devolucion'];
        $total_pago = $info['total_pago'];
        
        
        $total_monto_productos_gestion = 0;
        foreach($productos_gestion as $key => $item){
            $total_monto_productos_gestion += $item['producto']['precio_pagado'];
        }

        $cliente_pago_logistico = true;
        foreach($productos_gestion as $key => $item){
            if($item['motivo'] == '5' || $item['motivo'] == '4'){
                $cliente_pago_logistico = false;
            }else{
                $cliente_pago_logistico = true;
            }
        }
        
        $new_gestion = Gestion::create([
            'codigo_repo' => '',
            'fecha_recojo' => '',
            'tipo_servicio' => $servicio,
            'codigo_compra' => $codigo_compra,
            'cliente_id' => 3,
            'tienda_id' => $codigo_tienda,
            'imagen_evidencia' => '',
            'total_productos_gestion' => $total_monto_productos_gestion,
            'costo_logistico' => $info['costo_logistico'],
            'cobro_neto' => $total_pago - $servicio,
            'total_pago' => $total_pago,
            'total_devolucion' => $total_devolucion,
            'estado' => true,
            'en_proceso' => $cliente_pago_logistico,
            'confirmacion_tienda' => $cliente_pago_logistico
        ]);

        if($servicio == 1){
            TrackerCambioEstandar::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($servicio == 2){
            TrackerCambioDeluxe::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($servicio == 3){
            TrackerDevolucion::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }
        
        if($servicio == 1 || $servicio === 2){
            $new_gestion->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($servicio == 3){
            $new_gestion->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        
        foreach($productos_gestion as $key => $item){
            $imageName = null;
            $comentario = null;
            if(isset($item['imagen']) && $item['imagen'] != null && $item['imagen'] != ""){
                
                $comentario = $item['imagen']['comentario'];
                if(isset($item['imagen']['imagen']['thumbUrl'])){
                    $image = $item['imagen']['imagen']['thumbUrl'];
                    $ext = explode(';base64',$image);
                    $ext = explode('/',$ext[0]);			
                    $ext = $ext[1];
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = uniqid().'.'.$ext;
        
                    Storage::disk('my_files')->put("productos-gestiones/".$imageName,  base64_decode($image));
                }
            }
            
            ProductoGestion::create([
                'producto_id' => $item['producto']['id'],
                'sku_producto' => $item['producto']['sku'],
                'gestion_id' => $new_gestion->id,
                'motivo' => $item['motivo'],
                'evidencia' => $imageName,
                'comentario_evidencia' => $comentario,
                'nombre_producto' => $item['producto']['nombre_producto'],
                'precio' => $item['producto']['precio_pagado']
            ]);
        }
        
        if(count($nuevos_productos) > 0 && $servicio !== 3){
            foreach($nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'producto_id' => $item['id'],
                    'precio' => $item['precio_final'],
                    'sku_producto' => $item['sku_producto'],
                    'gestion_id' => $new_gestion->id,
                    'cantidad' => $item['cantidad']
                ]);
            }
        }
        return response()->json([
            'state'=> true,
            'codigo' => $new_gestion->codigo_repo,
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function storeInvitado1(Request $request)
    {
        //print_r($request->all());
        //return $request->all();
        //return gettype($request->all());
        $data_cliente = json_decode($request->all()['cliente'], true);
        //return $data_cliente;
        $info = json_decode($request->all()['info'], true);
        $info_gestion = $info['gestion'];
        $info_datos_envio = $info['datos_envio'];
        $info_nuevos_productos = $info['nuevos_productos'];
        $all_new_productos = [];

        $imagen = "";
        if($info_gestion[0]['imagen']){
            $imagen = $info_gestion[0]['imagen']['imagen']['thumbUrl'];
        }

        $new_delivery = DatosDelivery::create($data_cliente);
        
        $new_gestion = Gestion::create([
            'codigo_repo' => "",
            'fecha_solicitud' => $data_cliente['fecha_recojo'],
            'tipo_servicio' => $info_datos_envio['codigo'],
            'codigo_compra' => $info_gestion[0]['producto']['numero_compra'],
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => $new_delivery->id,
            'imagen_evidencia' => $imagen,
            'total_pago' => $info['total_pago'],
            'total_devolucion' => $info['total_devolucion']
        ]);
        
        if($new_gestion->tipo_servicio == 1 || $new_gestion->tipo_servicio === 2){
            Gestion::where('id', $new_gestion->id)->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($new_gestion->tipo_servicio == 3){
            Gestion::where('id', $new_gestion->id)->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }

        foreach($info_gestion as $key => $item){
            ProductoGestion::create([
                'sku_producto' => $item['producto']['sku_producto'],
                'gestion_id' => $new_gestion->id
            ]);
        }

        if(count($info_nuevos_productos) > 0 && $info_datos_envio['codigo'] !== 3){
            foreach($info_nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'precio' => $item['precio_final'],
                    'sku_producto' => $item['sku_producto'],
                    'gestion_id' => $new_gestion->id,
                    'cantidad' => $item['cantidad']
                ]);
            }
        }

        return response()->json([
            'state'=> 1,
            'data' => Gestion::with('delivery')->where('id', '=', $new_gestion->id)->first(),
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function storeLogueado(Request $request)
    {
        $info = json_decode($request->all()['info'], true);
        $info_gestion = $info['gestion'];
        $info_datos_envio = $info['datos_envio'];
        $info_nuevos_productos = $info['nuevos_productos'];
        $all_new_productos = [];
        $imagen = "";

        if($info_gestion[0]['imagen']){
            $imagen = $info_gestion[0]['imagen']['imagen']['thumbUrl'];
        }

        $new_gestion = Gestion::create([
            'fecha_solicitud' => Carbon::now()->format('Y-m-d H:i:s'),
            'tipo_servicio' => $info_datos_envio['codigo'],
            'codigo_compra' => $info_gestion[0]['producto']['numero_compra'],
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => $new_delivery->id,
            'imagen_evidencia' => $imagen,
            'total_pago_tienda' => 'pago tienda',
            'total_pago_logistico' => 'pago logistico'
        ]);

        foreach($info_gestion as $key => $item){
            ProductoGestion::create([
                'sku_producto' => $item['producto']['sku_producto'],
                'gestion_id' => $new_gestion->id
            ]);
        }

        if(count($info_nuevos_productos) > 0 && $info_datos_envio['codigo'] !== 3){
            foreach($info_nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'precio' => $item['precio_final'],
                    'sku_producto' => $item['sku_producto'],
                    'gestion_id' => $new_gestion->id,
                    'cantidad' => $item['cantidad']
                ]);
            }
        }

        return response()->json([
            'state'=> 1,
            'data' => $new_gestion,
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function getByCodigo(Request $request, $codigo, $tienda_id)
    {
        $gestion = Gestion::with(
                            'trackerDevolucion',
                            'trackerCambioEstandar',
                            'trackerCambioDeluxe',
                            'servicioLogistico.carrier',
                            'delivery',
                            'tienda',
                            'servicio'
                        )
                        ->where('codigo_repo', $codigo)
                        ->where('tienda_id', $tienda_id)
                        ->first();
        return $gestion;
    }

    public function getByCodigoRepoAndTienda(Request $request)
    {
        $data = $request->all();
        $gestion = Gestion::where('codigo_repo', '=', $data['codigo_repo'])
                        ->where('tienda_id', '=', $data['tienda'])
                        ->first();
        return $gestion;
    }

    public function getByCodigoRepo(Request $request, $codigo_repo)
    {
        $gestion = Gestion::where('codigo_repo', $codigo_repo)->first();
        $datos = Sale::where('codigo_compra', $gestion->codigo_compra)->first();

        $api_key = 'pO9Iev4lV6w91e4WVUlIu6DDI0';
        $merchant_id = 953133;
        $referencia = $gestion->codigo_repo;
        if($gestion->en_proceso){
            $monto =  $gestion->total_pago;
        }else{
            $monto =  $gestion->cobro_neto;
        }
        
        //$md5 = md5($api_key.$merchant_id.$referencia.$monto."PEN");
        $md5 = md5($api_key.'~'.$merchant_id.'~'.$referencia.'~'.$monto.'~'."PEN", false);
        return response()->json([
            'servicio'=> $gestion->servicio,
            'info' => $gestion,
            'data' => $datos,
            'referencia' => $md5
        ]);
    }

    public function realizarPagoPayU(Request $request)
    {
        $body = $request->all();
        $client = new \GuzzleHttp\Client();
        $url = 'https://checkout.payulatam.com/ppp-web-gateway-payu';
        $query = $client->request('POST', $url, [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => $body
        ]);
        return $query;
        // return response()->json([
        //     'status'=> true,
        //     'message' => 'Pago completado'
        // ]);
    }

    public function confirmarPagoWeb($codigo_repo)
    {
        // $carrier = Carrier::where('codigo', 'NIREX')->first();
        // return gettype($carrier);
        $gestion = Gestion::with(
                'productoGestion.producto', 
                'nuevosProductos', 
                'servicio', 
                'tienda',
                'delivery'
                )
                ->where('codigo_repo', $codigo_repo)
                ->first();
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();
        $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.sku_producto', 'products.sku_producto')
                            ->get()
                            ->toArray();
        
        foreach($new_productos as $key => $item){
            $producto = Producto::find($item['id']);
            $producto->update([
                'stock_quantity' => $producto->stock_quantity - 1
            ]);
        }
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $codigo_repo, $new_productos));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($codigo_repo, "Repo", $gestion->tienda->business_name));
        $gestion->update([
            'confirmacion_web' => true
        ]);
        return response()->json([
            'status'=> true,
            'message' => 'Pago completado'
        ]);
    }

    public function show($id)
    {
        $gestion = Gestion::with(['productoGestion', 'compra'])->find($id);
        return $gestion;
    }

    public function confirmarGestionSinPago($id)
    {
        $gestion = Gestion::with('productoGestion')->find($id);
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);
        $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, []));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));
        // Mail::to($tienda->email)->send(new NuevoProceso($tienda));
        foreach($personal_tienda as $key => $personal){
            Mail::to($personal->email)->send(new NuevoProceso($tienda));
        }
        $gestion->update([
            'confirmacion_web' => true
        ]);

        return $gestion;
    }

    public function confirmarGestion($id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_web' => true
        ]);
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();

        $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);
        $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, $new_productos));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));
        //Mail::to($tienda->email)->send(new NuevoProceso($tienda));
        foreach($personal_tienda as $key => $personal){
            Mail::to($personal->email)->send(new NuevoProceso($tienda));
        }
        foreach($new_productos as $key => $item){
            $producto = Producto::find($item['id']);
            $producto->update([
                'stock_quantity' => $producto->stock_quantity - 1
            ]);
        }
        
        return $gestion;
    }

    public function crearGestionTuRepo(Request $request, $tienda_id)
    {
        if($request->costo_logistico == "15.00"){
            $tipo_servicio = 1;
        }elseif($request->costo_logistico == "10.00"){
            $tipo_servicio = 3;
        }

        $new_gestion = Gestion::create([
            'entorno_id' => 3,
            'codigo_compra' => "NA",
            'codigo_repo' => 'NW'.random_int(99999, 1000000),
            'tipo_servicio' => $tipo_servicio,
            'cliente_id' => 3,
            'tienda_id' => $tienda_id,
            'costo_logistico' => $request->costo_logistico
        ]);

        return response()->json([
            'id' => $new_gestion->id,
            'state'=> true,
            'message' => 'Gestión creada correctamente.'
        ]);
    }

    public function asignarInformacionProductoGestionTuRepo(Request $request, $gestion_id)
    {
        $data = $request->all();
        $gestion = Gestion::find($gestion_id);
        $producto_gestion = ProductoGestion::where('gestion_id', $gestion_id)->first();
        $tienda_conf_motivo = TiendaMotivoDevolucion::where('tienda_id', $gestion->tienda_id)
                                ->where('motivo_devolucion_id' ,$request->motivo)
                                ->first();
        //return $tienda_conf_motivo;
        if(isset($data['filesBoleta'])){
            $imagesUploaded = $request->file('filesBoleta');
            $imageBoleta = array_map( 
                function($file) {
                    $fileDirectory = 'productos-gestiones/';
                    $originalName = $file->getClientOriginalName();
                    $filename = pathinfo($originalName, PATHINFO_FILENAME);
                    $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                    $newFileName = $filename . '_' . uniqid() . '.'. $extension;
                    $nFile = [
                        'path' => $newFileName,
                        'full_path' => $fileDirectory . $newFileName
                    ];
                    Storage::disk('my_files')->put($nFile['full_path'],  file_get_contents($file));
                    return $nFile;
                },
                $imagesUploaded
            );
        }
        if(isset($data['filesProducto'])){
            $imagesUploadedProducto = $request->file('filesProducto');
            $imageProducto = array_map( 
                function($file) {
                    $fileDirectory = 'productos-gestiones/';
                    $originalName = $file->getClientOriginalName();
                    $filename = pathinfo($originalName, PATHINFO_FILENAME);
                    $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                    $newFileName = $filename . '_' . uniqid() . '.'. $extension;
                    $nFile = [
                        'path' => $newFileName,
                        'full_path' => $fileDirectory . $newFileName
                    ];
                    Storage::disk('my_files')->put($nFile['full_path'],  file_get_contents($file));
                    return $nFile;
                },
                $imagesUploadedProducto
            );
        }
        if(isset($data['filesEvidencia'])){
            $imagesUploadedEvidencia = $request->file('filesEvidencia');
            $imageEvidencia = array_map( 
                function($file) {
                    $fileDirectory = 'productos-gestiones/';
                    $originalName = $file->getClientOriginalName();
                    $filename = pathinfo($originalName, PATHINFO_FILENAME);
                    $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                    $newFileName = $filename . '_' . uniqid() . '.'. $extension;
                    $nFile = [
                        'path' => $newFileName,
                        'full_path' => $fileDirectory . $newFileName
                    ];
                    Storage::disk('my_files')->put($nFile['full_path'],  file_get_contents($file));
                    return $nFile;
                },
                $imagesUploadedEvidencia
            );
        }
        
        if($producto_gestion){
            $producto_gestion->update([
                'gestion_id' => $gestion_id,
                'nombre_producto' => $request->producto,
                'link_producto' => $request->link_producto,
                'precio' => $request->precio,
                'motivo' => $request->motivo,
                'imagen_boleta' => isset($imageBoleta) ? $imageBoleta[0]['full_path'] : '',
                'imagen_producto' => isset($imageProducto) ? $imageProducto[0]['full_path'] : '',
                'evidencia' => isset($imageEvidencia) ? $imageEvidencia[0]['full_path'] : '',
                'comentario_evidencia' => $request->comentario
            ]);
        }
        else{
            ProductoGestion::create([
                'gestion_id' => $gestion_id,
                'nombre_producto' => $request->producto,
                'link_producto' => $request->link_producto,
                'precio' => $request->precio,
                'evidencia' => isset($image) ? $image[0]['full_path'] : '',
                'comentario_evidencia' => $request->comentario,
                'motivo' => $request->motivo,
                'imagen_boleta' => isset($imageProducto) ? $imageProducto[0]['full_path'] : ''
            ]);
        }

        $gestion->update([
            'total_productos_gestion' => $request->precio,
            'total_nuevos_productos' => 0.00,
            'confirmacion_tienda' => ($request->motivo == 4 || $request->motivo == 5) ? false : true,
            'cliente_paga_logistico' => $tienda_conf_motivo->pago_logistico == 1 ? true : false
        ]);

        if($gestion->tipo_servicio == 3 ){
            if($tienda_conf_motivo->pago_logistico == 2){
                $gestion->update([
                    'cobro_neto' => 0.00,
                    'total_devolucion' => $request->precio,
                    'total_pago' => 0.00,
                    'confirmacion_web' => true,
                    'en_proceso' => true,
                    'estado' => 1
                ]);
                return response()->json([
                    'state'=> true,
                    'servicio' => $gestion->tipo_servicio,
                    'codigo' => $gestion->codigo_repo,
                    'message' => 'Información del producto creada correctamente.',
                    'pagar' => false
                ]);
            }elseif($tienda_conf_motivo->pago_logistico == 1){
                $gestion->update([
                    'cobro_neto' => 10.00,
                    'total_devolucion' => $request->precio,
                    'total_pago' => 10.00
                ]);
            }
        }
        
        return response()->json([
            'state'=> true,
            'servicio' => $gestion->tipo_servicio,
            'codigo' => $gestion->codigo_repo,
            'message' => 'Información del producto creada correctamente.',
            'pagar' => true
        ]);
    }

    public function agregarNuevosProductos(Request $request, $gestion_id)
    {
        $data = $request->all();
        $gestion = Gestion::find($gestion_id);
        $total_nuevos_productos = 0;
        foreach($data as $key => $item){
            $total_nuevos_productos = $total_nuevos_productos + $item['precio_final'];
            NuevoProductoServicioCambio::create([
                'producto_id' => $item['id'],
                'precio' => $item['precio_final'],
                'sku_producto' => $item['sku_producto'],
                'gestion_id' => $gestion_id,
                'cantidad' => $item['cantidad']
            ]);
        }

        if($gestion->cliente_paga_logistico){
            $gestion->update([
                'total_pago' => $total_nuevos_productos - $gestion->total_productos_gestion + $gestion->costo_logistico,
                'cobro_neto' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'total_nuevos_productos' => $total_nuevos_productos
            ]);
        }else{
            $gestion->update([
                'total_pago' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'cobro_neto' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'total_nuevos_productos' => $total_nuevos_productos
            ]);
        }
        
        return response()->json([
            'gestion' => $gestion->codigo_repo,
            'state'=> true,
            'message' => 'Productos añadidos correctamente'
        ]);
    }

    public function asignDatosDeliveryService(Request $request, $codigo_repo)
    {
        $data = $request->all();
        $gestion = Gestion::where('codigo_repo' , $codigo_repo)->first();
        $datos_delivery = DatosDelivery::create([
            'apellidos' => $data['apellidos'],
            'celular' => $data['celular'],
            'correo' => $data['email'],
            'direccion' => $data['direccion_recojo'],
            'distrito' => $data['distrito'],
            'dni' => $data['dni'],
            'fecha_recojo' => $data['fecha_recojo'],
            'gestion_id' => $gestion->id,
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud'],
            'nombres' => $data['nombres'],
            'referencia' => isset($data['referencia']) ? $data['referencia'] : ''
        ]);

        $gestion->update([
            'datos_delivery_id' => $datos_delivery->id,
            'fecha_recojo' => $data['fecha_recojo']
        ]);

        return response()->json([
            'state'=> true,
            'message' => 'Datos delivery creado correctamente',
            'codigo' => $gestion->codigo_repo,
            'id' => $gestion->id
        ]);
    }
}
