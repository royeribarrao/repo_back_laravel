<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Web\TiendaAliada;
use App\Models\Web\TiendaAliada as TiendaModel;
use Mail;

class TiendaAliadaController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        
        TiendaModel::create($data);
        Mail::to($this->correo_prod)->send(new TiendaAliada($data));
        return response()->json([
            'state'=> true,
            'message' => 'Información enviada correctamente.'
        ]);
    }
    
    public function hola()
    {
        return "hola";
    }
}