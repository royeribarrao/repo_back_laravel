<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Models\Gestion;
use App\Models\User;
use App\Models\Sale;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\DatosDelivery;
use App\Models\Producto;
use App\Models\ConfiguracionTienda;
use App\Models\TrackerCambioEstandar;
use App\Models\TrackerCambioDeluxe;
use App\Models\TrackerDevolucion;
use App\Models\TiendaMotivoDevolucion;
use App\Models\Carrier;
use App\Models\Woocommerce\WoocommerceProducto;
use App\Mail\ConfirmacionPedido;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\Tienda\NuevoProceso;
use Mail;

class GestionRepoController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function holaRoyer()
    {
        return $this->correo_dev;
    }

    public function crearGestion(Request $request, $codigo_compra, $tienda_id)
    {
        $info = json_decode($request->all()['info'], true);
        $productos_gestion = $info['productos_gestion'];
        $tipo_servicio = $info['servicio'];
        $nuevos_productos = $info['nuevos_productos'];
        $total_devolucion = $info['total_devolucion'];
        $total_pago = $info['total_pago'];
        $tienda_asume_costo_logistico = $info['tienda_asume_costo_logistico'];
        
        $total_monto_productos_gestion = 0;
        foreach($productos_gestion as $key => $item){
            $total_monto_productos_gestion += $item['producto']['precio_pagado'];
        }

        $total_nuevos_productos = 0;
        foreach($nuevos_productos as $key => $item){
            $total_nuevos_productos += $item['price'];
        }

        if($tipo_servicio == 1){
            $costo_logistico = 15;
        }elseif($tipo_servicio == 2){
            $costo_logistico = 20;
        }elseif($tipo_servicio == 3){
            $costo_logistico = 10;
        }
        
        $new_gestion = Gestion::create([
            'entorno_id' => 1,
            'tipo_servicio' => $tipo_servicio,
            'codigo_compra' => $codigo_compra,
            'cliente_id' => 3,
            'tienda_id' => $tienda_id,
            'total_productos_gestion' => $total_monto_productos_gestion,
            'total_nuevos_productos' => $total_nuevos_productos,
            'costo_logistico' => $costo_logistico,
            'cobro_neto' => $total_nuevos_productos - $total_monto_productos_gestion,
            'total_pago' => $total_pago,
            'total_devolucion' => $total_devolucion,
            'confirmacion_tienda' => !$tienda_asume_costo_logistico,
            'cliente_paga_logistico' => !$tienda_asume_costo_logistico
        ]);

        if($tipo_servicio == 1){
            TrackerCambioEstandar::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($tipo_servicio == 2){
            TrackerCambioDeluxe::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($tipo_servicio == 3){
            TrackerDevolucion::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }
        
        if($tipo_servicio == 1 || $tipo_servicio === 2){
            $new_gestion->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($tipo_servicio == 3){
            $new_gestion->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        
        foreach($productos_gestion as $key => $item){
            $imageName = null;
            $comentario = null;
            if(isset($item['imagen']) && $item['imagen'] != null && $item['imagen'] != ""){
                
                $comentario = $item['imagen']['comentario'];
                if(isset($item['imagen']['imagen']['thumbUrl'])){
                    $image = $item['imagen']['imagen']['thumbUrl'];
                    $ext = explode(';base64',$image);
                    $ext = explode('/',$ext[0]);			
                    $ext = $ext[1];
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = uniqid().'.'.$ext;
        
                    Storage::disk('my_files')->put("productos-gestiones/".$imageName,  base64_decode($image));
                }
            }
            
            ProductoGestion::create([
                'producto_id' => $item['producto']['id'],
                'sku_producto' => $item['producto']['sku'],
                'gestion_id' => $new_gestion->id,
                'motivo' => $item['motivo'],
                'imagen_producto' => $imageName,
                'comentario_evidencia' => $comentario,
                'nombre_producto' => $item['producto']['nombre_producto'],
                'precio' => $item['producto']['precio_pagado']
            ]);
        }
        
        if(count($nuevos_productos) > 0 && $tipo_servicio !== 3){
            foreach($nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'producto_id' => $item['id'],
                    'price' => $item['price'],
                    'sku' => $item['sku'],
                    'gestion_id' => $new_gestion->id,
                    'cantidad' => $item['cantidad']
                ]);
            }
        }
        return response()->json([
            'state'=> true,
            'codigo' => $new_gestion->codigo_repo,
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function guardarDatosDeliveryRepo(Request $request, $codigo_repo)
    {
        $gestion = Gestion::where('codigo_repo' , $codigo_repo)->first();
        $data = $request->all();
        $datos_delivery = DatosDelivery::create([
            'nombres' => $data['nombre_cliente'],
            'apellidos' => $data['apellido_cliente'],
            'departamento' => $data['departamento'],
            'provincia' => $data['provincia'],
            'distrito' => $data['distrito'],
            'direccion' => $data['direccion'],
            'referencia' => $data['referencia'],
            'celular' => $data['telefono'],
            'correo' => $data['email'],
            'fecha_recojo' => $data['fecha_recojo'],
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud']
        ]);
        $gestion->update([
            'datos_delivery_id' => $datos_delivery->id,
            'fecha_recojo' => $data['fecha_recojo']
        ]);
        $ir_payu = false;
        if($gestion->cliente_paga_logistico || $gestion->cobro_neto > 0){
            $ir_payu = true;
        }
        
        return response()->json([
            'state' => true,
            'gestion' => $gestion,
            'payU' => $ir_payu,
            'message' => 'Datos delivery creado correctamente.'
        ]);
    }

    public function confirmarGestionSinPago($id)
    {
        $gestion = Gestion::with('productoGestion')->find($id);
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);
        $configuracion = ConfiguracionTienda::where('tienda_id', $tienda->id)->first();
        $gestion->update([
            'confirmacion_web' => true,
            'en_proceso' => 1
        ]);
        if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2){
            $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
                ->get()
                ->toArray();
            foreach($new_productos as $key => $item){
                $producto = Producto::find($item['id']);
                $producto->update([
                    'stock_quantity' => $producto->stock_quantity - 1
                ]);
            }
            // if($configuracion->integracion_id == 1)
            // {
            //     foreach($new_productos as $key => $item){
            //         $producto = Producto::find($item['id']);
            //         $producto->update([
            //             'stock_quantity' => $producto->stock_quantity - 1
            //         ]);
            //     }
            // }

            // if($configuracion->integracion_id == 2)
            // {
            //     foreach($new_productos as $key => $item){
            //         $producto = WoocommerceProducto::find($item['id']);
            //         $producto->update([
            //             'stock_quantity' => $producto->stock_quantity - 1
            //         ]);
            //     }
            // }
            
        }
        $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, []));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));

        //Mail::to($tienda->email)->send(new NuevoProceso($tienda));
        foreach($personal_tienda as $key => $personal){
            Mail::to($personal->email)->send(new NuevoProceso($tienda));
        }
        return $gestion;
    }

    public function getByCodigo(Request $request, $codigo, $tienda_id)
    {
        $gestion = Gestion::with(
                            'trackerDevolucion',
                            'trackerCambioEstandar',
                            'trackerCambioDeluxe',
                            'servicioLogistico.carrier',
                            'delivery',
                            'tienda',
                            'servicio'
                        )
                        ->where('codigo_repo', $codigo)
                        ->where('tienda_id', $tienda_id)
                        ->first();
        return $gestion;
    }

    public function getByCodigoRepoAndTienda(Request $request)
    {
        $data = $request->all();
        $gestion = Gestion::where('codigo_repo', '=', $data['codigo_repo'])
                        ->where('tienda_id', '=', $data['tienda'])
                        ->first();
        return $gestion;
    }

    public function getByCodigoRepo(Request $request, $codigo_repo)
    {
        $gestion = Gestion::where('codigo_repo', $codigo_repo)->first();
        $datos = Sale::where('codigo_compra', $gestion->codigo_compra)->first();

        $api_key = 'pO9Iev4lV6w91e4WVUlIu6DDI0';
        $merchant_id = 953133;
        $referencia = $gestion->codigo_repo;
        if($gestion->en_proceso){
            $monto =  $gestion->total_pago;
        }else{
            $monto =  $gestion->cobro_neto;
        }
        
        //$md5 = md5($api_key.$merchant_id.$referencia.$monto."PEN");
        $md5 = md5($api_key.'~'.$merchant_id.'~'.$referencia.'~'.$monto.'~'."PEN", false);
        return response()->json([
            'servicio'=> $gestion->servicio,
            'info' => $gestion,
            'data' => $datos,
            'referencia' => $md5
        ]);
    }

    public function show($id)
    {
        $gestion = Gestion::with(['productoGestion', 'compra'])->find($id);
        return $gestion;
    }

    public function confirmarGestion($id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_web' => true,
            'estado' => 1
        ]);
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();

        $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);
        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, $new_productos));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));
        //Mail::to($tienda->email)->send(new NuevoProceso($tienda));
        $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
        foreach($personal_tienda as $key => $personal){
            Mail::to($personal->email)->send(new NuevoProceso($tienda));
        }
        foreach($new_productos as $key => $item){
            $producto = Producto::find($item['id']);
            $producto->update([
                'stock_quantity' => $producto->stock_quantity - 1
            ]);
        }
        
        return $gestion;
    }

    public function crearGestionTuRepo(Request $request, $tienda_id)
    {
        if($request->costo_logistico == "15.00"){
            $tipo_servicio = 1;
        }elseif($request->costo_logistico == "10.00"){
            $tipo_servicio = 3;
        }

        $new_gestion = Gestion::create([
            'entorno_id' => 3,
            'codigo_compra' => "NA",
            'codigo_repo' => 'NW'.random_int(99999, 1000000),
            'tipo_servicio' => $tipo_servicio,
            'cliente_id' => 3,
            'tienda_id' => $tienda_id,
            'costo_logistico' => $request->costo_logistico
        ]);

        return response()->json([
            'id' => $new_gestion->id,
            'state'=> true,
            'message' => 'Gestión creada correctamente.'
        ]);
    }
}