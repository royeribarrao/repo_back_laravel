<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SdkPayU\lib\PayU;
use App\SdkPayU\lib\PayU\PayUPayments;
use App\SdkPayU\lib\PayU\util\PayUParameters;
use App\SdkPayU\lib\PayU\api\PayUCountries;
use App\Models\TuRepo\RepoDatosDelivery;
use App\Models\TuRepo\RepoServicio;
use App\Models\PayUTransaccion;
use App\Models\DatosDelivery;
use App\Models\Gestion;
use App\Models\User;
use App\Models\Producto;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Mail\TuRepo\ConfirmacionProceso;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\ConfirmacionPedido;
use App\Mail\Tienda\NuevoProceso;
use App\Models\TuRepo\TuRepoCuponDescuento;
use Mail;

class PayUController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function pagarPagoEfectivo(Request $request)
    { 
        $reference = "payment_test_00000001";
        $value = "100";


        

        $parameters = array(
            //Ingresa aquí el identificador de la cuenta
            PayUParameters::ACCOUNT_ID => "960756",
            // Ingresa aquí la referencia de pago.
            PayUParameters::REFERENCE_CODE => $reference,
            // Ingresa aquí la descripción del pago.
            PayUParameters::DESCRIPTION => "payment test",

            // -- Valores --
                //Ingresa aquí el valor.
            PayUParameters::VALUE => $value,
            // Ingresa aquí la moneda.
            PayUParameters::CURRENCY => "PEN",

            // -- Comprador --
            // Ingresa aquí el identificador del comprador.
            PayUParameters::BUYER_ID => "1",
            // Ingresa aquí el nombre del comprador.
            PayUParameters::BUYER_NAME => "First name and second buyer name",
            // Ingresa aquí el correo electrónico del comprador.
            PayUParameters::BUYER_EMAIL => "royer@repo.com.pe",
            // Ingresa aquí el teléfono de contacto del comprador.
            PayUParameters::BUYER_CONTACT_PHONE => "939784580",
            // Ingresa aquí el número de identificación del comprador.
            PayUParameters::BUYER_DNI => "70019408",
            // Ingresa aquí la dirección del comprador.
            PayUParameters::BUYER_STREET => "Av. Isabel La Católica 103-La Victoria",
            PayUParameters::BUYER_STREET_2 => "5555487",
            PayUParameters::BUYER_CITY => "Lima",
            PayUParameters::BUYER_STATE => "Lima y Callao",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => "939784580",


            // -- Pagador --
            // Ingresa aquí el identificador del pagador.
            //PayUParameters::PARAMETERS.PAYER_ID => "1",
            PayUParameters::PAYER_ID => "1",
                /// Ingresa aquí el nombre del pagador
            PayUParameters::PAYER_NAME => "First name and second payer name",
            // Ingresa aquí el correo electrónico del pagador
            PayUParameters::PAYER_EMAIL => "payer_test@test.com",
            // Ingresa aquí el número de teléfono del pagador.
            PayUParameters::PAYER_CONTACT_PHONE => "7563126",
            // Ingresa aquí el número de identificación del pagador.
            PayUParameters::PAYER_DNI => "5415668464654",
            // Ingresa aquí el tipo de identificación del pagador
            //PayUParameters::PARAMETERS.PAYER_DNI_TYPE => "DNI",
            PayUParameters::BUYER_DNI => "DNI",
            // Ingresa aquí la dirección del pagador.
            PayUParameters::PAYER_STREET => "Av. Isabel La Católica 103-La Victoria",
            PayUParameters::PAYER_STREET_2 => "5555487",
            PayUParameters::PAYER_CITY => "Lima",
            PayUParameters::PAYER_STATE => "Lima y Callao",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "000000",
            PayUParameters::PAYER_PHONE => "7563126",

            // -- Datos de la tarjeta de crédito --
            PayUParameters::INSTALLMENTS_NUMBER => "installmentsNumber",
                // Ingresa aquí el número de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_NUMBER => "4097440000000004",
            // Ingresa aquí la fecha de expiración de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => "2022/12",
            // Ingresa aquí el código de seguridad de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> "321",
            // Ingresa aquí el nombre de la tarjeta de crédito
            PayUParameters::PAYMENT_METHOD => "PAGOEFECTIVO",

            // Ingresa aquí la fecha de vencimiento del pago
            PayUParameters::EXPIRATION_DATE => "2021-07-01T20:00:00",
            
            // Ingresa aquí el nombre del país.
            PayUParameters::COUNTRY => PayUCountries::PE,

            // IP del pagador
            PayUParameters::IP_ADDRESS => "127.0.0.1"
            );

        // Petición de Autorización
        
        $response = PayUPayments::doAuthorizationAndCapture($parameters);
        return $response;
        // Puedes obtener las propiedades en la respuesta
        if ($response) {
            $response->transactionResponse->orderId;
            $response->transactionResponse->transactionId;
            $response->transactionResponse->state;
            if ($response->transactionResponse->state=="PENDING"){
                $response->transactionResponse->pendingReason;
                $response->transactionResponse->trazabilityCode;
                $response->transactionResponse->authorizationCode;
                $response->transactionResponse->extraParameters->REFERENCE;
                $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_PDF;
                $response->transactionResponse->extraParameters->EXPIRATION_DATE;
                $response->transactionResponse->extraParameters->BAR_CODE;
                $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;   
            }
            $response->transactionResponse->paymentNetworkResponseCode;
            $response->transactionResponse->paymentNetworkResponseErrorMessage;
            $response->transactionResponse->trazabilityCode;
            $response->transactionResponse->responseCode;
            $response->transactionResponse->responseMessage;

        }
        

    }

    public function ping()
    {
        $response = PayUPayments::doPing();
        $response->code;
        return $response;
    }

    public function cobroUnPaso(Request $request)
    {
        $data = $request->all();
        $comprobar = PayUTransaccion::where('servicio_id', $data['info']['id'])
            ->where('state', 'APPROVED')
            ->first();
        if($comprobar){
            return response()->json([
                'state'=> false,
                'code'=> 204,
                'message' => "Usted ya pagó este servicio, por favor, comuníquese con el administrador de Repo."
            ]);
        }

        // $descuento = 0;
        // if(isset($data['cupon'])){
        //     $cupon = TuRepoCuponDescuento::where('codigo', $data['cupon'])->first();
        //     if($cupon){
        //         if($cupon->cantidad_restante > 0){
        //             $descuento = $cupon->monto;
        //         }elseif($cupon->cantidad_restante == 0){
        //             return response()->json([
        //                 'state'=> false,
        //                 'code'=> 204,
        //                 'message' => "El cupón de descuento que ingresó ya fue usado."
        //             ]);
        //         }
        //     }elseif(!$cupon){
        //         return response()->json([
        //             'state'=> false,
        //             'code'=> 204,
        //             'message' => "El cupón de descuento que ingresó no existe."
        //         ]);
        //     }
        // }
        
        $searchString = " ";
        $replaceString = "";
        $numero_tarjeta = str_replace($searchString, $replaceString, $data['numero_tarjeta']); 
        
        $fecha_vencimiento = substr($data['fecha_vencimiento'], 0, 4).'/'.substr($data['fecha_vencimiento'], 5, 2);
        $datos_delivery = RepoDatosDelivery::where('servicio_id', $data['info']['id'])->first();
        
        $reference = $data['info']['referenceCode'];
        $reference = $reference.random_int(9999, 100000);
        $value = $data['info']['total_pago'];
        $parameters = array(
            //Ingresa aquí el identificador de la cuenta
            PayUParameters::ACCOUNT_ID => "960756",
            PayUParameters::REFERENCE_CODE => "$reference",
            PayUParameters::DESCRIPTION => "Pago TuRepo",

            // -- Valores --
            PayUParameters::VALUE => "$value",
            PayUParameters::CURRENCY => "PEN",

            // -- Comprador --
            PayUParameters::BUYER_ID => "$datos_delivery->id",
            PayUParameters::BUYER_NAME => "$datos_delivery->nombres $datos_delivery->apellidos",
            PayUParameters::BUYER_EMAIL => $datos_delivery->email,
            PayUParameters::BUYER_CONTACT_PHONE => $datos_delivery->celular,
            PayUParameters::BUYER_DNI => $datos_delivery->dni,
            PayUParameters::BUYER_STREET => $datos_delivery->direccion_recojo,
            PayUParameters::BUYER_STREET_2 => $datos_delivery->direccion_recojo,
            PayUParameters::BUYER_CITY => "Lima",
            PayUParameters::BUYER_STATE => "Lima",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $datos_delivery->celular,

            // -- Pagador --
            PayUParameters::PAYER_ID => "1",
            PayUParameters::PAYER_NAME => $data['nombre_tarjeta'],
            PayUParameters::PAYER_EMAIL => $datos_delivery->email,
            PayUParameters::PAYER_CONTACT_PHONE => $datos_delivery->celular,
            PayUParameters::PAYER_DNI => $datos_delivery->dni,
            PayUParameters::BUYER_DNI => "DNI",
            PayUParameters::PAYER_STREET => $datos_delivery->direccion_recojo,
            PayUParameters::PAYER_STREET_2 => $datos_delivery->direccion_recojo,
            PayUParameters::PAYER_CITY => "Lima",
            PayUParameters::PAYER_STATE => "Lima",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "000000",
            PayUParameters::PAYER_PHONE => $datos_delivery->celular,
            PayUParameters::CREDIT_CARD_NUMBER => $numero_tarjeta,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $fecha_vencimiento,
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> $data['cvv'],
            PayUParameters::PAYMENT_METHOD => $data['tipo_banco'],
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            PayUParameters::COUNTRY => PayUCountries::PE,
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
            );
        $response = PayUPayments::doAuthorizationAndCapture($parameters);
        
        if(!($response->code)){
            return response()->json([
                'state'=> false,
                'message' => $response->error
            ]);
        }

        $servicio = RepoServicio::with(['opciones.opcion', 'delivery', 'tienda'])->where('id', $data['info']['id'])->first();
        $servicio->update(['confirmacion_web' => 1]);
        $fecha = substr($servicio->delivery->fecha_recojo,0, 10);

        if($response){
            if($response->transactionResponse->responseCode == "APPROVED"){
                $transaction = PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => $response->transactionResponse->orderId,
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => $response->transactionResponse->transactionId,
                    'authorizationCode' => $response->transactionResponse->authorizationCode
                ]);
                
                Mail::to($datos_delivery->email)->send(new ConfirmacionProceso($servicio, $servicio->codigo, $fecha));
                Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($servicio->codigo, "TuRepo", $servicio->tienda->business_name));
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => 'Pago Satisfactorio',
                    'orderId' => $response->transactionResponse->orderId,
                    'transactionId' => $response->transactionResponse->transactionId
                ]);
            }elseif(
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_CONFIRMATION" ||
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_TRANSMISSION"
                ){
                $transaction = PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }else{
                PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'state'=> false,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }
        }

        return $response;
    }

    public function Datos(){
        $respuestFailed = [
            "code" => "SUCCESS",
            "transactionResponse" => [
                "orderId" => 1896026242,
                "transactionId" => "e94bc0db-ec5a-4efb-8c61-4707a8edbcdd",
                "state" => "DECLINED",
                "responseCode" => "ANTIFRAUD_REJECTED",
                "operationDate" => "2022-03-07T13:49:17",
                "extraParameters" => ["BANK_REFERENCED_CODE"  => "CREDIT"]
        ]];
        $tarjetaVisa = [
                "tipo" => "VISA",
                "numero_tarjeta" => "4223750778806383",
                "fecha_vencimiento" => "2023/01",
                "cvv" => "275",
                "nombre" => "Royer Repo"
        ];
    }

    public function getMetodosDePago()
    {
        $array=PayUPayments::getPaymentMethods();
        $payment_methods=$array->paymentMethods;
        $royer = [];
        foreach ($payment_methods as $key => $payment_method){
            $payment_method->country;
            $payment_method->description;
            $payment_method->id;
            $royer[$key] = $payment_method;
        }
        return $royer;
    }

    public function consultaPorIdentificadorDelaOrden()
    {
        // Incluye aquí la referencia de la orden.
        $parameters = array(PayUParameters::ORDER_ID => "44469220");

        $order = PayUReports::getOrderDetail($parameters);

        if ($order) {
            $order->accountId;
            $order->status;
            $order->referenceCode;
            $order->additionalValues->TX_VALUE->value;
            $order->additionalValues->TX_TAX->value;
            if ($order->buyer) {
                $order->buyer->emailAddress;
                $order->buyer->fullName;
            }
            $transactions=$order->transactions;
            foreach ($transactions as $transaction) {
                $transaction->type;
                $transaction->transactionResponse->state;
                $transaction->transactionResponse->paymentNetworkResponseCode;
                $transaction->transactionResponse->trazabilityCode;
                $transaction->transactionResponse->responseCode;
                if ($transaction->payer) {
                    $transaction->payer->fullName;
                    $transaction->payer->emailAddress;
                }
            }
        }

    }

    public function consultaPorIdentificadorDelaTransaccion()
    {
        $parameters = array(PayUParameters::TRANSACTION_ID => "3310ba3b-cf64-49b2-80e6-3f9196917131");

        $response = PayUReports::getTransactionResponse($parameters);

        if ($response) {
            $response->state;
            $response->trazabilityCode;
            $response->authorizationCode;
            $response->responseCode;
            $response->operationDate;
        }

    }

    public function consultaPorIdentificadorDelaReferencia()
    {
        // Incluye aquí la referencia de la orden.
        $parameters = array(PayUParameters::REFERENCE_CODE => "payment_test_1625093692957");

        $response = PayUReports::getOrderDetailByReferenceCode($parameters);

        foreach ($response as $order) {
            $order->accountId;
            $order->status;
            $order->referenceCode;
            $order->additionalValues->TX_VALUE->value;
            $order->additionalValues->TX_TAX->value;

            if ($order->buyer) {
                $order->buyer->emailAddress;
                $order->buyer->fullName;
            }

            $transactions=$order->transactions;
            foreach ($transactions as $transaction) {
                $transaction->type;
                $transaction->transactionResponse->state;
                $transaction->transactionResponse->paymentNetworkResponseCode;
                $transaction->transactionResponse->trazabilityCode;
                $transaction->transactionResponse->responseCode;
                if ($transaction->payer) {
                    $transaction->payer->fullName;
                    $transaction->payer->emailAddress;
                }
            }
        }

    }

    public function cobroServicioRepo(Request $request)
    {
        $data = $request->all();
        $comprobar = PayUTransaccion::where('servicio_id', $data['info']['id'])
            ->where('state', 'APPROVED')
            ->first();
        if($comprobar){
            return response()->json([
                'state'=> false,
                'code'=> 204,
                'message' => "Usted ya pagó este servicio, por favor, comunicate con el administrador de Repo."
            ]);
        }
        $searchString = " ";
        $replaceString = "";
        $numero_tarjeta = str_replace($searchString, $replaceString, $data['numero_tarjeta']);
        $fecha_vencimiento = substr($data['fecha_vencimiento'], 0, 4).'/'.substr($data['fecha_vencimiento'], 5, 2);
        $datos_delivery = DatosDelivery::where('id', $data['info']['datos_delivery_id'])->first();
        $reference = $data['info']['codigo_repo'];
        $reference = $reference."-".random_int(9999, 100000);
        $value = $data['info']['total_pago'];
       
        $parameters = array(
            PayUParameters::ACCOUNT_ID => "960756",
            PayUParameters::REFERENCE_CODE => "$reference",
            PayUParameters::DESCRIPTION => "Pago TuRepo",
            PayUParameters::VALUE => "$value",
            PayUParameters::CURRENCY => "PEN",
            PayUParameters::BUYER_ID => "$datos_delivery->id",
            PayUParameters::BUYER_NAME => "$datos_delivery->nombres $datos_delivery->apellidos",
            PayUParameters::BUYER_EMAIL => $datos_delivery->email,
            PayUParameters::BUYER_CONTACT_PHONE => $datos_delivery->celular,
            PayUParameters::BUYER_DNI => $datos_delivery->dni,
            PayUParameters::BUYER_STREET => $datos_delivery->direccion_recojo,
            PayUParameters::BUYER_STREET_2 => $datos_delivery->direccion_recojo,
            PayUParameters::BUYER_CITY => "Lima",
            PayUParameters::BUYER_STATE => "Lima",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $datos_delivery->celular,
            PayUParameters::PAYER_ID => "1",
            PayUParameters::PAYER_NAME => $data['nombre_tarjeta'],
            PayUParameters::PAYER_EMAIL => $datos_delivery->email,
            PayUParameters::PAYER_CONTACT_PHONE => $datos_delivery->celular,
            PayUParameters::PAYER_DNI => $datos_delivery->dni,
            PayUParameters::BUYER_DNI => "DNI",
            PayUParameters::PAYER_STREET => $datos_delivery->direccion_recojo,
            PayUParameters::PAYER_STREET_2 => $datos_delivery->direccion_recojo,
            PayUParameters::PAYER_CITY => "Lima",
            PayUParameters::PAYER_STATE => "Lima",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "000000",
            PayUParameters::PAYER_PHONE => $datos_delivery->celular,
            PayUParameters::CREDIT_CARD_NUMBER => $numero_tarjeta,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $fecha_vencimiento,
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> $data['cvv'],
            PayUParameters::PAYMENT_METHOD => $data['tipo_banco'],
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            PayUParameters::COUNTRY => PayUCountries::PE,
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );
        $response = PayUPayments::doAuthorizationAndCapture($parameters);
        
        if(!($response->code)){
            return response()->json([
                'state'=> false,
                'message' => $response->error
            ]);
        }

        $gestion = Gestion::with(
            'productoGestion.producto', 
            'nuevosProductos', 
            'servicio', 
            'tienda',
            'delivery'
            )
            ->where('id', $data['info']['id'])
            ->first();
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();
        $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);
        $fecha = substr($gestion->delivery->fecha_recojo,0, 10);
        
        if($response){
            if($response->transactionResponse->responseCode == "APPROVED"){
                $transaction = PayUTransaccion::create([
                    'tipo' => 1,
                    'state' => $response->transactionResponse->state,
                    'orderId' => $response->transactionResponse->orderId,
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => $response->transactionResponse->transactionId,
                    'authorizationCode' => $response->transactionResponse->authorizationCode
                ]);

                $gestion->update([
                    'confirmacion_web' => 1,
                    'estado' => 1,
                    'en_proceso' => 1
                ]);
                foreach($new_productos as $key => $item){
                    $producto = Producto::find($item['id']);
                    $producto->update([
                        'stock_quantity' => $producto->stock_quantity - 1
                    ]);
                }
                Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, $new_productos));
                Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));
                //Mail::to($tienda->email)->send(new NuevoProceso($tienda));
                $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
                foreach($personal_tienda as $key => $personal){
                    Mail::to($personal->email)->send(new NuevoProceso($tienda));
                }
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => 'Pago Satisfactorio',
                    'orderId' => $response->transactionResponse->orderId,
                    'transactionId' => $response->transactionResponse->transactionId
                ]);
            }elseif(
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_CONFIRMATION" ||
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_TRANSMISSION"
                ){
                $transaction = PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }else{
                PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $data['info']['id'],
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'state'=> false,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }
        }

        return $response;
    }
}