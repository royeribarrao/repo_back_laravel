<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Gestion;
use App\Models\User;
use App\Models\Sale;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\DatosDelivery;
use App\Models\Producto;
use App\Models\TrackerCambioEstandar;
use App\Models\TrackerCambioDeluxe;
use App\Models\TrackerDevolucion;
use App\Models\TiendaMotivoDevolucion;
use App\Models\Carrier;
use App\Mail\ConfirmacionPedido;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\Tienda\NuevoProceso;
use Mail;
use App\Services\SaveImageService;

class GestionRepoRetailController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;
    protected $imagenService;
    protected $rutaArchivos = 'productos-gestiones/repo/';
    protected $storageDisk = 'my_files';

    public function __construct(
        SaveImageService $imagenService
    )
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
        $this->imagenService = $imagenService;
    }

    public function crearGestionRepoRetail(Request $request, $tienda_id)
    {
        if($request->peticion == 100 || $request->peticion == 200){
            $tipo_servicio = 1;
        }elseif($request->peticion == 300 ){
            $tipo_servicio = 3;
        }elseif($request->peticion == 400 ){
            $tipo_servicio = 4;
        }
        $new_gestion = Gestion::create([
            'entorno_id' => 3,
            'codigo_compra' => "NA",
            'codigo_repo' => 'NW'.random_int(99999, 1000000),
            'tipo_servicio' => $tipo_servicio,
            'cliente_id' => 1,
            'tienda_id' => $tienda_id,
            'costo_logistico' => $request->costo_logistico
        ]);

        if($request->peticion == 100 || $request->peticion == 400){
            TrackerCambioEstandar::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($request->peticion == 200){
            TrackerCambioDeluxe::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }elseif($request->peticion == 300){
            TrackerDevolucion::create([
                'gestion_id' => $new_gestion->id,
                'pedido_recibido' => true,
                'estado' => 1,
                'nombre_estado' => 'Pedido recibido'
            ]);
        }

        return response()->json([
            'id' => $new_gestion->id,
            'state'=> true,
            'message' => 'Gestión creada correctamente.'
        ]);
    }

    public function asignarInformacionProductoRepoRetail(Request $request, $gestion_id)
    {
        $data = $request->all();
        $gestion = Gestion::find($gestion_id);
        $producto_gestion = ProductoGestion::where('gestion_id', $gestion_id)->first();
        $motivo = TiendaMotivoDevolucion::where('tienda_id', $gestion->tienda_id)
                                ->where('motivo_devolucion_id' ,$request->motivo)
                                ->first();
        if(isset($data['filesBoleta'])){
            $imageBoleta = $this->imagenService->guardarImagen($this->storageDisk, $request->file('filesBoleta'), $this->rutaArchivos);
        }
        if(isset($data['filesProducto'])){
            $imageProducto = $this->imagenService->guardarImagen($this->storageDisk, $request->file('filesProducto'), $this->rutaArchivos);
        }
        if(isset($data['filesEvidencia'])){
            $imageEvidencia = $this->imagenService->guardarImagen($this->storageDisk, $request->file('filesEvidencia'), $this->rutaArchivos);
        }
        
        if($producto_gestion){
            $producto_gestion->update([
                'gestion_id' => $gestion_id,
                'nombre_producto' => $request->producto,
                'link_producto' => $request->link_producto,
                'precio' => $request->precio,
                'motivo' => $request->motivo,
                'imagen_boleta' => isset($imageBoleta) ? $imageBoleta[0]['full_path'] : '',
                'imagen_producto' => isset($imageProducto) ? $imageProducto[0]['full_path'] : '',
                'evidencia' => isset($imageEvidencia) ? $imageEvidencia[0]['full_path'] : '',
                'comentario_evidencia' => $request->comentario
            ]);
        }
        else{
            ProductoGestion::create([
                'gestion_id' => $gestion_id,
                'nombre_producto' => $request->producto,
                'link_producto' => $request->link_producto,
                'precio' => $request->precio,
                'motivo' => $request->motivo,
                'imagen_boleta' => isset($imageBoleta) ? $imageBoleta[0]['full_path'] : '',
                'imagen_producto' => isset($imageProducto) ? $imageProducto[0]['full_path'] : '',
                'evidencia' => isset($imageEvidencia) ? $imageEvidencia[0]['full_path'] : '',
                'comentario_evidencia' => $request->comentario
            ]);
        }

        $gestion->update([
            'total_productos_gestion' => $request->precio,
            'confirmacion_tienda' => $motivo->pago_logistico == 1 ? true : false,
            'cliente_paga_logistico' => $motivo->pago_logistico == 1 ? true : false
        ]);

        if($gestion->tipo_servicio == 3){
            $gestion->update([
                'total_devolucion' => $request->precio
            ]);
            if($motivo->pago_logistico == 1){
                $gestion->update([
                    'total_nuevos_productos' => 0.00,
                    'cobro_neto' => 10.00,
                    'total_pago' => 10.00
                ]);
            }elseif($motivo->pago_logistico == 2){
                $gestion->update([
                    'total_nuevos_productos' => 0.00,
                    'cobro_neto' => 0.00,
                    'total_pago' => 0.00
                ]);
            }
        }
        if($gestion->tipo_servicio == 4){
            if($motivo->pago_logistico == 1){
                $gestion->update([
                    'total_nuevos_productos' => 0.00,
                    'total_devolucion' => 0,
                    'cobro_neto' => 15.00,
                    'total_pago' => 15.00
                ]);
            }elseif($motivo->pago_logistico == 2){
                $gestion->update([
                    'total_nuevos_productos' => 0.00,
                    'total_devolucion' => 0,
                    'cobro_neto' => 0,
                    'total_pago' => 0
                ]);
            }
        }
        
        return response()->json([
            'state'=> true,
            'servicio' => $gestion->tipo_servicio,
            'codigo' => $gestion->codigo_repo,
            'gestion_id' => $gestion->id,
            'message' => 'Información del producto creada correctamente.',
            'pagar' => true
        ]);
    }

    public function agregarNuevosProductos(Request $request, $gestion_id)
    {
        $data = $request->all();
        $gestion = Gestion::find($gestion_id);
        $total_nuevos_productos = 0;
        foreach($data as $key => $item){
            $total_nuevos_productos = $total_nuevos_productos + $item['price'];
            NuevoProductoServicioCambio::create([
                'producto_id' => $item['id'],
                'price' => $item['price'],
                'sku' => $item['sku'],
                'gestion_id' => $gestion_id,
                'cantidad' => $item['cantidad']
            ]);
        }

        if($gestion->cliente_paga_logistico){
            $gestion->update([
                'total_pago' => $total_nuevos_productos - $gestion->total_productos_gestion + $gestion->costo_logistico,
                'cobro_neto' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'total_nuevos_productos' => $total_nuevos_productos,
                'total_devolucion' => 0
            ]);
        }else{
            $gestion->update([
                'total_pago' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'cobro_neto' => $total_nuevos_productos - $gestion->total_productos_gestion,
                'total_nuevos_productos' => $total_nuevos_productos,
                'total_devolucion' => 0
            ]);
        }
        
        return response()->json([
            'gestion' => $gestion,
            'state'=> true,
            'message' => 'Productos añadidos correctamente'
        ]);
    }

    public function asignarDatosDeliveryRepoRetail(Request $request, $gestion_id)
    {
        $data = $request->all();
        $gestion = Gestion::find($gestion_id);
        $datos_delivery = DatosDelivery::create([
            'apellidos' => $data['apellidos'],
            'celular' => $data['celular'],
            'correo' => $data['email'],
            'direccion' => $data['direccion'],
            'distrito' => $data['distrito'],
            'dni' => $data['dni'],
            'fecha_recojo' => $data['fecha_recojo'],
            'gestion_id' => $gestion->id,
            'latitud' => $data['latitud'],
            'longitud' => $data['longitud'],
            'nombres' => $data['nombres'],
            'referencia' => isset($data['referencia']) ? $data['referencia'] : ''
        ]);
        $gestion->update([
            'datos_delivery_id' => $datos_delivery->id,
            'fecha_recojo' => $data['fecha_recojo']
        ]);

        if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 2){
            if($gestion->cliente_paga_logistico){
                $pasar_a_pagar = true;
            }elseif(!$gestion->cliente_paga_logistico){
                if($gestion->total_pago > 0){
                    $pasar_a_pagar = true;
                }else{
                    $pasar_a_pagar = false;
                    return $this->confirmarGestionSinPago($gestion->id);
                }
            }
        }

        if($gestion->tipo_servicio == 3){
            if($gestion->cliente_paga_logistico){
                $pasar_a_pagar = true;
            }else{
                $pasar_a_pagar = false;
                return $this->confirmarGestionSinPago($gestion->id);
                
            }
        }

        if($gestion->tipo_servicio == 4){
            if($gestion->cliente_paga_logistico){
                $pasar_a_pagar = true;
            }else{
                $pasar_a_pagar = false;
                return $this->confirmarGestionSinPago($gestion->id);
            }
        }

        return response()->json([
            'state'=> true,
            'message' => 'Datos delivery creado correctamente',
            'codigo' => $gestion->codigo_repo,
            'id' => $gestion->id,
            'pasar_a_pagar' => $pasar_a_pagar
        ]);
    }

    public function confirmarGestionSinPago($id)
    {
        $gestion = Gestion::with('productoGestion', 'delivery')->find($id);
        $new_productos = NuevoProductoServicioCambio::where('gestion_id', $gestion->id)
                            ->join('products', 'nuevos_productos_servicio_cambio.producto_id', 'products.id')
                            ->get()
                            ->toArray();
        $productos = ProductoGestion::where('gestion_id', $gestion->id)
                            ->get()
                            ->toArray();
        $tienda = Tienda::find($gestion->tienda_id);

        if($gestion->tipo_servicio == 3){
            $gestion->update([
                'total_nuevos_productos' => 0.00,
                'cobro_neto' => 0.00,
                'total_pago' => 0.00,
                'confirmacion_web' => true,
                'en_proceso' => true,
                'estado' => 1
            ]);
            // return response()->json([
            //     'state'=> true,
            //     'servicio' => $gestion->tipo_servicio,
            //     'codigo' => $gestion->codigo_repo,
            //     'gestion_id' => $gestion->id,
            //     'message' => 'Información del producto creada correctamente.',
            //     'pagar' => false
            // ]);
        }

        if($gestion->tipo_servicio == 4){
            $gestion->update([
                'confirmacion_web' => true,
                'en_proceso' => true,
                'estado' => 1
            ]);
        }

        Mail::to($gestion->delivery->correo)->send(new ConfirmacionPedido($gestion, $productos, $gestion->codigo_repo, $new_productos));
        Mail::to($this->correo_prod)->send(new AvisoPedidoRepo($gestion->codigo_repo, "Repo", $tienda->business_name));
        //Mail::to($tienda->email)->send(new NuevoProceso($tienda));
        $personal_tienda = User::where('tienda_id', $gestion->tienda_id)->get();
        foreach($personal_tienda as $key => $personal){
            Mail::to($personal->email)->send(new NuevoProceso($tienda));
        }
        return response()->json([
            'state'=> true,
            'servicio' => $gestion->tipo_servicio,
            'codigo' => $gestion->codigo_repo,
            'id' => $gestion->id,
            'message' => 'Información del producto y gestión creada correctamente.',
            'pagar' => false
        ]);
    }

    public function getGestionById(Request $request, $gestion_id)
    {
        $gestion = Gestion::find($gestion_id);
        
        return response()->json([
            'state' => true,
            'gestion' => $gestion,
        ]);
    }

    public function show($id)
    {
        $gestion = Gestion::with('productoGestion')->find($id);
        return $gestion;
    }
}