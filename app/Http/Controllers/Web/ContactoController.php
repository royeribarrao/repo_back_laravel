<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Web\ContactoMail;
use Mail;

class ContactoController extends Controller
{
    public function enviarEmail(Request $request)
    {
        
        Mail::to($request->email)->send(new ContactoMail($request->email, $request->nombre, $request->mensaje));
        return response()->json([
            'state'=> true,
            'message' => 'Email enviado correctamente.'
        ]);
    }
    
    public function hola()
    {
        return "hola";
    }
}