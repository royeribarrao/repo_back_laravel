<?php

namespace App\Http\Controllers\Web;

use App\Models\Sale;
use App\Models\Producto;
use App\Models\ConfiguracionTienda;
use App\Models\Woocommerce\WoocommerceProducto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class CambioDevolucionController extends Controller
{
    public function obtenerCompra(Request $request)
    {
        $data = $request->all();
        $configuracion = ConfiguracionTienda::where('tienda_id', $data['tienda'])->first();
        if($configuracion->integracion_id == 1){
            return $this->obtenerCompraRepo($data['tienda'], $data['codigo_compra']);
        }
        if($configuracion->integracion_id == 2){
            return $this->obtenerCompraShopify($data['tienda'], $data['codigo_compra']);
        }
    }

    public function obtenerCompraRepo($tienda_id, $codigo_compra){
        $sales = Sale::with([
                        'producto' => function ($q) use ($tienda_id){
                            if ($tienda_id) {
                                $q->where('tienda_id', $tienda_id);
                            }
                        }
                    ])
                    ->select(
                        'id',
                        'codigo_compra',
                        'fecha_compra',
                        'sku',
                        'nombre_producto',
                        'marca',
                        'cantidad',
                        'precio_pagado',
                        'numero_documento',
                        'nombre_cliente',
                        'apellido_cliente',
                        'telefono',
                        'email',
                        'direccion',
                        'provincia',
                        'departamento'
                    )
                    ->where('codigo_compra', $codigo_compra)
                    ->where('tienda_id', $tienda_id)
                    ->where('status', true)
                    ->get();

        return $sales;
    }
    
    public function getAllProductsByStore(Request $request, $tienda_id)
    {
        $data =  $request->all();
        $products = Producto::where('stock_quantity', '>', 0)
            ->where('tienda_id', $tienda_id)
            ->orderBy('name', 'asc')
            ->paginate(12);
            
            if(isset($data['descripcion']) &&  $data['descripcion'] != ''){
                $products = Producto::where('stock_quantity', '>', 0)
                        ->where('tienda_id', $tienda_id)
                        ->where(function ($query) use ($data){
                            $query->where('color', 'like', '%'.$data['descripcion'].'%')
                            ->orWhere('name', 'like', '%'.$data['descripcion'].'%');
                        })
                        ->orderBy('name', 'asc')
                        ->paginate(12);
            }
        // $configuracion = ConfiguracionTienda::where('tienda_id', $tienda_id)->first();
        // if($configuracion->integracion_id == 1){
        //     $products = Producto::where('stock_quantity', '>', 0)
        //     ->where('tienda_id', $tienda_id)
        //     ->orderBy('name', 'asc')
        //     ->paginate(12);
            
        //     if(isset($data['descripcion']) &&  $data['descripcion'] != ''){
        //         $products = Producto::where('stock_quantity', '>', 0)
        //                 ->where('tienda_id', $tienda_id)
        //                 ->where(function ($query) use ($data){
        //                     $query->where('color', 'like', '%'.$data['descripcion'].'%')
        //                     ->orWhere('name', 'like', '%'.$data['descripcion'].'%');
        //                 })
        //                 ->orderBy('name', 'asc')
        //                 ->paginate(12);
        //     }
        // }elseif($configuracion->integracion_id == 2){
        //     $products = WoocommerceProducto::where('stock_quantity', '>', 0)
        //     ->where('tienda_id', $tienda_id)
        //     ->orderBy('name', 'asc')
        //     ->paginate(12);
            
        //     if(isset($data['descripcion']) &&  $data['descripcion'] != ''){
        //         $products = WoocommerceProducto::where('stock_quantity', '>', 0)
        //                 ->where('tienda_id', $tienda_id)
        //                 ->where(function ($query) use ($data){
        //                     $query->where('name', 'like', '%'.$data['descripcion'].'%');
        //                 })
        //                 ->orderBy('name', 'asc')
        //                 ->paginate(12);
        //     }
        // }
        return $products;
    }

    public function obtenerCompraShopify($tienda_id, $codigo_compra)
    {
        $sales = Sale::with(['productoShopify' => function ($q) use ($tienda_id){
                            if ($tienda_id) {
                                $q->where('tienda_id', $tienda_id);
                            }
                        }])->select(
                        'ventas.id',
                        'ventas.codigo_compra',
                        'ventas.fecha_compra',
                        'ventas.sku',
                        'ventas.nombre_producto',
                        'ventas.nombre_sin_espacios',
                        'ventas.marca',
                        'ventas.cantidad',
                        'ventas.precio_pagado',
                        'ventas.numero_documento',
                        'ventas.nombre_cliente',
                        'ventas.apellido_cliente',
                        'ventas.telefono',
                        'ventas.email',
                        'ventas.direccion',
                        'ventas.provincia',
                        'ventas.departamento'
                    )
                    ->where('codigo_compra', '=', $codigo_compra)
                    ->where('ventas.tienda_id', '=', $tienda_id)
                    ->get();
        return $sales;
    }
}
