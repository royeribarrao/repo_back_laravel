<?php

namespace App\Http\Controllers;

use App\Services\Contracts\StoreInterface as StoreService;
use Illuminate\Http\Request;
use App\Models\TipoServicio;

class TipoServicioController extends Controller
{
    public function get(Request $request) {
        $servicios = TipoServicio::with('precios')->get();
        return $servicios;
    }
}