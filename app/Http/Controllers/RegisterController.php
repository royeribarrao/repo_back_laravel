<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\CreacionCuenta;
use App\Mail\VerificacionCuenta;
use Mail;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    protected function validator(array $data)
    {
        $user = null;
        return User::where('email', '=', $data['email'])->get();
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        return $validator;
    }

    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return response()->json([
            'state'=> 200,
            'message' => 'Registro completado con éxito.'
        ]);
    }

    public function register(Request $request)
    {
        // $data =  $request->all();
        // return gettype($data->name);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'state'=> 422,
                'message' => 'Asegurese de no estar registrado y que sus contraseñas coincidan.'
            ]);
        }else{
            $user =  User::create([
                'name' => $request['name'],
                'father_lastname' => $request['fatherLastname'],
                'email' => $request['email'],
                'password' => $request['password'],
                'dni' => $request['dni'],
                'rol_id' => 3,
                'address' => $request['address'],
                'latitud' => $request['latitud'],
                'longitud' => $request['longitud'],
                'referencia' => $request['referencia'],
                'distrito' => $request['distrito'],
                'departamento' => $request['departamento']
            ]);
            Mail::to($request['email'])->send(new CreacionCuenta());
            //Mail::to($request['email'])->send(new VerificacionCuenta());
            return response()->json([
                'state'=> 200,
                'message' => 'Registro completado con éxito.'
            ]);
        }
    }
}
