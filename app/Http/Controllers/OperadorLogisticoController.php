<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\Carrier;

class OperadorLogisticoController extends Controller
{
    public function show($id){
        $gestion = Gestion::find($id);
        $day=date('w', strtotime($gestion->fecha_recojo));
        //$numero_de_dia = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m",$i),date("d",$i), date("Y",$i)) , 0 );
        //domingo = 0, lunes = 1, etc
        $carriers = null;
        $tipo_servicio = null;
        if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 4){
            if($day != 0){
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                    $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->whereIn('id', [1,2,3])->get();
            }else{
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                    $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->where('id', 2)->get();
            }
        }else if($gestion->tipo_servicio == 2){
            if($day != 0){
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                    $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->whereIn('id', [2,3])->get();
            }else{
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                        $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->where('id', 2)->get();
            }
        }else if($gestion->tipo_servicio == 3){
            if($day != 0){
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                        $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->whereIn('id', [1,2])->get();
            }else{
                $carriers = Carrier::with(['tarifas' => function ($q) use ($gestion){
                    $q->where('tipo_servicio_id', $gestion->tipo_servicio);
                    }]
                )
                ->where('id', 2)->get();
            }
        }
        return response()->json([
            'carriers'=> $carriers,
            'dia' => $day,
            'tipo_servicio' => $gestion->tipo_servicio
        ]);
    }

    public function prueba(){
        return "hola";
    }
}
