<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\Producto;
use App\Models\Tienda;
use App\Models\DatosDelivery;
use App\Models\TrackerCambioEstandar;
use App\Mail\AceptoCambio;
use App\Mail\NoAceptoCambio;
use App\Mail\Tienda\AceptaPagoLogistico;
use App\Mail\Tienda\RechazaPagoLogistico;
use App\Mail\ServicioTecnico\ProductoArreglado;
use Mail;

class TiendaCambioController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function cambiosTodosErrorPorTienda(Request $request)
    {
        $user = \Auth::user();
        $gestiones_error = Gestion::with([
                                'productoGestion.producto',
                                'productoGestion.motivoDevolucion',
                                'nuevosProductos',
                                'cliente',
                                'tienda',
                                'trackerDevolucion',
                                'delivery'
                            ])
                            ->where('tienda_id', $user->tienda_id)
                            ->whereIn('tipo_servicio', [1,4])
                            ->where('confirmacion_web', true)
                            ->where('confirmacion_tienda', false)
                            ->paginate(2);
        return $gestiones_error;
    }

    public function aceptarCambioError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => true
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        Mail::to($this->correo_prod)->send(new AceptaPagoLogistico($gestion, $tienda));
        return $gestion;
    }

    public function denegarCambioError(Request $request, $id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => false
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        $cliente = DatosDelivery::find($gestion->datos_delivery_id);
        Mail::to($gestion->delivery->correo)->send(new RechazaPagoLogistico($gestion, $tienda, $cliente));
        return $gestion;
    }

    public function allCambios(Request $request){
        $user = \Auth::user();
        $gestiones = Gestion::with([
                            'productoGestion.producto',
                            'productoGestion.motivoDevolucion',
                            'nuevosProductos.producto',
                            'cliente',
                            'tienda',
                            'trackerCambioEstandar',
                            'servicioLogistico',
                            'delivery' => function ($q) use ($request){
                                if (isset($request->cliente) && $request->cliente !== '') {
                                    $q->where('nombres','like', '%'.$request->cliente.'%');
                                }
                            }
                        ])
                        ->where(function ($q) use ($request){
                            if (isset($request->codigo) && $request->codigo !== '') {
                                $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                            }
                        })
                        ->where('tienda_id', $user->tienda_id)
                        ->whereIn('tipo_servicio', [1,4])
                        ->where('en_proceso', true)
                        ->where('confirmacion_tienda', true)
                        ->where('confirmacion_web', true)
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return $gestiones;
    }

    public function updateState(Request $request, $id){
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($id);
        $tracker = TrackerCambioEstandar::where('gestion_id',$id)->first();
        $tienda = Tienda::find($gestion->tienda_id);
        $estado_gestion = $gestion->estado;
        switch($estado_gestion) {
            
            case(4):
                $gestion->update([
                    'estado' => 5
                ]);
                $tracker->update([
                    'producto_devuelto' => true,
                    'nombre_estado' => 'Producto devuelto a tienda.',
                    'estado' => 5
                ]);
                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
                break;

            case(5):
                $gestion->update([
                    'estado' => 6,
                ]);
                $tracker->update([
                    'cambio_aceptado' => true,
                    'nombre_estado' => 'Cambio Aceptado',
                    'estado' => 6
                ]);
                if($gestion->tipo_servicio == 4){
                    Mail::to($gestion_actual->delivery->correo)->send(new ProductoArreglado($gestion, $gestion->delivery, $tienda));
                }else{
                    Mail::to($gestion->delivery->correo)->send(new AceptoCambio($gestion));
                }
                
                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
                break;
             default:
                $msg = 'Something went wrong.';
        }
    }

    public function aceptarCambio($gestion_id, $tracker_id)
    {
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion_id);
        $tracker = TrackerCambioEstandar::find($tracker_id);
        $tienda = Tienda::find($gestion->tienda_id);
        $gestion->update([
            'estado' => 6,
        ]);
        $tracker->update([
            'cambio_aceptado' => true,
            'nombre_estado' => 'Cambio Aceptado',
            'estado' => 6
        ]);
        if($gestion->tipo_servicio == 4){
            Mail::to($gestion_actual->delivery->correo)->send(new ProductoArreglado($gestion, $gestion->delivery, $tienda));
        }else{
            Mail::to($gestion->delivery->correo)->send(new AceptoCambio($gestion));
        }
        
        return response()->json([
            'state'=> 3,
            'message' => 'Cambio aceptado correctamente.'
        ]);
    }

    public function show($id)
    {
        $gestion = Gestion::find($id);
        return $gestion;
    }

    public function rechazarCambio(Request $request, $id)
    {
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($id);
        $motivo_final = $request->motivo.'-'.$request->comentario;                                    
        $tracker = TrackerCambioEstandar::where('gestion_id', $gestion->id)->first();
        Mail::to($gestion->delivery->correo)->send(new NoAceptoCambio($gestion, $motivo_final));
        $tracker->update([
            'nombre_estado' => 'Se rechazó la operación por '.$request->motivo.'-'.$request->comentario,
            'estado' => 99
        ]);
        $gestion->update([
            'estado' => 99,
            'finalizado' => true,
            'en_proceso' => false
        ]);
        
        return response()->json([
            'state'=> 3,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }
}