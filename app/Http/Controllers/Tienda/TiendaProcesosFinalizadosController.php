<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gestion;

class TiendaProcesosFinalizadosController extends Controller
{
    public function getAll(Request $request){
        $user = \Auth::user();
        $gestiones = Gestion::with([
                        'productoGestion.producto',
                        'nuevosProductos.producto', 
                        'cliente', 
                        'tienda',
                        'delivery' => function ($q) use ($request){
                            if (isset($request->cliente) && $request->cliente !== '') {
                                $q->where('nombres','like', '%'.$request->cliente.'%');
                            }
                        }
                    ])
                    ->where(function ($q) use ($request){
                        if (isset($request->codigo) && $request->codigo !== '') {
                            $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                        }
                    })
                    ->where('tienda_id', $user->tienda_id)
                    ->where('finalizado', true)
                    ->orderBy('created_at', 'desc')
                    ->paginate(10);
        return $gestiones;
    }
}