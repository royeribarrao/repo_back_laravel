<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\TrackerDevolucion;
use App\Models\Tienda;
use App\Models\DatosDelivery;
use App\Mail\AceptoDevolucion;
use App\Mail\NoAceptoDevolucion;
use App\Mail\NoAceptoCambio;
use App\Mail\Tienda\AceptaPagoLogistico;
use App\Mail\Tienda\RechazaPagoLogistico;
use Mail;

class TiendaDevolucionController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }
    
    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function allDevoluciones(Request $request){
        $user = $this->getAuthenticated();
        $gestiones = Gestion::with([
                                'productoGestion.producto',
                                'productoGestion.motivoDevolucion', 
                                'cliente', 
                                'tienda',
                                'trackerDevolucion',
                                'servicioLogistico',
                                'delivery'
                            ])
                            ->where(function ($q) use ($request){
                                if (isset($request->codigo) && $request->codigo != '') {
                                    $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                                }
                            })
                            ->where(function ($q) use ($request){
                                if (isset($request->tienda_id) && $request->tienda_id != '') {
                                    $q->where('tienda_id','=', $request->tienda_id);
                                }
                            })
                            ->where('tienda_id', '=', $user->tienda_id)
                            ->where('tipo_servicio', '=', 3)
                            ->where('en_proceso', true)
                            ->where('confirmacion_web', true)
                            ->where('confirmacion_tienda', true)
                            ->orderBy('created_at', 'desc')
                            ->paginate(15);
        return $gestiones;
    }

    public function allDevolucionesPorConfirmar(Request $request){
        $user = $this->getAuthenticated();
        $gestiones = Gestion::with([
                                'productoGestion.producto',
                                'productoGestion.motivoDevolucion', 
                                'cliente', 
                                'tienda',
                                'trackerDevolucion',
                                'servicioLogistico',
                                'delivery'
                            ])
                            ->where(function ($q) use ($request){
                                if (isset($request->codigo) && $request->codigo != '') {
                                    $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                                }
                            })
                            ->where(function ($q) use ($request){
                                if (isset($request->tienda_id) && $request->tienda_id != '') {
                                    $q->where('tienda_id','=', $request->tienda_id);
                                }
                            })
                            ->where('tienda_id', $user->tienda_id)
                            ->where('tipo_servicio', 3)
                            ->where('en_proceso', true)
                            ->where('confirmacion_web', true)
                            ->where('confirmacion_tienda', false)
                            ->orderBy('created_at', 'desc')
                            ->paginate(2);
        return $gestiones;
    }

    public function updateState(Request $request, $id){
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
        $tracker = TrackerDevolucion::where('gestion_id',$gestion->id)->first();
        $estado_gestion = $gestion->estado;
        switch($estado_gestion) {
 
            // case(2):
                 
            //     $gestion->update([
            //         'estado' => 3
            //     ]);
            //     $tracker->update([
            //         'en_camino' => true,
            //         'nombre_estado' => 'En camino',
            //         'estado' => 3
            //     ]);

            //     return response()->json([
            //         'state'=> 3,
            //         'message' => 'Gestión actualizado correctamente.'
            //     ]);
 
            //     break;

            // case(3):
                
            //     $gestion->update([
            //         'estado' => 4
            //     ]);
            //     $tracker->update([
            //         'producto_recogido' => true,
            //         'nombre_estado' => 'Producto Recogido',
            //         'estado' => 4
            //     ]);

            //     return response()->json([
            //         'state'=> 3,
            //         'message' => 'Gestión actualizado correctamente.'
            //     ]);
    
            //     break;
            
            case(4):
            
                $gestion->update([
                    'estado' => 5
                ]);
                $tracker->update([
                    'producto_devuelto' => true,
                    'nombre_estado' => 'Producto devuelto',
                    'estado' => 5
                ]);
                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
                break;

            case(5):
                $gestion->update([
                    'estado' => 6,
                ]);
                $tracker->update([
                    'devolucion_aceptada' => true,
                    'nombre_estado' => 'Devolución Aceptada',
                    'estado' => 6
                ]);
                Mail::to($gestion->delivery->correo)->send(new AceptoDevolucion($gestion));
                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
                break;
            
            case(6):
                $gestion->update([
                    'estado' => 7,
                    'en_proceso' => false,
                    'finalizado' => true
                ]);
                $tracker->update([
                    'dinero_devuelto' => true,
                    'nombre_estado' => 'Dinero Devuelto',
                    'estado' => 7
                ]);
                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;
 
            default:
                $msg = 'Something went wrong.';
        }
    }

    public function devolverProducto2(Request $request, $gestion_id, $tracker_id)
    {
        return $request->all();
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion_id);
        $tracker = TrackerDevolucion::find($tracker_id);
        $gestion->update([
            'estado' => 6,
        ]);
        $tracker->update([
            'devolucion_aceptada' => true,
            'nombre_estado' => 'Devolución Aceptada',
            'estado' => 6
        ]);
        Mail::to($gestion->delivery->correo)->send(new AceptoDevolucion($gestion));
        return response()->json([
            'state'=> 3,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }

    public function productoDevuelto($gestion_id, $tracker_id)
    {
        $gestion = Gestion::find($gestion_id);
        $tracker = TrackerDevolucion::find($tracker_id);
        $gestion->update([
            'estado' => 5
        ]);
        $tracker->update([
            'producto_devuelto' => true,
            'nombre_estado' => 'Producto devuelto',
            'estado' => 5
        ]);
        return response()->json([
            'state'=> true,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }

    public function productoNoDevuelto($gestion_id, $tracker_id)
    {
        $gestion = Gestion::find($gestion_id);
        $tracker = TrackerDevolucion::find($tracker_id);
        $gestion->update([
            'estado' => 20
        ]);
        $tracker->update([
            'nombre_estado' => 'Producto no devuelto',
            'estado' => 8
        ]);
        return response()->json([
            'state'=> true,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }

    public function aceptarDevolucionError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => true
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        Mail::to($this->correo_prod)->send(new AceptaPagoLogistico($gestion, $tienda));
        return $gestion;
    }

    public function aceptarDevolucion($gestion_id, $tracker_id){
        $gestion = Gestion::with('servicioLogistico.carrier', 'delivery')->find($gestion_id);
        $tracker = TrackerDevolucion::find($tracker_id);
        $gestion->update([
            'estado' => 6,
        ]);
        $tracker->update([
            'devolucion_aceptada' => true,
            'nombre_estado' => 'Devolución Aceptada',
            'estado' => 6
        ]);
        Mail::to($gestion->delivery->correo)->send(new AceptoDevolucion($gestion));
        return response()->json([
            'state'=> true,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }

    public function denegarDevolucionError(Request $request, $id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => false
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        $cliente = DatosDelivery::find($gestion->datos_delivery_id);
        Mail::to($gestion->delivery->correo)->send(new RechazaPagoLogistico($gestion, $tienda, $cliente));
        return $gestion;
    }

    public function rechazarDevolucion(Request $request, $id)
    {
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($id);
        $motivo_final = $request->motivo.'-'.$request->comentario;                                    
        $tracker = TrackerDevolucion::where('gestion_id', $gestion->id)->first();
        Mail::to($gestion->delivery->correo)->send(new NoAceptoDevolucion($gestion, $motivo_final));
        $tracker->update([
            'nombre_estado' => 'Se rechazó la operación por '.$request->motivo.'-'.$request->comentario,
            'estado' => 8
        ]);
        $gestion->update([
            'estado' => 20,
            'finalizado' => true,
            'en_proceso' => false
        ]);
        
        return response()->json([
            'state'=> 3,
            'message' => 'Gestión actualizado correctamente.'
        ]);
    }

    public function devolverDinero(Request $request, $gestionId, $trackerId)
    {
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestionId);                                
        $tracker = TrackerDevolucion::find($trackerId);

        $estado = $request->aceptado;
        $gestion->update([
            'estado' => $estado ? 7 : 99,
            'en_proceso' => $estado ? false : true,
            'finalizado' => $estado ? true : false
        ]);
        $tracker->update([
            'dinero_devuelto' => $estado ? true : false,
            'nombre_estado' => 'Dinero Devuelto',
            'estado' => $estado ? 7 : 99,
        ]);
        return response()->json([
            'state'=> true,
            'message' => 'Gestión actualizado correctamente.'
        ]);

    }
}
