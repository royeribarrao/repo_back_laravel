<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;

class TiendaPerfilController extends Controller
{
    public function get()
    {
        $user = \Auth::user();
        return $user;
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::find($id);
        $user->update([
            'dni' => $data['dni'],
            'email' => $data['email'],
            'father_lastname' => $data['father_lastname'],
            'name' => $data['name'],
            'password' => $data['password'],
            'phone' => $data['phone']
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Perfil actualizado correctamente'
        ]);
    }
}