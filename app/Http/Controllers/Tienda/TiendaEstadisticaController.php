<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\ProductoGestion;
use App\Models\ServicioLogistico;
use App\Models\NuevoProductoServicioCambio;

class TiendaEstadisticaController extends Controller
{
    public function getAuthenticated()
    {
        $usuario = \Auth::user();
        $usuario->tienda = \Auth::user()->tienda;
        return $usuario;
    }

    public function fechas(Request $request)
    {
        return $request->all();
    }

    public function porcentajeByTipoProcesos(Request $request)
    {
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }
        $user = \Auth::user();

        $p_estandares = Gestion::where('tipo_servicio', 1)
                    ->where('tienda_id', $user->tienda_id)
                    ->where('confirmacion_web', 1)
                    ->where('confirmacion_tienda', 1)
                    ->where(function ($q) use($fecha_inicio, $fecha_fin){
                        if ($fecha_inicio && $fecha_fin) {
                            $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                        }
                    })
                    ->count();
        $p_deluxes = Gestion::where('tipo_servicio', 2)
                    ->where('tienda_id', $user->tienda_id)
                    ->where('confirmacion_web', 1)
                    ->where('confirmacion_tienda', 1)
                    ->where(function ($q) use($fecha_inicio, $fecha_fin){
                        if ($fecha_inicio && $fecha_fin) {
                            $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                        }
                    })
                    ->count();
        $p_devoluciones = Gestion::where('tipo_servicio', 3)
                    ->where('tienda_id', $user->tienda_id)
                    ->where('confirmacion_web', 1)
                    ->where('confirmacion_tienda', 1)
                    ->where(function ($q) use($fecha_inicio, $fecha_fin){
                        if ($fecha_inicio && $fecha_fin) {
                            $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                        }
                    })
                    ->count();
        $p_servicios_tecnicos = Gestion::where('tipo_servicio', 4)
                    ->where('tienda_id', $user->tienda_id)
                    ->where('confirmacion_web', 1)
                    ->where('confirmacion_tienda', 1)
                    ->where(function ($q) use($fecha_inicio, $fecha_fin){
                        if ($fecha_inicio && $fecha_fin) {
                            $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                        }
                    })
                    ->count();
        $total_gestiones = Gestion::where('tienda_id', $user->tienda_id)
                            ->where('confirmacion_web', 1)
                            ->where('confirmacion_tienda', 1)
                            ->count();

        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["Estándar ($p_estandares)", $p_estandares],
                ["Express ($p_deluxes)", $p_deluxes],
                ["Devolución ($p_devoluciones)", $p_devoluciones],
                ["Servicio técnico ($p_servicios_tecnicos)", $p_servicios_tecnicos]
            ],
            'estandar' => $p_estandares,
            'express' => $p_deluxes,
            'devolucion' => $p_devoluciones,
            'servicioTecnico' => $p_servicios_tecnicos,
        ]);
    }

    public function totalGestiones(Request $request){
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }

        $user = \Auth::user();
        $total_gestiones = Gestion::where('tienda_id', $user->tienda_id)
            ->where('confirmacion_web', 1)
            ->where('confirmacion_tienda', 1)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->get()
            ->count();
        $total_cambios = Gestion::where('tienda_id', $user->tienda_id)
            ->where('confirmacion_web', 1)
            ->where('confirmacion_tienda', 1)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->whereIn('tipo_servicio', [1, 2])
            ->get()
            ->count();
        $total_devoluciones = Gestion::where('tienda_id', $user->tienda_id)
            ->where('confirmacion_web', 1)
            ->where('confirmacion_tienda', 1)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->where('tipo_servicio', 3)
            ->get()
            ->count();
        $total_servicios_tecnicos = Gestion::where('tienda_id', $user->tienda_id)
            ->where('confirmacion_web', 1)
            ->where('confirmacion_tienda', 1)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->where('tipo_servicio', 4)
            ->get()
            ->count();
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'gestiones' => $total_gestiones,
            'cambios' => $total_cambios,
            'devoluciones' => $total_devoluciones,
            'servicioTecnico' => $total_servicios_tecnicos
        ]);
    }

    public function porcentajeByEstadoProcesos(Request $request){
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }

        $user = \Auth::user();
        $gestiones_proceso = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('en_proceso', 1)
                            ->where('confirmacion_web', 1)
                            ->where('confirmacion_tienda', 1)
                            ->get()
                            ->count();
        $gestiones_aceptado = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->get()
                            ->count();
        $gestiones_rechazado = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('estado', 20)
                            ->get()
                            ->count();
        $total_gestiones = Gestion::where('tienda_id', $user->tienda_id)
            ->where('confirmacion_web', 1)
            ->where('confirmacion_tienda', 1)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->get()
            ->count();
        $porcentaje_gestiones_proceso = $gestiones_proceso/$total_gestiones;
        $porcentaje_gestiones_finalizado = $gestiones_aceptado/$total_gestiones;
        $porcentaje_gestiones_cancelado = $gestiones_rechazado/$total_gestiones;

        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["En proceso ($gestiones_proceso)", $gestiones_proceso],
                ["Aceptado ($gestiones_aceptado)", $gestiones_aceptado],
                ["Rechazado ($gestiones_rechazado)", $gestiones_rechazado]
            ],
            'proceso' => $gestiones_proceso,
            'aceptado' => $gestiones_aceptado,
            'rechazado' => $gestiones_rechazado
        ]);
    }

    public function porcentajeAceptacion(Request $request){
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }

        $user = \Auth::user();
        $total_cambios = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('confirmacion_tienda', 1)
                            ->whereIn('tipo_servicio', [1, 2])
                            ->get()
                            ->count();
        $total_cambios_aceptados = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->whereIn('tipo_servicio', [1, 2])
                            ->get()
                            ->count();
        $total_devoluciones = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('confirmacion_tienda', 1)
                            ->where('tipo_servicio', 3)
                            ->get()
                            ->count();
        $total_devoluciones_aceptadas = Gestion::where('tienda_id', $user->tienda_id)
                            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                                if ($fecha_inicio && $fecha_fin) {
                                    $q->whereBetween('fecha_recojo', [$fecha_inicio, $fecha_fin]);
                                }
                            })
                            ->where('confirmacion_web', 1)
                            ->where('estado', '!=', 20)
                            ->where('estado', '>=', 6)
                            ->where('tipo_servicio', 3)
                            ->get()
                            ->count();
        $porcentaje_devoluciones_aceptadas = $total_devoluciones ? $total_devoluciones_aceptadas/$total_devoluciones : 0;
        $porcentaje_cambios_aceptados = $total_cambios ? $total_cambios_aceptados/$total_cambios : 0;

        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'cambios_aceptados' => round($porcentaje_cambios_aceptados, 2),
            'devoluciones_aceptadas' => round($porcentaje_devoluciones_aceptadas, 2),
            'tienda_id' => $user->tienda_id
        ]);
    }

    public function gestionesByMotivo(Request $request){
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }

        $user = \Auth::user();
        $motivo_1 = ProductoGestion::where('motivo', 1)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->where(function ($q) use($fecha_inicio, $fecha_fin){
                    if ($fecha_inicio && $fecha_fin) {
                        $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                    }
                })
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->where('confirmacion_web', 1)
                ->where('estado', true)
                ->get()
                ->count();
        $motivo_2 = ProductoGestion::where('motivo', 2)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->where(function ($q) use($fecha_inicio, $fecha_fin){
                    if ($fecha_inicio && $fecha_fin) {
                        $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                    }
                })
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->where('confirmacion_web', 1)
                ->where('estado', true)
                ->get()
                ->count();
        $motivo_3 = ProductoGestion::where('motivo', 3)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->where(function ($q) use($fecha_inicio, $fecha_fin){
                    if ($fecha_inicio && $fecha_fin) {
                        $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                    }
                })
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->where('confirmacion_web', 1)
                ->where('estado', true)
                ->get()
                ->count();
        $motivo_4 = ProductoGestion::where('motivo', 4)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->where(function ($q) use($fecha_inicio, $fecha_fin){
                    if ($fecha_inicio && $fecha_fin) {
                        $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                    }
                })
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->where('confirmacion_web', 1)
                ->where('estado', true)
                ->get()
                ->count();
        $motivo_5 = ProductoGestion::where('motivo', 5)
                ->where('gestiones.tienda_id', $user->tienda_id)
                ->where(function ($q) use($fecha_inicio, $fecha_fin){
                    if ($fecha_inicio && $fecha_fin) {
                        $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                    }
                })
                ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
                ->where('confirmacion_web', 1)
                ->where('estado', true)
                ->get()
                ->count();
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'data' => [
                ["No me quedó bien ($motivo_1)", $motivo_1],
                ["No era lo que esperaba ($motivo_2)", $motivo_2],
                ["Pedí otro producto por error ($motivo_3)", $motivo_3],
                ["Me entregaron otro producto ($motivo_4)", $motivo_4],
                ["Producto dañado ($motivo_5)", $motivo_5]
            ],
            'motivo1' => $motivo_1,
            'motivo2' => $motivo_2,
            'motivo3' => $motivo_3,
            'motivo4' => $motivo_4,
            'motivo5' => $motivo_5
        ]);
    }

    public function montoTotalByTipoProceso(Request $request){
        $fecha_inicio = null;
        $fecha_fin = null;
        if(!empty($request->all())){
            $fecha = $request->all();
            $fecha_inicio = $fecha[0];
            $fecha_fin = $fecha[1];
        }

        $user = \Auth::user();
        $monto_total_cambios = ProductoGestion::where('gestiones.tienda_id', $user->tienda_id)
            ->whereIn('gestiones.tipo_servicio', [1, 2])
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
            ->sum('productos_gestion.precio');
        $monto_total_devoluciones = ProductoGestion::where('gestiones.tienda_id', $user->tienda_id)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->where('tipo_servicio', 3)
            ->join('gestiones', 'productos_gestion.gestion_id', 'gestiones.id')
            ->sum('productos_gestion.precio');
        $monto_total_nuevos_productos = NuevoProductoServicioCambio::where('gestiones.tienda_id', $user->tienda_id)
            ->where(function ($q) use($fecha_inicio, $fecha_fin){
                if ($fecha_inicio && $fecha_fin) {
                    $q->whereBetween('gestiones.fecha_recojo', [$fecha_inicio, $fecha_fin]);
                }
            })
            ->where('products.tienda_id', $user->tienda_id)
            ->join('gestiones', 'nuevos_productos_servicio_cambio.gestion_id', 'gestiones.id')
            ->leftJoin('products', 'nuevos_productos_servicio_cambio.sku', 'products.sku')
            ->sum('products.price');
        return response()->json([
            'state'=> 1,
            'message' => 'Información servida.',
            'total_cambios' => $monto_total_cambios,
            'total_devoluciones' => $monto_total_devoluciones,
            'total_nuevos_productos' => sprintf("%.2f", $monto_total_nuevos_productos - $monto_total_cambios),
            'upselling' => round((1-$monto_total_cambios/$monto_total_nuevos_productos)*100, 2)
        ]);
    }
}