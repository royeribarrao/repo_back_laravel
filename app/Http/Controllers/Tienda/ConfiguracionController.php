<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\ConfiguracionTienda;
use App\Models\TiendaDiasAtencion;
use App\Models\Gestion;
use App\Models\MotivoDevolucion;
use App\Models\TiendaMotivoDevolucion;

class ConfiguracionController extends Controller
{
    public function allMotivos()
    {
        $motivos = MotivoDevolucion::all();
        return $motivos;
    }

    public function store(Request $request){
        $data = $request->all();
        $user = \Auth::user();
        $dias_atencion = TiendaDiasAtencion::create([
            'tienda_id' => $user->tienda_id,
            'lunes' => in_array("A", $data['dias_atencion']) ? true: false,
            'martes' => in_array("B", $data['dias_atencion']) ? true: false,
            'miercoles' => in_array("C", $data['dias_atencion']) ? true: false,
            'jueves' => in_array("D", $data['dias_atencion']) ? true: false,
            'viernes' => in_array("E", $data['dias_atencion']) ? true: false,
            'sabado' => in_array("F", $data['dias_atencion']) ? true: false,
            'domingo' => in_array("G", $data['dias_atencion']) ? true: false
        ]);
        $configuracion = ConfiguracionTienda::create([
            'tienda_id' => $user->tienda_id,
            'devolucion' => $data['devolucion'],
            'estandar' => $data['estandar'],
            'deluxe' => $data['deluxe'],
            'servicio_tecnico' => $data['servicio_tecnico'],
            'plazo' => $data['plazo'],
            'tienda_dias_atencion_id' => $dias_atencion->id,
            'compra_no_web' => true
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Configuracion creada correctamente'
        ]);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $configuracion = ConfiguracionTienda::find($id);
        $configuracion->update([
            'devolucion' => $data['devolucion'],
            'estandar' => $data['estandar'],
            'deluxe' => $data['deluxe'],
            'servicio_tecnico' => $data['servicio_tecnico'],
            'plazo' => $data['plazo'],
            'compra_no_web' => $data['compra_no_web'],
            'requiere_boleta' => $data['requiere_boleta']
        ]);
        $dias_atencion = TiendaDiasAtencion::find($configuracion->tienda_dias_atencion_id);
        $dias_atencion->update([
            'lunes' => in_array("A", $data['dias_atencion']) ? true: false,
            'martes' => in_array("B", $data['dias_atencion']) ? true: false,
            'miercoles' => in_array("C", $data['dias_atencion']) ? true: false,
            'jueves' => in_array("D", $data['dias_atencion']) ? true: false,
            'viernes' => in_array("E", $data['dias_atencion']) ? true: false,
            'sabado' => in_array("F", $data['dias_atencion']) ? true: false,
            'domingo' => in_array("G", $data['dias_atencion']) ? true: false
        ]);

        foreach($data['motivosTienda'] as $key => $item)
        {
            $tienda = TiendaMotivoDevolucion::find($item['id']);
            $tienda->update([
                'activo' => $item['activo'],
                'pago_logistico' => $item['pago_logistico']
            ]);
        }

        return response()->json([
            'state'=> 1,
            'message' => 'Configuracion actualizada correctamente'
        ]);
    }

    public function getConfig(){
        $user = \Auth::user();
        $configuracion = ConfiguracionTienda::with(['diasAtencion', 'tiendaMotivoDevolucion.motivo'])
                        ->where('tienda_id', $user->tienda_id)
                        ->first();
        return $configuracion;
    }

    public function getConfiguracionByTienda($id){
        $configuracion = ConfiguracionTienda::with(['diasAtencion', 'tiendaMotivoDevolucion.motivo', 'tiendaDatosFee'])
                        ->where('tienda_id', $id)->first();
        return $configuracion;
    }

    public function getConfiguracionByCodigoRepo($codigo){
        $gestion = Gestion::where('codigo_repo', $codigo)->first();
        $configuracion = TiendaDiasAtencion::where('tienda_id', $gestion->tienda_id)->first();
        return $configuracion;
    }
}