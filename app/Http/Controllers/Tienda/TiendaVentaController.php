<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Contracts\SaleInterface as SaleService;
use App\Models\Sale;
use App\Models\ConfiguracionTienda;
use DB;

class TiendaVentaController extends Controller
{
    protected $saleService;

    public function __construct(
        SaleService $sale
    )
    {
        $this->saleService = $sale;
    }

    public function get(Request $request) {
        $req = $request->all();
        $sales = $this->saleService->paginate(15, $req);    
        return response()->json($sales);
    }

    public function getAll(Request $request) {
        $sales = $this->saleService->all();    
        return response()->json($sales);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {

        $this->saleService->create($request->all());           
        return response()->json([
            'state'=> 1,
            'message' => 'Venta creada correctamente.'
        ]);
    }
    
    public function show($id) {
        $sale = $this->saleService->find($id);
        return response()->json($sale);
    }

    public function update($id, Request $request) {
        $this->saleService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Venta actualizada correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->saleService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Venta actualizado correctamente.'
        ]);
    }

    public function uploadCsv(Request $request)
    {
        $files = $request->file('file_csv');
        $user = $this->getAuthenticated();
        $integracion = ConfiguracionTienda::where('tienda_id', $user->tienda->id)->first();
        if($integracion->integracion_id == 1){
            return $this->subirArchivoFormatoRepo($files);
        }
        if($integracion->integracion_id == 2){
            return $this->subirArchivoFormatoWooCommerce($files);
        }
        if($integracion->integracion_id == 3){
            return $this->subirArchivoFormatoShopify($files);
        }
        // return array_map( 
        //     function($file) {
        //         if ($file) {
        //             $filename = $file->getClientOriginalName();
        //             $extension = $file->getClientOriginalExtension();
        //             $tempPath = $file->getRealPath();
        //             $fileSize = $file->getSize();
        //             $this->checkUploadedFileProperties($extension, $fileSize);
        //             $location = 'uploads';
        //             $file->move($location, $filename);
        //             $filepath = public_path($location . "/" . $filename);
        //             $file = fopen($filepath, "r");
        //             $importData_arr = array();
        //             $i = 0;
        //             while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
        //                 $num = count($filedata);
        //                 if ($i == 0) {
        //                     $i++;
        //                     continue;
        //                 }
        //                 for ($c = 0; $c < $num; $c++) {
        //                     $importData_arr[$i][] = $filedata[$c];
        //                 }
        //                 $i++;
        //             }
        //             $j = 0;
        //             $user = \Auth::user();
                
        //             foreach ($importData_arr as $importData) {
                        
        //                 try {
        //                     DB::beginTransaction();
        //                     if(count($importData) > 1){
                                
        //                         $venta = Sale::where('tienda_id', $user->tienda_id)
        //                             ->where('codigo_compra', $importData[0])
        //                             ->where('sku', $importData[2])
        //                             ->where('nombre_producto', $importData[3])
        //                             ->first();
                                
        //                         if(!$venta){
        //                             $j++;
        //                             Sale::create([
        //                                 'tienda_id' => $user->tienda_id,
        //                                 'codigo_compra' => $importData[0],
        //                                 'fecha_compra' => $importData[1],
        //                                 'sku' => $importData[2],
        //                                 'nombre_producto' => $importData[3],
        //                                 'marca' => '',
        //                                 'cantidad' => $importData[4],
        //                                 'precio_pagado' => $importData[5],
        //                                 'numero_documento' => $importData[13],
        //                                 'nombre_cliente' => $importData[6],
        //                                 'apellido_cliente' => $importData[7],
        //                                 'telefono' => $importData[8],
        //                                 'email' => $importData[9],
        //                                 'direccion' => $importData[10],
        //                                 'distrito' => $importData[11],
        //                                 'provincia' => '',
        //                                 'departamento' => ''
        //                             ]);
        //                         }
                                
        //                     }

        //                     DB::commit();
        //                 } catch (\Exception $e) {
        //                     DB::rollBack();
        //                 }
        //             }

        //             return response()->json([
        //                 'message' => $j == 0 ? "Todas las ventas en este archivo ya fueron registrados.": "$j ventas subidos satisfactoriamente"
        //             ]);
        //         } else {
        //             throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
        //         }
        //     },
        //     $files
        // );
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('El archivo no fue cargado', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Extensión de archivo invalido', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }

    public function subirArchivoFormatoRepo($files)
    {
        return array_map( 
            function($file) {
                if ($file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $tempPath = $file->getRealPath();
                    $fileSize = $file->getSize();
                    $this->checkUploadedFileProperties($extension, $fileSize);
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location . "/" . $filename);
                    $file = fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    $j = 0;
                    $user = \Auth::user();
                
                    foreach ($importData_arr as $importData) {
                        
                        try {
                            DB::beginTransaction();
                            if(count($importData) > 1){
                                
                                $venta = Sale::where('tienda_id', $user->tienda_id)
                                    ->where('codigo_compra', $importData[0])
                                    ->where('sku', $importData[2])
                                    ->where('nombre_producto', $importData[3])
                                    ->first();
                                
                                if(!$venta){
                                    $j++;
                                    Sale::create([
                                        'tienda_id' => $user->tienda_id,
                                        'codigo_compra' => $importData[0],
                                        'fecha_compra' => $importData[1],
                                        'sku' => $importData[2],
                                        'nombre_producto' => $importData[3],
                                        'marca' => '',
                                        'cantidad' => $importData[4],
                                        'precio_pagado' => $importData[5],
                                        'numero_documento' => $importData[13],
                                        'nombre_cliente' => $importData[6],
                                        'apellido_cliente' => $importData[7],
                                        'telefono' => $importData[8],
                                        'email' => $importData[9],
                                        'direccion' => $importData[10],
                                        'distrito' => $importData[11],
                                        'provincia' => '',
                                        'departamento' => ''
                                    ]);
                                }
                                
                            }

                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                        }
                    }

                    return response()->json([
                        'message' => $j == 0 ? "Todas las ventas en este archivo ya fueron registrados.": "$j ventas subidos satisfactoriamente"
                    ]);
                } else {
                    throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
                }
            },
            $files
        );
    }

    public function subirArchivoFormatoWooCommerce($files)
    {
        return array_map( 
            function($file) {
                if ($file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $tempPath = $file->getRealPath();
                    $fileSize = $file->getSize();
                    $this->checkUploadedFileProperties($extension, $fileSize);
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location . "/" . $filename);
                    $file = fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    $j = 0;
                    $user = \Auth::user();
                
                    foreach ($importData_arr as $importData) {
                        
                        try {
                            DB::beginTransaction();
                            if(count($importData) > 1){
                                
                                $venta = Sale::where('tienda_id', $user->tienda_id)
                                    ->where('codigo_compra', $importData[0])
                                    ->where('sku', $importData[2])
                                    ->where('nombre_producto', $importData[3])
                                    ->first();
                                
                                if(!$venta){
                                    $j++;
                                    Sale::create([
                                        'tienda_id' => $user->tienda_id,
                                        'codigo_compra' => $importData[0],
                                        'fecha_compra' => $importData[1],
                                        'sku' => $importData[2],
                                        'nombre_producto' => $importData[3],
                                        'marca' => '',
                                        'cantidad' => $importData[4],
                                        'precio_pagado' => $importData[5],
                                        'numero_documento' => $importData[13],
                                        'nombre_cliente' => $importData[6],
                                        'apellido_cliente' => $importData[7],
                                        'telefono' => $importData[8],
                                        'email' => $importData[9],
                                        'direccion' => $importData[10],
                                        'distrito' => $importData[11],
                                        'provincia' => '',
                                        'departamento' => ''
                                    ]);
                                }
                                
                            }

                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                        }
                    }

                    return response()->json([
                        'message' => $j == 0 ? "Todas las ventas en este archivo ya fueron registrados.": "$j ventas subidos satisfactoriamente"
                    ]);
                } else {
                    throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
                }
            },
            $files
        );
    }

    public function subirArchivoFormatoShopify($files)
    {
        return array_map( 
            function($file) {
                if ($file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $tempPath = $file->getRealPath();
                    $fileSize = $file->getSize();
                    $this->checkUploadedFileProperties($extension, $fileSize);
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location . "/" . $filename);
                    $file = fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    $j = 0;
                    $user = \Auth::user();
                    $linea_anterior = [null];
                    foreach ($importData_arr as $importData) {
                        if((gettype($importData[0]) == 'string') && (count($importData) == 1)){
                            $cadena = $importData[0];
                            $importData = explode(",", $cadena);
                        }
                        
                        try {
                            DB::beginTransaction();
                            if(count($importData) > 1){
                                $venta = Sale::where('tienda_id', $user->tienda_id)
                                    ->where('codigo_compra', $importData[0])
                                    ->where('sku', $importData[20])
                                    ->where('nombre_producto', $importData[17])
                                    ->first();
                                
                                if(!$venta){
                                    if(!($importData[17] == 'Tip')){
                                        $j++;
                                        if($importData[0] === $linea_anterior[0]){
                                            Sale::create([
                                                'tienda_id' => $user->tienda_id,
                                                'codigo_compra' => $importData[0],
                                                'fecha_compra' => $linea_anterior[3],
                                                'sku' => $importData[20],
                                                'nombre_producto' => $importData[17],
                                                'marca' => '',
                                                'cantidad' => $importData[16],
                                                'precio_pagado' => $importData[18],
                                                'numero_documento' => '',
                                                'nombre_cliente' => $linea_anterior[34],
                                                'apellido_cliente' => '',
                                                'telefono' => $linea_anterior[43],
                                                'email' => $linea_anterior[1],
                                                'direccion' => $linea_anterior[35],
                                                'distrito' => $linea_anterior[39],
                                                'provincia' => '',
                                                'departamento' => '',
                                                'nombre_sin_espacios' => str_replace("-", "",str_replace(" ", "", $importData[17]))
                                            ]);
                                        }else{
                                            Sale::create([
                                                'tienda_id' => $user->tienda_id,
                                                'codigo_compra' => $importData[0],
                                                'fecha_compra' => $importData[3],
                                                'sku' => $importData[20],
                                                'nombre_producto' => $importData[17],
                                                'marca' => '',
                                                'cantidad' => $importData[16],
                                                'precio_pagado' => $importData[18],
                                                'numero_documento' => '',
                                                'nombre_cliente' => $importData[34],
                                                'apellido_cliente' => '',
                                                'telefono' => $importData[43],
                                                'email' => $importData[1],
                                                'direccion' => $importData[35],
                                                'distrito' => $importData[39],
                                                'provincia' => '',
                                                'departamento' => '',
                                                'nombre_sin_espacios' => str_replace("-", "",str_replace(" ", "", $importData[17]))
                                            ]);
                                        }
                                    }
                                }
                            }

                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                        }
                        if(!($importData[0] === $linea_anterior[0]))
                        {
                            $linea_anterior = $importData;
                        }
                    }

                    return response()->json([
                        'message' => $j == 0 ? "Todas las ventas en este archivo ya fueron registrados.": "$j ventas subidos satisfactoriamente"
                    ]);
                } else {
                    throw new \Exception('El archivo no fue importado', Response::HTTP_BAD_REQUEST);
                }
            },
            $files
        );
    }
}