<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\Tienda;
use App\Models\DatosDelivery;
use App\Models\TrackerCambioEstandar;
use App\Mail\AceptoCambio;
use App\Mail\Tienda\AceptaPagoLogistico;
use App\Mail\Tienda\RechazaPagoLogistico;
use Mail;

class TiendaCambioDeluxeController extends Controller
{
    protected $correo_dev;
    protected $correo_prod;

    public function __construct()
    {
        $this->correo_dev = config('constants.correo_desarrollo');
        $this->correo_prod = config('constants.correo_produccion');
    }
    
    public function allCambios(Request $request){
        $user = \Auth::user();
        $gestiones = Gestion::with([
                            'productoGestion.producto',
                            'productoGestion.motivoDevolucion',
                            'nuevosProductos.producto', 
                            'cliente', 
                            'tienda' => function ($q) use ($request){
                                if (isset($request->cliente) && $request->cliente !== '') {
                                    $q->where('nombres','like', '%'.$request->cliente.'%');
                                }
                            }, 
                            'trackerCambioDeluxe',
                            'servicioLogistico',
                            'delivery'
                        ])
                        ->where(function ($q) use ($request){
                            if (isset($request->codigo) && $request->codigo !== '') {
                                $q->where('codigo_repo','like', '%'.$request->codigo.'%');
                            }
                        })
                        ->where(function ($q) use ($request){
                            if (isset($request->tienda_id) && $request->tienda_id != '') {
                                $q->where('tienda_id','=', $request->tienda_id);
                            }
                        })
                        ->where('tienda_id', '=', $user->tienda_id)
                        ->where('tipo_servicio', 2)
                        ->where('en_proceso', true)
                        ->where('confirmacion_tienda', true)
                        ->where('confirmacion_web', true)
                        ->orderBy('created_at', 'desc')
                        ->paginate(15);
        return $gestiones;
    }

    public function aceptarCambioError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => true
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        Mail::to($this->correo_prod)->send(new AceptaPagoLogistico($gestion, $tienda));
        return $gestion;
    }

    public function denegarCambioError(Request $request, $id)
    {
        $gestion = Gestion::find($id);
        $gestion->update([
            'confirmacion_tienda' => false
        ]);
        $tienda = Tienda::find($gestion->tienda_id);
        $cliente = DatosDelivery::find($gestion->datos_delivery_id);
        Mail::to($gestion->delivery->correo)->send(new RechazaPagoLogistico($gestion, $tienda, $cliente));
        return $gestion;
    }

    public function updateState(Request $request, $id){
        $gestion = Gestion::with('servicioLogistico.carrier', 
                                            'tienda',
                                            'delivery')
                                            ->find($gestion->id);
        $tracker = TrackerCambioEstandar::where('gestion_id',$gestion->id)->first();
        
        $estado_gestion = $gestion->estado;
        
        switch($estado_gestion) {
            
            case(4):
            
                $gestion->update([
                    'estado' => 5
                ]);
                $tracker->update([
                    'producto_devuelto' => true,
                    'nombre_estado' => 'Producto devuelto',
                    'estado' => 5
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;

            // case(5):
            
            //     $gestion->update([
            //         'estado' => 6,
            //     ]);
            //     $tracker->update([
            //         'cambio_aceptado' => true,
            //         'nombre_estado' => 'Cambio Aceptado',
            //         'estado' => 6
            //     ]);
            case(7):
            
                    $gestion->update([
                        'estado' => 8,
                    ]);
                    $tracker->update([
                        'cambio_aceptado' => true,
                        'nombre_estado' => 'Cambio Aceptado',
                        'estado' => 8
                    ]);
                Mail::to($gestion->delivery->correo)->send(new AceptoCambio($gestion));

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;

             default:
                $msg = 'Something went wrong.';
        }
    }

    public function show($id)
    {
        $gestion = Gestion::find($id);
        return $gestion;
    }

    public function allCambiosPendientes()
    {
        $user = \Auth::user();
        $gestiones_pendientes = Gestion::with([
                    'productoGestion.producto',
                    'productoGestion.motivoDevolucion',
                    'nuevosProductos', 
                    'cliente', 
                    'tienda', 
                    'trackerCambioDeluxe'
                ])
                ->where('tienda_id', $user->tienda_id)
                ->where('tipo_servicio', 2)
                ->where('estado', 1)
                ->where('confirmacion_tienda', false)
                ->where('confirmacion_web', true)
                ->paginate(2);
        return $gestiones_pendientes;
    }
    
}
