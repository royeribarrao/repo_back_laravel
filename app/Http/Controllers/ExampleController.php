<?php

namespace App\Http\Controllers;

use App\Models\Gestion;
use App\Models\Carrier;
use App\Models\User;
use App\Models\Producto;
use App\Models\Sale;
use App\Models\Tienda;
use App\Models\ProductoGestion;
use App\Models\NuevoProductoServicioCambio;
use App\Models\Woocommerce\WoocommerceProducto;
use App\Models\Woocommerce\WoocommerceVariacionProductos;
use App\Models\TuRepo\RepoTiendaOpcion;
use Mail;
use App\Mail\ConfirmacionOperadorLogisticoEntrega;
use App\Mail\CreacionCuenta;
use App\Mail\ConfirmacionPedido;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Mail\ConfirmacionOperadorLogisticoRecojo;

use App\SdkPayU\lib\PayU\util\PayUParameters;
use App\SdkPayU\lib\PayU\PayUReports;
use Automattic\WooCommerce\Client;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\Tienda\NuevoProceso;
use App\Services\OlvaService;

class ExampleController extends Controller
{
    protected $olvaService;

    public function __construct(
        OlvaService $olva
    )
    {
        $this->olvaService = $olva;
    }

    public function verTiendaRoyer()
    {
        $tiendas = Tienda::all();
        return $tiendas;
    }

    public function apiWoo($page)
    {
        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        //return $woocommerce->get('reports/products/totals');
        $productos = $woocommerce->get('products', ['per_page' => 15, 'page' => $page]);
        $lastResponse = $woocommerce->http->getResponse();
        $headers = $lastResponse->getHeaders();
        $totalPages = $headers['x-wp-totalpages'];
        
        $total = $headers['x-wp-total'];
        //return $productos[0]->variations;
        foreach($productos as $key => $producto)
        {
            foreach($producto->variations as $key => $only)
            {
                WoocommerceProducto::create([
                    'producto_id' => $only
                ]);
            }
        }

        return response()->json([
            'productos' => $productos,
            'totalPages' => $totalPages,
            'total' => $total
        ]);
    }

    public function verProductoWoo($id)
    {
        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        //return $woocommerce->get('reports/products/totals');
        $producto = $woocommerce->get('products/'.$id);
        return $producto;
    }


    public function woocommerceAllProducts()
    {
        //metodo1
        $wc = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        $page = 1;
        $products = [];
        $all_products = [];
        do{
            try {
                $products = $wc->get('products',['per_page' => 50, 'page' => $page]);
            }catch(HttpClientException $e){
                die("Can't get products: $e");
            }
            $all_products = array_merge($all_products,$products);
            $page++;
        } while (count($products) > 0);
        return $all_products;
        foreach($all_products as $key => $item){
            WoocommerceProducto::create([
                'producto_id' => $item->id, 
                'type' => $item->type, 
                'status' => $item->status,
                'sku' => $item->sku,
                'purchasable' => $item->purchasable
            ]);
        }

        foreach($all_products as $key => $item){
            if ($item->type == "variable") {
                $variation = $wc->get('products/'.$item->id);
                foreach ($variation->variations as $var) {
                    $prod = $wc->get('products/'.$var);
                    WoocommerceVariacionProductos::create([
                        'producto_id' => $prod->id, 
                        'sku' => $prod->sku, 
                        'name' => $prod->name,
                        'stock_quantity' => $prod->stock_quantity,
                        'type' => $prod->type,
                        'status' => $prod->status,
                        'image' => $prod->images[0]->src,
                        'price' => $prod->price
                    ]);
                }
            }
        }


        return "trabajo hecho";

        

        return $all_products;
        //metodo2
        while (count($woocommerce->get('products',array('per_page' => 100, 'page' => $page))) > 0) {
            $all_products = array_merge($all_products,$woocommerce->get('products',array('per_page' => 100, 'page' => $page)));
            $page++;
          }
      
        if ($source_product->type = "variable") {
            $variation = $woocommerce->get('products/'.$source_product->id.'/variations');
            foreach ($variation as $source_child) {
                //do stuff
                
            }
        }

    }

    public function obtenerVariacionProducto()
    {
        $wc = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        $product = $wc->get('products/15489');
        return $product;
    }

    public function actualizarTablaProductoWoocommerce()
    {
        $productos = WoocommerceProducto::where('stock_quantity', null)->get();
        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );

        foreach($productos as $key => $producto)
        {
            $producto_repo = WoocommerceProducto::find($producto->id);
            $producto_woo = $woocommerce->get('products/'.$producto->producto_id);
            // $producto_repo->update([
            //     'color' => isset($producto_woo->attributes[0]) ? $producto_woo->attributes[0]->option : '',
            //     'description' => $producto_woo->description,
            //     'image' => isset($producto_woo->images[0]) ? $producto_woo->images[0]->src : '',
            //     'name' => $producto_woo->name,
            //     'parent_id' => $producto_woo->parent_id,
            //     'permalink' => $producto_woo->permalink,
            //     'price' => $producto_woo->price ? $producto_woo->price : 0,
            //     'purchasable' => $producto_woo->purchasable,
            //     'regular_price' => $producto_woo->regular_price ? $producto_woo->regular_price : 0,
            //     'sale_price' => $producto_woo->sale_price ? $producto_woo->sale_price : 0,
            //     'sku' => $producto_woo->sku,
            //     'status' => $producto_woo->status,
            //     'talla' => isset($producto_woo->attributes[1]) ? $producto_woo->attributes[1]->option : '',
            //     'type' => $producto_woo->type
            // ]);
            $producto_repo->update([
                'stock_quantity' => $producto_woo->stock_quantity ? $producto_woo->stock_quantity : 0
            ]);
        }

        
        
        return $producto;
    }

    public function actualizarId(){
        $productos = WoocommerceProducto::all();
        foreach($productos as $key => $producto)
        {
            $producto_repo = WoocommerceProducto::find($producto->id);
            $producto_repo->update([
                'id' => $key + 1
            ]);
        }
    }

    public function verVentas($page)
    {
        $ventas = Sale::where('id', '>', 2071)->get();
        foreach($ventas as $key => $venta)
        {
            $venta_re = Sale::find($venta->id);
            $venta_re->update([
                'id' => $venta->id - 2999
            ]);
        }
        return $ventas;

        $woocommerce = new Client(
            'https://inmaculada.shop/', 
            'ck_55bef335f7cc88eeacebb472aea6424674b8fe49', 
            'cs_87d74e0dd13d4dbac93b85494ebb46fa5d638c33',
            [
                'version' => 'wc/v3'
            ]
        );
        //return $woocommerce->get('reports/products/totals');
        $query = [
            'page' => $page,
            'per_page' => 15
        ];
        $ventas = $woocommerce->get('orders', $query);
        foreach($ventas as $key => $venta)
        {
            foreach($venta->line_items as $key => $line)
            {
                Sale::create([
                    'tienda_id'=> 1,
                    'codigo_compra'=> $venta->id,
                    'fecha_compra'=> $venta->date_created,
                    'sku'=> $line->sku,
                    'nombre_producto'=> $line->name,
                    'cantidad'=> $line->quantity,
                    'precio_pagado'=> $line->total,
                    'numero_documento'=> $venta->billing->company,
                    'nombre_cliente'=> $venta->billing->first_name,
                    'apellido_cliente'=> $venta->billing->last_name,
                    'telefono'=>  $venta->billing->phone,
                    'email'=>  $venta->billing->email,
                    'direccion'=> $venta->billing->address_1,
                    'distrito'=> $venta->billing->city
                ]);
            }
            
        }
        

        $lastResponse = $woocommerce->http->getResponse();
        $headers = $lastResponse->getHeaders();
        $totalPages = $headers['x-wp-totalpages'];
        $total = $headers['x-wp-total'];
        return response()->json([
            'prodventasuctos' => $ventas,
            'totalPages' => $totalPages,
            'total' => $total
        ]);



        $lastResponse = $woocommerce->http->getResponse();
        $headers = $lastResponse->getHeaders();
        $totalPages = $headers['x-wp-totalpages'];
        
        $total = $headers['x-wp-total'];
        //return $productos[0]->variations;
        foreach($productos as $key => $producto)
        {
            foreach($producto->variations as $key => $only)
            {
                WoocommerceProducto::create([
                    'producto_id' => $only
                ]);
            }
        }

        return response()->json([
            'productos' => $productos,
            'totalPages' => $totalPages,
            'total' => $total
        ]);
    }

    public function createOrden()
    {
        $client   = new \GuzzleHttp\Client();

        $body = $this->olvaService->getBody(163, 3);
        return $body;
        $bearerToken = $this->olvaService->token;
        

        $createOrder = 'https://api.aimo.co/core/orders/';
        
        $QueryCreateOrder = $client->request('POST', $createOrder, [
            'headers' => [
                    'Authorization' => "Bearer T7AeAFArKyXTbxKV3zeG5P6pXp7sau",
                    'Content-Type' => 'application/json'
            ],
            'json' => $body
        ]);
        $response = $QueryCreateOrder->getBody()->getContents();
        return $response;
    }

    public function getOrden()
    {
        $client   = new \GuzzleHttp\Client();
        $bearerToken = $this->olvaService->token;
        $getOrder = 'https://api.aimo.co/core/orders/501891/';
        $getAllOrders = 'https://api.aimo.co/core/orders/id?501891';
        $QueryGetOrder = $client->request('GET', $getOrder, [
            'headers' => [
                'Authorization' => "Bearer T7AeAFArKyXTbxKV3zeG5P6pXp7sau"
            ]
        ]);
        $response = $QueryGetOrder->getBody()->getContents();
        return $response;
    }

    public function getBatch()
    {
        $client   = new \GuzzleHttp\Client();
        $bearerToken = $this->olvaService->token;
        $direccion = 'Jirón Batallón Tarma N 508, 15039 Santiago de Surco';
        $obtenerBatch = "https://api.aimo.co/core/branch_offices/?location=$direccion";
        
        $QueryGetBatch = $client->request('GET', $obtenerBatch, [
            'headers' => [
                'Authorization' => "Bearer T7AeAFArKyXTbxKV3zeG5P6pXp7sau"
            ]
        ]);
        $response = $QueryGetBatch->getBody()->getContents();
        return $response;
    }

    public function verTrackerOlva()
    {
        $client   = new \GuzzleHttp\Client();
        
        $request = $client->request('GET', 'https://api.aimo.co/core/tracking/orders/TEhDNEFaRVU=', 
        [
            'headers' => [
                'Authorization' => "Bearer T7AeAFArKyXTbxKV3zeG5P6pXp7sau"
            ]
        ]);
        
        $response = $request->getBody()->getContents();
        $str=str_replace("\r\n","",$response);
        $array_response = json_decode($str, true);
        return $array_response['events'][0]['label'];
    }

    public function verGestionesOlva()
    {
        $gestiones = Gestion::with([
                        'servicioLogistico' => function ($q){
                            $q->where('carrier_id', 3);
                        }])
                        ->where('en_proceso', true)
                        ->where('tipo_servicio', 2)
                        ->where('confirmacion_web', true)
                        ->where('confirmacion_tienda', true)
                        ->get();
        return $gestiones;
    }
}
