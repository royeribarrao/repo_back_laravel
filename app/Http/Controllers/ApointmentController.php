<?php

namespace App\Http\Controllers;

use App\Services\ApointmentService as ApointmentService;
use Illuminate\Http\Request;
use App\Exports\AppointmentExport;
use App\Models\AppointmentCode;
use Maatwebsite\Excel\Facades\Excel;
use Codedge\Fpdf\Fpdf\Fpdf;

class ApointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $appointmentService;
    protected $apointmentService;

    public function __construct(
        ApointmentService $apointment

    )
    {
        $this->apointmentService = $apointment;
    }

    public function get(Request $request) {
        $req =  $request->all();
        if (!isset($req['dates'])) {
            $req['dates'] = [];
        }

        $apointments = $this->apointmentService->all(array('*'), $req);    
        return response()->json($apointments);
    }

    public function getNextDays($cant) {
        $days = [];
        for ($i = 0; $i < $cant; $i++) {
            $days[] = date("Y-m-d", time() + 86400 * $i);
        }
        return $days;
    }

    public function store(Request $request) {

        $res = $this->apointmentService->create($request->all());   
        return $res;
    }
    

    public function show($id) {
        $apointment = $this->apointmentService->find($id);
        return response()->json($apointment);
    }

    public function update($id, Request $request) {
        $res = $this->apointmentService->update($request->all(), $id);
        return $res;
    }

    public function getCodes()
    {
        return AppointmentCode::get(['id', 'name', 'type']);
    }

    public function destroy($id)
    {
        $res = $this->apointmentService->delete($id);
        return response()->json([
            'message' => $res['message'],

        ]);
    }

    public function getReportData($patientId, $typeExport, $addStudies) {

        if ($typeExport == 2 ) {
          return $this->getTreatments($patientId, $typeExport, $addStudies);
        }
        return $this->getControls($patientId, $typeExport, $addStudies);
    }



    public function getTreatments($patientId, $typeExport, $addStudies) {
        $arrCitas = [
            ['title' => 'CITA 1', 'code' => 'T01', 'title2'=> ''],
            ['title' => 'CITA 2', 'code' => 'T02', 'title2'=> ''],
            ['title' => 'CITA 3', 'code' => 'T03', 'title2'=> ''],
            ['title' => 'CITA 4', 'code' => 'T04', 'title2'=> ''],
            ['title' => 'CITA 5', 'code' => 'T05', 'title2'=> ''],
            ['title' => 'CONTROL', 'code' => 'CPO', 'title2'=> 'POST OPERATORIO'],
            ['title' => 'ESTUDIOS', 'code' => 'ER', 'title2'=> 'RADIOLOGICOS'],
            ['title' => 'CONTROL', 'code' => 'CF', 'title2'=> 'FINAL'],
        ];
  
        $patientInfo = \DB::table('patients as p')
          ->select(
            \DB::raw('UCASE(p.fullname) as nombre_paciente'),
            'p.dni',
          )
          ->where('id', $patientId)
          ->first();
          
  
        $apps = \DB::table('appointments as app')->select(
            'ac.name as code',
            'app.start_time as start_time'
          )
          ->leftJoin('appointments as app2', function($join)
            {
              $join->on('app.code', '=', 'app2.code');
              $join->on('app.start_time','<', 'app2.start_time');
            })
          ->leftJoin('appointment_codes as ac', 'ac.id', '=', 'app.code')
          ->whereNull('app2.start_time')
          ->where('app.patient_id', $patientId)
          ->where('ac.type', $typeExport)
          ->orWhere('ac.name', 'CPO')
          ->orWhere('ac.name', 'CF')
          ->orderBy('ac.id', 'ASC')
          ->get();
  
        $orderedCells = [
            'name' => $patientInfo->nombre_paciente,
            'document' => $patientInfo->dni,
            'citas' => []
        ];
  
        $hasCPO = false;
        $formatCPODate = '';

        foreach($arrCitas as $key => $cita) {
          $startDate = 'DIA POR DEFINIR';
          $startTime = 'HORARIO POR DEFINIR';
          $itemFound = $this->searchForCode($cita['code'], $apps);
          if ($itemFound && $cita['code'] != 'ER') {
            if ($cita['code'] == 'CPO') {
                $hasCPO = true;
                $dateCPO = strtotime($itemFound->start_time);
                $unformatCPODate =  strtotime("+7 day", $dateCPO);
                $formatCPODate = date("d F", $unformatCPODate);
            }  
            $startDate = date("d F", strtotime($itemFound->start_time));
            $startTime = date("h: i A", strtotime($itemFound->start_time));
          } 
          
          if ($cita['code'] == 'ER' && $hasCPO ) {
            $startDate = $formatCPODate;
          }

          $orderedCells['citas'][$cita['code']]['title'] = $cita['title'];
          $orderedCells['citas'][$cita['code']]['title2'] = $cita['title2'];
          $orderedCells['citas'][$cita['code']]['day'] = $startDate;
          $orderedCells['citas'][$cita['code']]['hour'] = $startTime;
        }
        
        // if ($addStudies == 1) {
        //   $orderedCells = $this->addStudies($orderedCells, 5);
        // }
        return $orderedCells;
      }
      
      public function getControls($patientId, $typeExport, $addStudies) {
        $arrControl = [
          ['title' => 'PRIMER CONTROL', 'code' => 'CM1'],
          ['title' => 'SEGUNDO CONTROL', 'code' => 'CM2'],
          ['title' => 'TERCER CONTROL', 'code' => 'CM3'],
          ['title' => 'CUARTO CONTROL', 'code' => 'CM4'],
          ['title' => 'QUINTO CONTROL', 'code' => 'CM5'],
          ['title' => 'SEXTO CONTROL', 'code' => 'CM6'],
          ['title' => 'ESTUDIOS RADIÓLOGICOS', 'code' => 'ER'],
          ['title' => 'ALTA MÉDICA', 'code' => 'AM'],
        ];
        $lastControl = 'CONTROL FINAL';
        $apps = [];
  
        $patientInfo = \DB::table('patients as p')
          ->select(   
            \DB::raw('UCASE(p.fullname) as nombre_paciente'),
            'pmi.monthly_apps',
            'p.dni'
          )
          ->leftJoin('medical_histories as mh', function($lj) {
            $lj->on('mh.patient_id', '=', 'p.id');
            $lj->on(\DB::raw('mh.active'), \DB::raw('='), \DB::raw(1));
          })
          ->leftJoin('patients_medical_info as pmi', 'pmi.medical_history_id', '=', 'mh.id')
          ->where('p.id', $patientId)
          ->first();

        $monthlyApps = $patientInfo->monthly_apps;
  
        if ($monthlyApps > 0) {
          $apps = \DB::table('appointments as app')->select(
            'ac.name as code',
            'app.start_time as start_time',
          )
          ->leftJoin('appointments as app2', function($join)
            {
                $join->on('app.code', '=', 'app2.code');
                $join->on('app.start_time','<', 'app2.start_time');
            })
          ->leftJoin('appointment_codes as ac', 'ac.id', '=', 'app.code')
          ->whereNull('app2.start_time')
          ->where('app.patient_id', $patientId)
          ->where('ac.type', $typeExport)
          ->orWhere('ac.name', 'AM')
          ->orderBy('ac.id', 'ASC')
          ->get();
        }

        $orderedCells = [
            'name' => $patientInfo->nombre_paciente,
            'document' => $patientInfo->dni,
            'citas' => []
        ];
       
        $i = 0;
        foreach($arrControl as $key => $cita) {
          if ($cita['code'] != 'ER' && $cita['code'] != 'AM' &&  $monthlyApps <= $i) {
            $i++;
            continue;
          } else {
            $startDate = 'DIA POR DEFINIR';
            $startTime = 'HORARIO POR DEFINIR';
            $itemFound = $this->searchForCode($cita['code'], $apps);
            if ($itemFound && $cita['code'] != 'ER') {
              $startDate = date("d F", strtotime($itemFound->start_time));
              $startTime = date("h: i A", strtotime($itemFound->start_time));
            }
  
            if ($cita['code'] == 'ER' ) {
              $orderedCells['citas']['ER']['title'] = 'ESTUDIOS ';
              $orderedCells['citas']['ER']['title2'] = 'RADIOLÓGICOS';
              $orderedCells['citas']['ER']['day'] = $startDate;
              $orderedCells['citas']['ER']['hour'] = $startTime;
            } else  {
              $orderedCells['citas'][$cita['code']]['title'] = $cita['title'];
              $orderedCells['citas'][$cita['code']]['title2'] = '';
              $orderedCells['citas'][$cita['code']]['day'] = $startDate;
              $orderedCells['citas'][$cita['code']]['hour'] = $startTime;
            }
            $i++;
          }
        }

  
        return $orderedCells;
      }
  
      private function addStudies ($orderedCells, $index) {
        $orderedCells['citas'][$index] = 'ESTUDIOS RADIOLOGICOS';
        $orderedCells['citas'][$index] = 'DIA POR DEFINIR';
        $orderedCells['citas'][$index] = 'HORARIO POR DEFINIR';
  
        return $orderedCells;
      }
     
      private function searchForCode($code, $array) {
        foreach ($array as $key => $item) {
            if ($item->code === $code) {
                return $item;
            }
        }
        return null;
     }


    public function exportMontlyReportPDF($patientId, $typeExport, $addStudies){
        $info = $this->getReportData($patientId, $typeExport, $addStudies);
        $pdf = new Fpdf('P', 'mm', array(80,297));
        $pdf->AddPage('P',array(207,290.8));
        $pdf->Cell(96.5, 15, '',0,1);

        $dates = $info['citas'];

        // IMAGENES 
        $pdf->Image(base_path('public').'/images/membrete-header.jpg',7.7, 8, 192);
        
        $pdf->SetXY(0, 30);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','B',12.5);
        if ($typeExport == 2) {
            $pdf->Cell(207, 5, utf8_decode('CRONOGRAMA DE ATENCIONES Y DONDICIONES DE SERVICIO'),0, 1,'C', false);
            $pdf->SetXY(0, 33);
            $pdf->Cell(207, 1, utf8_decode('_________________________________________________________'),0, 0,'C', false);

        } else {
            $pdf->Cell(206, 5, utf8_decode(' CRONOGRAMA DE CONTROLES MENSUALES FINALES'),0, 1,'C', false);
            $pdf->SetXY(0, 33);
            $pdf->Cell(207, 1, utf8_decode('_________________________________________________'),0, 0,'C', false);
        }
        
        $pdf->Image(base_path('public').'/images/parrafoMontlyReport.png',23, 50, 161);
        $pdf->Image(base_path('public').'/images/condicionesUso.png',23, 120, 161);
        $pdf->Image(base_path('public').'/images/membrete-footer.jpg',3.6 , 258, 200);
        // TITULO Y PARRAFO

        $pdf->SetXY(22, 42);
        $pdf->SetFont('Arial','',11);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(38, 2.8, 'Estimado paciente Sr(a): CALUA JIMENEZ, JOSE RAFAEL');

        // CREACION DE LA TABLA 1
        $i = 0;
        $x = 23.7;
        foreach ($dates as $key => $date) {
            $pdf->SetXY($x, 70);
            $pdf->SetFillColor(226,233,255);
            $pdf->SetTextColor(0,0,0);

            if($key == 'ER') {
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFillColor(255,0,0);
            } else if ($key == 'AM') {
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(57,265,20);
            }

            $pdf->SetFont('Arial','B',8.5);
            $pdf->Cell(32, 5, utf8_decode($date['title']),'T,R,L', 0,'C', true);
            $pdf->SetXY($x, 75);
            $pdf->Cell(32, 5, utf8_decode($date['title2']),'B,R,L',0,'C', true);
            $i++;
            $x = $x + 32;
            if ($i == 5) {
                break;
            }
        }
        
        // CONTENIDO DE LA TABLA
            // FILAR NUMERO 1 
        $pdf->SetXY(23.7, 80);
        $i = 0;
        $pdf->SetTextColor(0,0,0);
        foreach ($dates as $key => $date) {
            if ($i == 0 && $typeExport == 2) {
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(163,215,167);
            } else if($key == 'ER') {
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFillColor(255,0,0);
            } else if ($key == 'AM') {
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(57,265,20);
            }
            else if ($typeExport == 2){
                if ($i == 4) {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,233,0);
                } else {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                }
            }
            else {
                $pdf->SetFillColor(255,255,255);
            }
            
            $pdf->SetFont('Arial','',7);
            $pdf->Cell(32, 5, utf8_decode($date['day']),1, 0,'C', true);
            $i++;
            if ($i == 5) {
                break;
            }
        }

        // TABLA 1FILAR NUMERO 2
        $pdf->SetXY(23.7, 85);
        $i = 0;
        $pdf->SetTextColor(0,0,0);
        foreach ($dates as $key => $date) {
            if ($i == 0 && $typeExport == 2) {
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(163,215,167);
            } else if ($key == 'ER') {
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFillColor(255,0,0);
            } else if ($key == 'AM') {
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(57,265,20);
            } else if ($typeExport == 2){
                if ($i == 4) {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,233,0);
                } else {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                }
            }else {
                $pdf->SetFillColor(255,255,255);
            }
            $pdf->SetFont('Arial','',7);
            $pdf->Cell(32, 5, utf8_decode($date['hour']),1, 0,'C', true);
            $i++;
            if ($i == 5) {
                break;
            }
        }

        // TABLA  2 ///////////
        $i = 0;
        $x = 23.5;

        foreach ($dates as $key => $date) {
            if ($i < 5) {
                $i++;
                continue;
            } else{
                
                $pdf->SetXY($x, 95);
                if ($key == 'ER') {
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFillColor(255,0,0);
                } else if ($key == 'AM') {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(57,265,20);
                } else if ($typeExport == 2){
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                }else {
                    $pdf->SetFillColor(255,255,255);
                }
                $pdf->SetFont('Arial','B',8.5);
                $pdf->Cell(32, 5, utf8_decode($date['title']),'T,R,L',1,'C', true);
                $pdf->SetXY($x, 100);
                $pdf->Cell(32, 5, utf8_decode($date['title2']),'B,R,L',0,'C', true);
                
                $i++;
                $x = $x + 32;
            }
            
        }
                
        // FILA 2 - CFILA NUMERO 2
        $i = 0;
        $x = 23.5;
        $pdf->SetXY($x, 105);

        foreach ($dates as $key => $date) {
            if ($i < 5) {
                $i++;
                continue;
            } else{
                
                if ($key == 'ER') {
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFillColor(255,0,0);
                } else if ($key == 'AM') {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(57,265,20);
                } else if ($typeExport == 2){
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                } else {
                    $pdf->SetFillColor(255,255,255);
                }
                $pdf->SetFont('Arial','B',7);
                $pdf->Cell(32, 5, utf8_decode($date['day']),1,0,'C', true);
              
                $i++;
            }
        }

        #TABLA 2 FILA NUMERO 3 
        $i = 0;
        $x = 23.5;
        $pdf->SetXY($x, 110);

        foreach ($dates as $key => $date) {
            if ($i < 5) {
                $i++;
                continue;
            } else{
                
                if ($key == 'ER') {
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFillColor(255,0,0);
                } else if ($key == 'AM') {
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(57,265,20);
                } else if ($typeExport == 2){
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                } else {
                    $pdf->SetFillColor(255,255,255);
                }
                $pdf->SetFont('Arial','B',7);
                $pdf->Cell(32, 5, utf8_decode($date['hour']),1,0,'C', true);
              
                $i++;
            }
        }
        
        $nameText = 'Yo, '. $info['name'];
        $documentText = 'DNI: '. $info['document'];
        
        // DECLARACION
        $pdf->SetXY(23, 205);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','',10.5);
        $pdf->Cell(20, 5, utf8_decode($nameText),0, 1,'L', true);
        $pdf->SetX(23);
        $pdf->Cell(20, 5, utf8_decode('doy mi conformidad con el cronograma y tratamiento programado, y me doy por enterado de'),0, 1,'L', true);
        $pdf->SetX(23);
        $pdf->Cell(20, 5, utf8_decode('las condiciones del servicio Médico que adquirí. En señal de conformidad firmo a continuación. '),0, 1,'L', true);

        // HUELLA DIGITAL
        $pdf->SetXY(32, 222);
        $pdf->SetFillColor(255,255,255);
        $pdf->Cell(25.8, 30, utf8_decode(''),1, 1,'L', true);
        
        $pdf->SetXY(32.5, 247);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(20, 5, utf8_decode('HUELLA DIGITAL'),0, 1,'L', true);

        // FIRMA Y DNI
        $pdf->SetXY(80, 230);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(70, 5, utf8_decode(''),'B', 0,'L', true);

        $pdf->SetXY(98, 237);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(70, 5, utf8_decode('FIRMA del Paciente'),0, 1,'L', true);

        $pdf->SetXY(80, 247);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(70, 5, utf8_decode($documentText),0, 1,'L', true);



        $pdf->Output();   
        exit;
    }

}
