<?php
namespace App\Traits;

trait Fullname {
    protected static function boot() {
        parent::boot();
        static::creating( function ($model) {
        $model->fullname = $model->name.' '.$model->father_lastname.' '.$model->mother_lastname;
            if ( isset(auth()->user()->id) ) {
                $model->created_user = auth()->user()->id;
                $model->updated_user = auth()->user()->id;
            } else {
                $model->created_user = null;
                $model->updated_user = null;
            }
        });

        static::updating( function ($model) {
          $model->fullname = $model->name.' '.$model->father_lastname.' '.$model->mother_lastname;

            if ( isset(auth()->user()->id) ) {
                $model->updated_user = auth()->user()->id;
            } else {
                $model->updated_user = null;
            }
        });
    }
}