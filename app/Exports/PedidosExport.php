<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliDetallePedido;

// class PedidosExport implements FromCollection
// {
//     public function collection()
//     {
//         return WiqliPedido::all();
//     }
// }

class PedidosExport implements FromView
{
    public function view(): View
    {
        $totalesCantidad = $this->obtenerTotalPedido();
        $productosAptos = $this->obtenerProductosAptos($totalesCantidad);
        $pedidos = $this->obtenerPedidos();
        $personas = $this->obtenerTotalPersonas($productosAptos);

        return view('exports.pedidos', [
            'productosAptos' => $productosAptos,
            'clientes' => $pedidos,
            'totalesCantidad' => $totalesCantidad,
            'totalVenta' => 0
        ]);
    }

    public function obtenerProductosAptos($totalesCantidad)
    {
        $productos = WiqliProducto::with('unidad')->get()->toArray();
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')
            ->where('producto_id', '!=', 999)
            ->orderBy('producto_id')
            ->get()
            ->toArray();
        $detalle = [];
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }

        foreach ($new_prods as $key => $value) {
            $new_prods[$key]['cantidad'] = $totalesCantidad[$key];
            $new_prods[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }

        return $new_prods;
    }

    public function obtenerPedidos()
    {
        $pedidos = WiqliPedido::with([
            'cliente', 
            'detalle' => function ($q){
                $q->where('producto_id', '!=', 999);
        }])->get();
        return $pedidos;
    }

    public function obtenerTotalPedido()
    {
        $totales_cantidad = [];
        $prods = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('producto_id');
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;
    }

    public function obtenerTotalPersonas($productos)
    {
        foreach ($productosAptos as $key => $value) {
            $productosAptos[$key]['cantidad'] = $totalesCantidad[$key];
            $productosAptos[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }

        foreach ($productosAptos as $key => $producto) {
            $id_productoApto= $key;
            foreach ($clientes as $key => $cliente) {
                $id_pedido = $cliente['id'];
                foreach ($cliente['detalle'] as $key => $detalle) {
                    if($detalle['id'] == $producto['id'])
                    {
                        $productosAptos[$id_productoApto]['personas'][$key]['cantidad'] = $detalle['cantidad'];
                        $productosAptos[$id_productoApto]['personas'][$key]['precio'] = $detalle['precio_unitario'];
                    }
                }
            }
        }
    }
}