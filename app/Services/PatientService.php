<?php namespace App\Services;

use App\Services\Contracts\PatientInterface as PatientInterface;
use App\Models\Patient as PatientModel;
use App\Models\PatientSimpleDiagnostic as PatientSimpleDiagnosticModel;
use App\Models\PatientImage as PatientImageModel;
use App\Models\PatientFile as PatientFileModel;
use App\Models\PatientFileRecorded as PatientFileRecordedModel;
use App\Models\Order as OrderModel;
use App\Models\OrderStudy as OrderStudyModel;
use App\Models\PatientMedicalInfo as PatientMedicalInfoModel;

class PatientService implements PatientInterface {
  
  protected $patient;
  protected $patientSimpleDiagnostic;
  protected $order;
  protected $orderStudy;
  protected $patientImage;
  protected $patientFile;
  protected $patientMedicalInfo;
  protected $patientFileRecorded;
  
  public function __construct(
      PatientModel $patientModel,
      PatientFileRecordedModel $patientFileRecordedModel,
      PatientSimpleDiagnosticModel $patientSimpleDiagnosticModel,
      PatientImageModel $patientImageModel,
      PatientFileModel $patientFileModel,
      OrderModel $orderModel,
      OrderStudyModel $orderStudyModel,
      PatientMedicalInfoModel $patientMedicalInfoModel
      )
  {
    $this->patient = $patientModel;
    $this->patientSimpleDiagnostic = $patientSimpleDiagnosticModel;
    $this->order = $orderModel;
    $this->orderStudy = $orderStudyModel;
    $this->patientImage = $patientImageModel;
    $this->patientFile = $patientFileModel;
    $this->patientMedicalInfo = $patientMedicalInfoModel;
    $this->patientFileRecorded = $patientFileRecordedModel;
  }
  
  public function all($columns = array('*'), $relation = array('*'))
  {
    return $this->patient->get($columns);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    // with('sede', 'simpleDiagnostics')
    return $this->patient
      ->select(
        'patients.id as id',
        'patients.fullname',
        'patients.dni',
        'patients.birthdate',
        'patients.phone',
        'patients.state',
        'patients.reference_age',
        'ms.name as sede_name',
        'mser.alias as service_name',
        's.name as situation_name',
        \DB::raw('group_concat(psd.simple_diagnostic_id SEPARATOR ", ") as simple_diagnostics')
      )
      ->where(function ($q) use ($columns){
        if (isset($columns['fullname']) && $columns['fullname'] !== '') {
          $q->where('patients.fullname','like', '%'.$columns['fullname'].'%');
        }
        if (isset($columns['dni']) && $columns['dni'] !== '') {
          $q->where('patients.dni', $columns['dni']);
        }
        if (isset($columns['sede_id']) && $columns['sede_id'] !== '') {
          $q->where('patients.sede_id', $columns['sede_id']);
        }
        if (isset($columns['service_id']) && $columns['service_id'] !== '') {
          $q->where('patients.service_id', $columns['service_id']);
        }

      })
      ->leftJoin('maintenance_sedes as ms', 'ms.id', '=', 'patients.sede_id')
      ->leftJoin('medical_histories as mh', function($lj) {
        $lj->on('mh.patient_id', '=', 'patients.id');
        $lj->on(\DB::raw('mh.active'), \DB::raw('='), \DB::raw(1));
      })
      ->leftJoin('patients_medical_info as pmi', 'pmi.medical_history_id', '=', 'mh.id')
      ->leftJoin('maintenance_services as mser', 'mser.id', '=', 'pmi.service_id')
      ->leftJoin('patient_simple_diagnostics as psd', 'psd.medical_history_id', '=', 'mh.id')
      ->leftJoin('situations as s', 's.id', '=', 'pmi.situation_id')
      ->groupBy('patients.id')
      ->orderBy('id', $order_type)
      ->paginate($perPage);
  }

  public function create(array $data) {

  }
  
  public function createPatient($request)
  {
    $data = $request->all();
    $newPatient = $this->patient->create($data);

    return $newPatient;
  }
  public function update(array $data, $id) {

  }
  
  public function updatePatient($request, $id)
  {
    try {
      //code...
      $data = $request->all();
      $patientGot = $this->patient->find($id);
      $patientGot->update($data);
      return $patientGot;

    } catch (\Throwable $th) {
      throw $th;
    }
  }

  public function formatFiles($files, $historyId, $id, $type) {
    return array_map( 
        function($file) use ($id, $type, $historyId) { 
            $fileDirectory = 'public/' . $id.'-'.$historyId . '/' . $type . '/';

            if ($type === 'images') {
            $newFileName = uniqid().'.'.$file->extension();
            } else {
            $originalName = $file->getClientOriginalName();
            $filename = pathinfo($originalName, PATHINFO_FILENAME);
            $extension = pathinfo($originalName, PATHINFO_EXTENSION);
            $newFileName = $filename . '_' . uniqid() . '.'. $extension;
            }
            

            $nFile = [
            'path' => $newFileName,
            'full_path' => $fileDirectory . $newFileName,
            'patient_id' => $id,
            'medical_history_id' => $historyId
            ];

            \Storage::put($nFile['full_path'],  
            file_get_contents($file));

            return $nFile;
        },
        $files
    );
  }
  
  public function delete($id)
  {
    return $this->patient->destroy($id);
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->patient->with('simpleDiagnostics', 'sede', 'images', 'files')->find($id, $columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->patient->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    return $this->patient->where('fullname', 'like', '%'.$searching.'%')
          ->leftJoin('patients_medical_info as pmi', 'pmi.patient_id', '=', 'patients.id')
          ->where('state', 1)
          ->where('pmi.situation_id', '!=', 1)
          ->groupBy('patients.id')
          ->get(['patients.id', 'patients.fullname', 'pmi.situation_id', 'patients.sede_id']);
  }

  public function createMedicalInfo($request) {
    $data = $request->all();
    $newMedicalInfo = $this->patientMedicalInfo->create($data);
    $diagnostics = explode(',', $data['simple_diagnostics']);
    $imagesUploaded = $request->file('imagesNew');
    $filesUploaded = $request->file('filesNew');
    $filesRecordedUploaded = $request->file('filesRecordedNew');

    /*guardando imagenes */
    if ($imagesUploaded !== null && count($imagesUploaded)) {
      $newImages = $this->formatFiles($imagesUploaded, $request->medical_history_id, $newMedicalInfo->id , 'images');
      $this->patientImage->insert($newImages);
    }
    /*guardando imagenes */

    /*GUARDANDO FILES */
    if ($filesUploaded !== null && count($filesUploaded)) {
      $filesNew = $this->formatFiles($filesUploaded, $request->medical_history_id, $newMedicalInfo->id , 'files');
      $this->patientFile->insert($filesNew);
    }
    /*GUARDANDO FILES */

    /*GUARDANDO FILES  ANTECEDENTEs*/
    if ($filesRecordedUploaded !== null && count($filesRecordedUploaded)) {
      $filesRecordedNew = $this->formatFiles($filesRecordedUploaded, $request->medical_history_id, $newMedicalInfo->id , 'files_recorded');
      $this->patientFileRecorded->insert($filesRecordedNew);
    }
    /*GUARDANDO FILES */

    $newDiagnostics = array_map( 
      function($diagnostic) use ($newMedicalInfo, $request) { 
        $nDiagnostic = ['patient_id' => $newMedicalInfo->id, 
                        'medical_history_id' => $request->medical_history_id, 
                        'simple_diagnostic_id' => $diagnostic];
        return $nDiagnostic;
       },
      $diagnostics
    );

    $this->patientSimpleDiagnostic->insert($newDiagnostics);

    return $newMedicalInfo;
  }

  public function updateMedicalInfo($request, $patientId) {
    try {
      //code...
      $data = $request->all();
      $medicalInfoGot = $this->patientMedicalInfo->find($patientId);
      $medicalInfoGot->update($data);
      $imagesUploaded = $request->file('imagesNew');
      $imagesDeleted = $request->imagesDeleted;
      $filesUploaded = $request->file('filesNew');
      $filesDeleted = $request->filesDeleted;
      $filesRecordedUploaded = $request->file('filesRecordedNew');
      $filesRecordedDeleted = $request->filesRecordedDeleted;
      /*eliminando imagenes */
      if ($imagesDeleted !== null && count($imagesDeleted)) {
        $this->patientImage
          ->whereIn('full_path', $imagesDeleted)
          ->delete();

        foreach ($imagesDeleted as $imgDeleted) {
          unlink(storage_path(). '/app/' .$imgDeleted);
        }
      }
      /*eliminando imagenes */
      
      /*GUARDANDO IMAGENES */
      
      if ($imagesUploaded !== null && count($imagesUploaded)) {
        $newImages = $this->formatFiles($imagesUploaded, $request->medical_history_id, $patientId, 'images');
        $this->patientImage->insert($newImages);
      }
      /*GUARDANDO IMAGENES */

      /*eliminando ARCHIVOS */
      if ($filesDeleted !== null && count($filesDeleted)) {
        $this->patientFile
          ->whereIn('full_path', $filesDeleted)
          ->delete();

        foreach ($filesDeleted as $fileDeleted) {
          unlink(storage_path(). '/app/' .$fileDeleted);
        }
      }
      /*eliminando ARCHIVOS */

      /*GUARDANDO ARCHIVOS*/
      if ($filesUploaded !== null && count($filesUploaded)) {
        $newFiles = $this->formatFiles($filesUploaded, $request->medical_history_id, $patientId, 'files');
        $this->patientFile->insert($newFiles);
      }
      /*GUARDANDO ARCHIVOS*/



      /*eliminando ARCHIVOS ANTECEDENTES*/
      if ($filesRecordedDeleted !== null && count($filesRecordedDeleted)) {
        $this->patientFileRecorded
          ->whereIn('full_path', $filesRecordedDeleted)
          ->delete();

        foreach ($filesRecordedDeleted as $fileRecordedDeleted) {
          unlink(storage_path(). '/app/' .$fileRecordedDeleted);
        }
      }
      /*eliminando ARCHIVOS ANTECEDENTES*/

      /*GUARDANDO ARCHIVOS ANTECEDENTES*/
      if ($filesRecordedUploaded !== null && count($filesRecordedUploaded)) {
        $newRecordedFiles = $this->formatFiles($filesRecordedUploaded, $request->medical_history_id, $patientId, 'files_recorded');
        $this->patientFileRecorded->insert($newRecordedFiles);
      }
      /*GUARDANDO ARCHIVOS ANTECEDENTES*/

      /*actualizando diangosticos imagenes */
      $this->patientSimpleDiagnostic->where('patient_id', $medicalInfoGot->id)->delete();

      $diagnostics = explode(',', $data['simple_diagnostics']);
      $newDiagnostics = array_map( 
        function($diagnostic) use ($patientId, $request) { 
          $nDiagnostic = ['patient_id' => $patientId, 
                          'medical_history_id' => $request->medical_history_id, 
                          'simple_diagnostic_id' => $diagnostic];
          return $nDiagnostic;
        },
        $diagnostics
      );
      /*actualizando diangosticos imagenes */

      $this->patientSimpleDiagnostic->insert($newDiagnostics);

    } catch (\Throwable $th) {
      throw $th;
    }
    return $medicalInfoGot;
  }

  public function showMedicalInfo($historyId) {
    return $this->patientMedicalInfo
      ->with('simpleDiagnostics', 'sede', 'images', 'files', 'files_recorded')
      ->where('medical_history_id', $historyId)
      ->first();
  }

  public function createOrders(array $data)
  {
    //must exist one final order
    if ($data['type_order'] === 2) {
      $this->order->where('patient_id', $data['patient_id'])
          ->update(['type_order' => 1]);
    }

    $newOrder= $this->order->create($data);
    $studies = $data['studies'];
    $newStudies = array_map( 
      function($study) use ($newOrder) { 
        $study['order_id'] = $newOrder->id;
        return $study;
       },
      $studies
    );

    $this->orderStudy->insert($newStudies);

    return $newOrder;
  }

  public function updateOrders(array $data, $id)
  {
    //must exist one final order
    if ($data['type_order'] === 2) {
      $this->order->where('patient_id', $data['patient_id'])
          ->where('id', '!=', $id)
          ->update(['type_order' => 1]);
    }


    $orderGot = $this->order->find($id);
    $orderGot->update($data);

    $this->orderStudy->where('order_id', $id)->delete();

    $studies = $data['studies'];
    $newStudies = array_map( 
      function($study) use ($orderGot) { 
        $study['order_id'] = $orderGot->id;
        return $study;
       },
      $studies
    );

    $this->orderStudy->insert($newStudies);

    return $orderGot;
  }

  
  
  public function findOrders($id, $historyId, $columns = array('*'))
  {
    return $this->order->with('studies')
      ->where('patient_id', $id)
      ->where('medical_history_id', $historyId)
      ->orderBy('id', 'desc')
      ->get();
  }
  
}