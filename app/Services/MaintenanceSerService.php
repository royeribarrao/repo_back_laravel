<?php namespace App\Services;

use App\Services\Contracts\MaintenanceSerInterface as MaintenanceInterface;
use App\Models\MaintenanceService as MaintenanceServiceModel;

class MaintenanceSerService implements MaintenanceInterface {
  
  protected $maintenanceSer;
  
  public function __construct(MaintenanceServiceModel $maintenanceServiceModel)
  {
    $this->maintenanceSer = $maintenanceServiceModel;
  }
  
  public function all($columns = array('*'), $relations = [])
  {
    return $this->maintenanceSer->get($columns);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    return $this->maintenanceSer->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {
    $newMaintenanceSer = $this->maintenanceSer->create($data);
    return $newMaintenanceSer;
  }
  
  public function update(array $data, $id)
  {
    $maintenanceServiceGot = $this->maintenanceSer->find($id);
    $maintenanceServiceGot->update($data);
    return $maintenanceServiceGot;
  }
  
  public function delete($id)
  {
    return $this->maintenanceSer->destroy($id);
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->maintenanceSer->find($id, $columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->maintenanceSer->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Implement search() method.
  }
  
  public function updateState($id, $state)
  {
    $maintenanceServiceGot = $this->maintenanceSer->find($id);
    $maintenanceServiceGot->state = !$state;
    $maintenanceServiceGot->save();
    return $maintenanceServiceGot;
  }
}