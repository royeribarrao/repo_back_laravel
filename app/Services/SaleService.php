<?php namespace App\Services;

use App\Services\Contracts\SaleInterface as SaleInterface;
use App\Models\Sale as SaleModel;

class SaleService implements SaleInterface {
  
    protected $sale;
    
    public function __construct(
        SaleModel $saleModel
        )
    {
        $this->sale = $saleModel;
    }
    
    public function all($columns = array('*'), $relation = [])
    {
        return $this->sale->get($columns);
    }
  
    public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
    {
        $user = \Auth::user();
        return $this->sale->where(function ($q) use ($columns){
            if (isset($columns['sku']) && $columns['sku'] !== '') {
            $q->where('sku','like', '%'.$columns['sku'].'%');
            }
            if (isset($columns['nombre_cliente']) && $columns['nombre_cliente'] !== '') {
            $q->where('nombre_cliente', $columns['nombre_cliente']);
            }
            if (isset($columns['codigo_compra']) && $columns['codigo_compra'] !== '') {
                $q->where('codigo_compra', $columns['codigo_compra']);
                }
        })
        ->where('tienda_id', $user->tienda_id)
        ->orderBy('id', $order_type)->paginate($perPage);
    }
    
    public function create(array $data)
    {
        $newSale = $this->sale->create($data);
        return $newSale;
    }
    
    public function update(array $data, $id)
    {
        $saleGot = $this->stsaleore->find($id);
        $saleGot->update($data);
        
        return $saleGot;
    }
    
    public function delete($id)
    {
        return $this->sale->destroy($id);
    }
    
    public function find($id, $columns = array('*'))
    {
        return $this->sale->find($id, $columns);
    }
    
    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->sale->where($field, '=', $value)->first($columns);
    }
    
    public function search($searching, $columns = array('*'))
    {
        // TODO: Impselement search() method.
    }

    public function updateState($id, $state)
    {
        $saleGot = $this->sale->find($id);
        $saleGot->state = !$state;
        $saleGot->save();
        return $saleGot;
    }

    public function formatFiles($files, $id, $type) {
        return array_map( 
          function($file) use ($id, $type) { 
            if(file_exists($file)) {
                File::delete($file);
            }
            $newFileName = uniqid().'.'.$file->extension();    
            \Storage::disk('public')->put("tiendas/logos".$newFileName,  \File::get($file));
            return $newFileName;
          },
          $files
        );
    }
}