<?php namespace App\Services\Contracts;

interface PatientInterface extends Repository {
    public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc');

    public function updatePatient($request, $id);
    public function createPatient($request);
}
