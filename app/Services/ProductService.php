<?php namespace App\Services;

use App\Services\Contracts\ProductInterface as ProductInterface;
use App\Models\Producto as ProductoModel;

class ProductService implements ProductInterface {
  
    protected $product;
    
    public function __construct(
        ProductoModel $productModel
        )
    {
        $this->product = $productModel;
    }
    
    public function all($columns = array('*'), $relation = [])
    {
        return $this->product->get($columns);
    }
    
    public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
    {
        $user = \Auth::user();
        return $this->product->where(function ($q) use ($columns){
            if (isset($columns['sku_producto']) && $columns['sku_producto'] !== '') {
                $q->where('sku','like', '%'.$columns['sku_producto'].'%');
            }
            if (isset($columns['sku_description']) && $columns['sku_description'] !== '') {
                $q->where('name','like', '%'.$columns['sku_description'].'%');
            }
        })
        ->where('tienda_id', $user->tienda_id)
        ->orderBy('id', $order_type)->paginate($perPage);
    }
    
    public function create(array $data)
    {
        return ($data);
        $newProduct = $this->product->create($data);
        return $newProduct;
    }
    
    public function update(array $data, $id)
    {
        $productGot = $this->product->find($id);
        $productGot->update($data);
        
        return $productGot;
    }
    
    public function delete($id)
    {
        return $this->product->destroy($id);
    }
    
    public function find($id, $columns = array('*'))
    {
        return $this->product->find($id, $columns);
    }
    
    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->product->where($field, '=', $value)->first($columns);
    }
    
    public function search($searching, $columns = array('*'))
    {
        // TODO: Impselement search() method.
    }  
}