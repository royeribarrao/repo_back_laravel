<?php namespace App\Services;

use Illuminate\Support\Facades\Storage;

class SaveImageService
{
    public function guardarImagen($disk, $arrayImages, $folder)
    {
        $images = array_map( 
            function($file) use ($folder, $disk) {
                $fileDirectory = $folder;
                $originalName = $file->getClientOriginalName();
                $filename = pathinfo($originalName, PATHINFO_FILENAME);
                $extension = pathinfo($originalName, PATHINFO_EXTENSION);
                $newFileName = $filename . '_' . uniqid() . '.'. $extension;
                $nFile = [
                    'path' => $newFileName,
                    'full_path' => $fileDirectory . $newFileName
                ];
                Storage::disk($disk)->put($nFile['full_path'],  file_get_contents($file));
                return $nFile;
            },
            $arrayImages
        );
        return $images;
    }
}