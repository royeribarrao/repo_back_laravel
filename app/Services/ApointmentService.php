<?php namespace App\Services;

use App\Services\Contracts\ApointmentInterface as ApointmentInterface;
use App\Models\Appointment as AppointmentModel;

class ApointmentService implements ApointmentInterface {
  
  protected $appointment;
  
  public function __construct(AppointmentModel $appointmentModel)
  {
    $this->appointment = $appointmentModel;
  }
  
  public function all($columns = array('*'), $relations = [])
  {
    $dates = $relations['dates'];
   
    // $appointments = $this->appointment

    if ($relations['searchType'] == 2 && 
      ((!isset($relations['fullname']) || $relations['fullname'] === '') &&
      (!isset($relations['dni']) || $relations['dni'] === ''))) {
        return [];
    } else {
      $appointments = \DB::table('appointments as app')
      ->select(
        'app.id as id',
        'ac.name as app_code_name',
        'app.code',
        'app.type',
        'app.start_time',
        'app.end_time',
        'app.status_start',
        'app.status_end',
        'app.date',
        'app.sede_id',
        'app.type',
        'app.patient_id',
        'p.fullname',
        'p.monthly_apps',
        'p.phone as patient_phone',
        'p.place as patient_place',
        'ms.name as sede_name',
        'ms.color as sede_color',
        'mser.alias as service_name',
        \DB::raw('group_concat(psd.simple_diagnostic_id SEPARATOR ", ") as simple_diagnostics')
      )
      ->leftJoin('patients as p', 'p.id', '=', 'app.patient_id')
      ->leftJoin('maintenance_sedes as ms', 'ms.id', '=', 'app.sede_id')
      ->leftJoin('appointment_codes as ac', 'ac.id', '=', 'app.code')
      ->leftJoin('medical_histories as mh', function($lj) {
        $lj->on('mh.patient_id', '=', 'app.patient_id');
        $lj->on(\DB::raw('mh.active'), \DB::raw('='), \DB::raw(1));
      })
      ->leftJoin('patients_medical_info as pmi', 'pmi.medical_history_id',  '=', 'mh.id')
      // ->leftJoin(
      //   \DB::raw('
      //   (SELECT * FROM patients_medical_info A 
      //     WHERE id = (SELECT MAX(id) FROM patients_medical_info B WHERE A.id=B.id)) AS pmi'), 
      //     function($join) {
      //       $join->on('pmi.patient_id', '=', 'p.id');
      // })
      ->leftJoin('patient_simple_diagnostics as psd', 'psd.patient_id', '=', 'pmi.patient_id')
      ->leftJoin('maintenance_services as mser', 'mser.id', '=', 'pmi.service_id')
      ->where(function ($q) use ($relations){
        if (isset($relations['fullname']) && $relations['fullname'] !== '') {
          $q->where('p.fullname','like', '%'.$relations['fullname'].'%');
        }
        if (isset($relations['code']) && $relations['code'] !== '') {
          $q->where('app.code', $relations['code']);
        }
        if (isset($relations['services']) && $relations['services'] !== '') {
          $q->where('p.service_id', $relations['services']);
        }
        if (isset($relations['sedes']) && $relations['sedes'] !== '') {
          $q->where('app.sede_id', $relations['sedes']);
        }
        if (isset($relations['dni']) && $relations['dni'] !== '') {
          $q->where('p.dni', $relations['dni']);
        }
        if(count($relations['dates'])) {
          $q->whereIn('app.date', $relations['dates']);
        }  
        
      })
      ->groupBy('app.id')
      ->orderBy('app.start_time', 'ASC')
      ->get()
      ->toArray();
    }
    
    $newAppointments = [];
  
    usort($dates, function ($a, $b) {
        return strtotime($a) - strtotime($b);
    });


    if ($relations['searchType'] == 1) {
      foreach ($dates as $date) {
        $newAppointments[$date] = [];
        foreach ($appointments as $key => $appointment) {
          if ($date === $appointment->date) {
            $newAppointments[$date][] = $appointment;

          }
        }
      }
    } else {
      foreach ($appointments as $key => $appointment) {
        $newAppointments[$appointment->date][] = $appointment;
      }
    }

    return $newAppointments;

  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {   
    return $this->appointment->with('profile')->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {
    $newAppointments = $data['appointments'];
    $statesArr = [];
    $message = 'Citas creadas correctamente';
    $state = 1;
    $canCreate = 1; //1 es posible // 0 error / 2 no se creo

    foreach($newAppointments as $key => $newAppointment) {
      if ($canCreate === 1) {

        $existAppointments = $this->appointment
                ->where(function ($q) use($newAppointment){
                  $q->where('start_time', '<=', $newAppointment['start_time'])
                    ->where('end_time', '>', $newAppointment['start_time']);
                })
                ->orWhere(function ($q) use($newAppointment){
                  $q->where('start_time', '<', $newAppointment['end_time'])
                    ->where('end_time', '>=', $newAppointment['end_time']);
                })
                ->get();
                
        if (count($existAppointments)) {
          foreach($existAppointments as $existApp) {
            if ($existApp['sede_id'] === $newAppointment['sede_id']) {
              $canCreate = 0;
              $msg = 'Ya existe una cita agendada en esta sede para este horario';
              break;
            }
            else if($existApp['code'] === 1 && in_array($newAppointment['code'], [1,2])) {
              $canCreate = 0;
              $msg = 'Ya existe una CN en este horario';
              break;
            }
            else if($existApp['code'] === 2 && in_array($newAppointment['code'], [1,2])) {
              $canCreate = 0;
              $msg = 'Ya existe una T01 en este horario';
              break;
            }
          }
        }        
      }            
          
      if ($canCreate === 1) {
        $newAppointment['patient_id'] = $data['patient_id'];
        $newAppointment = $this->appointment->create($newAppointment);
        $statesArr[$key]['status'] = $canCreate;
        $statesArr[$key]['msg'] = 'Se creo la cita correctamente.';
      } else if ($canCreate === 0) {
        $statesArr[$key]['status'] = $canCreate;
        $message = 'Error para la cita '. ($key + 1) . ': ' . $msg;
        $statesArr[$key]['msg'] = $message;
        $state = 0;
        $canCreate = 2;
      } else {
        $statesArr[$key]['status'] = 0;
        $statesArr[$key]['msg'] = 'No se logro crear la cita';
      }

    }
    
    return ['message' => $message,  'errors'=> $statesArr, 'state' => $state];
  }
  
  public function update(array $data, $id)
  {
    list($updAppointment) = $data['appointments'];
    $existAppointments = $this->appointment
                ->where(function ($q) use($updAppointment, $id){
                  $q->where('start_time', '<=', $updAppointment['start_time'])
                    ->where('end_time', '>', $updAppointment['start_time'])
                    ->where('id', '!=', $id);
                })
                ->orWhere(function ($q) use($updAppointment, $id){
                  $q->where('start_time', '<', $updAppointment['end_time'])
                    ->where('end_time', '>=', $updAppointment['end_time'])
                    ->where('id', '!=', $id);
                })
                ->get();

    $canUpdate = 1;
    if (count($existAppointments)) {

      foreach($existAppointments as $appointment) {
        if ($appointment['sede_id'] === $updAppointment['sede_id']) {
          $canUpdate = 0;
          $msg = 'Ya existe una cita agendada en esta sede para este horario';
          break;
        }
        else if($appointment['code'] === 1 && in_array($updAppointment['code'], [1,2])) {
          $canUpdate = 0;
          $msg = 'Ya existe una CN en este horario';
          break;
        }
        else if($appointment['code'] === 2 && in_array($updAppointment['code'], [1,2])) {
          $canUpdate = 0;
          $msg = 'Ya existe una T01 en este horario';
          break;
        }
      }
    }            

    if ($canUpdate === 1) {
      $msg = 'Se actualizo la cita correctamente';
      $updAppointments['patient_id'] = $data['patient_id'];
      $appointmentGot = $this->appointment->find($id);
      $appointmentGot->update($updAppointment);
      $statesArr = [['status' =>  $canUpdate, 'msg' =>'Se actualizo la cita']];
    } else  {
      $statesArr = [['status' =>  $canUpdate, 'msg' => $msg]];
    } 

    return ['message' => $msg,  'errors'=> $statesArr, 'state' => $canUpdate];
  }
  
  public function delete($id)
  {
    $this->appointment->destroy($id);
    return ['message' => 'Cita eliminada'];
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->appointment->find($id, $columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->appointment->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Impselement search() method.
  }

}