<?php namespace App\Services;

use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;

class OlvaService
{
    public $token = 'T7AeAFArKyXTbxKV3zeG5P6pXp7sau';
    public $urlCreateOrder = 'https://api.aimo.co/core/orders/';
    public $urlGetOrder = 'https://api.aimo.co/core/orders/';
    public $urlGetBatch = "https://api.aimo.co/core/branch_offices/?location=";

    public function getBody($gestionId, $tipo, $fecha)
    {
        $gestion = Gestion::find($gestionId);
        $cliente = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);

        if($tipo == 1){
            $tipo_servicio = $gestion->codigo_repo.'-ida';

            $CustomerPhone = $cliente->celular;
            $CustomerAddress = $cliente->direccion;
            $CustomerName = $cliente->nombres.' '.$cliente->apellidos;
            $CustomerEmail = $cliente->correo;
            $CustomerLatitud = $cliente->latitud;
            $CustomerLangitud = $cliente->longitud;

            $ConsigneeAddress = $tienda->address;
            $ConsigneeEmail = $tienda->email;
            $ConsigneeLatitud = $tienda->latitud;
            $ConsigneeLangitud = $tienda->longitud;
            $ConsigneeName = $tienda->business_name;
            $ConsigneePhone = $tienda->phone;
        }if($tipo == 2){
            $tipo_servicio = $gestion->codigo_repo.'-ida';

            $CustomerPhone = $tienda->phone;
            $CustomerAddress = $tienda->address;
            $CustomerName = $tienda->business_name;
            $CustomerEmail = $tienda->email;
            $CustomerLatitud = $tienda->latitud;
            $CustomerLangitud = $tienda->longitud;

            $ConsigneeAddress = $cliente->direccion;
            $ConsigneeEmail = $cliente->correo;
            $ConsigneeLatitud = $cliente->latitud;
            $ConsigneeLangitud = $cliente->longitud;
            $ConsigneeName = $cliente->nombres.' '.$cliente->apellidos;
            $ConsigneePhone = $cliente->celular;
        }elseif ($tipo == 3) {
            $tipo_servicio = $gestion->codigo_repo.'-idayvuelta_origin';
            $CustomerPhone = $cliente->celular;
            $CustomerAddress = $cliente->direccion;
            $CustomerName = $cliente->nombres.' '.$cliente->apellidos;
            $CustomerEmail = $cliente->correo;
            $CustomerLatitud = $cliente->latitud;
            $CustomerLangitud = $cliente->longitud;

            $ConsigneeAddress = $tienda->address;
            $ConsigneeEmail = $tienda->email;
            $ConsigneeLatitud = $tienda->latitud;
            $ConsigneeLangitud = $tienda->longitud;
            $ConsigneeName = $tienda->business_name;
            $ConsigneePhone = $tienda->phone;
        }elseif ($tipo == 4) {
            $tipo_servicio = $gestion->codigo_repo.'-idayvuelta_destination';
            $CustomerPhone = $tienda->phone;
            $CustomerAddress = $tienda->address;
            $CustomerName = $tienda->business_name;
            $CustomerEmail = $tienda->email;
            $CustomerLatitud = $tienda->latitud;
            $CustomerLangitud = $tienda->longitud;

            $ConsigneeAddress = $cliente->direccion;
            $ConsigneeEmail = $cliente->correo;
            $ConsigneeLatitud = $cliente->latitud;
            $ConsigneeLangitud = $cliente->longitud;
            $ConsigneeName = $cliente->nombres.' '.$cliente->apellidos;
            $ConsigneePhone = $cliente->celular;
        }
        $body = [
            "reference_code" => "$tipo_servicio",
            "order_type" => "delivery",
            "programmed_date" => "$fecha",
            "time_window" => "09:00-18:00",
            "is_immediate" => true,
            "time_window_start" => "09:00",
            "time_window_end" => "18:00",
            "batch" => "REPO",
            "branch_office" => "REPO",
            "auto_assign_worker" => false,
            "client_id" => 16,
            "tasks" => [
                [
                    "task_type" => "pickup",
                    "coordinates" => [
                        "latitude" => $CustomerLatitud,
                        "longitude" => $CustomerLangitud
                    ],
                    "address" => "$CustomerAddress",
                    "address_detail" => "Almacen central Olva Courier",
                    "contact_person" => "$CustomerName",
                    "contact_phone" => "$CustomerPhone",
                    "contact_email" => "$CustomerEmail" 
                ],
                [
                    "task_type" => "dropoff",
                    "address" => "$ConsigneeAddress",
                    "ubigeo" => "150130",
                    "address_detail" => "",
                    "contact_person" => "$ConsigneeName",
                    "contact_phone" => "$ConsigneePhone",
                    "contact_email" => "$ConsigneeEmail",
                    "items" => [
                        "description" => "SOBRE",
                        "quantity" => 1,
                        "weight" => "1.000",
                        "height" => "0.000",
                        "depth" => "0.000",
                        "width" => "0.000"
                    ],
                    "description" => "SERVICIO CREADO POR pruebas de royer."
                ]
            ]
        ];

        return $body;
    }

    public function obtenerBatch()
    {
        // API Nearby para obtener la zona correspondiente, enviar solo la dirección de destino.
        // GET -> https://api.aimo.co/core/branch_offices/?location=Calle Los Capulíes, Miraflores
        // Authorization: Bearer {bearer_token}
        //Json de ejemplo que retorna, solo se debe enviar el reference_code del objecto correspondiente al resultado que arroje.
        //Nota: Cuando no se tenga resultado de las Zonas, enviar reference_code: REPO
        // [
        //     {
        //       "id": 122,
        //       "name": "OLVA PRINCIPAL - REPO",
        //       "reference_code": "REPO"  <====== Por defecto cuando el API no devuelva una zona.
        //     },
        // ]
        return $batch;
    }


}