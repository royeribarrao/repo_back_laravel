<?php namespace App\Services;

use App\Services\Contracts\InvoiceInterface as InvoiceInterface;
use App\Models\Invoice as InvoiceModel;
use App\Models\InvoiceDiagnostic as InvoiceDiagnosticModel;
use App\Models\InvoiceDiscount as InvoiceDiscountModel;
use App\Models\Patient as PatientModel;

class InvoiceService implements InvoiceInterface {
  
  protected $invoice;
  protected $discounts;
  protected $diagnostics;
  protected $patient;
  
  public function __construct(
                  PatientModel $patientModel,
                  InvoiceModel $invoiceModel, 
                  InvoiceDiagnosticModel $invoiceDiagnosticModel, 
                  InvoiceDiscountModel $invoiceDiscountModel)
  {
    $this->patient = $patientModel;
    $this->invoice = $invoiceModel;
    $this->discounts = $invoiceDiscountModel;
    $this->diagnostics = $invoiceDiagnosticModel;
  }
  
  public function all($columns = array('*'), $relation = [])
  {
    return $this->patient->get($columns);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    return $this->patient->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {
    $diagnostics = $data['diagnosticos'];
    $discounts = $data['descuentos'];  

    $newInvoice = $this->invoice->create($data);

    $diagnostics = array_map( 
      function($diagnostic) use ($newInvoice) { 
        $diagnostic['invoice_id'] = $newInvoice->id;
        return $diagnostic;
       },
      $diagnostics
    );

    $discounts = array_map( 
      function($discount) use ($newInvoice) { 
        $discount['invoice_id'] = $newInvoice->id;
        return $discount;
       },
      $discounts
    );

    $this->discounts->insert($discounts);
    $this->diagnostics->insert($diagnostics);

    return $newInvoice;
  }
  
  public function update(array $data, $id)
  {
    $invoicetGot = $this->invoice->find($id);
    $invoicetGot->fill($data);
    $invoicetGot->save();

    $this->discounts->where('invoice_id', $invoicetGot->id)->delete();
    $this->diagnostics->where('invoice_id', $invoicetGot->id)->delete();

    $diagnostics = $data['diagnosticos'];
    $discounts = $data['descuentos'];  

    $diagnostics = array_map( 
      function($diagnostic) use ($id) { 
        $diagnostic['invoice_id'] = $id;
        return $diagnostic;
       },
      $diagnostics
    );

    $discounts = array_map( 
      function($discount) use ($id) { 
        $discount['invoice_id'] = $id;
        return $discount;
       },
      $discounts
    );

    $this->discounts->insert($discounts);
    $this->diagnostics->insert($diagnostics);
    
    return $invoicetGot;
  }
  
  public function delete($id)
  {
    $this->invoice->destroy($id);
    return ['message' => 'Proforma eliminada'];
  }
  
  public function find($userId, $columns = array('*'))
  {
    return $this->invoice
      ->with('diagnostics', 'discounts')
      ->where('user_id', $userId)
      ->orderBy('id', 'desc')
      ->get($columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->invoice->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Implement search() method.
  }
  
}