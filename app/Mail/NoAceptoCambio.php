<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NoAceptoCambio extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $motivo;

    public function __construct($info, $motivo)
    {
        $this->info = $info;
        $this->motivo = $motivo;
    }

    public function build()
    {
        return $this->subject('Respuesta a su solicitud de cambio')->view('mails.cambio.no_acepto_cambio');
    }
}