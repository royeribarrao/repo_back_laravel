<?php

namespace App\Mail\Repo;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvisoPedidoRepo extends Mailable
{
    use Queueable, SerializesModels;

    public $codigo;
    public $tienda;
    public $flujo;

    public function __construct($codigo, $flujo, $tienda)
    {
        $this->codigo = $codigo;
        $this->flujo = $flujo;
        $this->tienda = $tienda;
    }

    public function build()
    {
        return $this->subject('¡Nuevo pedido!')->view('mails.repo.aviso');
    }
}
