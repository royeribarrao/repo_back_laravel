<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductoNuevoEntregado extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $carrier;

    public function __construct($info, $carrier)
    {
        $this->info = $info;
        $this->carrier = $carrier;
    }

    public function build()
    {
        return $this->subject('¡Nuevo producto entregado!')->view('mails.cambio.producto_nuevo_entregado');
    }
}