<?php

namespace App\Mail\Wiqli;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvisoDashboard extends Mailable
{
    use Queueable, SerializesModels;

    public $pedido;
    public $cliente;
    public $productos;
    public $productosAdicionales;
    public function __construct($pedido, $cliente, $productos, $productosAdicionales)
    {
        $this->pedido = $pedido;
        $this->cliente = $cliente;
        $this->productos = $productos;
        $this->productosAdicionales = $productosAdicionales;
    }

    public function build()
    {
        return $this->subject('¡Muchas gracias por comprar en Wiqli!')->view('wiqli.avisoWiqli');
    }
}
