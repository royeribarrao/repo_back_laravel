<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmacionOperadorLogisticoEntrega extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $carrier;
    public $fecha;

    public function __construct($info, $carrier, $fecha)
    {
        $this->info = $info;
        $this->carrier = $carrier;
        $this->fecha = $fecha;
    }

    public function build()
    {
        return $this->subject('Tu nuevo producto llegará el '.$this->fecha)->view('mails.confirmacion_operador_logistico_entrega');
    }
}