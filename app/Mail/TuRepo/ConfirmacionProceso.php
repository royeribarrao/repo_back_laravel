<?php

namespace App\Mail\TuRepo;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmacionProceso extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $codigo;
    public $fecha;

    public function __construct($info, $codigo, $fecha)
    {
        $this->info = $info;
        $this->codigo = $codigo;
        $this->fecha = $fecha;
    }

    public function build()
    {
        return $this->subject('Solicitud N° '.$this->codigo.' confirmada')->view('mails.tuRepo.confirmacion_pedido');
    }
}
