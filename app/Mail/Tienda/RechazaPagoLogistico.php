<?php

namespace App\Mail\Tienda;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RechazaPagoLogistico extends Mailable
{
    use Queueable, SerializesModels;

    public $gestion;
    public $tienda;
    public $cliente;

    public function __construct($gestion, $tienda, $cliente)
    {
        $this->gestion = $gestion;
        $this->tienda = $tienda;
        $this->cliente = $cliente;
    }
    public function build()
    {
        return $this->view('view.name');
    }
}
