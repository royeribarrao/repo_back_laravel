<?php

namespace App\Mail\Tienda;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AceptaPagoLogistico extends Mailable
{
    use Queueable, SerializesModels;

    public $gestion;
    public $tienda;

    public function __construct($gestion, $tienda)
    {
        $this->gestion = $gestion;
        $this->tienda = $tienda;
    }

    public function build()
    {
        return $this->view('view.name');
    }
}
