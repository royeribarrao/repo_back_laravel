<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmacionOperadorLogisticoRecojo extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $carrier;

    public function __construct($info, $carrier)
    {
        $this->info = $info;
        $this->carrier = $carrier;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Operador logístico de recojo asignado')->view('mails.confirmacion_operador_logistico_recojo');
    }
}
