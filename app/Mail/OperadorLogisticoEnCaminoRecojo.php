<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OperadorLogisticoEnCaminoRecojo extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $carrier;

    public function __construct($info, $carrier)
    {
        $this->info = $info;
        $this->carrier = $carrier;
    }

    public function build()
    {
        return $this->subject('¡Tu operador logístico está en camino a recoger tus productos!')->view('mails.operador_logistico_camino_recojo');
    }
}
