<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductoNuevoEnCamino extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $carrier;

    public function __construct($info, $carrier)
    {
        $this->info = $info;
        $this->carrier = $carrier;
    }

    public function build()
    {
        return $this->subject('¡Nuevo producto en camino!')->view('mails.cambio.producto_nuevo_en_camino');
    }
}