<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\TrackerDevolucion;

class AvisoTrackerDevolucion extends Mailable
{
    use Queueable, SerializesModels;

    public $tracker;

    public function __construct(TrackerDevolucion $tracker)
    {
        $this->tracker = $tracker;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.tracker_devolucion');
    }
}
