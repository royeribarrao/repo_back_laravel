<?php

namespace App\Mail\ServicioTecnico;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductoArreglado extends Mailable
{
    use Queueable, SerializesModels;

    public $gestion;
    public $cliente;
    public $tienda;

    public function __construct($gestion, $cliente, $tienda)
    {
        $this->gestion = $gestion;
        $this->cliente = $cliente;
        $this->tienda = $tienda;
    }

    public function build()
    {
        return $this->subject('¡Producto arreglado!')->view('newMails.productoArreglado');
    }
}
