<?php

namespace App\Mail\ServicioTecnico;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LogisticoEnCaminoTecnico extends Mailable
{
    use Queueable, SerializesModels;

    public $gestion;
    public $cliente;
    public $tienda;
    public $logistico;

    public function __construct($gestion, $cliente, $tienda, $logistico)
    {
        $this->gestion = $gestion;
        $this->cliente = $cliente;
        $this->tienda = $tienda;
        $this->logistico = $logistico;
    }

    public function build()
    {
        return $this->subject('¡Logístico en camino técnico!')->view('newMails.logisticoEnCaminoTecnico');
    }
}
