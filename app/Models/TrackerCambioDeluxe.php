<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackerCambioDeluxe extends Model
{
    protected $table = 'tracker_cambio_deluxe';
    protected $fillable = [
        'id',
        'gestion_id',
        'pedido_recibido',
        'operador_logistico_confirmado',
        'en_camino',
        'producto_recogido',
        'producto_devuelto',
        'cambio_aceptado',
        'producto_nuevo_en_camino',
        'producto_nuevo_entregado',
        'nombre_estado',
        'estado'
    ];
}