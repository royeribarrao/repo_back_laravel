<?php namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Traits\Fullname;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Fullname;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'fullname',
        'father_lastname',
        'mother_lastname',
        'dni',
        'phone',
        'address',
        'latitud',
        'longitud',
        'referencia',
        'distrito',
        'departamento',
        'state',
        'rol_id',
        'place_id',
        'password',
        'tienda_id',
        'updated_user',
        'created_user',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    public function setPasswordAttribute($value){
        if ($value != ''){
            $this->attributes['password']= app('hash')->make($value);
        }
    }

    public function rol() {
        return $this->hasOne(Rol::class, 'id', 'rol_id');
    }

    public function tienda() {
        return $this->hasOne(Tienda::class, 'id', 'tienda_id');
    }

    public function sede() {
        return $this->hasOne(Sede::class, 'id', 'place_id');
    }
}
