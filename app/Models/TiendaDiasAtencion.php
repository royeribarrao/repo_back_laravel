<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaDiasAtencion extends Model
{
    use HasFactory;
    protected $table = 'tienda_dias_atencions';
    protected $fillable = [
        'id',
        'tienda_id',
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado',
        'domingo'
    ];
}