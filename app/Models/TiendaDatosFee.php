<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaDatosFee extends Model
{
    use HasFactory;
    protected $table = 'tienda_datos_fees';
    protected $fillable = [
        'id',
        'tienda_id',
        'nombre',
        'codigo',
        'tipo',
        'monto'
    ];
}
