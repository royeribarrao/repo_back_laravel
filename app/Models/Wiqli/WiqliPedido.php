<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliPedido extends Model
{
    use HasFactory;
    protected $table = 'wiqli_pedidos';
    protected $fillable = [
        'id',
        'cliente_id',
        'total',
        'costo_delivery',
        'fecha_pedido',
        'fecha_entrega',
        'fecha_pago',
        'observacion'
    ];

    public function cliente() {
        return $this->hasOne(WiqliCliente::class, 'id', 'cliente_id');
    }

    public function detalle() {
        return $this->hasMany(WiqliDetallePedido::class, 'pedido_id', 'id');
    }
}
