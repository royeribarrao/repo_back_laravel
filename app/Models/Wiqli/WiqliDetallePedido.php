<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliDetallePedido extends Model
{
    use HasFactory;
    protected $table = 'wiqli_detalles_pedido';
    protected $fillable = [
        'id',
        'pedido_id',
        'producto_id',
        'nombre_desc',
        'cantidad_desc',
        'cantidad',
        'precio_unitario',
        'total'
    ];

    public function producto() {
        return $this->hasOne(WiqliProducto::class, 'id', 'producto_id');
    }
}
