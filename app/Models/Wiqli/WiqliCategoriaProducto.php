<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliCategoriaProducto extends Model
{
    use HasFactory;
    protected $table = 'wiqli_categorias_producto';
    protected $fillable = [
        'id',
        'nombre',
        'codigo'
    ];
}