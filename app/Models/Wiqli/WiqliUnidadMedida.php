<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliUnidadMedida extends Model
{
    use HasFactory;
    protected $table = 'wiqli_unidades_medida';
    protected $fillable = [
        'id',
        'nombre',
        'valor',
        'abreviatura'
    ];
}
