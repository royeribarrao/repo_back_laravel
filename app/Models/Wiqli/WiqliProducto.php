<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliProducto extends Model
{
    use HasFactory;
    protected $table = 'wiqli_productos';
    protected $fillable = [
        'id',
        'categoria_id',
        'unidad_id',
        'nombre',
        'precio_unitario',
        'precio_promedio',
        'cantidad_minima',
        'activo',
        'stock'
    ];

    public function unidad() {
        return $this->hasOne(WiqliUnidadMedida::class, 'id', 'unidad_id');
    }

    public function categoria() {
        return $this->hasOne(WiqliCategoriaProducto::class, 'id', 'categoria_id');
    }
}
