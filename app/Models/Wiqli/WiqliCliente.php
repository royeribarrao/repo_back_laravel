<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliCliente extends Model
{
    use HasFactory;
    protected $table = 'wiqli_clientes';
    protected $fillable = [
        'id',
        'nombres',
        'apellidos',
        'direccion',
        'referencia',
        'distrito',
        'provincia',
        'departamento',
        'telefono',
        'correo',
        'latitud',
        'longitud'
    ];
}
