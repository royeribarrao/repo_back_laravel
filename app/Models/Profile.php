<?php

namespace App\Models;

use App\Traits\Fullname;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    
  protected $fillable = [
    'id',
    'name',
  ];

}
