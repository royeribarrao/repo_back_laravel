<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tienda extends Model
{
    protected $table = 'tiendas';
    protected $fillable = [
        'id',
        'business_name',
        'razon_social',
        'state',
        'ruc',
        'logo',
        'departamento',
        'provincia',
        'distrito',
        'address',
        'codigo_postal',
        'email',
        'phone',
        'latitud',
        'longitud',
        'aliada',
        'orden'
    ];

    public function contacto()
    {
        return $this->hasOne(ContactoTienda::class, 'tienda_id', 'id');
    }
    
    public function opciones() {
        return $this->hasMany(\App\Models\TuRepo\RepoTiendaOpcion::class, 'tienda_id', 'id');
    }

    public function correos() {
        return $this->hasMany(\App\Models\Tienda\TiendaCorreo::class, 'tienda_id', 'id');
    }

    public function integracion()
    {
        return $this->hasOne(Integracion::class, 'tienda_id', 'id');
    }

    public function diasAtencion()
    {
        return $this->hasOne(TiendaDiasAtencion::class, 'tienda_id', 'id');
    }

    public function configuracion()
    {
        return $this->hasOne(ConfiguracionTienda::class, 'tienda_id', 'id');
    }

    public function tiendaFee()
    {
        return $this->hasMany(TiendaDatosFee::class, 'tienda_id', 'id');
    }
}