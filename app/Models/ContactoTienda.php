<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactoTienda extends Model
{
    use HasFactory;
    protected $table = 'contacto_tienda';
    protected $fillable = [
        'id',
        'tienda_id',
        'nombre_contacto',
        'apellido_contacto',
        'telefono_contacto'
    ];
}
