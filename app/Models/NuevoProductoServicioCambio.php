<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NuevoProductoServicioCambio extends Model
{
    protected $table = 'nuevos_productos_servicio_cambio';
    protected $fillable = [
        'id',
        'gestion_id',
        'producto_id',
        'price',
        'name',
        'sku',
        'cantidad'
    ];

    public function producto() {
        return $this->hasOne(Producto::class, 'id', 'producto_id');
    }

    public function productoWooCommerce() {
        return $this->hasOne(\App\Models\Woocommerce\WoocommerceProducto::class, 'id', 'producto_id');
    }

    public function gestion() {
        return $this->belongTo(Gestion::class, 'id', 'gestion_id');
    }
}