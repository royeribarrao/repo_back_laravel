<?php

namespace App\Models;

use App\Traits\Fullname;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceSede extends Model
{
    
//   use SoftDeletes;

  protected $table = "maintenance_sedes";
  protected $fillable = [
    'id',
    'name',
    'color',
    'order',
  ];

}
