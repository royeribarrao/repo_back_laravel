<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'id',
        'tienda_id',
        'integracion_id',
        'producto_id',
        'categoria_id',
        'color',
        'description',
        'image',
        'name',
        'parent_id',
        'permalink',
        'price',
        'purchasable',
        'regular_price',
        'sale_price',
        'sku',
        'status',
        'stock_quantity',
        'talla',
        'type',
        'actualizado',
        'nombre_concatenado'
    ];
}
