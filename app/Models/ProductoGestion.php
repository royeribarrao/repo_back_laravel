<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoGestion extends Model
{
    protected $table = 'productos_gestion';
    protected $fillable = [
        'id',
        'gestion_id',
        'producto_id',
        'sku_producto',
        'nombre_producto',
        'link_producto',
        'precio',
        'motivo',
        'imagen_boleta',
        'imagen_producto',
        'evidencia',
        'comentario_evidencia'
    ];

    public function producto() {
        return $this->hasOne(Producto::class, 'id', 'producto_id');
    }

    public function gestion() {
        return $this->belongTo(Gestion::class, 'id', 'gestion_id');
    }

    public function motivoDevolucion() {
        return $this->hasOne(MotivoDevolucion::class, 'id', 'motivo');
    }
}