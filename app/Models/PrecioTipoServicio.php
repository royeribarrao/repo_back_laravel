<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrecioTipoServicio extends Model
{
    protected $table = 'precio_tipo_servicio';
    protected $fillable = [
        'id',
        'nombre',
        'tipo_servicio_id',
        'costo'
    ];
}