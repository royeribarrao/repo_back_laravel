<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatosDelivery extends Model
{
    protected $table = 'datos_delivery';
    protected $fillable = [
        'id',
        'gestion_id',
        'nombres',
        'apellidos',
        'dni',
        'departamento',
        'provincia',
        'distrito',
        'direccion',
        'referencia',
        'celular',
        'correo',
        'fecha_recojo',
        'latitud',
        'longitud'
    ];
}