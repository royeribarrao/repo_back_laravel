<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Integracion extends Model
{
    use HasFactory;
    protected $table = 'integracions';
    protected $fillable = [
        'id',
        'nombre'
    ];
}
