<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoTiendaOpcion extends Model
{
    use HasFactory;
    protected $table = 'repo_tienda_opcions';
    protected $fillable = [
        'id',
        'tienda_id',
        'opcion_id'
    ];

    public function opcion() {
        return $this->hasOne(RepoOpciones::class, 'id', 'opcion_id');
    }
}
