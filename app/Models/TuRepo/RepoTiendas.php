<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoTiendas extends Model
{
    use HasFactory;
    protected $table = 'repo_tiendas';
    protected $fillable = [
        'id',
        'nombre_tienda',
        'razon_social',
        'es_aliada',
        'ruc',
        'logo'
    ];
    
    public function opciones() {
        return $this->hasMany(RepoTiendaOpcion::class, 'tienda_id', 'id');
    }
}
