<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoOpciones extends Model
{
    use HasFactory;
    protected $table = 'repo_opciones';
    protected $fillable = [
        'id',
        'codigo',
        'nombre'
    ];
}
