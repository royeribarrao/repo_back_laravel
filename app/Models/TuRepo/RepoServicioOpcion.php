<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoServicioOpcion extends Model
{
    use HasFactory;
    protected $table = 'repo_servicio_opcions';
    protected $fillable = [
        'id',
        'servicio_id',
        'prioridad',
        'opcion_id'
    ];

    public function servicio() {
        return $this->belongTo(RepoServicio::class, 'id', 'servicio_id');
    }

    public function opcion() {
        return $this->hasOne(RepoOpciones::class, 'id', 'opcion_id');
    }
}
