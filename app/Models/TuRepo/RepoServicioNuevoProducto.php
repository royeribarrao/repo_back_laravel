<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoServicioNuevoProducto extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'producto_id',
        'servicio_id',
        'precio'
    ];
}