<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoServicio extends Model
{
    use HasFactory;
    protected $table = 'repo_servicios';
    protected $fillable = [
        'id',
        'estado',
        'confirmacion_web',
        'tienda_id',
        'nombre_tienda',
        'referencia_tienda',
        'codigo',
        'producto',
        'link_producto',
        'precio',
        'imagen_boleta',
        'imagen_producto',
        'motivo',
        'distrito',
        'peticion',
        'nueva_talla',
        'comentario'
    ];

    public function delivery() {
        return $this->hasOne(RepoDatosDelivery::class, 'servicio_id', 'id');
    }

    public function opciones() {
        return $this->hasMany(RepoServicioOpcion::class, 'servicio_id', 'id');
    }

    public function tienda() {
        return $this->hasOne(\App\Models\Tienda::class, 'id', 'tienda_id');
    }
}
