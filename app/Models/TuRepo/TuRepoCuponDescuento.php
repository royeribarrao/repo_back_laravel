<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuRepoCuponDescuento extends Model
{
    use HasFactory;
    protected $table = 'tu_repo_cupones_descuento';
    protected $fillable = [
        'id',
        'codigo',
        'descuento',
        'cantidad_total',
        'cantidad_usada',
        'cantidad_restante',
        'fecha_inicio',
        'fecha_expiracion'
    ];
}
