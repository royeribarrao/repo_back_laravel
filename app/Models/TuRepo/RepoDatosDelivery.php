<?php

namespace App\Models\TuRepo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepoDatosDelivery extends Model
{
    use HasFactory;
    protected $table = 'repo_datos_deliveries';
    protected $fillable = [
        'id',
        'servicio_id',
        'nombres',
        'apellidos',
        'dni',
        'email',
        'direccion_recojo',
        'referencia',
        'distrito',
        'latitud',
        'longitud',
        'fecha_recojo',
        'celular',
        'metodo_pago'
    ];
}
