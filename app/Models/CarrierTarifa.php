<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarrierTarifa extends Model
{
    protected $table = 'carrier_tarifas';
    protected $fillable = [
        'id',
        'carrier_id',
        'tipo_servicio_id',
        'tipo_vehiculo',
        'precio',
        'hora_maxima_pedido',
        'hora_recojo_l_v',
        'hora_recojo_s',
        'hora_recojo_d',
        'hora_entrega'
    ];
}