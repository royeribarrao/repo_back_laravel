<?php

namespace App\Models\Tienda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaCorreo extends Model
{
    use HasFactory;
    protected $table = 'tienda_correos';
    protected $fillable = [
        'id',
        'tienda_id',
        'email',
        'tipo'
    ];
}
