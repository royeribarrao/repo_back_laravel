<?php

namespace App\Models\Tienda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaSede extends Model
{
    use HasFactory;
    protected $table = 'tienda_sedes';
    protected $fillable = [
        'id',
        'tienda_id',
        'usuario_id',
        'tipo'
    ];
}
