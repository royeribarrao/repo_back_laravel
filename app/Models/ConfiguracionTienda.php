<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfiguracionTienda extends Model
{
    protected $table = 'configuracion_tienda';
    protected $fillable = [
        'id',
        'tienda_id',
        'plazo',
        'integracion_id',
        'tienda_dias_atencion_id',
        'compra_no_web',
        'requiere_boleta',
        'devolucion',
        'estandar',
        'deluxe',
        'servicio_tecnico'
    ];

    public function diasAtencion() {
        return $this->hasOne(TiendaDiasAtencion::class, 'tienda_id', 'tienda_id');
    }

    public function tiendaMotivoDevolucion() {
        return $this->hasMany(TiendaMotivoDevolucion::class, 'tienda_id', 'tienda_id');
    }

    public function tiendaDatosFee() {
        return $this->hasMany(TiendaDatosFee::class, 'tienda_id', 'tienda_id');
    }
}