<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayUTransaccion extends Model
{
    use HasFactory;
    protected $table = 'pay_u_transaccions';
    protected $fillable = [
        'id',
        'tipo',
        'state',
        'orderId',
        'servicio_id',
        'responseCode',
        'transactionId',
        'authorizationCode',
        'paymentNetworkResponseErrorMessage'
    ];
}
