<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackerDevolucion extends Model
{
    protected $table = 'tracker_devolucion';
    protected $fillable = [
        'id',
        'gestion_id',
        'pedido_recibido',
        'operador_logistico_confirmado',
        'en_camino',
        'producto_recogido',
        'producto_devuelto',
        'devolucion_aceptada',
        'dinero_devuelto',
        'nombre_estado',
        'estado'
    ];
}