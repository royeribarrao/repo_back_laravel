<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gestion extends Model
{
    protected $table = 'gestiones';
    protected $fillable = [
        'id',
        'entorno_id',
        'cliente_id',
        'tienda_id',
        'datos_delivery_id',
        'tipo_servicio',
        'codigo_compra',
        'codigo_repo',
        'fecha_recojo',
        'total_pago',
        'total_pago_cliente',
        'total_productos_gestion',
        'total_nuevos_productos',
        'total_devolucion',
        'cobro_neto',
        'costo_logistico',
        'cliente_paga_logistico',
        'confirmacion_web',
        'confirmacion_tienda',
        'estado',
        'en_proceso',
        'finalizado',
        'es_facturado'
    ];

    public function productoGestion() {
        return $this->hasMany(ProductoGestion::class, 'gestion_id', 'id');
    }

    public function nuevosProductos() {
        return $this->hasMany(NuevoProductoServicioCambio::class, 'gestion_id', 'id');
    }

    public function servicio() {
        return $this->hasOne(TipoServicio::class, 'codigo', 'tipo_servicio');
    }

    public function cliente() {
        return $this->hasOne(User::class, 'id', 'cliente_id');
    }

    public function tienda() {
        return $this->hasOne(Tienda::class, 'id', 'tienda_id');
    }

    public function delivery() {
        return $this->hasOne(DatosDelivery::class, 'id', 'datos_delivery_id');
    }

    public function servicioLogistico() {
        return $this->hasMany(ServicioLogistico::class, 'gestion_id', 'id');
    }

    public function trackerDevolucion() {
        return $this->hasOne(TrackerDevolucion::class, 'gestion_id', 'id');
    }

    public function trackerCambioEstandar() {
        return $this->hasOne(TrackerCambioEstandar::class, 'gestion_id', 'id');
    }

    public function trackerCambioDeluxe() {
        return $this->hasOne(TrackerCambioDeluxe::class, 'gestion_id', 'id');
    }
    public function compra() {
        return $this->hasOne(Sale::class, 'codigo_compra', 'codigo_compra');
    }
}