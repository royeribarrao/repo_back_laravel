<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicioLogistico extends Model
{
    protected $table = 'servicio_logistico';
    protected $fillable = [
        'id',
        'gestion_id',
        'carrier_id',
        'waybillNumber',
        'carrierWayBill',
        'tipo',
        'reference_code',
        'tracking_link'
    ];

    public function carrier() {
        return $this->hasOne(Carrier::class, 'id', 'carrier_id');
    }
}