<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaMotivoDevolucion extends Model
{
    use HasFactory;
    protected $table = 'tienda_motivo_devolucions';
    protected $fillable = [
        'id',
        'tienda_id',
        'motivo_devolucion_id',
        'activo',
        'pago_logistico'
    ];

    public function motivo() {
        return $this->hasOne(MotivoDevolucion::class, 'id', 'motivo_devolucion_id');
    }
}
