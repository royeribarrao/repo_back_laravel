<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    protected $table = 'ventas',
    $searchString = " ",
    $replaceString = "";
    protected $fillable = [
        'id',
        'tienda_id',
        'integracion_id',
        'status',
        'codigo_compra',
        'fecha_compra',
        'sku',
        'nombre_producto',
        'nombre_sin_espacios',
        'marca',
        'cantidad',
        'precio_pagado',
        'numero_documento',
        'nombre_cliente',
        'apellido_cliente',
        'telefono',
        'email',
        'direccion',
        'distrito',
        'provincia',
        'departamento'
    ];

    public function producto() {
        return $this->hasOne(Producto::class, 'sku', 'sku');
    }

    public function productoShopify() {
        return $this->hasOne(Producto::class, 'nombre_concatenado', 'nombre_sin_espacios');
    }

    // public function setNombreProductoAttribute($value)
    // {
    //     $this->attributes['nombre_producto'] = str_replace("-", "",str_replace(" ", "", $value));
    // }
}