<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackerCambioEstandar extends Model
{
    protected $table = 'tracker_cambio_estandar';
    protected $fillable = [
        'id',
        'gestion_id',
        'pedido_recibido',
        'operador_logistico_confirmado',
        'en_camino',
        'producto_recogido',
        'producto_devuelto',
        'cambio_aceptado',
        'operador_logistico_confirmado_vuelta',
        'producto_nuevo_en_camino',
        'producto_nuevo_entregado',
        'nombre_estado',
        'estado'
    ];
}