<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaAliada extends Model
{
    use HasFactory;
    protected $table = 'tienda_aliadas';
    protected $fillable = [
        'id',
        'nombre_empresa',
        'pagina_web',
        'categoria_productos',
        'persona_contacto',
        'email',
        'telefono',
        'mensaje'
    ];
}
