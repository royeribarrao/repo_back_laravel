<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperadorAliado extends Model
{
    use HasFactory;
    protected $table = 'operador_aliados';
    protected $fillable = [
        'id',
        'nombre_empresa',
        'pagina_web',
        'persona_contacto',
        'email',
        'telefono',
        'mensaje'
    ];
}
