<?php

namespace App\Models\Woocommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WoocommerceVariacionProductos extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'producto_id', 
        'sku', 
        'name',
        'stock_quantity',
        'type',
        'status',
        'image',
        'price'
    ];
}
