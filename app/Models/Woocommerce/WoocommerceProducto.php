<?php

namespace App\Models\Woocommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WoocommerceProducto extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'tienda_id',
        'color',
        'description',
        'image',
        'name',
        'parent_id',
        'permalink',
        'price',
        'producto_id',
        'purchasable',
        'regular_price',
        'sale_price',
        'sku',
        'status',
        'stock_quantity',
        'talla',
        'type',
        'actualizado'
    ];
}