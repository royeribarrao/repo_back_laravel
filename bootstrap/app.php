<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->bind(\App\Services\Contracts\ApointmentInterface::class, \App\Services\ApointmentService::class);
$app->bind(\App\Services\Contracts\UserInterface::class, \App\Services\UserService::class);

$app->bind(\App\Services\Contracts\StoreInterface::class, \App\Services\StoreService::class);
$app->bind(\App\Services\Contracts\ProductInterface::class, \App\Services\ProductService::class);
$app->bind(\App\Services\Contracts\SaleInterface::class, \App\Services\SaleService::class);

$app->bind(\App\Services\Contracts\MaintenanceSerInterface::class, \App\Services\MaintenanceSerService::class);
$app->bind(\App\Services\Contracts\MaintenanceSedeInterface::class, \App\Services\MaintenanceSedeService::class);
$app->bind(\App\Services\Contracts\InvoiceInterface::class, \App\Services\InvoiceService::class);
$app->bind(\App\Services\Contracts\PatientInterface::class, \App\Services\PatientService::class);
$app->bind(\App\Services\Contracts\DiagnosticInterface::class, \App\Services\DiagnosticService::class);
$app->bind(\App\Services\Contracts\MedicalHistoryInterface::class, \App\Services\MedicalHistoryInterface::class);
/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
