<style>
    table, th, td {
        border: 1px solid black;
    }
</style>
<html>
    <body>
        <h4>Reporte del usuario</h4>
        <table>
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Dirección</th>
                    <th>Fecha de entrega</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $pedido['cliente']['nombres'] }} {{ $pedido['cliente']['apellidos'] }}</td>
                    <td>{{ $pedido['cliente']['direccion'] .'-'. $pedido['cliente']['referencia'] }}</td>
                    <td>{{ $pedido['fecha_entrega'] }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <thead>
                <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Unidad</th>
                    <th>Total</th>
                  </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                        <td>{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                        <td>{{ $producto['producto'] ? $producto['producto']['unidad']['nombre'] : 'No tiene unidad' }}</td>
                        <td>{{ $producto['total'] }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">Total</td>
                    <td>{{$pedido['total']}}</td>
                </tr>
                <tr>
                    <td colspan="3">Costo Delivery</td>
                    <td>S/ 10.00</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
