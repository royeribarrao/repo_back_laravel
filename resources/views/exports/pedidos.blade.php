<table>
    
        
    <tbody>
        <tr>
            <td>Nombre</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($clientes as $cliente)
                <td>{{ $cliente['cliente']['nombres'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr>
            <td>Apellido</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($clientes as $cliente)
                <td>{{ $cliente['cliente']['apellidos'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr>
            <td>Celular</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($clientes as $cliente)
                <td>{{ $cliente['cliente']['telefono'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr>
            <th>Producto</th>
            <th>Unidad de medida</th>
            <th>Venta total</th>
            <th>Cantidad total</th>
            <th>Cantidad desagregada</th>
        </tr>
    @foreach($productosAptos as $producto)
        <tr>
            <td>{{ $producto['nombre'] }}</td>
            <td>{{ $producto['unidad']['abreviatura'] }}</td>
            <td>{{ $producto['total'] }}</td>
            <td>{{ $producto['cantidad'] }}</td>
            <td> </td>
            @foreach ($producto['personas'] as $item)
                <td>{{ $item['cantidad'] }}</td>
                <td>{{ $item['precio'] }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>