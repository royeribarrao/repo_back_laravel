@extends('mails.layout.body')
@section('content')

<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
    <tr>
        <td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">
            <p>Hola, {{ $info->delivery->nombres }} {{ $info->delivery->apellidos }}</p>
            <div class="col-md-12">
                <p>¡Gracias por usar Repo para devolver tu producto de {{ $info->tienda->business_name }}!</p>
                <p>Tu número de pedido Repo es el N° {{ $info->codigo_repo }}. Olvídate de los códigos. Créate una cuenta de Repo 
                   <a href="http://repo.com.pe/#/registrar-usuario">aquí</a>  
                   y sigue en vivo el estado de todos tu procesos.
                </p>
            </div>
            <div class="col-md-12">
                <h3>Resumen de tu proceso:</h3>
                <table>
                    <tr>
                        <th colspan="2">Montos (S/. )</th>
                    </tr>
                    <tr>
                        <td>Productos a devolver</td>
                        <td>S/. {{ $info->total_productos_gestion }}</td>
                    </tr>
                    <tr>
                        <td>Costo logístico</td>
                        @if($info->cliente_paga_logistico)
                            <td>S/. {{ $info->costo_logistico }}</td>
                        @endif
                        @if(!($info->cliente_paga_logistico))
                            <td>S/. 0.00</td>
                        @endif
                    </tr>
                </table>
                <table class="bg_white" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid black">
                    <tr>
                        <th colspan="5">Productos a devolver</th>
                    </tr>
                    @foreach ($productos as $key => $item)
                        <tr>
                            <td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">
                                <div class="product-entry">
                                    <img src="" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">
                                    <div class="text">
                                        <h3>{{ $item['sku_producto'] }}</h3>
                                        <p>{{ $item['nombre_producto'] }}</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" width="30%" style="text-align:center; padding-right: 2.5em;">
                                <span class="price" style="color: #f34949; font-size: 20px; display: block;">
                                    @if($item['motivo'] == 1)
                                        Razon de devolucion: No me quedó bien
                                    @endif
                                    @if($item['motivo'] == 2)
                                        Razon de devolucion: No era lo que esperaba
                                    @endif
                                    @if($item['motivo'] == 3)
                                        Razon de devolucion: Pedí otro producto por error
                                    @endif
                                    @if($item['motivo'] == 4)
                                        Razon de devolucion: Me entregaron otro producto
                                    @endif
                                    @if($item['motivo'] == 5)
                                        Razon de devolucion: Producto dañado
                                    @endif
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <table class="bg_white" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid black">
                    <tr>
                        <th colspan="5">Nuevos Productos Solicitados</th>
                    </tr>
                    @foreach ($nuevosproductos as $key => $item)
                        <tr>
                            <td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">
                                <div class="product-entry">
                                    <img src={{ $item['imagen'] }} alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">
                                    <div class="text">
                                        <h3>{{ $item['sku_producto'] }}</h3>
                                        <span>{{ $item['marca'] }} - {{ $item['talla'] }} </span>
                                        <p>{{ $item['sku_description'] }}</p>
                                    </div>
                                </div>
                            </td>
                            <td valign="middle" width="30%" style="text-align:center; padding-right: 2.5em;">
                                <span class="price" style="color: #f34949; font-size: 20px; display: block;">
                                    S/. {{ $item['precio_final'] }}
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <h3>Pasaremos a recoger tu pedido el día {{ $info->delivery->fecha_recojo }} entre las 9:00 y 13:00 horas
            a {{ $info->delivery->direccion }}</h3>
            <h3>Recuerda que</h3>
            <ol>
                <li>Debes guardar tu producto en una bolsa o empaque seguro y completamente cerrado.</li>
                @if($info->tipo_servicio == 3)
                    <li>Una vez que recibamos tus productos y tu solicitud sea aprobada, te devolveremos tu dinero.</li>
                @endif
                @if($info->tipo_servicio == 1 || $info->tipo_servicio == 2)
                    <li>Una vez que recibamos tus productos y tu solicitud sea aprobada, te enviaremos tus nuevos productos.</li>
                @endif
            </ol>
            <p>Para ver el estado de este proceso, haz click en el botón:</p>
            <span><a href="https://repo.com.pe/#/" class="btn btn-primary">Ver estado de mi proceso</a></span>
        </td>
    </tr>
</table>

@endsection