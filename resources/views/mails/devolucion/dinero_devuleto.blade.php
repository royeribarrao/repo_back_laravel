<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Dinero devuelto, terminó tu proceso.</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="http://repo.com.pe/static/media/repo.eef68e68.svg" />
            </div>
            <p>Hola: {{ $info->delivery->nombres }} - {{ $info->delivery->apellidos }} </p>
            <div class="col-md-12">
                <p>El dinero de tu proceso {{ $info->codigo_repo }} ha sido devuelto.
                </p>
            <p>¡Muchas gracias por confiar en nososotros!</p>
            <p>¿Qué te pareció la experiencia Repo?</p>
            <p>¿Qué tal ha sido tu experiencia con [operador logistico]?</p>
        </div>
        <p>Para ingresar a tu cuenta o registrarte, haz click 
            <a href="http://repo.com.pe/#/registrar-usuario">aquí</a>
        </p>
        <p>¿Tienes alguna duda? Contáctate con nostros 
            <a href="http://repo.com.pe/#/contacto">aquí</a>
            o revisa nuestras <a href="http://repo.com.pe/#/ayuda">Preguntas frecuentes</a>
        </p>
    </div>
</body>
</html>