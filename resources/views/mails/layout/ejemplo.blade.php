@extends('mails.layout.body')
@section('content')

<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
    <tr>
        <td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="padding: 0 2.5em; text-align: left;">
                        <div class="text">
                            <h2>Exclusives Offers For Selected Product Items</h2>
                            <h3>Amazing deals, updates, interesting news right in your inbox</h3>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    <td class="bg_white" style="padding: 0 0 4em 0;">
        <table class="bg_white" role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.05);">
                    <td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">
                        <div class="product-entry">
                            <img src="images/product-1.jpg" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">
                            <div class="text">
                                <h3>Young Woman Dress</h3>
                                <span>S, M, L, XL</span>
                                <p>A small river named Duden flows by their place and supplies</p>
                            </div>
                        </div>
                    </td>
                    <td valign="middle" width="30%" style="text-align:center; padding-right: 2.5em;">
                        <span class="price" style="color: #f34949; font-size: 20px; display: block;">$120</span>
                        <span style="display: block;">50% Discount</span>
                        <span><a href="#" class="btn btn-primary">Shop now</a></span>
                    </td>
                    </tr>
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.05);">
                    <td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">
                        <div class="product-entry">
                            <img src="images/product-2.jpg" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">
                            <div class="text">
                                <h3>Young Woman Dress</h3>
                                <span>S, M, L, XL</span>
                                <p>A small river named Duden flows by their place and supplies</p>
                            </div>
                        </div>
                    </td>
                    <td valign="middle" width="30%" style="text-align:center; padding-right: 2.5em;">
                        <span class="price" style="color: #f34949; font-size: 20px; display: block;">$120</span>
                        <span style="display: block;">50% Discount</span>
                        <span><a href="#" class="btn btn-primary">Shop now</a></span>
                    </td>
                    </tr>
                    <tr style="border-bottom: 1px solid rgba(0,0,0,.05);">
                    <td valign="middle" width="70%" style="text-align:left; padding: 0 2.5em;">
                        <div class="product-entry">
                            <img src="images/product-3.jpg" alt="" style="width: 100px; max-width: 600px; height: auto; margin-bottom: 20px; display: block;">
                            <div class="text">
                                <h3>Young Woman Dress</h3>
                                <span>S, M, L, XL</span>
                                <p>A small river named Duden flows by their place and supplies</p>
                            </div>
                        </div>
                    </td>
                    <td valign="middle" width="30%" style="text-align:center; padding-right: 2.5em;">
                        <span class="price" style="color: #f34949; font-size: 20px; display: block;">$120</span>
                        <span style="display: block;">50% Discount</span>
                        <span><a href="#" class="btn btn-primary">Shop now</a></span>
                    </td>
                    </tr>
        </table>
        </td>
    </tr>
</table>


@endsection