<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
    <tr>
        <td class="bg_light" style="text-align: center;">
            <p>Para ingresar a tu cuenta o registrarte, haz click 
                <a href="https://repo.com.pe/#/registrar-usuario">aquí</a>
            </p>
            <p>¿Tienes alguna duda? Contáctate con nosotros 
                <a href="https://repo.com.pe/#/contacto">aquí</a>
                o revisa nuestras <a href="https://repo.com.pe/#/ayuda">Preguntas frecuentes</a> 
            </p>
        </td>
    </tr>
</table>