{{-- <div class="col-md-12">
    <img src="{{ $message->embed("repo_logo.png") }}" src="repo_logo.png">
</div> --}}
<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
    <tr>
        <td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="logo" style="text-align: left;">
                        <img style="width: 30%" src="{{ $message->embed("repo_logo.png") }}">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>