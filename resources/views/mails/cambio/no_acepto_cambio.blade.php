<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Solicitud aprobada por la tienda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="http://repo.com.pe/static/media/repo.eef68e68.svg" />
            </div>
            <p>Hola: {{ $info->delivery->nombres }} - {{ $info->delivery->apellidos }} </p>
            <p>Te informamos que lamentablemente {{ $info->tienda->business_name }} no ha aceptado tu 
                solicitud de cambio N° {{ $info->codigo_repo }}</p>
            <div class="col-md-12">
                <p>A continuación, te detallamos la explicacion que brindó la tienda:
                </p>
                <p>{{ $motivo}}</p>
            </div>
        <p>Por favor, contáctate con nosotros <a href="http://repo.com.pe/#/contacto">aquí</a>  o, si prefieres, con la tienda para coordinar la entrega de tu producto.</p>
        <p>En caso consideres que ha sido un error, no dudes en contactarnos.</p>
        <p>Lamentamos el inconveniente,</p>
        <p>Para ingresar a tu cuenta o registrarte, haz click 
            <a href="http://repo.com.pe/#/registrar-usuario">aquí</a>
        </p>
        <p>¿Tienes alguna duda? Contáctate con nostros 
            <a href="http://repo.com.pe/#/contacto">aquí</a>
            o revisa nuestras <a href="http://repo.com.pe/#/ayuda">Preguntas frecuentes</a>
        </p>
    </div>
</body>
</html>