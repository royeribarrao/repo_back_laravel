<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Correo de fecha de delivery de nuevo producto (solo cambio estandar)</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <h2>¡Tu nuevo producto llegará el xxx!</h2>
            <div class="col-md-6">
                <img src="/static/media/images/repo.eef68e68.svg" />
            </div>
            <p>Hola: Royer</p>
            <div class="col-md-12">
                <p>El operador logístico de la compañía [*] te entregará tu nuevo producto.</p>
                <p>Llegará el día [XX/XX] a [Av. Direccion].</p>
            <p>Para ver el estado de tu proceso N° [123124], haz click en el botón:</p>
            <button>Ver estado de mi proceso</button>
        </div>
        <h2>!Ayudanos a darte una mejor experiencia!</h2>
        <p>Si no estarás en la dirección, por favor pídele a alguien que lo reciba por ti.</p>
        <p>Para ingresar a tu cuenta o registrarte, haz click 
            <a href="http://ec2-52-23-176-239.compute-1.amazonaws.com/#/">aquí</a>
        </p>
        <p>¿Tiene alguna duda? Contáctate con nostros 
            <a href="http://ec2-52-23-176-239.compute-1.amazonaws.com/#/">aquí</a>
            o revisa en nuestro <a href="http://ec2-52-23-176-239.compute-1.amazonaws.com/#/"></a> FAQ
        </p>
    </div>
</body>
</html>