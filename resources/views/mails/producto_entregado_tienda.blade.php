<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Correo de producto entregado a tienda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="http://repo.com.pe/static/media/repo.eef68e68.svg" />
            </div>
            <p>Hola: {{ $info->delivery->nombres }} - {{ $info->delivery->apellidos }}</p>
            <div class="col-md-12">
                <p>La tienda {{ $info->tienda->business_name }} ha recibido tus productos de la orden N° {{ $info->codigo_repo }}. Tu solicitud está siendo revisada. 
                    Te avisaremos si es aprobada.
                </p>
            <p>Para ver el estado de este proceso, haz click en el siguiente botón:</p>
            <button>Ver estado de mi proceso</button>
        </div>
        <p>Muchas gracias por confiar en Repo</p>
        <p>Para ingresar a tu cuenta o registrarte, haz click 
            <a href="https://repo.com.pe/#/registrar-usuario">aquí</a>
        </p>
        <p>¿Tienes alguna duda? Contáctate con nosotros 
            <a href="https://repo.com.pe/#/contacto">aquí</a>
            o revisa en nuestro <a href="https://repo.com.pe/#/ayuda">Preguntas frecuentes</a> 
        </p>
    </div>
</body>
</html>