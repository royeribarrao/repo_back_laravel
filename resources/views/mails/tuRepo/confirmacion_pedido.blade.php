@extends('mails.layout.body')
@section('content')

<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
    <tr>
        <td valign="middle" class="hero bg_white" style="padding: 2em 0 2em 0;">
            <p>Hola, {{ $info->delivery->nombres }} {{ $info->delivery->apellidos }}</p>
            <div class="col-md-12">
                <p>¡Muchas gracias por usar TuRepo!</p>
                <p>Tu solicitud N° {{ $info->codigo }} ha sido confirmada</p>
            </div>
            <h3>Nos haremos cargo de recoger tu producto el día {{ $fecha }} entre las 9:00 am y la 1:00 pm
            a {{ $info->delivery->direccion_recojo }}</h3>
            
            <p>Para cualquier duda no olvides que puedes comunicarte con nosotros haciendo click en el botón de abajo.</p>
            <span><a href="https://api.whatsapp.com/send?phone=+51976038057" class="btn btn-primary">Quiero consultar mi proceso</a></span>
        </td>
    </tr>
</table>

@endsection