<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="/cssNewMails/style.css" rel="stylesheet" />
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="text-center rounded contenedorGrande">
            <div class="header">
            <div class="marcaHeader">
            <img 
            class="logoRepo"
            src="https://repo.com.pe/static/media/repoLogoGrande.44629e3f.svg" 
            width="100"
            height="60"
            />
            <h6>Elige ser libre</h6>

            </div>
            </div>

            <div class="segmentoMensaje">
            <h1 class="saludoCorreo">
                ¡Hola {{ $cliente->nombres}} {{ $cliente->apellidos}}!
            </h1>
            <div class="mensajeCorreo">
            <h5 >
                El operador logístico de la compañía {{ $logistico }}  ya está en camino para entregarte el producto revisado.
            </h5>
            <p class="avisoCorto">Si no estarás en la dirección, por favor, pídele a alguien que lo reciba por ti.</p>

            </div>
            <div class="ctaCorreo">
            <h6>
                Para ver el estado de tu proceso N° {{ $gestion->codigo_repo }}, haz click en el siguiente botón
            </h6>

            <a href="https://repo.com.pe/#/login-tienda"><button class="botonPrincipal" >Ver proceso</button></a>

            </div>
            </div>
            <div class="footer">
                <div class="consultasRepo" >
                    <h6>¿Tienes alguna duda? </h6> 
                    <p class="contactoRepo">Contáctate con nosotros
                    <a href="http://repo.com.pe/#/contacto">aquí</a>
                    o revisa nuestras <a href="http://repo.com.pe/#/ayuda">preguntas frecuentes</a> 
                    </p>
                </div>
                    <div class="footerPrimeraParte">
                        <h6>Síguenos en:</h6>
                        <div className='logosFooter'>
                            <a href="https://www.facebook.com/repocompany">
                                <img
                                src=https://repo.com.pe/static/media/facebook.091bf6ee.svg
                                width="30"
                                height="30"
                                className="logoRedes"
                                alt="linkedin"
                                >
                                </img>
                                </a>

                                <a href="https://www.instagram.com/repo.pe/">
                                    <img
                                    src=https://repo.com.pe/static/media/instagram.f8508ec6.svg
                                    width="30"
                                    height="30"
                                    className="logoRedes"
                                    alt="instagram Repo"
                                    >
                                    </img>
                                    </a>
                            
                            <a href="hhttps://www.linkedin.com/company/turepo/">
                            <img
                            src=https://repo.com.pe/static/media/linkedin.e3edd675.svg
                            width="30"
                            height="30"
                            className="logoRedes"
                            alt="linkedin Repo"
                            >
                            </img>
                            </a>
                        </div>
                    </div>
                            <div class="mensajeBottom">
                            <a class="linkRepo" href="https://repo.com.pe/#/">www.repo.com.pe</a>
                            <p>*Términos y condiciones</p>
                            <p>Copyright 2022 Repo</p>
                            </div>
            </div>
    </div>
    


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,700;1,800;1,900&display=swap" rel="stylesheet">
</body>
</html>