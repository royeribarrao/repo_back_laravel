<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreciosTipoServicio extends Migration
{
    public function up()
    {
        Schema::create('precio_tipo_servicio', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('tipo_servicio_id')->nullable();
            $table->decimal('costo', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('precio_tipo_servicio');
    }
}