<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoServicioOpcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repo_servicio_opcions', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_id')->nullable();
            $table->integer('opcion_id')->nullable();
            $table->integer('prioridad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repo_servicio_opcions');
    }
}
