<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ventas extends Migration
{
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->integer('integracion_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('estado')->nullable();
            $table->string('apellido_cliente')->nullable();
            $table->string('cantidad')->nullable();
            $table->string('codigo_compra')->nullable();
            $table->string('color')->nullable();
            $table->string('departamento')->nullable();
            $table->string('direccion')->nullable();
            $table->string('distrito')->nullable();
            $table->string('email')->nullable();
            $table->string('fecha_compra')->nullable();
            $table->string('marca')->nullable();
            $table->string('nombre_cliente')->nullable();
            $table->string('nombre_producto')->nullable();
            $table->string('numero_documento')->nullable();
            $table->string('provincia')->nullable();
            $table->decimal('precio_pagado', 10, 2)->nullable();
            $table->string('sku')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}