<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->integer('integracion_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('categoria_id')->nullable();
            $table->string('color')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('imagen')->nullable();
            $table->string('nombre')->nullable();
            $table->string('sku');
            $table->string('marca')->nullable();
            $table->string('talla')->nullable();
            $table->string('stock')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->decimal('descuento', 10, 2)->nullable();
            $table->decimal('precio_final', 10, 2)->nullable();
            $table->string('dimensiones')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('productos');
    }
}