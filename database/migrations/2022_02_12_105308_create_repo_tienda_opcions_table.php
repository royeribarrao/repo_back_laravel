<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoTiendaOpcionsTable extends Migration
{
    public function up()
    {
        Schema::create('repo_tienda_opcions', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->integer('opcion_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('repo_tienda_opcions');
    }
}