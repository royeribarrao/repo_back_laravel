<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrackerCambioEstandar extends Migration
{
    public function up()
    {
        Schema::create('tracker_cambio_estandar', function (Blueprint $table) {
            $table->id();
            $table->integer('gestion_id')->nullable();
            $table->boolean('pedido_recibido')->nullable();
            $table->boolean('operador_logistico_confirmado')->nullable();
            $table->boolean('en_camino')->nullable();
            $table->boolean('producto_recogido')->nullable();
            $table->boolean('producto_devuelto')->nullable();
            $table->boolean('cambio_aceptado')->nullable();
            $table->boolean('operador_logistico_confirmado_vuelta')->nullable();
            $table->boolean('producto_nuevo_en_camino')->nullable();
            $table->boolean('producto_nuevo_entregado')->nullable();
            $table->string('nombre_estado')->nullable();
            $table->integer('estado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracker_cambio_estandar');
    }
}
