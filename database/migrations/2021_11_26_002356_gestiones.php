<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Gestiones extends Migration
{
    public function up()
    {
        Schema::create('gestiones', function (Blueprint $table) {
            $table->id();
            $table->integer('entorno_id')->nullable();
            $table->integer('tienda_id')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->string('codigo_compra')->nullable();
            $table->string('codigo_repo')->nullable();
            $table->integer('datos_delivery_id')->nullable();
            $table->string('fecha_recojo')->nullable();
            $table->integer('tipo_servicio')->nullable();
            $table->boolean('cliente_paga_logistico')->default(true);
            $table->boolean('confirmacion_web')->default(false);
            $table->boolean('confirmacion_tienda')->default(false);
            $table->integer('estado')->nullable();
            $table->boolean('en_proceso')->default(false);
            $table->boolean('finalizado')->default(false);
            $table->decimal('es_facturado', 10, 2)->nullable();
            $table->decimal('total_productos_gestion', 10, 2)->nullable();
            $table->decimal('total_nuevos_productos', 10, 2)->nullable();
            $table->decimal('total_devolucion', 10, 2)->nullable();
            $table->decimal('cobro_neto', 10, 2)->nullable();
            $table->decimal('costo_logistico', 10, 2)->nullable();
            $table->decimal('total_pago', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gestiones');
    }
}
