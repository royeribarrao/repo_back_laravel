<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NuevosProductosServicioCambio extends Migration
{
    public function up()
    {
        Schema::create('nuevos_productos_servicio_cambio', function (Blueprint $table) {
            $table->id();
            $table->integer('gestion_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('nombre_producto')->nullable();
            $table->string('sku')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nuevos_productos_servicio_cambio');
    }
}