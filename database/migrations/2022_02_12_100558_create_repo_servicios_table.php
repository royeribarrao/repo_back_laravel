<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repo_servicios', function (Blueprint $table) {
            $table->id();
            $table->integer('estado')->default(1);
            $table->boolean('confirmacion_web')->nullable();
            $table->string('codigo')->nullable();
            $table->integer('tienda_id')->nullable();
            $table->string('nombre_tienda')->nullable();
            $table->string('referencia_tienda')->nullable();
            $table->string('distrito')->nullable();
            $table->string('producto')->nullable();
            $table->string('link_producto')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->string('imagen_producto')->nullable();
            $table->string('imagen_boleta')->nullable();
            $table->string('motivo')->nullable();
            $table->integer('peticion')->nullable();
            $table->string('nueva_talla')->nullable();
            $table->string('comentario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repo_servicios');
    }
}
