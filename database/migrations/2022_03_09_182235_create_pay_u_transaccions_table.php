<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayUTransaccionsTable extends Migration
{
    public function up()
    {
        Schema::create('pay_u_transaccions', function (Blueprint $table) {
            $table->id();
            $table->integer('tipo')->nullable();
            $table->string('state')->nullable();
            $table->string('orderId')->nullable();
            $table->integer('servicio_id')->nullable();
            $table->string('responseCode')->nullable();
            $table->string('transactionId')->nullable();
            $table->string('authorizationCode')->nullable();
            $table->string('paymentNetworkResponseErrorMessage')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pay_u_transaccions');
    }
}