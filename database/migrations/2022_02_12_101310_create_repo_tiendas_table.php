<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoTiendasTable extends Migration
{
    public function up()
    {
        Schema::create('repo_tiendas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_tienda')->nullable();
            $table->string('razon_social')->nullable();
            $table->boolean('es_aliada')->nullable();
            $table->string('ruc')->unique();
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('repo_tiendas');
    }
}