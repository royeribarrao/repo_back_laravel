<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoServicioNuevoProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repo_servicio_nuevo_productos', function (Blueprint $table) {
            $table->id();
            $table->integer('producto_id')->nullable();
            $table->integer('servicio_id')->nullable();
            $table->integer('precio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repo_servicio_nuevo_productos');
    }
}
