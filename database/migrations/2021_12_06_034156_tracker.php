<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tracker extends Migration
{
    public function up()
    {
        Schema::create('tracker', function (Blueprint $table) {
            $table->id();
            $table->integer('gestion_id')->nullable();
            $table->integer('tipo_tracker_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tracker');
    }
}