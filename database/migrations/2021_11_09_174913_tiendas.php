<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tiendas extends Migration
{
    public function up()
    {
        Schema::create('tiendas', function (Blueprint $table) {
            $table->id();
            $table->boolean('aliada')->nullable();
            $table->string('contacto')->nullable();
            $table->string('departamento')->nullable();
            $table->string('distrito')->nullable();
            $table->string('direccion')->nullable();
            $table->string('email')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('logo')->nullable();
            $table->string('nombre_tienda')->nullable();
            $table->integer('orden')->nullable();
            $table->string('provincia')->nullable();
            $table->string('razon_social')->nullable();
            $table->string('ruc')->unique();
            $table->boolean('state')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tiendas');
    }
}