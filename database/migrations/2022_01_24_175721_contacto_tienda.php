<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactoTienda extends Migration
{
    public function up()
    {
        Schema::create('contacto_tienda', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->string('nombre_contacto')->nullable();
            $table->string('apellido_contacto')->nullable();
            $table->string('telefono_contacto')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contacto_tienda');
    }
}