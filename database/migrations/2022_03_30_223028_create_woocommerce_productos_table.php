<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWoocommerceProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('woocommerce_productos', function (Blueprint $table) {
            $table->id();
            $table->integer('producto_id')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('sku')->nullable();
            $table->string('purchasable')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('woocommerce_productos');
    }
}
