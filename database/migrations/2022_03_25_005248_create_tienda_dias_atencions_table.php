<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiendaDiasAtencionsTable extends Migration
{
    public function up()
    {
        Schema::create('tienda_dias_atencions', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->boolean('lunes')->default(1);
            $table->boolean('martes')->default(1);
            $table->boolean('miercoles')->default(1);
            $table->boolean('jueves')->default(1);
            $table->boolean('viernes')->default(1);
            $table->boolean('sabado')->default(1);
            $table->boolean('domingo')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tienda_dias_atencions');
    }
}