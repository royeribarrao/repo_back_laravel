<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CarrierTarifas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_tarifas', function (Blueprint $table) {
            $table->id();
            $table->integer('carrier_id')->nullable();
            $table->integer('tipo_servicio_id')->nullable();
            $table->string('tipo_vehiculo')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->string('hora_maxima_pedido')->nullable();
            $table->string('hora_recojo_l_v')->nullable();
            $table->string('hora_recojo_s')->nullable();
            $table->string('hora_recojo_d')->nullable();
            $table->string('hora_entrega')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_tarifas');
    }
}
