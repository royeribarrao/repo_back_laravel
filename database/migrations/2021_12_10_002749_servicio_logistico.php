<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicioLogistico extends Migration
{
    public function up()
    {
        Schema::create('servicio_logistico', function (Blueprint $table) {
            $table->id();
            $table->integer('gestion_id')->nullable();
            $table->integer('carrier_id')->nullable();
            $table->string('waybillNumber')->nullable();
            $table->string('carrierWaybill')->nullable();
            $table->integer('tipo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicio_logistico');
    }
}
