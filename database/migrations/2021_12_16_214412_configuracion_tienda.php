<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConfiguracionTienda extends Migration
{
    public function up()
    {
        Schema::create('configuracion_tienda', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->integer('integracion_id')->nullable();
            $table->integer('tienda_dias_atencion_id')->nullable();
            $table->boolean('compra_no_web')->nullable();
            $table->boolean('compra_web')->nullable();
            $table->boolean('requiere_boleta')->nullable();
            $table->boolean('devolucion')->nullable();
            $table->boolean('estandar')->nullable();
            $table->boolean('deluxe')->nullable();
            $table->boolean('servicio_tecnico')->nullable();
            $table->integer('plazo')->default(60);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configuracion_tienda');
    }
}