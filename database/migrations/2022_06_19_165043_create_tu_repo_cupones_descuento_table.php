<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuRepoCuponesDescuentoTable extends Migration
{
    public function up()
    {
        Schema::create('tu_repo_cupones_descuento', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->integer('cantidad_total');
            $table->integer('cantidad_usada');
            $table->integer('cantidad_restante');
            $table->string('fecha_inicio');
            $table->string('fecha_expiracion');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tu_repo_cupones_descuento');
    }
}