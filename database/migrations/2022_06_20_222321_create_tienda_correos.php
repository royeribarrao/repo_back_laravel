<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiendaCorreos extends Migration
{
    public function up()
    {
        Schema::create('tienda_correos', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id');
            $table->string('email');
            $table->integer('tipo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tienda_correos');
    }
}