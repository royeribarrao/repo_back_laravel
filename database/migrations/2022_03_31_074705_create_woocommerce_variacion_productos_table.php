<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWoocommerceVariacionProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('woocommerce_variacion_productos', function (Blueprint $table) {
            $table->id();
            $table->integer('producto_id')->nullable();
            $table->string('sku')->nullable();
            $table->string('name')->nullable();
            $table->string('stock_quantity')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('woocommerce_variacion_productos');
    }
}
