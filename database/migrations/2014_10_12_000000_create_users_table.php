<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tienda_id')->nullable();
            $table->integer('sede_id')->nullable();
            $table->integer('rol_id')->nullable();
            $table->string('apellido_materno', 100)->nullable();
            $table->string('apellido_paterno', 100)->nullable();
            $table->string('direccion', 100)->nullable();
            $table->string('departamento', 100)->nullable();
            $table->string('dni', 20)->unique();
            $table->string('distrito', 100)->nullable();
            $table->string('email', 80)->unique();
            $table->string('latitud', 100)->nullable();
            $table->string('longitud', 100)->nullable();
            $table->string('nombres', 100);
            $table->string('nombre_completo', 100);
            $table->string('password', 150);
            $table->string('referencia', 100)->nullable();
            $table->string('telefono', 10)->nullable();
            $table->smallInteger('estado')->default(1);
            $table->integer('updated_user')->nullable();
            $table->integer('created_user')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}