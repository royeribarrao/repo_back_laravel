<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipoServicio extends Migration
{
    public function up()
    {
        Schema::create('tipos_servicio', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo')->nullable();
            $table->string('nombre')->nullable();
            $table->string('nombre_tarifa_base')->nullable();
            $table->decimal('precio_base', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tipos_servicio');
    }
}