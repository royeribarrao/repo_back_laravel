<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiendaMotivoDevolucionsTable extends Migration
{

    public function up()
    {
        Schema::create('tienda_motivo_devolucions', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->integer('motivo_devolucion_id')->nullable();
            $table->boolean('activo')->nullable();
            $table->integer('pago_logistico')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tienda_motivo_devolucions');
    }
}