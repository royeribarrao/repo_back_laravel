<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductosGestion extends Migration
{
    public function up()
    {
        Schema::create('productos_gestion', function (Blueprint $table) {
            $table->id();
            $table->integer('gestion_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->string('sku')->nullable();
            $table->string('nombre_producto')->nullable();
            $table->string('link_producto')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->integer('motivo')->nullable();
            $table->string('imagen_boleta')->nullable();
            $table->string('imagen_producto')->nullable();
            $table->string('evidencia')->nullable();
            $table->string('comentario_evidencia')->nullable();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('productos_gestion');
    }
}