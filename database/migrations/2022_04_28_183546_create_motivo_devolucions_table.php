<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotivoDevolucionsTable extends Migration
{
    public function up()
    {
        Schema::create('motivo_devolucions', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->integer('pago_logistico')->nullable();
            $table->integer('activo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('motivo_devolucions');
    }
}