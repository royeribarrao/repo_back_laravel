<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepoDatosDeliveriesTable extends Migration
{
    public function up()
    {
        Schema::create('repo_datos_deliveries', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_id')->nullable();
            $table->string('nombres')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('dni')->nullable();
            $table->string('email')->nullable();
            $table->string('direccion_recojo')->nullable();
            $table->string('referencia')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('distrito')->nullable();
            $table->string('fecha_recojo')->nullable();
            $table->string('celular')->nullable();
            $table->string('metodo_pago')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('repo_datos_deliveries');
    }
}