<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiendaDatosFeesTable extends Migration
{
    public function up()
    {
        Schema::create('tienda_datos_fees', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->string('codigo')->nullable();
            $table->string('nombre')->nullable();
            $table->integer('tipo')->nullable();
            $table->integer('monto')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tienda_datos_fees');
    }
}