<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TrackerCambioDeluxeSeeder extends Seeder
{
    public function run()
    {
        \DB::table('tracker_cambio_deluxe')->insert([
            'gestion_id' => 3,
            'pedido_recibido' => true,
            'operador_logistico_confirmado' => false,
            'en_camino' => false,
            'producto_recogido' => false,
            'producto_devuelto' => false,
            'cambio_aceptado' => false,
            'producto_nuevo_en_camino' => false,
            'producto_nuevo_entregado' => false,
            'nombre_estado' => 'Pedido recibido',
            'estado' => 1
        ]);
    }
}