<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tiendas')->insert([
            'business_name' => 'Inmaculada',
            'razon_social' => 'Inmaculada',
            'state' => true,
            'ruc' => '20601050456',
            'logo' => '',
            'distrito' => 'San Isidro', 
            'address' => 'Calle Miguel Dasso 144, oficina 6A',
            'phone' => '993652001',
            'contacto' => 'Katherine M.',
            'email' => 'info@inmaculada.shop',
            'latitud' => '-12.106530',
            'longitud' => '-77.040170'
        ]);

        \DB::table('tiendas')->insert([
            'business_name' => 'Brunella Marquez',
            'razon_social' => 'Brunella Marquez',
            'state' => true,
            'ruc' => '20608476653',
            'logo' => '',
            'distrito' => 'Jesús María', 
            'address' => 'Av. Cayetano Heredia 1057 Dpto. 1001',
            'phone' => '989347366',
            'contacto' => 'Andrea Palacios',
            'email' => 'andrea95pal@gmail.com'
        ]);

        \DB::table('tiendas')->insert([
            'business_name' => 'Gea',
            'razon_social' => 'Gea',
            'state' => true,
            'ruc' => '20603460309',
            'logo' => '',
            'distrito' => 'San Isidro', 
            'address' => 'Calle A 210',
            'phone' => '941441237',
            'contacto' => 'Ariana Castagnola',
            'email' => 'hola@geacorporation.com'
        ]);

        \DB::table('tiendas')->insert([
            'business_name' => 'Demo Repo',
            'razon_social' => 'Repo',
            'state' => true,
            'ruc' => '20200200200',
            'logo' => '',
            'distrito' => 'San Isidro', 
            'address' => 'Calle A 210',
            'phone' => '999666555',
            'contacto' => 'Ricardo Anaya',
            'email' => 'contacto@repo.com.pe'
        ]);
    }
}
