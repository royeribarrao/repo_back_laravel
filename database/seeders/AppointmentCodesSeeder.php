<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AppointmentCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('appointment_codes')->insert([
            [
                'name' => 'CN',//consulta nueva
                'type' => 1
            ], 
            [
                'name' => 'T01',//tratamientos
                'type' => 2
            ],
            [
                'name' => 'T02',
                'type' => 2
            ],
            [
                'name' => 'T03',
                'type' => 2
            ],
            [
                'name' => 'T04',
                'type' => 2
            ],
            [
                'name' => 'T05',
                'type' => 2
            ],
            [
                'name' => 'CPO',//control post operatio
                'type' => 5
            ],
            [
                'name' => 'CF',//consulta final
                'type' => 5
            ],
            [
                'name' => 'CM1',//consultas mensuales
                'type' => 3
            ],
            [
                'name' => 'CM2',
                'type' => 3
            ],
            [
                'name' => 'CM3',
                'type' => 3
            ],
            [
                'name' => 'CM4',
                'type' => 3
            ],
            [
                'name' => 'CM5',
                'type' => 3
            ],
            [
                'name' => 'CM6',
                'type' => 3
            ],
            [
                'name' => 'AM',//Alta médica
                'type' => 4
            ],

        ]);
    }
}
