<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RepoTiendaOpcionSeeder extends Seeder
{
    public function run()
    {
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 1,
            'opcion_id' => 2
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 1,
            'opcion_id' => 4
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 2,
            'opcion_id' => 1
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 2,
            'opcion_id' => 4
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 3,
            'opcion_id' => 2
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 3,
            'opcion_id' => 4
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 4,
            'opcion_id' => 2
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 4,
            'opcion_id' => 3
        ]);
        \DB::table('repo_tienda_opcions')->insert([
            'tienda_id' => 4,
            'opcion_id' => 4
        ]);
    }
}