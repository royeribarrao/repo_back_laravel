<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RepoTiendaSeeder extends Seeder
{
    public function run()
    {
        \DB::table('repo_tiendas')->insert([
            'nombre_tienda' => 'Falabella',
            'razon_social' => 'Saga Falabella',
            'es_aliada' => false,
            'ruc' => 'ruc saga',
            'logo' => ''
        ]);

        \DB::table('repo_tiendas')->insert([
            'nombre_tienda' => 'Ripley',
            'razon_social' => 'Ripley',
            'es_aliada' => false,
            'ruc' => 'ruc ripley',
            'logo' => ''
        ]);

        \DB::table('repo_tiendas')->insert([
            'nombre_tienda' => 'Oeschle',
            'razon_social' => 'Oeschele',
            'es_aliada' => false,
            'ruc' => 'ruc oeschle',
            'logo' => ''
        ]);

        \DB::table('repo_tiendas')->insert([
            'nombre_tienda' => 'Marathon',
            'razon_social' => 'Marathon',
            'es_aliada' => false,
            'ruc' => 'ruc maratohn',
            'logo' => ''
        ]);
    }
}