<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('patients')->insert([
            'dni' => '1234456',
            'name' => 'frank',
            'fullname' => 'rojas condezo',
            'father_lastname' => 'rojas',
            'mother_lastname' => 'condezo',
            'email' => 'admin@admin.com',
            'code' => 'abc123',
            'address' => 'Jr. lucindas 123',
            'state' => '1',
            'phone' => '964511505',
            'deleted_user'=> 1,
            'birthdate' => '2019-12-01'
          ]);
    }
}
