<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TiendaMotivoDevolucionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tienda_motivo_devolucions')->insert([
            'tienda_id' => 1,
            'motivo_devolucion_id' => 1
        ]);
        \DB::table('tienda_motivo_devolucions')->insert([
            'tienda_id' => 1,
            'motivo_devolucion_id' => 2
        ]);
        \DB::table('tienda_motivo_devolucions')->insert([
            'tienda_id' => 1,
            'motivo_devolucion_id' => 3
        ]);
        \DB::table('tienda_motivo_devolucions')->insert([
            'tienda_id' => 1,
            'motivo_devolucion_id' => 4
        ]);
        \DB::table('tienda_motivo_devolucions')->insert([
            'tienda_id' => 1,
            'motivo_devolucion_id' => 5
        ]);
    }
}
