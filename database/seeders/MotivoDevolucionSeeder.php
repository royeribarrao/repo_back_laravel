<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MotivoDevolucionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('motivo_devolucions')->insert([
            'nombre' => 'No me quedó bien.',
            'pago_logistico' => 1
        ]);
        \DB::table('motivo_devolucions')->insert([
            'nombre' => 'No era lo que esperaba.',
            'pago_logistico' => 1
        ]);
        \DB::table('motivo_devolucions')->insert([
            'nombre' => 'Pedí otro producto por error.',
            'pago_logistico' => 1
        ]);
        \DB::table('motivo_devolucions')->insert([
            'nombre' => 'Me entregaron otro producto.',
            'pago_logistico' => 1
        ]);
        \DB::table('motivo_devolucions')->insert([
            'nombre' => 'Producto dañado.',
            'pago_logistico' => 1
        ]);
    }
}
