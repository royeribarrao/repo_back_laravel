<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TrackerCambioEstandarSeeder extends Seeder
{
    public function run()
    {
        \DB::table('tracker_cambio_estandar')->insert([
            'gestion_id' => 1,
            'pedido_recibido' => true,
            'operador_logistico_confirmado' => false,
            'en_camino' => false,
            'producto_recogido' => false,
            'producto_devuelto' => false,
            'cambio_aceptado' => false,
            'operador_logistico_confirmado_vuelta' => false,
            'producto_nuevo_en_camino' => false,
            'producto_nuevo_entregado' => false,
            'nombre_estado' => 'Pedido recibido',
            'estado' => 1
        ]);

        \DB::table('tracker_cambio_estandar')->insert([
            'gestion_id' => 2,
            'pedido_recibido' => true,
            'operador_logistico_confirmado' => false,
            'en_camino' => false,
            'producto_recogido' => false,
            'producto_devuelto' => false,
            'cambio_aceptado' => false,
            'operador_logistico_confirmado_vuelta' => false,
            'producto_nuevo_en_camino' => false,
            'producto_nuevo_entregado' => false,
            'nombre_estado' => 'Pedido recibido',
            'estado' => 1
        ]);
    }
}