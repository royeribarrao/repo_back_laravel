<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SedeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('maintenance_sedes')->insert([
            [
                'name' => 'Lima',
                'order' => '1',
                'color' => 'lila',
                'state' => '1',
            ], [
                'name' => 'Chiclayo',
                'order' => '2',
                'color' => 'azul-entero',
                'state' => '1',
            ],[
                'name' => 'Huancayo',
                'order' => '3',
                'color' => 'verde',
                'state' => '1'
            ],[
                'name' => 'Arequipa',
                'order' => '4',
                'color' => 'amarillo',
                'state' => '1'
            ] 
        ]);
    }
}
