<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GesionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('gestiones')->insert([
            'fecha_recojo' => '2021-12-20',
            'tipo_servicio' => 1,
            'codigo_compra' => '14688',
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => 1,
            'total_pago' => 15.00,
            'codigo_repo' => 'C000001',
            'estado' => 1,
            'en_proceso' => true,
            'imagen_evidencia' => 'https://home.ripley.com.pe/Attachment/WOP_5/2033270657399/2033270657399_2.jpg'
        ]);

        \DB::table('gestiones')->insert([
            'fecha_recojo' => '2022-04-05',
            'tipo_servicio' => 1,
            'codigo_compra' => '14689',
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => 1,
            'total_pago' => 16.00,
            'codigo_repo' => 'C000002',
            'estado' => 1,
            'en_proceso' => true,
            'imagen_evidencia' => 'https://frutocuatro.com/wp-content/uploads/2018/06/18500-azul-marino-deportivo-jaspe.jpg'
        ]);

        \DB::table('gestiones')->insert([
            'fecha_recojo' => '2022-02-17',
            'tipo_servicio' => 2,
            'codigo_compra' => '14691',
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => 2,
            'total_pago' => 18.00,
            'codigo_repo' => 'C000003',
            'estado' => 1,
            'en_proceso' => true,
            'imagen_evidencia' => 'https://home.ripley.com.pe/Attachment/WOP_5/2033270657399/2033270657399_2.jpg'
        ]);

        \DB::table('gestiones')->insert([
            'fecha_recojo' => '2021-12-19',
            'tipo_servicio' => 3,
            'codigo_compra' => '14714',
            'cliente_id' => 4,
            'tienda_id' => 1,
            'datos_delivery_id' => 2,
            'total_pago' => 10.00,
            'total_devolucion' => 130.00,
            'codigo_repo' => 'D000001',
            'estado' => 1,
            'en_proceso' => true,
            'imagen_evidencia' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxQqXmPuo3G_FtC2PboygrgnKynsYczJjofA&usqp=CAU'
        ]);
    }
}
